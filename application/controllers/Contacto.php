<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contacto extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
    	parent::__construct();

    	$this->load->helper('tools_helper');
        $this->load->helper('date');
    	$this->load->helper('cookie');
    	$this->load->library('session');
    	$this->load->library("email");
    	$this->load->model('Smtp_model');
    	$this->load->model('recluta_model');
    	$this->load->model('meta_model');
        $this->load->model('usuario_model');
        $this->load->model('mensajes_model');
        $this->load->model('contacto_model');
    }
    
    public function contactajax(){
            $postdata = file_get_contents("php://input");
            $request = json_encode($_POST);
            $requestpost = json_decode($request);

            $objContacto = new StdClass();
            $objContacto->nom_apellido	= @$requestpost->nom_apellido;
            $objContacto->correo        = @$requestpost->correo;
            $objContacto->telefono      = @$requestpost->telefono;
            $objContacto->mensaje       = @$requestpost->mensaje;

            $data = $this->contacto_model->addcontacto($objContacto);

		if ($data) {
            echo json_encode($data);

					$config = $this->Smtp_model->smtp();	
					$Correo = $objContacto->correo;
                    $CorreoContacto = "ysrr27@gmail.com";
					$email_body = "<b><h2>CONTACTO</h2><br>Nombre: </b>".$objContacto->nom_apellido."<br><b>Telefono: </b>".$objContacto->telefono."<br><b>Direccion de correo electronico: </b>".$objContacto->correo."<br><b>Mensaje: </b>".$objContacto->mensaje;

					$this->email->initialize($config);
					$this->email->set_mailtype('html');
					$this->email->from($Correo, $objContacto->nom_apellido);
					$this->email->to($CorreoContacto);
					$this->email->subject('Nuevo contacto Tecnotalento');
					$this->email->message($email_body);
					$this->email->send();
			
		}else{
			return false;
		}
	
    }
}

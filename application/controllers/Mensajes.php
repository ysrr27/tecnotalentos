<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mensajes extends CI_Controller {

   /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *       http://example.com/index.php/welcome
    * - or -
    *       http://example.com/index.php/welcome/index
    * - or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

   public function __construct(){
      parent::__construct();

      $this->load->helper('tools_helper');
      $this->load->model('usuario_model');
      $this->load->model('mensajes_model');


  }


  public function home(){  
    if($this->session->userdata('logueado')){
        $view_data = array();
        $view_data['nombre'] = $this->session->userdata('nombre');
        $view_data['apellido'] = $this->session->userdata('apellido');
        $view_data['rol'] = $this->session->userdata('rol');
        $view_data["menu"] = true;
        $idusario = (int)$this->session->userdata('id');
    }else{
        redirect('/admin');
    }  


    $response = new StdClass(); 

    $response->rows = $this->mensajes_model->get_users_send_messages();
    $response->rowsmessagenew = $this->mensajes_model->get_users_send_messages_news($idusario);
    $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
    $view_data["count_mensajes"] = $count_mensajes;


    $mensajes = array();
    $i = 0;
    if (!empty($response->rowsmessagenew)) {
        foreach ($response->rowsmessagenew as $record) {

            $mensajes[$i]['useridto'] = $record->useridto;
            $mensajes[$i]['cantidad'] = $record->cantidad;
            $mensajes[$i]['nombre'] = $record->nombre;
            $mensajes[$i]['foto'] = $record->foto;
            $i++;

        }

        foreach ($response->rows as $key => $record) {
            foreach ($mensajes as $key => $row) {
                if ($record->useridto == $row["useridto"]) {
                    continue;
                }else{
                    $mensajes[$i]['useridto'] = $record->useridto;
                    $mensajes[$i]['cantidad'] = 0;
                    $mensajes[$i]['nombre'] = $record->nombre;
                    $mensajes[$i]['foto'] = $record->foto;
                }
            }
            $i++;         

        }


        $data = array_reverse($mensajes);

        $result = array_reverse( 
            array_values( 
                array_combine( 
                    array_column($data, 'useridto'), 
                    $data
                )
            )
        );




        usort($result, function($a, $b) {
            return $a['cantidad'] <=> $b['cantidad'];
        });

        $reclutas = array_reverse($result);
        $useridto = intval($reclutas[0]['useridto']);

    }else{
        $reclutas = array_reverse($response->rows);
        $useridto = intval($reclutas[0]->useridto);
    }

    $response->ultimomensaje = $this->mensajes_model->get_lastmessage_user($useridto);
/*    print_r("<pre>");
    var_dump($response->ultimomensaje);
    exit();*/
    $response->ultimomensaje_detalle = $this->mensajes_model->ultimom_mensaje($useridto);
    $response->reclutas = $reclutas;


    $view_data["data"] = $response;



    $this->load->view('Head', $view_data);
    $this->load->view('mensajes/home',$view_data);
    $this->load->view('Footer', $view_data);
}

 //mensajes
public function get_mensajes(){ 
    $useridto = intval($_GET["useridto"]);

    $data = $this->mensajes_model->get_lastmessage_user($useridto);

   // $response->ultimomensaje_detalle = $this->mensajes_model->ultimom_mensaje($useridto);

  //  $data = $this->recluta_model->detalles_mensajes($idmensaje);
    echo json_encode($data);

}





}

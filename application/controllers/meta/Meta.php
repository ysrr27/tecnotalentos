<?php 
    $this->load->model('usuario_model');
    $this->load->model('meta_model');

	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['apellido'] = $this->session->userdata('apellido');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor"  or $view_data['rol'] =="Supervisor") {
					redirect('meta/meta/editar');
				}
				$view_data["menu"] = true;
			}else{
				redirect('/admin');
			}	 


			$response = new StdClass();	

			$response->rows = $this->meta_model->get_Metas();
			$view_data["data"] = $response;
			$this->load->view('Head', $view_data);
			$this->load->view('meta/home',$view_data);
			$this->load->view('Footer', $view_data);

		break;

		case "editar":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor" or $view_data['rol'] =="Supervisor") {
					redirect('meta/meta/editar');
				}
				$view_data["menu"] = true;

			}else{
				redirect('/admin');
			}

			$objUsuario = new StdClass();

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}
			 $objMeta = $this->meta_model->get_byIdMeta( $id );

			 $view_data["data"] = $objMeta;	

			$this->load->view('Head', $view_data);
			$this->load->view('meta/editar', $view_data);
			$this->load->view('Footer', $view_data);
		break;

		case "update":

		if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('censo/censo/editar');
				}
				$view_data["menu"] = true;

			}else{
				redirect('/admin');
			}
			$objMeta = new StdClass();
			
			$objMeta->idmeta = (int)$this->input->post('idmeta', TRUE);
			$objMeta->pagina = $this->input->post('pagina', TRUE);
			$objMeta->title = $this->input->post('title', TRUE);
			$objMeta->type = $this->input->post('type', TRUE);
			$objMeta->url = $this->input->post('url', TRUE);
			$objMeta->description = $this->input->post('description', TRUE);
			$objMeta->site_name = $this->input->post('site_name', TRUE);
			$objMeta->locale = $this->input->post('locale', TRUE);
			$objMeta->keywords = $this->input->post('keywords', TRUE);


			$data = $this->meta_model->addmeta($objMeta);


			redirect('meta/home');



			break;
		
	}


 ?>
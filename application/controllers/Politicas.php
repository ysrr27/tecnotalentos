<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Politicas extends CI_Controller {
	
	public function __construct(){
		parent::__construct();
        //always check if session userdata value "logged_in" is not true
		$this->load->helper('tools_helper');
		$this->load->model('meta_model');


	}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		$view_data["menu"] = false;
		$responsemetas = new StdClass(); 
		$metaname = $this->uri->segment(1);

		if( $metaname == "" or $metaname == "0"){
			$metaname = "home";
		}

		if($this->session->userdata('logueado')){
			redirect('reclutas/micuenta');
		}else{
			$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
			$view_data["data"] = $responsemetas;
			$view_data['logueado'] = false;
			$this->load->view('Head', $view_data);
			$this->load->view('politicas');
			$this->load->view('Footer', $view_data);
		}
	}


}

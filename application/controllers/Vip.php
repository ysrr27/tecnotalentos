<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Vip extends CI_Controller {

   /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *       http://example.com/index.php/welcome
    * - or -
    *       http://example.com/index.php/welcome/index
    * - or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */

   public function __construct(){
      parent::__construct();

      $this->load->helper('tools_helper');
      $this->load->model('usuario_model');
      $this->load->model('vip_model');
      $this->load->model('banner_model');
      $this->load->model('mensajes_model');
  }


  public function home(){  
    if($this->session->userdata('logueado')){
        $view_data = array();
        $view_data['nombre'] = $this->session->userdata('nombre');
        $view_data['apellido'] = $this->session->userdata('apellido');
        $view_data['rol'] = $this->session->userdata('rol');
        $idusario = (int)$this->session->userdata('id');
        $view_data["menu"] = true;
    }else{
        redirect('/admin');
    }  


    $response = new StdClass(); 
    $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
    $view_data["count_mensajes"] = $count_mensajes;

    $response->rows = $this->vip_model->get_publications();
    $view_data["data"] = $response;
    $this->load->view('Head', $view_data);
    $this->load->view('banners/home',$view_data);
    $this->load->view('Footer', $view_data);
}
public function editar(){  
    if($this->session->userdata('logueado')){
        $view_data = array();
        $view_data['nombre'] = $this->session->userdata('nombre');
        $view_data['rol'] = $this->session->userdata('rol');
        $view_data["menu"] = true;
        $idusario = (int)$this->session->userdata('id');

    }else{
        redirect('/admin');
    }

    $objBanner = new StdClass();

    $id = $this->uri->segment(3);
    if( $id == "" or $id == "0"){
        $id = 0;
    }
    $objBanner = $this->banner_model->get_byIdBanner( $id );
    $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
    $view_data["count_mensajes"] = $count_mensajes;
    $view_data["data"] = $objBanner;  

    $this->load->view('Head', $view_data);
    $this->load->view('vip/editar', $view_data);
    $this->load->view('Footer', $view_data);
}
public function update(){  

    if($this->session->userdata('logueado')){
        $view_data = array();
        $view_data['nombre'] = $this->session->userdata('nombre');
        $view_data['rol'] = $this->session->userdata('rol');
        if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
            redirect('censo/censo/editar');
        }
        $view_data["menu"] = true;

    }else{
        redirect('/admin');
    }
    $objBanner = new StdClass();
    $objBanner->idbanner = intval($this->input->post('idbanner', TRUE));
    $idbanner = (int)$objBanner->idbanner;

    $objBanner->nombre_banner = $this->input->post('nombre_banner', TRUE);
    $objBanner->imagen = $this->input->post('imagen', TRUE);

    $data = $this->banner_model->addbanner($objBanner);

    if($data){
        redirect('banners/home');
    }else{
        $objBanner->error = "Error verifique los datos";
        $view_data["data"] = $objBanner; 
        $this->load->view('Head', $view_data);
        $this->load->view('banners/editar', $view_data);
        $this->load->view('Footer', $view_data);
    }
}
public function delete(){  
    $idbanner = intval($_GET["idbanner"]);
    $data = $this->banner_model->deleteBanner($idbanner);
    echo json_encode($data);
}

/* public function SavePhotoBanner(){   
    $file = $_GET["file"];
    $idusario = (int)$this->session->userdata('id');
    $data = $this->banner_model->update_photo_profile($idusario,$file);

    if ($data) {
       $usuario_data = array(
           'id' =>$this->session->userdata('id'),
           'nombre' => $this->session->userdata('nombre'),
           'apellido' => $this->session->userdata('apellido'),
           'rol' => $this->session->userdata('rol'),
           'foto' => $file,
           'correo' => $this->session->userdata('id'),
           'logueado' => TRUE
       );
       $this->session->set_userdata($usuario_data);
       echo json_encode($data);
   }
}

 */
public function validar_correo($correo){


    if ($this->usuario_model->checkEmail($correo)) { 
      $this->form_validation->set_message('validar_correo', 'Disculpe, el correo ya se encuentra registrado en el sistema'); 
      return FALSE; 
  } 

  return TRUE;

}

public function recover(){
    $objCenso = new StdClass();
        $view_data["menu"] = false;
        $responsemetas = new StdClass();    
        $metaname = $this->uri->segment(1);
        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }
        //$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        //$view_data["data"] = $responsemetas;

        $this->load->view('Head', $view_data);
        $this->load->view('usuarios/recover',$view_data);
        $this->load->view('Footer', $view_data);
}



}

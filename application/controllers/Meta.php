<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Meta extends CI_Controller {

   /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *       http://example.com/index.php/welcome
    * - or -
    *       http://example.com/index.php/welcome/index
    * - or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */
    

   public function __construct(){
    parent::__construct();
    $this->load->helper('tools_helper');
    $this->load->model('usuario_model');
    $this->load->model('meta_model');
    $this->load->model('mensajes_model');



    }
    
    public function home(){
     if($this->session->userdata('logueado')){
        $view_data = array();
        $view_data['nombre'] = $this->session->userdata('nombre');
        $view_data['apellido'] = $this->session->userdata('apellido');
        $view_data['rol'] = $this->session->userdata('rol');
        $idusario = (int)$this->session->userdata('id');

      $view_data["menu"] = true;
    }else{
        redirect('/admin');
    }  


    $response = new StdClass(); 
    $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
    $view_data["count_mensajes"] = $count_mensajes;
    $response->rows = $this->meta_model->get_Metas();
    $view_data["data"] = $response;
    $this->load->view('Head', $view_data);
    $this->load->view('meta/home',$view_data);
    $this->load->view('Footer', $view_data);   
  }

      public function editar(){
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor" or $view_data['rol'] =="Supervisor") {
                redirect('meta/meta/editar');
            }
            $view_data["menu"] = true;

        }else{
            redirect('/admin');
        }

        $idusr = $this->session->userdata('id');
        $objUsuario = new StdClass();
        $count_mensajes = $this->mensajes_model->get_Count_Sms();
        $view_data["count_mensajes"] = $count_mensajes;
        $id = $this->uri->segment(3);
        if( $id == "" or $id == "0"){
            $id = 0;
        }
        $objMeta = $this->meta_model->get_byIdMeta( $id );

        $view_data["data"] = $objMeta; 

        $this->load->view('Head', $view_data);
        $this->load->view('meta/editar', $view_data);
        $this->load->view('Footer', $view_data);
    }  

    public function update(){  

        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
                redirect('censo/censo/editar');
            }
            $view_data["menu"] = true;

        }else{
            redirect('/admin');
        }
        $objMeta = new StdClass();

        $objMeta->idmeta = (int)$this->input->post('idmeta', TRUE);
        $objMeta->pagina = $this->input->post('pagina', TRUE);
        $objMeta->title = $this->input->post('title', TRUE);
        $objMeta->type = $this->input->post('type', TRUE);
        $objMeta->url = $this->input->post('url', TRUE);
        $objMeta->description = $this->input->post('description', TRUE);
        $objMeta->site_name = $this->input->post('site_name', TRUE);
        $objMeta->locale = $this->input->post('locale', TRUE);
        $objMeta->keywords = $this->input->post('keywords', TRUE);


        $data = $this->meta_model->addmeta($objMeta);


        redirect('meta/home');


    }

   public function validar_correo($correo){


    if ($this->usuario_model->checkEmail($correo)) { 
      $this->form_validation->set_message('validar_correo', 'Disculpe, el correo ya se encuentra registrado en el sistema'); 
      return FALSE; 
    } 
 
    return TRUE;

   }
   

}

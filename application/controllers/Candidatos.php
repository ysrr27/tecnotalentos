<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Candidatos extends CI_Controller {

    /**
     * Index Page for this controller.
     *
     * Maps to the following URL
     *      http://example.com/index.php/welcome
     *  - or -
     *      http://example.com/index.php/welcome/index
     *  - or -
     * Since this controller is set as the default controller in
     * config/routes.php, it's displayed at http://example.com/
     *
     * So any other public methods not prefixed with an underscore will
     * map to /index.php/welcome/<method_name>
     * @see http://codeigniter.com/user_guide/general/urls.html
     */
     
    public function __construct(){
        parent::__construct();
        $this->load->helper('tools_helper');
        $this->load->helper('date');
        $this->load->model('candidato_model');
        $this->load->model('recluta_model');
        $this->load->model('meta_model');
        $this->load->model('preguntas_model');
        $this->load->model('Smtp_model');
        $this->load->library("email");
        $this->load->model('mensajes_model');
        $this->load->model('usuario_model');
    }
    
    public function home(){ 
        if($this->session->userdata('logueado')){
                $view_data = array();
                $view_data['nombre'] = $this->session->userdata('nombre');
                $view_data['rol'] = $this->session->userdata('rol');
                $idusario = (int)$this->session->userdata('id');


                $response = new StdClass(); 

                $response->rows = $this->candidato_model->get_Canditatos();
                $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);


                $view_data["data"] = $response;
                $view_data["count_mensajes"] = $count_mensajes;
                $view_data["menu"] = true;
                $this->load->view('Head', $view_data);
                $this->load->view('candidatos/home', $view_data);
                $this->load->view('Footer', $view_data);
            }else{
                redirect('/admin');
            }   

    }

    public function editar(){
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
        }else{
            redirect('/admin');
        }
        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;

        $objCandidato = new StdClass();


        $view_data["menu"] = true;

        $id = $this->uri->segment(3);
        if( $id == "" or $id == "0"){
            $id = 0;
        }
        $objCandidato = $this->candidato_model->get_byIdCandidato( $id );

        $view_data["data"] = $objCandidato;

        if( $this->uri->segment(2) == "editar"){

            $this->load->view('Head', $view_data);
            $this->load->view('candidatos/editar',$view_data);
            $this->load->view('Footer', $view_data);

        }else if( $this->uri->segment(2) == "preview"){

            $this->load->view('Head', $view_data);
            $this->load->view('candidatos/preview',$view_data);
            $this->load->view('Footer', $view_data);

        }   
    }
    public function preview(){  
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
            $objCandidato = new StdClass();

            $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
            $view_data["count_mensajes"] = $count_mensajes;
            $view_data["menu"] = true;

            $id = intval($this->uri->segment(3));
            if( $id == "" or $id == "0"){
                $id = 0;
            }

            $objCandidato = $this->candidato_model->get_Candidato($id);
            $view_data["data"] = $objCandidato;

            $this->load->view('Head', $view_data);
            $this->load->view('candidatos/preview',$view_data);
            $this->load->view('Footer', $view_data);


        }else{
            redirect('/admin');
        }
    }
    public function postulate(){    
        $objCenso = new StdClass();
        $objEmpadronador = new StdClass();

        $view_data["menu"] = false;;

        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(1);

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

            if($this->session->userdata('logueado')){

                $view_data['nombre'] = $this->session->userdata('nombre');
                $view_data['apellido'] = $this->session->userdata('apellido');
                $view_data['foto'] = $this->session->userdata('foto');
                $view_data['correo'] = $this->session->userdata('correo');
                $view_data['rol'] = $this->session->userdata('rol');
                $view_data['logueado'] = $this->session->userdata('logueado');

            }else{
                $view_data['logueado'] = false;
            }
                $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
                $responsemetas->preguntas = $this->preguntas_model->get_preguntas();
                $responsemetas->pasos = $this->preguntas_model->get_pasos();
                $view_data["data"] = $responsemetas;


                if( $this->uri->segment(2) == "postulate"){

                    $this->load->view('Head', $view_data);
                    $this->load->view('candidatos/postulate',$view_data);
                    $this->load->view('Footer', $view_data);

                }
    }
    public function update(){

        if($this->session->userdata('logueado')){
            $postdata = file_get_contents("php://input");
            $request = json_encode($_POST);
            $requestpost = json_decode($request);

            $objRespuestas                  = new StdClass();
            $objRespuestas->idcandidatos    = (int)@$requestpost->idcandidatos;
            $objRespuestas->respuestas      = json_encode(@$requestpost);
            $objRespuestas->fecha           = date("d/m/Y");

            $data = $this->candidato_model->updateCandidato($objRespuestas);
            echo json_encode($data);
        }else{
            redirect('/admin');
        }

    }
    public function candatoajax(){  
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);

        $ArrCandidato = array();
        $objCandidato          = new StdClass();
        $objdatospersonales    = new StdClass();
        $objeducacion        = new StdClass();
        $objexperiencia        = new StdClass();

        //datospersonales
        $objdatospersonales->iddatospersonales= @$requestpost->iddatospersonales;
        $objdatospersonales->Correo           = @$requestpost->Correo;
        $objdatospersonales->Nombre           = @$requestpost->Nombre;
        $objdatospersonales->Apellido         = @$requestpost->Apellido;
        $objdatospersonales->Telefono         = @$requestpost->Telefono;
        $objdatospersonales->Fecha_nacimiento = @$requestpost->Fecha_nacimiento;
        $objdatospersonales->Direccion        = @$requestpost->Direccion;

        //Educacion
        $objeducacion->ideducacion       = @$requestpost->ideducacion;
        $objeducacion->desde1            = @$requestpost->desde1;
        $objeducacion->hasta1            = @$requestpost->hasta1;
        $objeducacion->titulo1           = @$requestpost->titulo1;
        $objeducacion->institucion1      = @$requestpost->institucion1;
        $objeducacion->desde2            = @$requestpost->desde2;
        $objeducacion->hasta2            = @$requestpost->hasta2;
        $objeducacion->titulo2           = @$requestpost->titulo2;
        $objeducacion->institucion2      = @$requestpost->institucion2;
        $objeducacion->desde3            = @$requestpost->desde3;
        $objeducacion->hasta3            = @$requestpost->hasta3;
        $objeducacion->titulo3           = @$requestpost->titulo3;
        $objeducacion->institucion3      = @$requestpost->institucion3;
        $objeducacion->Idiomas_requerido = @$requestpost->Idiomas_requerido;
        $objeducacion->Nivel             = @$requestpost->Nivel;
        $objeducacion->Area              = @$requestpost->Area;
        $objeducacion->Nivel_cargo       = @$requestpost->Nivel_cargo;
        $objeducacion->Otro_cargo        = @$requestpost->Otro_cargo;
        $objeducacion->especificacion    = @$requestpost->especificacion;
        $objeducacion->Grado_especializacion = @$requestpost->Grado_especializacion;
        $objeducacion->Conocimientos_habilidades = @$requestpost->Conocimientos_habilidades;

        //experiencia profesional
        $objexperiencia->idexperiencia      = @$requestpost->idexperiencia;
        $objexperiencia->e_desde1           = @$requestpost->e_desde1;
        $objexperiencia->e_hasta1           = @$requestpost->e_hasta1;
        $objexperiencia->empresa1           = @$requestpost->empresa1;
        $objexperiencia->funcion_cargo1     = @$requestpost->funcion_cargo1;
        $objexperiencia->descripcion_cargo1 = @$requestpost->descripcion_cargo1;
        $objexperiencia->e_desde2           = @$requestpost->e_desde2;
        $objexperiencia->e_hasta2           = @$requestpost->e_hasta2;
        $objexperiencia->empresa2           = @$requestpost->empresa2;
        $objexperiencia->funcion_cargo2     = @$requestpost->funcion_cargo2;
        $objexperiencia->descripcion_cargo2 = @$requestpost->descripcion_cargo2;
        $objexperiencia->Pais               = @$requestpost->Pais;
        $objexperiencia->Region             = @$requestpost->Region;
        $objexperiencia->Ciudad             = @$requestpost->Ciudad;
        $objexperiencia->Jornada_laboral    = @$requestpost->Jornada_laboral;
        $objexperiencia->Tipo_contrato      = @$requestpost->Tipo_contrato;
        $objexperiencia->especifique_Tipo_contrato      = @$requestpost->especifique_Tipo_contrato;
        $objexperiencia->Salario            = @$requestpost->Salario;
        $objexperiencia->Monto              = @$requestpost->Monto;
        $objexperiencia->Jerarquia          = @$requestpost->Jerarquia;
        $objexperiencia->Experiencia        = @$requestpost->Experiencia;

        $idcandidato = intval($requestpost->idcandidato);

        $ArrCandidato['iduser'] = intval($this->session->userdata('id'));
        $ArrCandidato['fecha'] = local_to_gmt(time());
        $ArrCandidato['Acerca_de_mi'] = @$requestpost->Acerca_de_mi;
        $ArrCandidato['estatus'] = "Evaluado";
       
        $data = $this->candidato_model->addCandidato($idcandidato,$objdatospersonales,$objeducacion,$objexperiencia,$ArrCandidato);

        if ($data) {
            echo json_encode($data);
            
        }else{
            return false;
        }

    }
    public function delete(){   
        $idcandidato = (int) $_GET["id"];
        $data = $this->candidato_model->deleteCandidato($idcandidato);
        echo json_encode($data);
    }


   public function status(){   
        $idcandidato = (int) $_GET["idcandidato"];
        $status = $_GET["status"];
        $data = $this->candidato_model->update_status($idcandidato,$status);
        echo json_encode($data);
    }

    public function export(){  

        if($this->session->userdata('logueado')){
            $postdata = file_get_contents("php://input");
            $request = json_encode($_POST);
            $requestpost = json_decode($request);
            
            $result = $this->candidato_model->get_export();

                      //      $response->rows = $this->candidato_model->get_Canditatos();


            $html = "<table border='1'>";
            $html.="<tr>";
            $html.="<th> Nombre </th>";
            $html.="<th> Apellido </th>";
            $html.="<th> Correo </th>";
            $html.="<th> Teléfono </th>";
            $html.="<th> Titulo </th>";
            $html.="<th> fecha </th>";
            $html.="</tr>";
            $x = 0; 
            $html .= "<tr>";
            foreach($result as $row){
                foreach($row as $clave => $valor) {
                    $html .= "<td>".$valor."</td>";

                }
                $html .= "</tr>";
                $x = $x + 1 ;
            }
            $html.= "</table>";

         // $html = "<table border='1'><tr><td>ok</td></tr></table>";
            $filena= "Candiatos_".date("d/m/Y");
            header('Content-Encoding: UTF-8');
            header("Content-Type: application/xls; charset=UTF-8");    
            header("Content-Disposition: attachment; filename=".$filena.".xls"); 
            header('Content-Transfer-Encoding: binary');

            header("Pragma: no-cache"); 
            header("Expires: 0");

          //echo $html;
            echo chr(255).chr(254).iconv("UTF-8", "UTF-16LE//IGNORE", $html); 

            $view_data["cabeceras"] = $html;
            $view_data["data"] = $html;

            $view_data["menu"] = true;
            $this->load->view('Head', $view_data);
            $this->load->view('reportes/export');
            $this->load->view('Footer', $view_data);
            exit(); 

        }else{
            redirect('/');
        }

    }
 public function micuenta(){ 
        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 
        $Candidato = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        $rol = $this->session->userdata('rol');
        if($this->session->userdata('logueado') && $rol =='candidato'){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');
            $view_data['id'] = intval($this->session->userdata('id'));

            $idcandidato = intval($this->session->userdata('id'));
            $responsemetas->pasos = $this->preguntas_model->get_pasos();
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $id = intval($this->session->userdata('id'));
           

            $objUsuario = $this->usuario_model->get_byIdUser($id);
            $response->rows = $this->candidato_model->get_Candidato($id);


            if($response->rows) {
                $view_data["datacandidatos"] = $response;
                $view_data["candidato"] = $Candidato;
                $view_data["data"] = $responsemetas;
                 $this->load->view('Head', $view_data);
               // $this->load->view('micuenta/candidato_cuenta20200923');
                 $this->load->view('micuenta/candidato/perfil');
                 $this->load->view('Footer', $view_data);
             }else{
                redirect('/candidatos/editar_curriculum');
             }

            

        }elseif($this->session->userdata('logueado') && $rol =='recluta') {
            redirect('/reclutas/micuenta');
        }else{
            redirect('/login/sign_in/candidatos');
        }
    }
    public function editar_curriculum(){ 

        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        if($this->session->userdata('logueado')){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $objUsuario = new StdClass();
            $iduser = intval($this->session->userdata('id'));
            $objUsuario = $this->usuario_model->get_byIdUser( $iduser );
            $view_data["datauser"] = $objUsuario;  
            $response->rows = $this->candidato_model->get_Candidato($iduser);
            $view_data["datacandidatos"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/candidato/editar_curriculum');
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/login/sign_in/candidatos');
        }

    }

    public function mensajes(){ 

        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        if($this->session->userdata('logueado')){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $objUsuario = new StdClass();
            $iduser = intval($this->session->userdata('id'));
            $objUsuario = $this->usuario_model->get_byIdUser( $iduser );
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($iduser);
            $view_data['mensajes_nuevos'] = intval($this->mensajes_model->get_Count_Mensajes_Candidatos($iduser));
            $idsms = intval($response->rows_mensajes[0]->idmensaje);
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje($idsms);
            //$response->rows_ultimo_mensaje = $this->recluta_model->detalles_mensajes($idsms);
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $view_data["datauser"] = $objUsuario;  
            $response->rows = $this->candidato_model->get_Candidato($iduser);

            $view_data["datacandidatos"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/candidato/mensaje');
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/login/sign_in/candidatos');

        }

    }
    public function configuracion(){ 

        $view_data["menu"] = false;
        $responsemetas = new StdClass();    
        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        if($this->session->userdata('logueado')){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
           
            $objUsuario = new StdClass();

            $iduser = intval($this->session->userdata('id'));
            $objUsuario = $this->usuario_model->get_byIdUser( $iduser );
            $response->user = $objUsuario;
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($iduser);

            $view_data['mensajes_nuevos'] = intval($this->mensajes_model->get_Count_Mensajes_Candidatos($iduser));

            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje($iduser);
           
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $view_data["datauser"] = $objUsuario;  

            $response->rows = $this->candidato_model->get_Candidato($iduser);

            $view_data["datacandidatos"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/candidato/configuracion');
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/login/sign_in/candidatos');

        }

    }
    public function settins(){  
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data["menu"] = true;
            $idusario = (int)$this->session->userdata('id');

        }else{
            redirect('/admin');
        }

        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);

        $objUsuario = new StdClass();
        $objUsuario->id =  $idusario;
        $objUsuario->nombre =  @$requestpost->nombre;
        $objUsuario->apellido =  @$requestpost->apellido;
        $objUsuario->correo =  @$requestpost->correo;
        $objUsuario->telefono =  @$requestpost->telefono;
        
        $contrasena = $this->input->post('clave_actual', TRUE);
        $get_pwd_actual= $this->usuario_model->pass($idusario);

        if (!empty($contrasena) && sha1($contrasena) == $get_pwd_actual->contrasena) {
            $objUsuario->contrasena = sha1($this->input->post('nueva_clave', TRUE));
        }else{
           $objUsuario->contrasena = $get_pwd_actual;
        }

        $data = $this->usuario_model->updateuser($objUsuario);

        if ($data) {
          $usuario_data = array(
             'id' => $idusario,
             'nombre' => $objUsuario->nombre,
             'apellido' => $objUsuario->apellido,
             'rol' =>  $this->session->userdata('rol'),
             'logueado' => TRUE
         );
          $this->session->set_userdata($usuario_data);
          echo json_encode($data);
      }

    }


}



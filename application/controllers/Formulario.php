<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Formulario extends CI_Controller {

   /**
    * Index Page for this controller.
    *
    * Maps to the following URL
    *       http://example.com/index.php/welcome
    * - or -
    *       http://example.com/index.php/welcome/index
    * - or -
    * Since this controller is set as the default controller in
    * config/routes.php, it's displayed at http://example.com/
    *
    * So any other public methods not prefixed with an underscore will
    * map to /index.php/welcome/<method_name>
    * @see http://codeigniter.com/user_guide/general/urls.html
    */
   public function __construct(){
      parent::__construct();
      $this->load->helper('tools_helper');
      $this->load->model('usuario_model');
      $this->load->model('preguntas_model');
      $this->load->model('mensajes_model');

  }

      public function home() {     
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data["menu"] = true;
            $idusario = (int)$this->session->userdata('id');

        }else{
            redirect('/admin');
        }  


        $response = new StdClass(); 
        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;

        $response->rows = $this->preguntas_model->get_Metas();
        $view_data["data"] = $response;
        $this->load->view('Head', $view_data);
        $this->load->view('formulario/home',$view_data);
        $this->load->view('Footer', $view_data);
    }
    public function editar() { 
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data["menu"] = true;
            $idusario = (int)$this->session->userdata('id');

        }else{
            redirect('/admin');
        }

        $objUsuario = new StdClass();
        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;

        $id = $this->uri->segment(4);
        if( $id == "" or $id == "0"){
            $id = 0;
        }

        $response = new StdClass();    

        $response->rows = $this->preguntas_model->get_preguntas();
        $response->pasos = $this->preguntas_model->get_pasos();
        $view_data["data"] = $response;

        $this->load->view('Head', $view_data);
        $this->load->view('formulario/editar', $view_data);
        $this->load->view('Footer', $view_data); 
    }
    public function update() {  

        if($this->session->userdata('logueado')){           
            $postdata = file_get_contents("php://input");
            $request = json_encode($_POST);
            $requestpost = json_decode($request);
            $objPreguntas = new StdClass();
            $objPreguntas->idpregunta       = (int)@$requestpost->idpregunta;
            $objPreguntas->pregunta         = @$requestpost->pregunta;
            $objPreguntas->formato_pregunta = @$requestpost->formato_pregunta;
            $objPreguntas->tipo_pregunta    = @$requestpost->tipo_pregunta;
            $objPreguntas->paso             = @$requestpost->paso;
            $objPreguntas->opciones         = @$requestpost->opciones;
            $objPreguntas->requerido        = @$requestpost->requerido;

            $data = $this->preguntas_model->addPreguntas($objPreguntas);
            echo json_encode($data);
        }else{
            redirect('/admin');
        }
    }
    public function pasos() {  
        if($this->session->userdata('logueado')){            
            $idpaso = (int) $_GET["idpaso"];

            $objPasos = new StdClass();

            $objPasos->nombre_paso      = "Paso ".$idpaso;
            $objPasos->valor_paso       = $idpaso;

            $data = $this->preguntas_model->addPaso($objPasos);
            echo json_encode($data);
        }else{
            redirect('/admin');
        }echo json_encode($data);
    }
    public function preguntas() {  
        if($this->session->userdata('logueado')){           
            $idpregunta = (int) $_GET["idpregunta"];

            $data = $this->preguntas_model->get_byPregunta($idpregunta);
            echo json_encode($data);
        }else{
            redirect('/admin');
        }
    }


    public function ajaxPro() {  
        if($this->session->userdata('logueado')){           
                $postdata = file_get_contents("php://input");
            $request = json_encode($_POST);
            $requestpost = json_decode($request);
            $objPoaition = new StdClass();

            $position = $_POST['position'];
      

            $i=1;
            foreach($position as $k=>$v){

               $data = $this->preguntas_model->order($i,$v);

                $i++;
            }

            exit();

        }else{
            redirect('/admin');
        }
    }


    public function delete(){  
        $idpregunta = (int) $_GET["idpregunta"];
        $data = $this->preguntas_model->deletePregunta($idpregunta);
        echo json_encode($data);
    }

   public function validar_correo($correo){


    if ($this->usuario_model->checkEmail($correo)) { 
      $this->form_validation->set_message('validar_correo', 'Disculpe, el correo ya se encuentra registrado en el sistema'); 
      return FALSE; 
    } 
 
    return TRUE;

   }
   

}

<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pdfx extends CI_Controller {

/**
* Index Page for this controller.
*
* Maps to the following URL
*      http://example.com/index.php/welcome
*  - or -
*      http://example.com/index.php/welcome/index
*  - or -
* Since this controller is set as the default controller in
* config/routes.php, it's displayed at http://example.com/
*
* So any other public methods not prefixed with an underscore will
* map to /index.php/welcome/<method_name>
* @see http://codeigniter.com/user_guide/general/urls.html
*/

public function __construct(){
    parent::__construct();
    $this->load->helper('tools_helper');
    $this->load->model('candidato_model');
    $this->load->model('recluta_model');
    $this->load->model('meta_model');
    $this->load->model('preguntas_model');
    $this->load->library('Pdf');



}

public function cv_candidato(){ 
    if($this->session->userdata('logueado')){

        $Candidato = new StdClass();

        $idusario = (int)$this->session->userdata('id');

        $Candidato = $this->candidato_model->get_Candidato($idusario);

        // create new PDF document
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

        // remove default header/footer
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);

        // set margins
        $pdf->SetMargins(0, 0, 0, true);

        // set auto page breaks false
        $pdf->SetAutoPageBreak(false, 0);
        $photo = (!empty($Candidato->file) && $Candidato->file > "") ? $Candidato->file : "assets/img/fondo.jpg";
        
        // add a page
        $pdf->AddPage('P', 'A4');

        // Display image on full page
        $pdf->Image($photo, 0, 0, 210, 297, 'JPG', '', '', true, 200, '', false, false, 0, false, false, true);
        $photo2 = (!empty($Candidato->foto) && $Candidato->foto > "") ? $Candidato->foto : "assets/img/img.png";
        //logo
        $logo = "assets/img/TecnoTalento_Logo.png";
        $pdf->Image($logo, 5, 5, 40, 9, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
        //perfil
        $pdf->StartTransform();
        $pdf->StarPolygon(107, 47, 22, 90, 3, 0, 1, 'CNZ');
        $pdf->Image($photo2, 85, 25, 44, 44, '', '', '', true, 300);
        $pdf->StopTransform();
        $pdf->SetTextColor(127,132,137);

        $html = '<b>'.strtoupper($Candidato->nombre).' '.strtoupper($Candidato->apellido).'</b><br><br>
        <b>Fecha de Nacimiento:</b> '.$Candidato->Fecha_nacimiento.'<br>
        <b>Ubicación Actual:</b>'.$Candidato->Direccion.'<br>
        <b>Teléfono:</b> '.$Candidato->Telefono.'<br>
        <b>Email:</b> '.$Candidato->Correo;
        $pdf->writeHTMLCell(0, 0, '',72, $html, 'LRTB', 1, 1, true, 'C', true);


        $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">ACERCA DE MI</h4>';
        $pdf->writeHTMLCell(0, 0, 20, 127, $html, 'LRTB', 1, 0, true, 'L', true);
        $html = '<p>'.$Candidato->Acerca_de_mi.'</p>';
        $pdf->writeHTMLCell(86, 50, 20, 137, $html, 'LRTB', 1, 0, true, 'L', true);


        $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">EXPERIENCA</h4>';
        $pdf->writeHTMLCell(0, 0, 110, 127, $html, 'LRTB', 1, 0, true, 'L', true);

        //Experiencia
        $html = '<ul>';
        $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde1.' - '.$Candidato->hasta1.'</small><h4>'.$Candidato->titulo1.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion1.'</p></li>';

        if (!empty($Candidato->desde2) && !empty($Candidato->titulo2) && !empty($Candidato->institucion2)) {
           $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde2.' - '.$Candidato->hasta2.'</small><h4>'.$Candidato->titulo2.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion2.'</p></li>';
       }

       if (!empty($Candidato->desde3) && !empty($Candidato->titulo3) && !empty($Candidato->institucion3)) {
           $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde3.' - '.$Candidato->hasta3.'</small><h4>'.$Candidato->titulo3.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion3.'</p></li>';
       }

       $html .= '</ul>';
       $pdf->writeHTMLCell(86, 50, 105, 137, $html, 'LRTB', 1, 0, true, 'L', true);

        //educacion
       $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">EDUCACIÓN</h4>';
       $pdf->writeHTMLCell(0, 0, 20, 175, $html, 'LRTB', 1, 0, true, 'L', true);

       $html = '<ul>';
       $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde1.' - '.$Candidato->hasta1.'</small><h4>'.$Candidato->titulo1.'</h4><p style="color: #7f8489;font-size: 9px;">'.$Candidato->institucion1.'</p><br></li>';

       if (!empty($Candidato->desde2) && !empty($Candidato->titulo2) && !empty($Candidato->institucion2)) {
           $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde2.' - '.$Candidato->hasta2.'</small><h4>'.$Candidato->titulo2.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion2.'</p></li>';
       }

        /*    if (!empty($Candidato->desde3) && !empty($Candidato->titulo3) && !empty($Candidato->institucion3)) {
                 $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde3.' - '.$Candidato->hasta3.'</small><h4>'.$Candidato->titulo3.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion3.'</p></li>';
             }*/

             $html .= '</ul>';
             $pdf->writeHTMLCell(86, 50, 10, 185, $html, 'LRTB', 1, 0, true, 'L', true);

        //idiomas
             $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">IDIOMAS</h4>';
             $pdf->writeHTMLCell(0, 0, 0, 260, $html, 'LRTB', 1, 0, true, 'C', true);

             $html = '<h4 style="font-weight: bold;">Ingles</h4><p style="color: #7f8489;font-size: 10px;">Avanzado</p>';
             $pdf->writeHTMLCell(0, 0, 20, 270, $html, 'LRTB', 1, 0, true, 'L', true);

             $pdf->Output('cv_'.strtolower($Candidato->Nombre).' '.strtolower($Candidato->Apellido).'-@tecnotalentos.pdf', 'D');
         }else{
            redirect('/admin');
        }   
    }

    public function cv_export_candidato(){ 
        if($this->session->userdata('logueado')){

            $Candidato = new StdClass();

            $idusario = $this->uri->segment(3);
            $Candidato = $this->candidato_model->get_Candidato($idusario);

            // create new PDF document
            $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

            // remove default header/footer
            $pdf->setPrintHeader(false);
            $pdf->setPrintFooter(false);

            // set margins
            $pdf->SetMargins(0, 0, 0, true);

            // set auto page breaks false
            $pdf->SetAutoPageBreak(false, 0);
            $photo = (!empty($Candidato->file) && $Candidato->file > "") ? $Candidato->file : "assets/img/fondo.jpg";

            // add a page
            $pdf->AddPage('P', 'A4');

            // Display image on full page
            $pdf->Image($photo, 0, 0, 210, 297, 'JPG', '', '', true, 200, '', false, false, 0, false, false, true);
            $photo2 = (!empty($Candidato->foto) && $Candidato->foto > "") ? $Candidato->foto : "assets/img/img.png";
            //logo
            $logo = "assets/img/TecnoTalento_Logo.png";
            $pdf->Image($logo, 5, 5, 40, 9, 'PNG', '', '', false, 150, '', false, false, 0, false, false, false);
             //perfil
            $pdf->StartTransform();
            $pdf->StarPolygon(107, 47, 22, 90, 3, 0, 1, 'CNZ');
            $pdf->Image($photo2, 85, 25, 44, 44, '', '', '', true, 300);
            $pdf->StopTransform();
            $pdf->SetTextColor(127,132,137);

            $html = '<b>'.strtoupper($Candidato->nombre).' '.strtoupper($Candidato->apellido).'</b><br><br>
            <b>Fecha de Nacimiento:</b> '.$Candidato->Fecha_nacimiento.'<br>
            <b>Ubicación Actual:</b>'.$Candidato->Direccion.'<br>
            <b>Teléfono:</b> '.$Candidato->Telefono.'<br>
            <b>Email:</b> '.$Candidato->Correo;
            $pdf->writeHTMLCell(0, 0, '',72, $html, 'LRTB', 1, 1, true, 'C', true);


            $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">ACERCA DE MI</h4>';
            $pdf->writeHTMLCell(0, 0, 20, 127, $html, 'LRTB', 1, 0, true, 'L', true);
            $html = '<p>'.$Candidato->Acerca_de_mi.'</p>';
            $pdf->writeHTMLCell(86, 50, 20, 137, $html, 'LRTB', 1, 0, true, 'L', true);


            $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">EXPERIENCA</h4>';
            $pdf->writeHTMLCell(0, 0, 110, 127, $html, 'LRTB', 1, 0, true, 'L', true);

            //Experiencia
            $html = '<ul>';
            $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde1.' - '.$Candidato->hasta1.'</small><h4>'.$Candidato->titulo1.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion1.'</p></li>';

            if (!empty($Candidato->desde2) && !empty($Candidato->titulo2) && !empty($Candidato->institucion2)) {
               $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde2.' - '.$Candidato->hasta2.'</small><h4>'.$Candidato->titulo2.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion2.'</p></li>';
           }

           if (!empty($Candidato->desde3) && !empty($Candidato->titulo3) && !empty($Candidato->institucion3)) {
               $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde3.' - '.$Candidato->hasta3.'</small><h4>'.$Candidato->titulo3.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion3.'</p></li>';
           }

           $html .= '</ul>';
           $pdf->writeHTMLCell(86, 50, 105, 137, $html, 'LRTB', 1, 0, true, 'L', true);

            //educacion
           $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">EDUCACIÓN</h4>';
           $pdf->writeHTMLCell(0, 0, 20, 175, $html, 'LRTB', 1, 0, true, 'L', true);

           $html = '<ul>';
           $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde1.' - '.$Candidato->hasta1.'</small><h4>'.$Candidato->titulo1.'</h4><p style="color: #7f8489;font-size: 9px;">'.$Candidato->institucion1.'</p><br></li>';

           if (!empty($Candidato->desde2) && !empty($Candidato->titulo2) && !empty($Candidato->institucion2)) {
               $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde2.' - '.$Candidato->hasta2.'</small><h4>'.$Candidato->titulo2.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion2.'</p></li>';
           }

        /*    if (!empty($Candidato->desde3) && !empty($Candidato->titulo3) && !empty($Candidato->institucion3)) {
                 $html .= '<li><small style="color:#3d65a7;font-weight: bold;font-size: 10px;">'.$Candidato->desde3.' - '.$Candidato->hasta3.'</small><h4>'.$Candidato->titulo3.'</h4><p style="color: #7f8489;font-size: 10px;">'.$Candidato->institucion3.'</p></li>';
             }*/

             $html .= '</ul>';
             $pdf->writeHTMLCell(86, 50, 10, 185, $html, 'LRTB', 1, 0, true, 'L', true);

             //idiomas
             $html = '<h4 style="color:#3d65a7;letter-spacing:4px;font-weight: bold;">IDIOMAS</h4>';
             $pdf->writeHTMLCell(0, 0, 0, 260, $html, 'LRTB', 1, 0, true, 'C', true);

             $html = '<h4 style="font-weight: bold;">Ingles</h4><p style="color: #7f8489;font-size: 10px;">Avanzado</p>';
             $pdf->writeHTMLCell(0, 0, 20, 270, $html, 'LRTB', 1, 0, true, 'L', true);

             $pdf->Output('cv_'.strtolower($Candidato->Nombre).' '.strtolower($Candidato->Apellido).'-@tecnotalentos.pdf', 'I');
         }else{
            redirect('/admin');
        }   
    }
}



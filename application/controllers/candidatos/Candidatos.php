<?php 

$this->load->model('candidato_model');
$this->load->model('meta_model');
$this->load->model('preguntas_model');
$this->load->model('Smtp_model');
$this->load->library("email");


	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('censo/censo/editar');
				}
			}else{
				redirect('/admin');
			}	

		    $response = new StdClass();	

			$response->rows = $this->candidato_model->get_Canditatos();
			$view_data["data"] = $response;

			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('candidatos/home', $view_data);
			$this->load->view('Footer', $view_data);
		
		break;

		case "editar":
		case "preview":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			}else{
				redirect('/admin');
			}

			$objCandidato = new StdClass();


			$view_data["menu"] = true;

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}
			 $objCandidato = $this->candidato_model->get_byIdCandidato( $id );

			 $view_data["data"] = $objCandidato;

			 if( $this->uri->segment(3) == "editar"){

				$this->load->view('Head', $view_data);
				$this->load->view('candidatos/editar',$view_data);
				$this->load->view('Footer', $view_data);

			 }else if( $this->uri->segment(3) == "preview"){

			 	$this->load->view('Head', $view_data);
				$this->load->view('candidatos/preview',$view_data);
				$this->load->view('Footer', $view_data);

			 }


		break;
		case "postulate":

			$objCenso = new StdClass();
			$objEmpadronador = new StdClass();


			$view_data["menu"] = false;

			
		$responsemetas = new StdClass();	

//		$metaname = $this->uri->segment(2);
		$metaname = "$this->uri->segment(2)";




		if( $metaname == "" or $metaname == "0"){
				$metaname = "home";
			}


			//$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
			$responsemetas->rows = $metaname;
			/*$responsemetas->preguntas = $this->preguntas_model->get_preguntas();
			$responsemetas->pasos = $this->preguntas_model->get_pasos();
			$view_data["data"] = $responsemetas;*/


			// if( $this->uri->segment(1) == "postulate"){

				$this->load->view('Head', $view_data);
				$this->load->view('candidatos/postulate',$view_data);
				$this->load->view('Footer', $view_data);

			 //}

		break;

		case "update":

		/*$postdata = file_get_contents("php://input");
  	  	$request = json_decode($postdata);*/

  	  	if($this->session->userdata('logueado')){


		$postdata = file_get_contents("php://input");
  	  	$request = json_encode($_POST);
  	  	$requestpost = json_decode($request);


  	  	$objRespuestas  				= new StdClass();
  	  	$objRespuestas->idcandidatos	= (int)@$requestpost->idcandidatos;
  	  	$objRespuestas->respuestas		= json_encode(@$requestpost);
  	  	$objRespuestas->fecha 			= date("d/m/Y");



  	  	$data = $this->candidato_model->updateCandidato($objRespuestas);

  	  	echo json_encode($data);



  	  	}else{
  	  		redirect('/admin');
  	  	}

		break;
		case "candatoajax":


		$postdata = file_get_contents("php://input");
  	  	$request = json_encode($_POST);
  	  	$requestpost = json_decode($request);


  	  	$objRespuestas  			= new StdClass();
  	  	$objCorreo	  				= new StdClass();
  	  	$objRespuestas->respuestas	= json_encode(@$requestpost);
  	  	$objRespuestas->fecha 		= date("d/m/Y");

		$data = $this->candidato_model->addCandidato($objRespuestas);

			if ($data) {
				echo json_encode($data);

				$email_body = "";
				foreach($requestpost as $key => $value)  {

					$email_body .= "<br><b>".$key.":</b> ".$value;

					if ($key == "Correo") {
						$objCorreo->correo = $value;
					}

					if ($key == "Nombre") {
						$objCorreo->Nombre = $value;
					}
					if ($key == "Apellido") {
						$objCorreo->Apellido = $value;
					}


				}

					$config = $this->Smtp_model->smtp();

					$Correo = $objCorreo->correo;
					$email_message = "<b><h2>Gracias por Contáctarnos</h2> <br> <h3>Pronto nos estaremos comunicando con usted</h3></b>";

					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from('jaime@revenuemanagementworld.com', 'Jaime Chicheri');
					$this->email->to($Correo);
					$this->email->subject('Contacto');
					$this->email->message($email_message);
					$this->email->send();


					//Correo para Jaime

					$CorreoContacto = "jaime@revenuemanagementworld.com";
					//$CorreoContacto = "ysrr27@gmail.com";
					


					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from($Correo, $objCorreo->Nombre." ".$objCorreo->Apellido);
					$this->email->to($CorreoContacto);
					$this->email->subject('Candidato');
					$this->email->message($email_body);
					$this->email->send();
			
		}else{
			return false;
		}

		break;		
		case 'delete':
				//echo "deleted:".$_GET["idsuscripcion"];
		$idcandidato = (int) $_GET["id"];

		$data = $this->candidato_model->deleteCandidato($idcandidato);

		echo json_encode($data);

		break;
		case "export":

		if($this->session->userdata('logueado')){


			$html = '<table border="1">';
			$html .='<tr>';
			$html .='<th>Dirección de correo electrónico</th>';
			$html .='<th>Nombre</th>';
			$html .='<th>Apellido</th>';
			$html .='<th>Telefono</th>';
			$html .='<th>Fecha de nacimiento</th>';
			$html .='<th>Blog / canal de Youtube u otro perfil donde transmitas tus conocimientos</th>';
			$html .='<th>Perfil de Facebook (indica la url completa)</th>';
			$html .='<th>Perfil de Twitter (indica la url completa)</th>';
			$html .='<th>Perfil de Instagram (indica la url completa)</th>';
			$html .='<th>Perfil de Linkedin (indica la url completa)</th>';
			$html .='<th>Ciudad de Nacimiento</th>';
			$html .='<th>País de Nacimiento</th>';
			$html .='<th>Ciudad de Residencia</th>';
			$html .='<th>País de Residencia</th>';
			$html .='<th>Ciudades y Países en las que te gustaría trabajar</th>';
			$html .='<th>Predisposición para trabajar en otro paises</th>';
			$html .='<th>Cuéntanos, en breves palabras, lo que te hace un gran profesional</th>';
			$html .='<th>¿Qué buscas?</th>';
			$html .='<th>Años de experiencia totales Mercado 1: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia</th>';
			$html .='<th>Años de experiencia totales Mercado 2: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia</th>';
			$html .='<th>Años de experiencia totales Mercado 3: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia</th>';
			$html .='<th>Años de experiencia totales Mercado 4: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia</th>';
			$html .='<th>Indica tu experiencia en el segmento Urbano</th>';
			$html .='<th>Indica tu experiencia en el segmento Vacacional</th>';
			$html .='<th>Indica tu experiencia en Rentals / Apartamentos turísticos / Viviendas de uso vacacional</th>';
			$html .='<th>Cuéntanos aquello que te hace grande en este campo ¿algún logro que quieras destacar?</th>';
			$html .='<th>Indica los países donde has gestionado las estrategias de distribución y los años de experiencia</th>';
			$html .='<th>Experiencia en distribución Online (nivel)</th>';
			$html .='<th>Si lo deseas cuéntanos más sobre tu experiencia en distribución online</th>';
			$html .='<th>Experiencia en distribución Offline</th>';
			$html .='<th>Si lo deseas cuéntanos más sobre tu experiencia en distribución offline</th>';
			$html .='<th>¿Has hecho Revenue para otros sectores? (campos de golf, restaurantes, lineas aéreas, spas, salones...) ¡Cuéntanos tu experiencia!</th>';
			$html .='<th>SEO Técnico</th>';
			$html .='<th>SEO Offpage</th>';
			$html .='<th>SEM</th>';
			$html .='<th>RRSS</th>';
			$html .='<th>Email Marketing</th>';
			$html .='<th>Redacción de contenidos (Blogging y copywriting)</th>';
			$html .='<th>Diseño de Imágenes</th>';
			$html .='<th>Embudos de Conversión</th>';
			$html .='<th>Marketing Tradicional (relación con medios, radio, prensa, cartelería)</th>';
			$html .='<th>Planificación de estrategias de Marketing Digital</th>';
			$html .='<th>Nivel</th>';
			$html .='<th>Cuéntanos aquello que te hace grande en este campo ¿algún logro que quieras destacar?</th>';
			$html .='<th>¿Has dado conferencias y/o escrito en medios profesionales del sector Hotelero? (en caso afirmativo indica todo lo que pueda ayudarnos a conocerte mejor)</th>';
			$html .='<th>¿Donde has aprendido Revenue Management? (en caso de no tener formación sobre esta materia indícalo)</th>';
			$html .='<th>¿Donde has aprendido Marketing Hotelero? (en caso de no tener formación sobre esta materia indícalo)</th>';
			$html .='<th>¿Donde has aprendido Control de gestión? (en caso de no tener formación sobre esta materia indícalo)</th>';
			$html .='<th>¿Donde has aprendido Marketing Hotelero? (en caso de no tener formación sobre esta materia indícalo)</th>';
			$html .='<th>Inglés</th>';
			$html .='<th>Francés</th>';
			$html .='<th>Alemán</th>';
			$html .='<th>Indica Conocimientos en otros idiomas (o explicaciones extra sobre los anteriores tales como titulaciones etc...)</th>';
			$html .='<th>Excel</th>';
			$html .='<th>PMS (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>Channel Manager (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>RMS(Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>Reviewer (reputación online) (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>Shopper (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>Power Bi (Indica tu nivel de conocimiento)</th>';
			$html .='<th>CRM(Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una)</th>';
			$html .='<th>GDS (Indica cuáles conoces y tu nivel de conocimiento en cada uno)</th>';
			$html .='<th>Python, R u otras herramaientas de análisis de datos</th>';
			$html .='<th>Carnet de conducir</th>';
			$html .='<th>Vehículo propio</th>';
			$html .='<th>Acepto la política de privacidad</th>';
			

			$html .='</tr>';

			$result = $this->candidato_model->get_export();


			$arr = array();
 				$x = 0; 
 				$html .= "<tr>";

 				foreach($result as $row){
 					foreach($row as $clave => $valor) {
 						$html .= "<td>".$valor."</td>";

 					}
	 				$html .= "</tr>";
	 		 		$x = $x + 1 ;
 				}

			$html.= '</table>';

			$filena= "Candidatos_".date("d/m/Y");

			header('Content-Encoding: UTF-8');
			header("Content-Type: application/xls;charset=UTF-8");     
			header("Content-Disposition: attachment; filename=".$filena.".xls");  
			header("Pragma: no-cache"); 
			header("Expires: 0");

			echo $html;
			exit();

		//	echo json_encode($data);

		}else{
			redirect('/');
		}
		
	
		break;		
	}


 ?>
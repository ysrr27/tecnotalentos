<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Solicitudes extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
    	parent::__construct();

    	$this->load->helper('tools_helper');
        $this->load->helper('date');
    	$this->load->helper('cookie');
    	$this->load->library('session');
    	$this->load->library("email");
    	$this->load->model('Smtp_model');
    	$this->load->model('recluta_model');
    	$this->load->model('meta_model');
        $this->load->model('usuario_model');
        $this->load->model('solicitudes_model');
        $this->load->model('mensajes_model');



    }
    
    public function home(){	
    	if($this->session->userdata('logueado')){
    		$view_data = array();
    		$view_data['nombre'] = $this->session->userdata('nombre');
    		$view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
        	$response = new StdClass();	
               
            $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
            $view_data["count_mensajes"] = $count_mensajes;

        	$response->rows = $this->solicitudes_model->get_Solicitudes();
        	$view_data["data"] = $response;
        	$view_data["menu"] = true;
        	$this->load->view('Head', $view_data);
        	$this->load->view('solicitudes/home', $view_data);
        	$this->load->view('Footer', $view_data);
    	
    	}else{
    		redirect('/admin');
    	}	

    }

    public function nuevo_mensaje(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);
        $objMensaje = new StdClass();
        $objMensaje_detalle = new StdClass();

        $file_name = str_replace("uploads/", "", @$requestpost->file);
        $objMensaje->useridfrom    = intval($this->session->userdata('id'));
        $objMensaje->useridto      = intval(@$requestpost->useridto_respuesta);
        $objMensaje->subject       = @$requestpost->subject;
        $objMensaje->fullmensaje   = @$requestpost->fullmensaje;
        $objMensaje->smallmessage  = @$requestpost->subject;
        $objMensaje->idsolicitud   = @$requestpost->idsolicitud_respuesta;
        $objMensaje->status        = "message--new";
        $objMensaje->fecha    = local_to_gmt(time()); 

        $idmensaje = $this->solicitudes_model->addmensaje($objMensaje);

        $objMensaje_detalle->idmensaje      = intval($idmensaje);
        $objMensaje_detalle->useridfrom     = intval($this->session->userdata('id'));
        $objMensaje_detalle->useridto       = intval(@$requestpost->useridto_respuesta);
        $objMensaje_detalle->send_userid    = intval($this->session->userdata('id'));
        $objMensaje_detalle->fullmensaje    = @$requestpost->fullmensaje;
        $objMensaje_detalle->file           = $file_name;
        $objMensaje_detalle->file_url       = @$requestpost->file;
        $objMensaje_detalle->status         = "message--new";
        $objMensaje_detalle->timecreate     = local_to_gmt(time()); 

        $data = $this->solicitudes_model->addmensaje_detalle($objMensaje_detalle);

        if ($data) {
           echo json_encode($data);
        }else{
            echo "false";
        }
    }


    public function delete(){  
        $idsolicitud = (int) $_GET["idsolicitud"];
        $data = $this->solicitudes_model->deleteSolicitud($idsolicitud);
        echo json_encode($data);
    }

    public function preview(){  
        if($this->session->userdata('logueado')){
            $view_data = array();
            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
            $objSolicitudes = new StdClass();

            $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
            $view_data["count_mensajes"] = $count_mensajes;
            $view_data["menu"] = true;

            $id = intval($this->uri->segment(3));
            if( $id == "" or $id == "0"){
                $id = 0;
            }


            $objSolicitudes = $this->solicitudes_model->getSolicitudbyid($id);

            $view_data["data"] = $objSolicitudes[0];

            $this->load->view('Head', $view_data);
            $this->load->view('solicitudes/preview',$view_data);
            $this->load->view('Footer', $view_data);


        }else{
            redirect('/admin');
        }
    }
    
   public function status(){   
        $idsolicitud = (int) $_GET["idsolicitud"];
        $status = $_GET["status"];
        $data = $this->solicitudes_model->update_status($idsolicitud,$status);
        echo json_encode($data);
    }

}

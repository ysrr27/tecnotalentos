<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Welcome extends CI_Controller {
	
	public function __construct(){
    	parent::__construct();
        //always check if session userdata value "logged_in" is not true
         $this->load->helper('tools_helper');
		 $this->load->model('meta_model');
		 $this->load->model('banner_model');

        
    }
	public function index()
	{
		$view_data["menu"] = false;
		$responsemetas = new StdClass();
		$dataview = new StdClass();		

		$metaname = $this->uri->segment(2);

		if( $metaname == "" or $metaname == "0"){
				$metaname = "home";
			}


			if($this->session->userdata('logueado')){

				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['apellido'] = $this->session->userdata('apellido');
				$view_data['foto'] = $this->session->userdata('foto');
				$view_data['correo'] = $this->session->userdata('correo');
				$view_data['rol'] = $this->session->userdata('rol');
				$view_data['logueado'] = $this->session->userdata('logueado');

				$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
				$dataview->banners = $this->banner_model->get_banners();
				$view_data['logueado'] = true;
				$view_data["data"] = $responsemetas;
				$view_data["dataview"] = $dataview;
				$this->load->view('Head', $view_data);
				$this->load->view('inicio');
				$this->load->view('Footer', $view_data);
			}else{

				$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
				$view_data['logueado'] = false;
				$view_data["data"] = $responsemetas;
				$this->load->view('Head', $view_data);
				$this->load->view('inicio');
				$this->load->view('Footer', $view_data);
			}
	}

}

<?php 
		$this->load->library('session');
		$this->load->helper('cookie');
		$this->load->model('Smtp_model');
		$this->load->library("email");
		$this->load->model('recluta_model');
		$this->load->model('meta_model');

		switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home": 

			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor") {
					redirect('censo/censo/editar');
				}
			}else{
				redirect('/admin');
			}	

		    $response = new StdClass();	

			$response->rows = $this->recluta_model->get_Reclutas();
			$view_data["data"] = $response;
			$view_data["menu"] = true;
			$this->load->view('Head', $view_data);
			$this->load->view('reclutas/home', $view_data);
			$this->load->view('Footer', $view_data);
		
		break;

		case "editar":
		case "preview":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
			}else{
				redirect('/admin');
			}

			$objRecluta = new StdClass();

			$view_data["menu"] = true;

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}


			 $objRecluta = $this->recluta_model->get_byIdRecluta( $id );

			 $view_data["data"] = $objRecluta;

			 if( $this->uri->segment(3) == "editar"){

				$this->load->view('Head', $view_data);
				$this->load->view('reclutas/editar',$view_data);
				$this->load->view('Footer', $view_data);

			 }else if( $this->uri->segment(3) == "preview"){

			 	$this->load->view('Head', $view_data);
				$this->load->view('reclutas/preview',$view_data);
				$this->load->view('Footer', $view_data);

			 }	

		break;
		case "contacto":

			$objCenso = new StdClass();
			$objEmpadronador = new StdClass();


			$view_data["menu"] = false;

			$responsemetas = new StdClass();	

			$metaname = $this->uri->segment(2);

			if( $metaname == "" or $metaname == "0"){
				$metaname = "home";
			}


			$responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
			$view_data["data"] = $responsemetas;

			 if( $this->uri->segment(3) == "contacto"){

				$this->load->view('Head', $view_data);
				$this->load->view('reclutas/contacto',$view_data);
				$this->load->view('Footer', $view_data);

			 }

		break;
		case "update":

		/*$postdata = file_get_contents("php://input");
  	  	$request = json_decode($postdata);*/

  	  	if($this->session->userdata('logueado')){

			$postdata = file_get_contents("php://input");
	  	  	$request = json_encode($_POST);
	  	  	$requestpost = json_decode($request);


	  	  	$objRecluta = new StdClass();

			/* Obj Identificacion del Contribuyente */
			$idrecluta						= @$requestpost->id_recluta;
			$objRecluta->id_recluta			= (int) $idrecluta;
			$objRecluta->Nombre_recluta		= @$requestpost->Nombre_recluta;
			$objRecluta->Apellido_recluta	= @$requestpost->Apellido_recluta;
			$objRecluta->Telefono_recluta	= @$requestpost->Telefono_recluta;
			$objRecluta->Correo_recluta		= @$requestpost->Correo_recluta;
			$objRecluta->Empresa_recluta	= @$requestpost->Empresa_recluta;
			$objRecluta->Mesaje_recluta		= @$requestpost->Mesaje_recluta;
			$objRecluta->Fecha_recluta		= date("d/m/Y");


			$data = $this->recluta_model->addrecluta($objRecluta);

			if($data){
				redirect('reclutas/reclutas/home');
			}else{
				echo "error verifique los datos";
			}

  	  	}else{
  	  		redirect('/admin');
  	  	}

		break;
		case "contactajax":

			$postdata = file_get_contents("php://input");
	  	  	$request = json_encode($_POST);
	  	  	$requestpost = json_decode($request);


	  	  	$objRecluta = new StdClass();

			/* Obj Identificacion del Contribuyente */
			$idrecluta						= @$requestpost->id_recluta;
			$objRecluta->id_recluta			= (int) $idrecluta;
			$objRecluta->Nombre_recluta		= @$requestpost->Nombre_recluta;
			$objRecluta->Apellido_recluta	= @$requestpost->Apellido_recluta;
			$objRecluta->Telefono_recluta	= @$requestpost->Telefono_recluta;
			$objRecluta->Correo_recluta		= @$requestpost->Correo_recluta;
			$objRecluta->Empresa_recluta	= @$requestpost->Empresa_recluta;
			$objRecluta->Mesaje_recluta		= @$requestpost->Mesaje_recluta;
			$objRecluta->Fecha_recluta		= date("d/m/Y");




			$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));

			$userIp=$this->input->ip_address();

			$secret = $this->config->item('google_secret');

			$credential = array(
				'secret' => $secret,
				'response' => $this->input->post('g-recaptcha-response')
			);

			$verify = curl_init();
			curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($verify, CURLOPT_POST, true);
			curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
			curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($verify);

			$status= json_decode($response, true);

			if($status['success']){ 
				 $data = $this->recluta_model->addrecluta($objRecluta);
				 $this->session->set_flashdata('message', 'Google Recaptcha Successful');
			}else{
				$this->session->set_flashdata('message', 'Sorry Google Recaptcha Unsuccessful!!');
			}

									
		if ($data) {
					echo json_encode($data);

					$config = $this->Smtp_model->smtp();

					$Correo = $objRecluta->Correo_recluta;
					$email_message = "<b><h2>Gracias por Contáctarnos</h2> <br> <h3>Pronto nos estaremos comunicando con usted</h3></b>";

					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from('jaime@revenuemanagementworld.com', 'Jaime Chicheri');
					$this->email->to($Correo);
					$this->email->subject('Contacto');
					$this->email->message($email_message);
					$this->email->send();


					//Correo para Jaime

					$CorreoContacto = "jaime@revenuemanagementworld.com";
					$email_body = "<b><h2>Reclutador</h2><br>Nombre: </b>".$objRecluta->Nombre_recluta."<br><b>Apellido: </b>".$objRecluta->Apellido_recluta."<br><b>Teléfono: </b>".$objRecluta->Telefono_recluta."<br><b>Dirección de correo electronico: </b>".$objRecluta->Correo_recluta."<br><b>Mensaje: </b>".$objRecluta->Mesaje_recluta;


					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from($Correo, $objRecluta->Nombre_recluta." ".$objRecluta->Apellido_recluta);
					$this->email->to($CorreoContacto);
					$this->email->subject('Contacto Reclutador');
					$this->email->message($email_body);
					$this->email->send();
			
		}else{
			return false;
		}




		break;		
		case 'delete':
				//echo "deleted:".$_GET["idsuscripcion"];
		$idrecluta = $_GET["id"];
		$idrecluta2 = (int) $idrecluta;

		$data = $this->recluta_model->deleteRecluta($idrecluta2);

		echo json_encode($data);

		break;
		case "export":

		if($this->session->userdata('logueado')){


			$html = '<table border="1">';
			$html .='<tr>';
			$html .='<th>id</th>';
			$html .='<th>Nombre</th>';
			$html .='<th>Apellido</th>';
			$html .='<th>Telefono</th>';
			$html .='<th>Correo</th>';
			$html .='<th>Empresa</th>';
			$html .='<th>Mensaje</th>';
			$html .='<th>Fecha</th>';
			$html .='</tr>';

			$result = $this->recluta_model->get_export();

			$arr = array();
 				$x = 0; 
 				$html .= "<tr>";

 				foreach($result as $row){
 					foreach($row as $clave => $valor) {
 						$html .= "<td>".$valor."</td>";

 					}
	 				$html .= "</tr>";
	 		 		$x = $x + 1 ;
 				}

			$html.= '</table>';

			$filena= "Reclutas_".date("d/m/Y");

			header("Content-Type: application/xls");    
			header("Content-Disposition: attachment; filename=".$filena.".xls");  
			header("Pragma: no-cache"); 
			header("Expires: 0");

			echo $html;
			exit();

		//	echo json_encode($data);

		}else{
			redirect('/');
		}

	
		break;	
	}


 ?>
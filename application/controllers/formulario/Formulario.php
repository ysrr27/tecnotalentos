<?php 
    $this->load->model('usuario_model');
    $this->load->model('preguntas_model');

	switch($this->uri->segment(3)){
		default:
		case "search": 
		case "home":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['apellido'] = $this->session->userdata('apellido');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor"  or $view_data['rol'] =="Supervisor") {
					redirect('meta/meta/editar');
				}
				$view_data["menu"] = true;
			}else{
				redirect('/admin');
			}	 


			$response = new StdClass();	

			$response->rows = $this->preguntas_model->get_Metas();
			$view_data["data"] = $response;
			$this->load->view('Head', $view_data);
			$this->load->view('formulario/home',$view_data);
			$this->load->view('Footer', $view_data);

		break;

		case "editar":
			if($this->session->userdata('logueado')){
				$view_data = array();
				$view_data['nombre'] = $this->session->userdata('nombre');
				$view_data['rol'] = $this->session->userdata('rol');
				if ($view_data['rol'] =="Empadronador" or $view_data['rol'] =="Transcriptor" or $view_data['rol'] =="Supervisor") {
					redirect('formulario/formulario/editar');
				}
				$view_data["menu"] = true;

			}else{
				redirect('/admin');
			}

			$objUsuario = new StdClass();

			$id = $this->uri->segment(4);
			if( $id == "" or $id == "0"){
				$id = 0;
			}

			 $response = new StdClass();	

			$response->rows = $this->preguntas_model->get_preguntas();
			$response->pasos = $this->preguntas_model->get_pasos();
			$view_data["data"] = $response;

			$this->load->view('Head', $view_data);
			$this->load->view('formulario/editar', $view_data);
			$this->load->view('Footer', $view_data);
		break;

		case "update":

			if($this->session->userdata('logueado')){			
				$postdata = file_get_contents("php://input");
		  	  	$request = json_encode($_POST);
		  	  	$requestpost = json_decode($request);
		  	  	$objPreguntas = new StdClass();
		  	  	$objPreguntas->idpregunta		= (int)@$requestpost->idpregunta;
		  	  	$objPreguntas->pregunta 		= @$requestpost->pregunta;
		  	  	$objPreguntas->formato_pregunta = @$requestpost->formato_pregunta;
		  	  	$objPreguntas->tipo_pregunta 	= @$requestpost->tipo_pregunta;
		  	  	$objPreguntas->paso 			= @$requestpost->paso;
		  	  	$objPreguntas->opciones 		= @$requestpost->opciones;
		  	  	$objPreguntas->requerido 		= @$requestpost->requerido;

				$data = $this->preguntas_model->addPreguntas($objPreguntas);
				echo json_encode($data);
			}else{
				redirect('/admin');
			}
			break;
			case 'pasos':
				if($this->session->userdata('logueado')){			
				$idpaso = (int) $_GET["idpaso"];
		  	  
		  	  	$objPasos = new StdClass();

		  	  	$objPasos->nombre_paso		= "Paso ".$idpaso;
		  	  	$objPasos->valor_paso		= $idpaso;

				$data = $this->preguntas_model->addPaso($objPasos);
				echo json_encode($data);
			}else{
				redirect('/admin');
			}
			break;
			case 'preguntas':
				if($this->session->userdata('logueado')){			
					$idpregunta = (int) $_GET["idpregunta"];

					$data = $this->preguntas_model->get_byPregunta($idpregunta);
					echo json_encode($data);
				}else{
					redirect('/admin');
				}

			break;		
	}


 ?>
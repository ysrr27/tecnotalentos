<?php
if (!defined('BASEPATH'))
   exit('No direct script access allowed');

class Login extends CI_Controller {
   public function __construct() {
      parent::__construct();
      $this->load->model('usuario_model');
      $this->load->model('meta_model');
      $this->load->model('Smtp_model');
      $this->load->helper('tools_helper');
      $this->load->helper('date');
      $this->load->helper('cookie');
      $this->load->library('session');
      $this->load->library("email");
      $this->load->model('Smtp_model');
      $this->load->model('mensajes_model');

   }


   public function iniciar_sesion_post() {

      if ($this->input->post()) {
         $correo = $this->input->post('correo');
         $contrasena = sha1($this->input->post('contrasena', TRUE));
         $usuario = $this->usuario_model->session($correo, $contrasena);

         if ($usuario && $usuario->rol == 'Administrador') {
            $usuario_data = array(
               'id' => $usuario->id,
               'nombre' => $usuario->nombre,
               'apellido' => $usuario->apellido,
               'rol' => $usuario->rol,
               'logueado' => TRUE
            );
            $this->session->set_userdata($usuario_data);
            redirect('dashboard');
         } else {
            redirect('/admin');
         }
      } else {
         $this->iniciar_sesion();
      }
   }

   public function logueado() {
      if($this->session->userdata('logueado')){
         $data = array();
         $data['nombre'] = $this->session->userdata('nombre');
         $this->load->view('usuarios/logueado', $data);
      }else{
         redirect('usuarios/iniciar_sesion');
      }
   }

   public function cerrar_sesion() {
      $usuario_data = array(
         'logueado' => FALSE
      );
      $this->session->set_userdata($usuario_data);
      redirect('/admin');
   }

   public function sign_in(){  
      $objCenso = new StdClass();
      $objEmpadronador = new StdClass();
      $response = new StdClass();
      $view_data["menu"] = false;
      $responsemetas = new StdClass(); 
      $metaname = $this->uri->segment(1);

      if( $metaname == "" or $metaname == "0"){
         $metaname = "home";
      }

      if($this->session->userdata('logueado')){
         redirect('reclutas/micuenta');
      }else{
        $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        $view_data["data"] = $responsemetas;
        $view_data['logueado'] = false;
           $this->load->view('Head', $view_data);
           $this->load->view('login/register_signin',$view_data);
           $this->load->view('Footer', $view_data);
     }

  }

      //registro    
    public function registro(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);

        $objRegistro = new StdClass();
        $data = new StdClass();
        $checkEmail  = $this->usuario_model->checkEmail(@$requestpost->correo);

        if ($checkEmail) {
            $data->estatus = false;
        }else{
            $objRegistro->id         = intval(0);
            $objRegistro->nombre     = @$requestpost->nombre;
            $objRegistro->correo     = @$requestpost->correo;
            $objRegistro->contrasena = sha1(@$requestpost->contrasena);
            $objRegistro->rol        = substr($requestpost->user, 0, -1);

            $data->idregistro = $this->usuario_model->adduser($objRegistro);
            $data->estatus = true;

            $usuario_data = array(
                'id' => $data->idregistro,
                'nombre' => $objRegistro->nombre,
                'rol' => $objRegistro->rol,
                'correo' => $objRegistro->correo,
                'logueado' => TRUE
            );
            $this->session->set_userdata($usuario_data);
        }
        echo json_encode($data);

    }

  public function iniciar_sesion_recluta() {
   $postdata = file_get_contents("php://input");
   $request = json_encode($_POST);
   $requestpost = json_decode($request);
   $data = new StdClass();
   $correo = @$requestpost->correo_session;
   $contrasena = sha1( @$requestpost->contrasena_session);
   $usuario = $this->usuario_model->session($correo, $contrasena);

   if ($usuario && $usuario->rol == 'recluta' || $usuario->rol == 'candidato') {
      $usuario_data = array(
         'id' => $usuario->id,
         'nombre' => $usuario->nombre,
         'apellido' => $usuario->apellido,
         'rol' => $usuario->rol,
         'foto' => $usuario->foto,
         'correo' => $usuario->correo,
         'logueado' => TRUE
      );
      $this->session->set_userdata($usuario_data);
      $data->usuario = $usuario;
      $data->estatus = true;
   } else {
      $data->estatus = false;
   }

   echo json_encode($data);
}

public function cerrar_sesion_recluta() {
   $usuario_data = array(
      'logueado' => FALSE,
      'id'=> '',
      'nombre'=> '',
      'apellido'=> '',
      'foto'=> '',
      'correo'=> '',
   );
   $this->session->set_userdata($usuario_data);
   redirect('/');
}

public function recover(){
   $postdata = file_get_contents("php://input");
   $request = json_encode($_POST);
   $requestpost = json_decode($request);


   $objUsuario = new StdClass();

   $usuario = $this->usuario_model->checkEmail($requestpost->correo);


   if (!is_null($usuario)) {
      $config = $this->Smtp_model->smtp();
      $Correo = $usuario->correo;
      $objUsuario->correo = $usuario->correo;
      $longitud = 8;
      $pass = substr(MD5(rand(5, 100)), 0, $longitud);
      $objUsuario->contrasena = sha1($pass);
      $data = $this->usuario_model->recoverypassword($objUsuario);
      if($data){
               //echo json_encode($data);

        $email_message = '<table width="100%" height="100%" style="min-width:348px" border="0" cellspacing="0" cellpadding="0" lang="en"><tbody><tr height="32" style="height:32px"><td></td></tr><tr align="center"><td><div><div></div></div><table border="0" cellspacing="0" cellpadding="0" style="padding-bottom:20px;max-width:516px;min-width:220px"><tbody><tr><td width="8" style="width:8px"></td><td><div style="background-color:#f5f5f5;direction:ltr;padding:16px;margin-bottom:6px"><table width="100%" border="0" cellspacing="0" cellpadding="0"><tbody><tr><td style="vertical-align:top"></td><td width="13" style="width:13px"></td><td style="direction:ltr"></td></tr></tbody></table></div><div style="border-style:solid;border-width:thin;border-color:#dadce0;border-radius:8px;padding:40px 20px" align="center" class="m_3756075729702722652mdv2rw"><div style="border-bottom:thin solid #dadce0;color:rgba(0,0,0,0.87);line-height:32px;padding-bottom:24px;text-align:center;word-break:break-word"><div style="font-size:24px">Se recupero la cuenta <span class="il">correctamente</span></div><table align="center" style="margin-top:8px"><tbody><tr style="line-height:normal"><td align="right" style="padding-right:8px"><img width="20" height="20" style="width:20px;height:20px;vertical-align:sub;border-radius:50%" src="https://ci4.googleusercontent.com/proxy/QpsGaULeBaBhhOTpb-uwGsICda8b1ae95rM7JtYlDtcjbrJ_fDlrGcQ9nUwocVilT_dWdlntnRieTr4GY_IFycf2zxXXuPXiHCdY7G5yRw7uJHHhalp2NYvY=s0-d-e1-ft#https://www.gstatic.com/accountalerts/email/anonymous_profile_photo.png" class="CToWUd"></td><td><a style="color:rgba(0,0,0,0.87);font-size:14px;line-height:20px">'.$Correo.'</a></td></tr><tr style="line-height:normal"><td colspan="2"><a style="color:rgba(0,0,0,0.87);font-size:14px;line-height:20px">Su clave es:</a></td></tr><tr style="line-height:normal"><td colspan="2"><a style="background: #ffc;color:rgba(0,0,0,0.87);font-size:14px;font-weight:bold;line-height:20px">'.$pass.'</a></td></tr></tbody></table></div><div style="font-family:Roboto-Regular,Helvetica,Arial,sans-serif;font-size:14px;color:rgba(0,0,0,0.87);line-height:20px;padding-top:20px;text-align:left"><h3>Te damos nuevamente la bienvenida a tu cuenta</h3></div></div></div></td><td width="8" style="width:8px"></td></tr></tbody></table></td></tr><tr height="32" style="height:32px"><td></td></tr></tbody></table>';

        $this->email->initialize($config);
        $this->email->set_mailtype('html');
        $this->email->from('jaime@revenuemanagementworld.com', 'Jaime Chicheri');
        $this->email->to($Correo);
        $this->email->subject('Recuperacion de cuenta Trabajos de Revenue Manager');
        $this->email->message($email_message);
        $this->email->send();
        var_dump($this->email->print_debugger());


					//exit();	
     }

  }else{
   $null = intval(1);
   echo  $null;
   exit();
}

}
}
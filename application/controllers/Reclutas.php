<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Reclutas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	public function __construct(){
    	parent::__construct();

    	$this->load->helper('tools_helper');
        $this->load->helper('date');
    	$this->load->helper('cookie');
    	$this->load->library('session');
    	$this->load->library("email");
    	$this->load->model('Smtp_model');
    	$this->load->model('recluta_model');
    	$this->load->model('meta_model');
        $this->load->model('usuario_model');
        $this->load->model('mensajes_model');
    }
    
    public function home(){	
    	if($this->session->userdata('logueado')){
    		$view_data = array();
    		$view_data['nombre'] = $this->session->userdata('nombre');
    		$view_data['rol'] = $this->session->userdata('rol');
    		$idusario = (int)$this->session->userdata('id');
    	}else{
    		redirect('/admin');
    	}	

    	$response = new StdClass();	


        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;
    	$response->rows = $this->usuario_model->get_users_reclutas();
    	$view_data["data"] = $response;
    	$view_data["menu"] = true;
    	$this->load->view('Head', $view_data);
    	$this->load->view('reclutas/home', $view_data);
    	$this->load->view('Footer', $view_data);
    }
    public function editar(){
    	if($this->session->userdata('logueado')){
    		$view_data = array();
    		$view_data['nombre'] = $this->session->userdata('nombre');
    		$view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
    	}else{
    		redirect('/admin');
    	}

    	$objRecluta = new StdClass();
        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;
    	$view_data["menu"] = true;

    	$id = $this->uri->segment(3);
    	if( $id == "" or $id == "0"){
    		$id = 0;
    	}


    	$objRecluta = $this->recluta_model->get_byIdRecluta( $id );

    	$view_data["data"] = $objRecluta;

    	if( $this->uri->segment(2) == "editar"){

    		$this->load->view('Head', $view_data);
    		$this->load->view('reclutas/editar',$view_data);
    		$this->load->view('Footer', $view_data);

    	}else if( $this->uri->segment(2) == "preview"){

    		$this->load->view('Head', $view_data);
    		$this->load->view('reclutas/preview',$view_data);
    		$this->load->view('Footer', $view_data);

    	}		
    }
    public function preview(){	
    	if($this->session->userdata('logueado')){
    		$view_data = array();
    		$view_data['nombre'] = $this->session->userdata('nombre');
    		$view_data['rol'] = $this->session->userdata('rol');
            $idusario = (int)$this->session->userdata('id');
    	}else{
    		redirect('/admin');
    	}

    	$objRecluta = new StdClass();

    	$view_data["menu"] = true;

    	$id = $this->uri->segment(4);
    	if( $id == "" or $id == "0"){
    		$id = 0;
    	}

        $count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
        $view_data["count_mensajes"] = $count_mensajes;
    	$objRecluta = $this->recluta_model->get_byIdRecluta( $id );

    	$view_data["data"] = $objRecluta;

    	if( $this->uri->segment(2) == "editar"){

    		$this->load->view('Head', $view_data);
    		$this->load->view('reclutas/editar',$view_data);
    		$this->load->view('Footer', $view_data);

    	}else if( $this->uri->segment(2) == "preview"){

    		$this->load->view('Head', $view_data);
    		$this->load->view('reclutas/preview',$view_data);
    		$this->load->view('Footer', $view_data);

    	}	
    }

    public function update(){	
    	if($this->session->userdata('logueado')){

    		$postdata = file_get_contents("php://input");
    		$request = json_encode($_POST);
    		$requestpost = json_decode($request);


    		$objRecluta = new StdClass();

    		/* Obj Identificacion del Contribuyente */
    		$idrecluta						= @$requestpost->id_recluta;
    		$objRecluta->id_recluta			= (int) $idrecluta;
    		$objRecluta->Nombre_recluta		= @$requestpost->Nombre_recluta;
    		$objRecluta->Apellido_recluta	= @$requestpost->Apellido_recluta;
    		$objRecluta->Telefono_recluta	= @$requestpost->Telefono_recluta;
    		$objRecluta->Correo_recluta		= @$requestpost->Correo_recluta;
    		$objRecluta->Empresa_recluta	= @$requestpost->Empresa_recluta;
    		$objRecluta->Mesaje_recluta		= @$requestpost->Mesaje_recluta;
    		$objRecluta->Fecha_recluta		= date("d/m/Y");


    		$data = $this->recluta_model->addrecluta($objRecluta);

    		if($data){
    			redirect('reclutas/reclutas/home');
    		}else{
    			echo "error verifique los datos";
    		}

    	}else{
    		redirect('/admin');
    	}

    }
    public function contactajax(){
    	$postdata = file_get_contents("php://input");
	  	  	$request = json_encode($_POST);
	  	  	$requestpost = json_decode($request);


	  	  	$objRecluta = new StdClass();

			/* Obj Identificacion del Contribuyente */
			$idrecluta						= @$requestpost->id_recluta;
			$objRecluta->id_recluta			= (int) $idrecluta;
			$objRecluta->Nombre_recluta		= @$requestpost->Nombre_recluta;
			$objRecluta->Apellido_recluta	= @$requestpost->Apellido_recluta;
			$objRecluta->Telefono_recluta	= @$requestpost->Telefono_recluta;
			$objRecluta->Correo_recluta		= @$requestpost->Correo_recluta;
			$objRecluta->Empresa_recluta	= @$requestpost->Empresa_recluta;
			$objRecluta->Mesaje_recluta		= @$requestpost->Mesaje_recluta;
            $objRecluta->Ciudad_trabajo     = @$requestpost->Ciudad_trabajo;
            $objRecluta->Pais_trabajo     = @$requestpost->Pais_trabajo;
            $objRecluta->Nombre_empresa     = @$requestpost->Nombre_empresa;
			
            $objRecluta->Fecha_recluta		= date("d/m/Y");
			$recaptchaResponse = trim($this->input->post('g-recaptcha-response'));
			
            $userIp=$this->input->ip_address();
			$secret = $this->config->item('google_secret');
			$credential = array(
				'secret' => $secret,
				'response' => $this->input->post('g-recaptcha-response')
			);

			$verify = curl_init();
			curl_setopt($verify, CURLOPT_URL, "https://www.google.com/recaptcha/api/siteverify");
			curl_setopt($verify, CURLOPT_POST, true);
			curl_setopt($verify, CURLOPT_POSTFIELDS, http_build_query($credential));
			curl_setopt($verify, CURLOPT_SSL_VERIFYPEER, false);
			curl_setopt($verify, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($verify);

			$status= json_decode($response, true);

			if($status['success']){ 
				 $data = $this->recluta_model->addrecluta($objRecluta);
				 $this->session->set_flashdata('message', 'Google Recaptcha Successful');
			}else{
				$this->session->set_flashdata('message', 'Sorry Google Recaptcha Unsuccessful!!');
			}

									
		if ($data) {
					echo json_encode($data);

					$config = $this->Smtp_model->smtp();

	
					$Correo = $objRecluta->Correo_recluta;
					$email_message = "<b><h2>En primer lugar agradecer tu confianza en que te ayudemos a encontrar talento para tu empresa.</h2> <br> <h3>Pronto nos pondremos en contacto contigo. Si tienes cualquier duda rogamos te pongas en contacto con nosotros.</h3></b>";

					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from('jaime@revenuemanagementworld.com', 'Jaime Chicheri');
					$this->email->to($Correo);
					$this->email->subject('Contacto Trabajos de Revenue Manager');
					$this->email->message($email_message);
					$this->email->send();
					//Correo para Jaime

					//$CorreoContacto = "jaime@revenuemanagementworld.com";
                    $CorreoContacto = "trabajosderevenuemanagement@gmail.com";
					$email_body = "<b><h2>Reclutador</h2><br>Nombre: </b>".$objRecluta->Nombre_recluta."<br><b>Apellido: </b>".$objRecluta->Apellido_recluta."<br><b>Telefono: </b>".$objRecluta->Telefono_recluta."<br><b>Direccion de correo electronico: </b>".$objRecluta->Correo_recluta."<br><b>Mensaje: </b>".$objRecluta->Mesaje_recluta;


					$this->email->initialize($config);

					$this->email->set_mailtype('html');

					$this->email->from($Correo, $objRecluta->Nombre_recluta." ".$objRecluta->Apellido_recluta);
					$this->email->to($CorreoContacto);
					$this->email->subject('Nuevo Recluta Trabajos de Revenue Manager');
					$this->email->message($email_body);
					$this->email->send();
			
		}else{
			return false;
		}
	
    }
    public function perfil_empresa(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);


        if (!empty($requestpost) && $requestpost) {
            $objEmpresa = new StdClass();
            $objUSuario = new StdClass();

            /* Obj Identificacion de la empresa */
            $objEmpresa->idempresa              = (int)@$requestpost->idempresa;
            $objEmpresa->idrecluta              = intval($this->session->userdata('id'));
            $objEmpresa->Razon_social_empresa   = @$requestpost->Razon_social_empresa;
            $objEmpresa->Descripcion_empresa    = @$requestpost->Descripcion_empresa;
            $objEmpresa->Rif_empresa            = @$requestpost->Rif_empresa;
            $objEmpresa->Email_contacto         = @$requestpost->Email_contacto;
            $objEmpresa->Estado_empresa         = @$requestpost->Estado_empresa;
            $objEmpresa->Ciudad_empresa         = @$requestpost->Ciudad_empresa;
            $objEmpresa->Direccion_empresa      = @$requestpost->Direccion_empresa;
            $objEmpresa->Fecha_empresa          = date("d/m/Y");

            $data = $this->recluta_model->addempresa($objEmpresa);
            echo json_encode($data);
        }else{
            return false;

        }



    }
    public function buzon(){
         $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        $rol = $this->session->userdata('rol');
        if($this->session->userdata('logueado') && $rol =='recluta'){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');
            $idrecluta = intval($this->session->userdata('id'));
            $view_data['idrecluta'] = $idrecluta;


            $response->rows = $this->recluta_model->get_solicitudes_recluta($idrecluta);
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($idrecluta);
            $view_data['mensajes_nuevos'] = intval($this->recluta_model->get_Count_Mensajes());
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje_recluta($idrecluta);
            $response->rows_perfil = $this->recluta_model->get_data_perfil_empresa($idrecluta);

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        
            $view_data["dataperfil"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/recluta/buzon');
            $this->load->view('Footer', $view_data);

        }elseif($this->session->userdata('logueado') && $rol =='candidato') {
            redirect('/login/sign_in/candidatos');
        }else{
            redirect('/login/sign_in/reclutas');
        }
    }
      public function correo(){
         $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        $rol = $this->session->userdata('rol');
        if($this->session->userdata('logueado') && $rol =='recluta'){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');
            $idrecluta = intval($this->session->userdata('id'));
            $view_data['idrecluta'] = $idrecluta;


            $response->rows = $this->recluta_model->get_solicitudes_recluta($idrecluta);
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($idrecluta);
            $view_data['mensajes_nuevos'] = intval($this->recluta_model->get_Count_Mensajes());
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje_recluta($idrecluta);
            $response->rows_perfil = $this->recluta_model->get_data_perfil_empresa($idrecluta);

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        
            $view_data["datasolicitudes"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/recluta/correo');
            $this->load->view('Footer', $view_data);

        }elseif($this->session->userdata('logueado') && $rol =='candidato') {
            redirect('/login/sign_in/candidatos');
        }else{
            redirect('/login/sign_in/reclutas');
        }
    }
    public function delete(){	
    					//echo "deleted:".$_GET["idsuscripcion"];
    	$idrecluta = $_GET["id"];
    	$idrecluta2 = (int) $idrecluta;

    	$data = $this->recluta_model->deleteRecluta($idrecluta2);

    	echo json_encode($data);
    }
    public function export(){	

		if($this->session->userdata('logueado')){


			$html = '<table border="1">';
			$html .='<tr>';
			$html .='<th>id</th>';
			$html .='<th>Nombre</th>';
			$html .='<th>Apellido</th>';
			$html .='<th>Telefono</th>';
			$html .='<th>Correo</th>';
			$html .='<th>Empresa</th>';
			$html .='<th>Mensaje</th>';
            $html .='<th>Ciudad en la que se ejecutaría el Trabajo</th>';
            $html .='<th>País en el que se ejecutaría el Trabajo</th>';
            $html .='<th>¿Tienes problemas en que comuniquemos el nombre de tu empresa?</th>';
			$html .='<th>Fecha</th>';
			$html .='</tr>';

			$result = $this->recluta_model->get_export();

			$arr = array();
 				$x = 0; 
 				$html .= "<tr>";

 				foreach($result as $row){
 					foreach($row as $clave => $valor) {
 						$html .= "<td>".$valor."</td>";

 					}
	 				$html .= "</tr>";
	 		 		$x = $x + 1 ;
 				}

			$html.= '</table>';

			$filena= "Reclutas_".date("d/m/Y");

			header("Content-Type: application/xls");    
			header("Content-Disposition: attachment; filename=".$filena.".xls");  
			header("Pragma: no-cache"); 
			header("Expires: 0");

			echo $html;
			exit();

		//	echo json_encode($data);

		}else{
			redirect('/');
		}
    }

    public function micuenta(){ 
        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        $rol = $this->session->userdata('rol');
        if($this->session->userdata('logueado') && $rol =='recluta'){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');
            $idrecluta = intval($this->session->userdata('id'));
            $view_data['idrecluta'] = $idrecluta;


            $response->rows = $this->recluta_model->get_solicitudes_recluta($idrecluta);
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($idrecluta);
            $view_data['mensajes_nuevos'] = intval($this->recluta_model->get_Count_Mensajes());
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje_recluta($idrecluta);
            $response->rows_perfil = $this->recluta_model->get_data_perfil_empresa($idrecluta);

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        

            $view_data["dataperfil"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/recluta/perfil');
            $this->load->view('Footer', $view_data);

        }elseif($this->session->userdata('logueado') && $rol =='candidato') {
          redirect('/login/sign_in/candidatos');
      }else{
        redirect('/login/sign_in/reclutas');

    }


    }
      public function solicitud(){ 
        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        $rol = $this->session->userdata('rol');
        if($this->session->userdata('logueado') && $rol =='recluta'){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');
            $idrecluta = intval($this->session->userdata('id'));
            $view_data['idrecluta'] = $idrecluta;


            $response->rows = $this->recluta_model->get_solicitudes_recluta($idrecluta);
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($idrecluta);
            $view_data['mensajes_nuevos'] = intval($this->recluta_model->get_Count_Mensajes());
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje_recluta($idrecluta);
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
        

            $view_data["datasolicitudes"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/recluta/solicitud');
            $this->load->view('Footer', $view_data);

        }elseif($this->session->userdata('logueado') && $rol =='candidato') {
            redirect('/login/sign_in/candidatos');
        }else{
            redirect('/login/sign_in/reclutas');
        }
    }

    public function perfil(){ 

        $view_data["menu"] = false;
        $responsemetas = new StdClass();    

        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }


        if($this->session->userdata('logueado')){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');

       //     $response->rows = $this->recluta_model->get_Solicitudes();
            $response->rows_mensajes = $this->recluta_model->get_Mensajes();
            $view_data['mensajes_nuevos'] = intval($this->recluta_model->get_Count_Mensajes());
            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje();
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
           
            $objUsuario = new StdClass();

            $iduser = intval($this->session->userdata('id'));
            $objUsuario = $this->usuario_model->get_byIdUser( $iduser );

            $view_data["datauser"] = $objUsuario;  

            $view_data["datasolicitudes"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/perfil');
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/login/sign_in/reclutas');
        }

    }

    public function actualizar_datos(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);

        $objActualizardatos = new StdClass();
        $data = new StdClass();      

        $objActualizardatos->id         = @$requestpost->id;
        $objActualizardatos->nombre     = @$requestpost->nombre;
        $objActualizardatos->apellido   = "";
        $objActualizardatos->correo     = @$requestpost->correo;
        $pass                           = @$requestpost->contrasena;
        $objActualizardatos->rol        = "recluta";
        $objActualizardatos->telefono   = @$requestpost->telefono;
        $objActualizardatos->foto       =str_replace(" ", "_", @$requestpost->file);


        $pws = $this->usuario_model->pass(@$requestpost->id);

        if ($pass == $pws->contrasena) {
            $objActualizardatos->contrasena = $pass;

        }else{
             $objActualizardatos->contrasena = sha1($pass);
        }

        $data = $this->usuario_model->updateuser($objActualizardatos);
       
        if ($data) {
            $usuario_data = array(
                'id' => $objActualizardatos->id,
                'nombre' => $objActualizardatos->nombre,
                'apellido' => $objActualizardatos->apellido,
                'rol' => $objActualizardatos->rol,
                'foto' => $objActualizardatos->foto,
                'correo' => $objActualizardatos->correo,
                'logueado' => TRUE
            );
            $this->session->set_userdata($usuario_data);
        }

        echo json_encode($data);

    }


    //solitudes
    public function solicitudajax(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);
  
        if (!empty($requestpost) && !empty($requestpost->Puesto_buscar) && !empty($requestpost->Pais) && !empty($requestpost->Estudios_minimos)) {
            $objSolicitud = new StdClass();

            $objSolicitud->idrecluta            = intval($this->session->userdata('id'));
            $objSolicitud->Puesto_buscar        = @$requestpost->Puesto_buscar;
            $objSolicitud->Descripcion_puesto   = @$requestpost->Descripcion_puesto;
            $objSolicitud->Pais                 = @$requestpost->Pais;
            $objSolicitud->Region               = @$requestpost->Region;
            $objSolicitud->Ciudad               = @$requestpost->Ciudad;
            $objSolicitud->Jornada_laboral      = @$requestpost->Jornada_laboral;
            $objSolicitud->Tipo_contrato        = (!empty(@$requestpost->especifique_Tipo_contrato)) ? @$requestpost->Tipo_contrato.' - '.@$requestpost->especifique_Tipo_contrato : @$requestpost->Tipo_contrato;
            $objSolicitud->Salario              = @$requestpost->Salario;
            $objSolicitud->Vacantes             = @$requestpost->Vacantes;
            $objSolicitud->Jerarquia            = (!empty(@$requestpost->especifique_tipo_contratoJerarquia)) ? @$requestpost->Jerarquia.' - '.@$requestpost->especifique_tipo_contratoJerarquia : @$requestpost->Jerarquia;





            $objSolicitud->Edad_minima          = @$requestpost->Edad_minima;
            $objSolicitud->Edad_maxima          = @$requestpost->Edad_maxima;
            $objSolicitud->Estudios_minimos     = @$requestpost->Estudios_minimos;
            $objSolicitud->Idiomas_requerido    = @$requestpost->Idiomas_requerido;
            $objSolicitud->Nivel                = @$requestpost->Nivel;
            $objSolicitud->Area                 = @$requestpost->Area;
            $objSolicitud->Nivel_cargo          = @$requestpost->Nivel_cargo;
            $objSolicitud->Grado_especializacion= @$requestpost->Grado_especializacion;
            $objSolicitud->Conocimientos_requeridos =(!empty(@$requestpost->especifique_Conocimientos_requeridos)) ? json_encode('Otro - '.@$requestpost->especifique_Conocimientos_requeridos) : json_encode(@$requestpost->Conocimientos_requeridos);           
            $objSolicitud->Conocimientos_deseables =(!empty(@$requestpost->especifique_Conocimientos_deseables)) ? json_encode('Otro - '.@$requestpost->especifique_Conocimientos_deseables) : json_encode(@$requestpost->Conocimientos_deseables);
            $objSolicitud->Disponibilidad_viaje = @$requestpost->Disponibilidad_viaje;
            $objSolicitud->Experiencia          = @$requestpost->Experiencia;
            $objSolicitud->fecha                = local_to_gmt(time()); 
            $objSolicitud->cv_file              = @$requestpost->cv_file;
            $data = $this->recluta_model->addsolicitud($objSolicitud);

            if ($data) {
                echo json_encode($data);
            }else{
                echo "false";
            }
        }else{
            echo "false";

        }
        


    }

    //mensajes
    public function mensajes(){ 
        $idmensaje = intval($_GET["idmensaje"]);

        $data = $this->recluta_model->detalles_mensajes($idmensaje);
        echo json_encode($data);

    }

    public function mensajeajax(){
        $postdata = file_get_contents("php://input");
        $request = json_encode($_POST);
        $requestpost = json_decode($request);
        $objMensaje = new StdClass();
        $response = new StdClass();

        $objMensaje->useridto      = intval($this->session->userdata('id'));
        $objMensaje->idmensaje     = intval(@$requestpost->idmensaje_respuesta);
        $objMensaje->useridfrom    = intval(@$requestpost->useridfrom_respuesta);
        $objMensaje->send_userid   = intval($this->session->userdata('id'));
        $objMensaje->fullmensaje   = @$requestpost->fullmensaje;
        $objMensaje->file          = @$requestpost->file;
        $objMensaje->file_url      = @$requestpost->file;
        $objMensaje->status        = "message--new";
        $objMensaje->timecreate    = local_to_gmt(time()); 

        $response->usuario = $this->session->userdata('nombre')." ".$this->session->userdata('apellido');
        $response->correo_mensaje = $this->session->userdata('correo');
        $response->fullmensaje = $objMensaje->fullmensaje;
        $response->file = $objMensaje->file;
        $response->timecreate = date( "d/m/Y H:i:s", $objMensaje->timecreate);

        $data = $this->recluta_model->addmensaje($objMensaje);

        if ($data) {
           echo json_encode($response);
        }else{
            echo "false";
        }
    }

    public function subir_archivo(){
      
        $new_name = time()."_".$_FILES["file"]['name'];
        $config['file_name'] = $new_name;
        $config['upload_path'] = 'uploads/';
        $config['allowed_types'] = '*';
        $config['max_filename'] = '255';
        $config['max_size'] = '3048'; //3 MB

        if (isset($_FILES['file']['name'])) {
            if (0 < $_FILES['file']['error']) {
                echo 'Error during file upload' . $_FILES['file']['error'];
            } else {
                if (file_exists('uploads/' . $_FILES['file']['name'])) {
                    echo $new_name;
                } else {
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('file')) {
                        echo $this->upload->display_errors();
                    } else {
                        echo $new_name;
                    }
                }
            }
        } else {
            echo 'Please choose a file';
        }
    }

    public function configuracion(){ 

        $view_data["menu"] = false;
        $responsemetas = new StdClass();    
        $metaname = $this->uri->segment(2);
        $response = new StdClass(); 

        if( $metaname == "" or $metaname == "0"){
            $metaname = "home";
        }

        if($this->session->userdata('logueado')){

            $view_data['nombre'] = $this->session->userdata('nombre');
            $view_data['apellido'] = $this->session->userdata('apellido');
            $view_data['foto'] = $this->session->userdata('foto');
            $view_data['correo'] = $this->session->userdata('correo');
            $view_data['rol'] = $this->session->userdata('rol');
            $view_data['logueado'] = $this->session->userdata('logueado');

            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
           
            $objUsuario = new StdClass();

            $iduser = intval($this->session->userdata('id'));
            $objUsuario = $this->usuario_model->get_byIdUser( $iduser );
            $response->user = $objUsuario;
            $response->rows_mensajes = $this->recluta_model->get_Mensajes_recluta($iduser);

            $view_data['mensajes_nuevos'] = intval($this->mensajes_model->get_Count_Mensajes_Candidatos($iduser));

            $response->rows_ultimo_mensaje = $this->recluta_model->ultimom_mensaje($iduser);
           
            $responsemetas->rows = $this->meta_model->get_byNameMeta($metaname);
            $view_data["datauser"] = $objUsuario;  

            $view_data["datacandidatos"] = $response;
            $view_data["data"] = $responsemetas;
            $this->load->view('Head', $view_data);
            $this->load->view('micuenta/candidato/configuracion');
            $this->load->view('Footer', $view_data);

        }else{
            redirect('/login/sign_in/reclutas');

        }

    }



}

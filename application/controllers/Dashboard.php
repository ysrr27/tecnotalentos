<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 /**Ysrrael Sanchez
	  *  26/03/2020		
	  */
	public function __construct(){
    	parent::__construct();
        $this->load->helper('tools_helper');
        $this->load->model('candidato_model');
        $this->load->model('mensajes_model');
    }

    
	 
	public function index()
	{	

		if($this->session->userdata('logueado')){
			$view_data = array();
			$view_data['nombre'] = $this->session->userdata('nombre');
			$view_data['rol'] = $this->session->userdata('rol');
			$view_data["menu"] = true;

			$response = new StdClass();	
			
			$idusario = (int)$this->session->userdata('id');

			$response->rows = $this->candidato_model->get_LastCanditatos();
			$count = $this->candidato_model->get_Counts_Candidatos();
			$count_corregidos = $this->candidato_model->get_Count_Recluta();
			$count_solicitudes = $this->candidato_model->get_Count_solicitudes();
			$last_solicitudes = $this->candidato_model->get_last_solicitudes();
			$count_mensajes = $this->mensajes_model->get_Count_Mensajes($idusario);
			
			$view_data["data"] = $response;
			$view_data["count"] = $count;
			$view_data["count_corregidos"] = $count_corregidos;
			$view_data["count_solicitudes"] = $count_solicitudes;
			$view_data["count_mensajes"] = $count_mensajes;
			$view_data["last_solicitudes"] = $last_solicitudes;

			$this->load->view('Head', $view_data);
			$this->load->view('Dashboard',$view_data);
			$this->load->view('Footer', $view_data);
		}else{
			redirect('/');
		}

	}
	

}

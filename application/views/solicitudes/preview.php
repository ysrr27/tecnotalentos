<!-- page content -->
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="breadcrumb">
          <a>Home</a>
          <a>Solicitud</a>
          Ver
        </div>

        <div class="x_panel">
          <div class="x_title">
            <h2><small><i class="fa fa-mortar-board"></i> SOLICITUD DE <b><?php echo strtoupper($data->nombre)?></b></small></h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <!-- NEW COL START -->
              <?php if (!empty($data->cv_file)) { ?>
                <a href="" target="_blank">
                  <div class="col-md-1 alert alert-success alert-dismissible fade in" role="alert">
                    <img src="<?php echo get_assets_url();?>assets/img/PDF.png" width="30">
                  </div>
                </a>
              <?php } ?>
              <article class="col-sm-12 col-md-12 col-lg-12">
                <fieldset>   
                  <h4><i class="fa fa-user"></i> DATOS PERSONALES</h4> <hr>

                  <div class="form-group row">
                    <div class="col-md-12">
                      <label for="Puesto_buscar">¿Qué perfil buscas?:</label>
                      <span class="view-text"><?php echo $data->Puesto_buscar?></span> 
                    </div>
                  </div>
                  <div class="form-group row">
                    <div class="col-md-12">
                      <label for="Nombre">Descripción <br> <small>Detalle las tareas específicas asignadas al cargo:</small> </label>
                      <span class="view-text"><?php echo $data->Descripcion_puesto?></span> 
                    </div>  

                                   <!--    <div class="col-md-6">
                                        <label for="Correo">Detalle las tareas que debe realizar:</label>
                                        <span class="view-text"><?php echo $data->Detalle_tareas?></span> 
                                      </div>  -->
                                    </div> 

                                    <hr> <h4><i class="fa fa-graduation-cap"></i> INFORMACIÓN REFERENCIAL DEL CARGO</h4> <hr>
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="Empresa">Empresa:</label>
                                        <span class="view-text"></span> 
                                      </div>
                                      <div class="col-md-6">
                                        <label for="Pais">País:</label>
                                        <span class="view-text"><?php echo $data->Pais?></span> 
                                      </div> 
                                    </div>
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="Region">Región:</label>
                                        <span class="view-text"><?php echo $data->Region?></span> 
                                      </div>  
                                      <div class="col-md-6">
                                        <label for="Ciudad">Ciudad:</label>
                                        <span class="view-text"><?php echo $data->Ciudad?></span> 
                                      </div> 
                                    </div> 
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="Jornada_laboral">Jornada laboral:</label>
                                        <span class="view-text"><?php echo $data->Jornada_laboral?></span> 
                                      </div>  
                                      <div class="col-md-6">
                                        <label for="Tipo_contrato">Tipo de contrato:</label>
                                        <span class="view-text"><?php echo $data->Tipo_contrato?></span> 
                                      </div> 
                                    </div> 
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="Salario">Salario (neto mensual):</label>
                                        <span class="view-text"><?php echo $data->Salario?></span> 
                                      </div>  
                                      <div class="col-md-6">
                                        <label for="Tipo_contratacion">Cantidad de vacantes:</label>
                                        <span class="view-text"><?php echo $data->Vacantes?></span> 
                                      </div> 
                                    </div> 

                                    <hr> <h4><i class="fa fa-child"></i> CARACTERÍSTICAS DEL CANDIDATO IDEAL</h4> <hr>
                                    <div class="form-group row">
                                      <div class="col-md-6">
                                        <label for="Jerarquia">Jerarquía:</label>
                                        <span class="view-text"><?php echo $data->Jerarquia?></span> 
                                      </div>
                                      <div class="col-md-6">
                                        <label for="Pais_Nacimiento">Antigüedad en el cargo o cargos similares:</label>
                                        <span class="view-text"><?php echo $data->Edad_minima." ".$data->Edad_maxima?></span> 
                                      </div> 
                                    </div>
                                    <div class="form-group row">
                                      <div class="col-md-12">
                                        <label for="Estudios_minimos">Estudios mínimos:</label>
                                        <span class="view-text"><?php echo $data->Estudios_minimos?></span> 
                                      </div>  
                                    </div>
                                    <div class="form-group row">  
                                      <div class="col-md-6">
                                        <label for="Idiomas_requerido">Idiomas requerido:</label>
                                        <span class="view-text"><?php echo $data->Idiomas_requerido?></span> 
                                      </div>
                                      <div class="col-md-6">
                                        <label for="Direccion_Completa">Nivel:</label>
                                        <span class="view-text"><?php echo $data->Nivel?></span> 
                                      </div>  
                                    </div>
                                    <div class="form-group row">
                                      <div class="col-md-12">
                                        <label for="Destrezas_conocimientos">Conocimientos Requeridos:</label>
                                        <span class="view-text"><?php


                          if (!empty($data->Conocimientos_requeridos)) {

                                          $ct = str_replace('"', '', $data->Conocimientos_requeridos);
                                          $ct2 = str_replace('[',  '', $ct);
                                          $ct3 = str_replace(']',  '', $ct2);
                                          echo $ct3;
                                        }

                                        ?></span> 
                                      </div> 
                                    </div> 
                                    <div class="form-group row">
                                      <div class="col-md-12">
                                        <label for="Destrezas_conocimientos">Conocimientos Deseables:</label>
                                        <span class="view-text"><?php

                                        if (!empty($data->Conocimientos_deseables)) {

                                          $st = str_replace('"', '', $data->Conocimientos_deseables);
                                          $st2 = str_replace('[',  '', $st);
                                          $st3 = str_replace(']',  '', $st2);
                                          echo $st3;
                                        }
                                        ?>
                                      </span> 
                                    </div> 
                                  </div>
                                  <div class="form-group row">
                                    <div class="col-md-3">
                                      <label for="Disponibilidad_viaje">Disponibilidad de viaje:</label>
                                      <span class="view-text"><?php echo $data->Disponibilidad_viaje?></span> 
                                    </div> 
                                  </div>
                                </div>    
                              </fieldset>
                            </article>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <!-- /page content -->


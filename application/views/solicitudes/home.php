
 <style type="text/css">
     .display{
          display: none;
     }
</style>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        <a>Reclutas</a>
                        Solicitudes
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                            <h2><small><i class="fa fa-folder-open"></i> SOLICITUDES</small></h2>
                            <!-- <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <a href="<?php echo get_site_url("reclutas/export/")?>" class="btn btn-primary sw-btn-next" type="submit">
                                        <i class="fa fa-download"></i> Descargar en Excel
                                    </a> 
                                </li>

                            </ul> -->
                    <div class="clearfix"></div>
                  </div>
                  <div id="loading-wrapper">
                        <div id="loading-text">Cargando...</div>
                        <div id="loading-content"></div>
                  </div>
                  <div class="alert alert-info alert-dismissible fade in sw-btn-next" id="msn_exito" role="alert" style="display: none">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
                    </button>
                </div>

                  <div class="x_content">
                  <table id="table_solicitudes" class="row-border" style="width:100%">
                    <thead>
                          <tr>
                              <th>id</th>
                              <th>Acción</th>
                              <th>Recluta</th>
                              <th>Titulo de la solicitud</th>
                              <th>Perfil que busca</th>
                              <th>Estatus</th>
                              <th>Fecha</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                if(is_array($data->rows )){
                  foreach(  $data->rows as $key =>$row){
                            ?>
                          <tr>
                              <td><?php echo  $row->idsolicitud ?></td>
                              <td>
                                <div class="btn-group  btn-group-sm">
                                  <a href="<?php echo get_site_url('solicitudes/preview/'.$row->idsolicitud)?>" class="btn btn-primary" type="button" title=”VER”><i class="fa fa-eye"></i></a>
        
                                  <a href="#myModal" class="btn-solicitud btn btn-default" type="button" data-toggle="modal" data-target="#myModal"  id="<?php echo  $row->idsolicitud ?>"  data-rowId ="<?php echo  $row->id ?>" data-rowrecluta="<?php echo  $row->nombre ?>" ><i class="fa fa-envelope"></i></a>
                                  <button class="btn btn-xs btn-danger btn-action" name="Activar" data-toggle="tooltip" data-placement="right" id="<?php echo  $row->idsolicitud?>"><i class="fa fa-times"></i></button> 
                                </div>
                              </td>
                              <td><?php echo  $row->nombre." ".$row->apellido ?></td>
                              <td><?php echo  $row->Puesto_buscar ?></td>
                              <td><?php echo  $row->Descripcion_puesto ?></td>
                              <td> 
                                <select id="<?php echo $row->idsolicitud?>" name="ranking" class="form-control status_solicitud" style="width: 75%">
                                 <?php echo (!empty($row->estatus) && $row->estatus > '') ? "<option value='".$row->estatus."' selected>".$row->estatus."</option>" : ''; ?>
                                     <option value="Revisado">Revisado</option>
                                     <option value="En proceso">En proceso</option>
                                     <option value="Asignado">Asignado</option>
                                     <option value="Cerrado">Cerrado</option>
                               </select>
                             </td>
                              <td><?php echo date( "d/m/Y", $row->fecha);?></td>
                          </tr>
                          <?php

                        }
                      }                          
                      ?>
                      </tbody>
                      <tfoot>
                          <tr>
                            <td colspan="7"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->

  <!-- Modal -->
  <div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">

            <form action="<?php echo get_site_url("solicitudes/nuevo_mensaje")?>" class="" id="nuevo_mensaje" role="form" data-toggle="validator" name="nuevo_mensaje" method="post" accept-charset="utf-8">
                <input type="hidden" class="form-control" name="idsolicitud_respuesta" id="idsolicitud_respuesta" value="">
                <input type="hidden" class="form-control" name="useridto_respuesta" id="useridto_respuesta" value="">
                <input type="hidden" class="form-control" name="file" id="file" value="">
                <div class="new-mail__top">
                    <div class="new-mail__title">
                        <span id="rpd"></span>

                    </div>
                    <i class="new-mail__close new-mail__toggle fa fa-close"></i>
                </div>

                <div class="new-mail-exp">
                    <div class="new-mail-exp__item">
                        <div class="new-mail-exp__label">Asunto</div>
                        <input placeholder="" type="text" name="subject" class="new-mail-exp__input">
                    </div>
                </div>
                <div class="new-mail__content">
                    <textarea id="fullmensaje" name="fullmensaje" 
                    class="new-mail__message" autofocus></textarea>
                </div>
                <div class="new-mail-foot">
                    <div class="new-mail-foot__insert">
                        <div class="input-group"> 
                            <div class="custom-file">
                                <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                <label class="custom-file-label" for="inputGroupFile01" style="margin-left: 0px;"><i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;position: absolute;left: 0px;top: 0px;"></i>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="new-mail-foot__actions">

                        <button type="button" class="new-mail__toggle preview-foot__button" data-dismiss="modal">Cancelar</button>
                        <button class="button--primary preview-foot__button" id="new-mensage">
                            <i class="fa fa-paper-plane"></i></button>                   
                        </div>
                    </div>
                </form> 
            </div>
        </div>
    </div>
</div>
<div class="example3">
  <nav class="navbar navbar-inverse navbar-static-top scrolled">
    <div class="container">
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar3">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button>
      <H1 id="logo"> 
        <a href="https://revenuemanagementworld.com"> 
            <span class="nspa"></span> 
            <span>Trabajos de</span> 
            <span>Revenue</span> 
            <span>Management</span> 
            <span>by Jaime Chicheri</span>
            <div style="clear:both"></div>
        </a>
    </H1>
</div>
<div id="navbar3" class="navbar-collapse collapse">
    <ul class="nav navbar-nav navbar-right col-marg">
       <li><a href="<?php echo get_site_url("/")?>">INICIO</a></li>
       <li class="color3"><a class="color3" href="<?php echo get_site_url("/candidatos/postulate")?>" style="color:#D8A545">CANDIDATO</a></li>
       <li><a href="<?php echo get_site_url("/reclutas/contacto")?>" style="color: #daa236 !important;">RECLUTADOR</a></li>

       <!--/.nav-collapse -->
   </div>
   <!--/.container-fluid -->
</nav>
</div>
<!--Jumbotron-->
<div id="loading-wrapper"  ng-show="dataloading === 'true'">
    <div id="loading-text">Cargando...</div>
    <div id="loading-content"></div>
</div>
<div>


  <div class="login_wrapper" style="margin-top: -3% !important;">
    <div class="animate form login_form">
      <section class="login_content">
        <br>
        <br>
        <br>    
        <form id="recover" method="post" action="<?php echo base_url() ?>index.php/login/recover" style="margin: 115px 0 !important;">

            <div class="" style="border: 0;">
                <a href="#" class="site_title" style="
                font-size: 68px;
                margin: -58px 43px 30px 5px;
                color: #6a6a6a !important;
                height: 85px;
                "><i class="fa fa-lock" style="
                padding: 7px 23px;
                "></i></a>
            </div>
            <div id="error"></div>    
            <p><b>¿Tienes problemas para iniciar sesión?</b></p>

            <p>Ingresa tu correo electrónico y te enviaremos los pasos para recuperar el acceso a tu cuenta.</p>
            <div>
                <input type="text" name="correo" class="form-control" placeholder="Correo Electronico" required="" />
            </div>
            <div class="col-md-4 col-md-offset-3">
                <input type="submit" class="btn btn-default submit sw-btn-next" value="Enviar datos" />
            </div>

            <div class="clearfix"></div>

            <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <p> Política y Privacidad  © Copyright 2019 Copyright.es - Todos los Derechos Reservados </p>
              </div>
          </div>
      </form>
  </section>
</div>
</div>
</div>




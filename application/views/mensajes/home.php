<!-- page content -->
<div class="right_col" role="main">
  <div class="">

    <div class="clearfix"></div>

    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="breadcrumb">
          <a>Home</a>
          Mensajes
      </div>
      <div class="x_panel">

        <div class="x_content">
            <div class="col-md-1 nav-sm">
                <div class="menu_section active menu-user">
                    <ul id="reclutas_user" class="nav side-menu">
                        <?php 
                        if(is_array($data->reclutas )){
                          foreach($data->reclutas as $key =>$reclutas){

                            $reclutas = (array)$reclutas;
                            if ($key === 0) {
                                ?>
                                <li><a><div class="user-active accounts__item accounts__item--active" id="<?php echo  $reclutas["useridto"] ?>">
                                <?php }else{ ?>  
                                    <li><a><div class="user-active accounts__item" id="<?php echo  $reclutas["useridto"] ?>">  
                                    <?php } ?>    
                                    <?php
                                    if (!isset($reclutas["cantidad"])) {        
                                        $cant_msn = 0;
                                    }else{
                                        $cant_msn = intval($reclutas["cantidad"]);
                                    }

                                    echo (!empty($cant_msn) && $cant_msn > 0) ? '<span class="accounts__pill pill pill--solid">'.$cant_msn.'</span>' : ''; ?>
                                    <?php if (!empty($reclutas["foto"])) { ?>
                                        <img class="accounts__avatar r_avatar" src="<?php echo get_assets_url().$reclutas["foto"];?>" title=''/>
                                    <?php }else{ ?>
                                        <img class="accounts__avatar r_avatar" src="<?php echo get_assets_url();?>assets/img/img.png" title=''/>
                                    <?php } ?>
                                </div> <?php echo  $reclutas["nombre"] ?></span>
                            </a>
                        </li>
                    <?php }} ?>

                </ul>
            </div>
        </div>
        <div class="col-md-11">
            <div class="new">
                <form action="<?php echo get_site_url("reclutas/mensajeajax")?>" class="" id="new_mensaje" role="form" data-toggle="validator" name="new_mensaje" method="post" accept-charset="utf-8">
                    <input type="hidden" class="form-control" name="idmensaje_respuesta" id="idmensaje_respuesta" value="">
                    <input type="hidden" class="form-control" name="useridfrom_respuesta" id="useridfrom_respuesta" value="">
                    <input type="hidden" class="form-control" name="file" id="file" value="">


                    <div class="new-mail">
                        <div class="new-mail__top">
                            <div class="new-mail__title">
                                <span>Responder:</span>

                            </div>
                            <i class="new-mail__close new-mail__toggle fa fa-close"></i>
                        </div>
                        <div class="new-mail-exp">
                        </div>
                        <div class="new-mail__content">
                            <textarea id="fullmensaje" name="fullmensaje" 
                            class="new-mail__message" autofocus></textarea>
                        </div>
                        <div class="new-mail-foot">
                            <div class="new-mail-foot__insert">
                                <div class="input-group"> 
                                    <div class="custom-file">
                                        <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                        <label class="custom-file-label" for="inputGroupFile01"><i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i>
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="new-mail-foot__actions">
                                <button id="btn-cancel" class="new-mail__toggle preview-foot__button">Cancelar</button>
                                <button class="button--primary preview-foot__button" id="send-message">
                                    <i class="fa fa-paper-plane"></i></button>                   
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="mails">
                    <div class="message-list scrollable">
                        <div class="scrollable__target">


                            <?php 
                            if(is_array($data->ultimomensaje )){
                              foreach($data->ultimomensaje as $key =>$row){
                                  if ($key === 0) {
                                    ?>
                                    <div class="message<?php echo $row->idmensaje ?> message message--active <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                    <?php }else{ ?>
                                        <div class="message <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                        <?php } ?>

                                        <div class="message-tags">
                                            <span class="dot dot--green"></span>
                                        </div>
                                        <div class="message__actions">
                                            <i class="message__icon far fa-square"></i>
                                            <i class="message__icon fas fa-trash-alt"></i>
                                            <i class="message__icon fas fa-archive"></i>
                                        </div>
                                        <div class="message__content">
                                            <div class="message__exp">
                                                <div><?php echo  $row->usuario ?></div>
                                                <div class="date"><?php echo  $row->fecha ?></div>
                                            </div>
                                            <div class="message__title">
                                             <?php echo  $row->subject ?>
                                         </div>
                                         <div class="message__expr">
                                            « <?php echo  $row->smallmessage ?> »
                                        </div>
                                    </div>
                                </div>

                            <?php }} ?>
                        </div>
                    </div>
                    <div class="preview">
                     <?php 
                     if(is_array($data->ultimomensaje_detalle)){
                /*        print_r("<pre>");
                        var_dump($data->ultimomensaje_detalle);exit();*/
                      foreach($data->ultimomensaje_detalle as $key =>$row){
                        if ($key === 0) {?>

                            <div class="preview-top" id="<?php echo $row->idmensaje; ?>"  data-rowId = "<?php echo $row->useridfrom; ?>">
                                <div class="preview__title"><?php echo $row->subject; ?></div>
                            </div>
                            <div class="scrollable">
                                <div class="preview-content scrollable__target">
                                <?php } ?>

                                <div class="preview-respond">
                                    <div class="preview-respond__head">
                                        <div class="profile-head">
                                            <div class="profile-head__id">

                                                <?php echo (!empty($row->foto) && $row->foto > '') ? '<img class="profile-head__avatar"
                                                src="'.get_assets_url().$row->foto.'"
                                                alt="">' : '<img class="profile-head__avatar"
                                                src="'.get_assets_url().'assets/img/img.png"
                                                alt="">'; ?>
                                                <div>
                                                    <div class="profile-head__name"><?php echo $row->usuario; ?></div>
                                                    <div class="profile-head__mail"><?php echo $row->correo_mensaje; ?></div>
                                                </div>
                                            </div>
                                            <div class="date"><?php echo date( "d/m/Y H:i:s", $row->timecreate); ?></div>
                                        </div>
                                    </div>
                                    <div class="preview-respond__content">
                                        <p class="paragraph"><?php echo $row->fullmensaje; ?></p>

                                        <?php echo (!empty($row->file) && $row->file > '') ? '<a href="'.get_assets_url().'uploads/'.$row->file.'"><b><i class="fa fa-paperclip" download></i> '.$row->file.' </b></a>' : ''; ?> 
                                    </div>
                                </div>

                                <?php
                            }
                        }                          
                        ?>
                    </div>
                </div>
                <div class="preview-foot">
                    <button class="new-mail__toggle btn preview-foot__button"><i class="fa fa-mail-reply"></i> Responder</button>
                </div>
            </div>
        </div>
    </div>                     
</div>
</div>
</div>
</div>
</div>
</div>
<!-- /page content -->


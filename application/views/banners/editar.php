<!-- page content -->
<div class="right_col" role="main">
  <div class="">
    <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-12 col-xs-12">
        <div class="breadcrumb">
          <a>Home</a>
          Banners
        </div>
        <div class="x_panel">
          <div class="x_title">
            <h2>Datos del banner</h2>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <br />
            <?php echo validation_errors(); ?>
            <form class="form-horizontal form-label-left input_mask" action="<?php echo get_site_url("banners/update") ?>" method="post">
              <input type="hidden" id="idbanner" name="idbanner" value="<?php echo (!empty($data->idbanner) && $data->idbanner > '') ? $data->idbanner : ''; ?>">
              <input type="hidden" id="imagen" name="imagen" value="">
              <?php echo (!empty($data->error) && $data->error > '') ? '<div class="alert alert-danger">' . $data->error . '</div>' : ''; ?>
              <div class="row">
                <div class="col-md-12 col-sm-6 col-xs-12 form-group has-feedback">
                  <div class="col-md-4 col-xs-12">
                    <div class="wrp_rec_center img-banner">
                      <div id="card-photo-empresa" class="usr-img_rec <?php echo (empty($data->rows_perfil)) ? 'card-perfile-empresa' : ''; ?>">
                        <?php
                        $foto = str_replace(" ", "_", $data->imagen);
                        if (!empty($foto)) { ?>
                          <img src="<?php echo get_assets_url() . $foto ?>" alt="" class="img-avatar img-thumbnail">
                        <?php } else { ?>
                          <div class="image-upload">
                            <label for="file-input">
                              <img id="blah2" src="<?php echo get_assets_url(); ?>assets/img/generic2.png" width="100%" alt="" class="img-avatar img-thumbnail">
                            </label>
                            <input id="file-input" type="file" />
                          </div>
                        <?php } ?>
                      </div>
                      <div id="card-edit-photo-empresa" class="user_forms-signup <?php echo (!empty($dataperfil->rows_perfil)) ? 'card-perfile-empresa' : ''; ?>">
                        <div class="alert"></div>
                        <div id='img_contain'>
                          <?php if (!empty($foto)) { ?>
                            <img id="blah2" align='middle' src="<?php echo get_assets_url() . $foto; ?>" alt="Imagen de perfil" title='' class="img-avatar img-thumbnail" />
                          <?php } else { ?>
                            <img align='middle' src="<?php echo get_assets_url(); ?>assets/img/generic2.png" alt="Imagen de perfil" class="img-avatar img-thumbnail" title='' />
                          <?php } ?>
                        </div>
                        <div class="input-group">
                          <div class="custom-file">
                            <input type="file" id="banner_image" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                            <label class="forms_buttons-action cmb-banner" for="banner_image"><i class="fa fa-image"></i></label>
                          </div>
                          <label for="categoria">Cambiar banner</label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-7 col-sm-6 col-xs-12 form-group has-feedback">
                    <input type="text" class="form-control has-feedback-left" name="nombre_banner" id="nombre_banner" placeholder="Nombre del banner" value="<?php echo (!empty($data->nombre_banner) && $data->nombre_banner > '') ? $data->nombre_banner : ''; ?>">
                    <span class="fa fa-image form-control-feedback left" aria-hidden="true"></span>
                  </div>
                </div>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-5">
                  <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button>
                </div>
              </div>

            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- /page content -->
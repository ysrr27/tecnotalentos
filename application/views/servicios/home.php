<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                  <a>Home</a>
                  Servicio
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><i class="fa fa-mortar-board"></i> Servicio</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <a href="<?php echo get_site_url("servicios/editar")?>" class="btn btn-primary sw-btn-next" type="submit">
                                        <i class="fa fa-plus"></i> Crear
                                    </a> 
                                </li>

                            </ul>
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                  <table id="table_banner" class="row-border" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>Titulo</th>
                              <th>Descripción</th>
                              <th>Categoría</th>
                              <th>Imagen</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                if(is_array($data->rows )){
                  foreach(  $data->rows as $key =>$row){
                            ?>
                          <tr>
                              <td>
                                <div class="btn-group  btn-group-sm">
                                  <a href="<?php echo get_site_url('servicios/editar/'.$row->idserv)?>" class="btn btn-primary" type="button"><i class="fa fa-edit"></i></a>
                                  <button class="btn btn-xs btn-danger btn-action" name="Activar" data-toggle="tooltip" data-placement="right" id="<?php echo  $row->idserv?>"><i class="fa fa-times"></i></button> 
                                </div>
                              </td>
                              <td><?php echo  $row->nombre_servicio ?></td>
                              <td><?php echo  $row->desc_servicio ?></td>
                              <td><?php echo  $row->cat_servico ?></td>
                              <td><?php 
                              $resultado = str_replace(" ", "_", $row->imagen);
                              ?>
                              <img src="<?php echo get_assets_url().$resultado;?>" class="img-rounded" alt="Cinque Terre" width="124" height="100"> 
 
                            </td>
                          </tr>
                          <?php

                        }
                      }                          
                      ?>
                      </tbody>
                      <tfoot>
                          <tr>
                            <td colspan="5"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb">
                        <a>Home</a>
                        <a>Reclutas</a>
                        Editar
                   </div>
                <div class="x_panel">
                
                  <div class="x_content">


                    <!-- Smart Wizard -->
                    <form action="<?php echo get_site_url("reclutas/update")?>" id="recluta" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
                            <div id="form-step-0" role="form" data-toggle="validator">
                                 <input type="hidden" class="form-control" name="id_recluta" id="id_recluta" value="<?php echo (!empty($data->id_recluta) && $data->id_recluta > '') ? $data->id_recluta : ''; ?>">
                                <br>
                                <div class="form-group row">
                                    <hr>
                                    <h4 class="text-center"><b>DATOS RECLUTA</b></h4>
                                    <hr>  
                                    <div class="col-md-6">
                                        <label for="Nombre_recluta">Nombre:</label>
                                        <input type="text" class="form-control" name="Nombre_recluta" id="Nombre_recluta" placeholder="" value="<?php echo (!empty($data->Nombre_recluta) && $data->Nombre_recluta > '') ? $data->Nombre_recluta : ''; ?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div>
                                    <div class="col-md-6">
                                        <label for="Apellido_recluta"> Apellido:</label>
                                        <input type="text" class="form-control" name="Apellido_recluta" id="Apellido_recluta" placeholder="" value="<?php echo (!empty($data->Apellido_recluta) && $data->Apellido_recluta > '') ? $data->Apellido_recluta : ''; ?>" required>
                                        <div class="help-block with-errors"></div> 
                                    </div>      
                                </div>

                                <div class="form-group row">
                                    <div class="col-md-6">
                                        <label for="Telefono_recluta">Teléfono:</label>
                                        <input type="text" class="form-control" name="Telefono_recluta" id="Telefono_recluta" placeholder="" value="<?php echo (!empty($data->Telefono_recluta) && $data->Telefono_recluta > '') ? $data->Telefono_recluta : ''; ?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                    <div class="col-md-6">
                                        <label for="Correo_recluta">Dirección de correo electronico:</label>
                                        <input type="text" class="form-control" name="Correo_recluta" id="Correo_recluta" placeholder="" value="<?php echo (!empty($data->Correo_recluta) && $data->Correo_recluta > '') ? $data->Correo_recluta : ''; ?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="Empresa_recluta">Empresa:</label>
                                        <input type="text" class="form-control" name="Empresa_recluta" id="Empresa_recluta" placeholder="" value="<?php echo (!empty($data->Empresa_recluta) && $data->Empresa_recluta > '') ? $data->Empresa_recluta : ''; ?>" required>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                </div>
                                <div class="form-group row">
                                    <div class="col-md-12">
                                        <label for="Mesaje_recluta">Mensaje:</label>
                                        <textarea id="Mesaje_recluta" required="required" class="form-control" name="Mesaje_recluta"><?php echo (!empty($data->Mesaje_recluta) && $data->Mesaje_recluta > '') ? $data->Mesaje_recluta : ''; ?></textarea>
                                        <div class="help-block with-errors"></div>
                                    </div> 
                                </div>
                                
                                    <div class="row">
                                        <div class="form-group col-md-6">
                                            <label for="Ciudad_trabajo">Ciudad en la que se ejecutaría el Trabajo:</label>
                                            <input type="text" class="form-control" name="Ciudad_trabajo" id="Ciudad_trabajo" placeholder="" value="<?php echo (!empty($data->Ciudad_trabajo) && $data->Ciudad_trabajo > '') ? $data->Ciudad_trabajo : ''; ?>" required>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label for="Pais_trabajo">País en el que se ejecutaría el Trabajo:</label>
                                            <input type="text" class="form-control" name="Pais_trabajo" id="Pais_trabajo" placeholder="" value="<?php echo (!empty($data->Pais_trabajo) && $data->Pais_trabajo > '') ? $data->Pais_trabajo : ''; ?>" required>
                                            <div class="help-block with-errors"></div>
                                        </div>  
                                    </div>
                                    <div class="col-md-12">
                                        <fieldset>
                                            <label for="Nombre_empresa">¿Tienes problemas en que comuniquemos el nombre de tu empresa?</label>
                                            <br>
                                            <div class="radiobutton">
                                                <input type="radio" name="Nombre_empresa" id="Si" value="Si" class="solvencia_inmueble" <?php echo (!empty($data->Nombre_empresa) && $data->Nombre_empresa == 'Si') ? 'checked' : ''; ?>  />
                                                <label for="Si">Si</label>
                                            </div>
                                            <div class="radiobutton">
                                                <input type="radio" name="Nombre_empresa" id="No" value="No" class="solvencia_inmueble" <?php echo (!empty($data->Nombre_empresa) && $data->Nombre_empresa == 'No') ? 'checked' : ''; ?>/>
                                                <label for="No">No</label>
                                            </div>
                                        </fieldset>
                                    </div> 
                                <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-4">
                                        <a href="<?php echo get_site_url('reclutas/home/')?>" class="btn btn-primary sw-btn-next"><i class="fa fa-reply"></i> Volver</a>
                                        <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                              </div>
                            </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


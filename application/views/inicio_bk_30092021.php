     <div class="intro-header">
      <div class="bg-overlay">
        <div class="container container2">
            <div class="row">
                <div class="col-lg-12">
                    <div class="intro-message">
                        <h1 class="header-title">FORMA PARTE DEL PRIMER <span class="colorh">HEAD HUNTER</span> ENFOCADO <br> EN EL SECTOR <span class="colorh2">TECNOLÓGICO</span></h1>
                        <ul class="list-inline intro-social-buttons">
                            <li>
                                <a href="<?php echo get_site_url("/login/sign_in/reclutas")?>"class="btn btn-default btn-lg link-btn-rct"> <img src="<?php echo get_assets_url();?>assets/img/Reclutador.png" alt="..." class="img-btn-rct"><br> RECLUTADOR</a>
                            </li>
                            <li>
                                <a href="<?php echo get_site_url("/login/sign_in/candidatos")?>"class="btn btn-default btn-lg link-btn-rct link-btn-rct2"> <img src="<?php echo get_assets_url();?>assets/img/Candidato.png" alt="..." class="img-btn-rct2"><br> CANDIDATO</a>
                            </li>
                        </ul>   
                    </div>
                </div>
            </div>

        </div>
        <!-- /.container -->
    </div>
</div>
<!-- /.intro-header -->



<main role="main" class="site-main">
    <section class="fixed-width">
        <h2>Innovamos para hacer los procesos de selección <br> más rápidos y efectivos</h2>
        <div class="container2">
            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                <div class="landing-icon testimonial-avatar">
                    <img src="<?php echo get_assets_url();?>assets/img/AHORRA_TIEMPO_Y_DINERO.png" alt="" class="">
                </div>
                 <div class="cont-desc">
                    <p class="title-cap">Ahorro de tiempo y dinero</p>
                    <p class="title-cap-desc">Herramientas optimizadas para reducir el tiempo y costo de la selección</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                <div class="landing-icon testimonial-avatar">
                    <img src="<?php echo get_assets_url();?>assets/img/CANDIDATOS_CALIFICADOS.png" alt="">
                </div>
                <div class="cont-desc">
                    <p class="title-cap"> Candidatos calificados</p>
                    <p class="title-cap-desc">Encuentra los candidatos que mejor se adaptan al perfil buscado</p>
                </div>
            </div>
            <div class="col-md-4 col-sm-12 col-xs-12 text-center">
                <div class="landing-icon testimonial-avatar">
                    <img src="<?php echo get_assets_url();?>assets/img/PROCESO_FACIL_Y_RAPIDO.png" alt="">
                </div>
                <div class="cont-desc">
                    <p class="title-cap">Proceso fácil y rápido</p>
                    <p class="title-cap-desc">Puedes gestionar tus procesos de selección más rápido y fácil que nunca</p>
                </div>
            </div>
        </div>     
    </section>

    <section class="fixed-width highlighted-bg">
        <div class="container2">
            <div class="col-md-offset-1 col-md-8">
                <h1 class="text-left title-contacto">Contacto</h1><br><br>
            </div>
            <div class="col-md-offset-2 col-md-9 col-sm-12 col-xs-12">
                <form action="<?php echo get_site_url("contacto/contactajax")?>" class="" id="contacto" role="form" data-toggle="validator" name="contacto" method="post" accept-charset="utf-8">
                    <div id="form-step-0" role="form" data-toggle="validator">
                        <div class="row">
                            <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                <label for="nom_apellido">Nombre y Apellido (requerido)</label>
                                <input type="text" class="form-control" name="nom_apellido" id="nom_apellido" placeholder="" value="" required>
                                <div class="help-block with-errors"></div>
                            </div>    
                        </div>

                        <div class="row">
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <label for="correo">E-mail  (requerido)</label>
                                <input type="email" class="form-control" name="correo" id="correo" placeholder="" value="" required>
                                <div class="help-block with-errors"></div>
                            </div> 
                            <div class="form-group col-md-6 col-sm-12 col-xs-12">
                                <label for="telefono">Teléfono</label>
                                <input type="text" class="form-control" name="telefono" id="telefono" placeholder="" value="" required>
                                <div class="help-block with-errors"></div>
                            </div> 
                        </div>
                        <div class="form-group row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label for="mensaje">Mensaje (requerido)</label>
                                <textarea id="mensaje" required="required" class="form-control" name="mensaje"></textarea>
                                <div class="help-block with-errors"></div>
                            </div> 
                        </div>

                        <div class="form-group row">
                            <div class="checkbox">
                                <input type="checkbox" name="Termino" id="Termino" value="Termino" class="flat Termino" />
                                <label for="Termino" class="Termino"><b>He leído y acepto los Términos y Condiciones de uso.</b></label>
                            </div>
                        </div>
                        <div class="row form-group">
                            <div class="pull-right">
                                <button type="submit" id="enviar_contactanos" class="btn sw-btn-next" name="enviar"> Enviar</button>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
</main>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>
<!-- Modal contacto -->
<div id="message-aler" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal-color">
            <div class="modal-header modal-color">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="menss" class="text-center">¡GRACIAS POR CONTACTARNOS!</h3><br>
                <P class="parr">Pronto nos comuncaremos con usted</P>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- /modals -->

<!DOCTYPE html>
<html class="wide wow-animation" lang="en">

<head>
  <title>Home</title>
  <meta name="format-detection" content="telephone=no">
  <meta name="viewport" content="width=device-width, height=device-height, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta charset="utf-8">
  <link rel="icon" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/favicon.ico" type="image/x-icon">
  <!-- Stylesheets-->

  <style>
    .ie-panel {
      display: none;
      background: #212121;
      padding: 10px 0;
      box-shadow: 3px 3px 5px 0 rgba(0, 0, 0, .3);
      clear: both;
      text-align: center;
      position: relative;
      z-index: 1;
    }

    html.ie-10 .ie-panel,
    html.lt-ie-10 .ie-panel {
      display: block;
    }
  </style>
</head>

<body>
  <!--     <div class="ie-panel"><a href="http://windows.microsoft.com/en-US/internet-explorer/"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/ie8-panel/warning_bar_0000_us.jpg" height="42" width="820" alt="You are using an outdated browser. For a faster, safer browsing experience, upgrade for free today."></a></div>
    

 -->
  <!--    <nav class="main-menu">
       <div>
        <a class="logo" href="http://startific.com">
        </a> 
    </div> 
    <div class="settings"></div>
    <div class="scrollbar" id="style-1">
        <ul>

            <li>                                   
                <a href="http://startific.com">
                    <i class="fa fa-home fa-lg"></i>
                    <span class="nav-text">Home</span>
                </a>
            </li>   

            <li>                                 
                <a href="http://startific.com">
                    <i class="fa fa-user fa-lg"></i>
                    <span class="nav-text">Login</span>
                </a>
            </li>   
            <li>                                 
                <a href="http://startific.com">
                    <i class="fa fa-envelope-o fa-lg"></i>
                    <span class="nav-text">Contact</span>
                </a>
            </li>   
            <li>
                <a href="http://startific.com">
                    <i class="fa fa-heart-o fa-lg"></i>
                    <span class="share"> 
                        <div class="addthis_default_style addthis_32x32_style">
                            <div style="position:absolute;
                            margin-left: 56px;top:3px;"> 
                            <a href="https://www.facebook.com/sharer/sharer.php?u=" target="_blank" class="share-popup"><img src="http://icons.iconarchive.com/icons/danleech/simple/512/facebook-icon.png" width="30px" height="30px"></a>
                            <a href="https://twitter.com/share" target="_blank" class="share-popup"><img src="https://cdn1.iconfinder.com/data/icons/metro-ui-dock-icon-set--icons-by-dakirby/512/Twitter_alt.png" width="30px" height="30px"></a>
                            <a href="https://plusone.google.com/_/+1/confirm?hl=en&url=_URL_&title=_TITLE_
                            " target="_blank" class="share-popup"><img src="http://icons.iconarchive.com/icons/danleech/simple/512/google-plus-icon.png" width="30px" height="30px"></a>   
                        </div>
                        <script type="text/javascript">var addthis_config = {"data_track_addressbar":true};</script>
                        <script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-4ff17589278d8b3a"></script>
                    </span>
                    <span class="twitter"></span>
                    <span class="google"></span>
                    <span class="fb-like">  
                        <iframe src="//www.facebook.com/plugins/like.php?href=http%3A%2F%2Ffacebook.com%2Fstartific&amp;width&amp;layout=button&amp;action=like&amp;show_faces=false&amp;share=false&amp;height=35" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:35px;" allowTransparency="true"></iframe>
                    </span>
                    <span class="nav-text">
                    </span>
                </a>
            </li>
        </li>
        <li class="darkerlishadow">
            <a href="http://startific.com">
                <i class="fa fa-clock-o fa-lg"></i>
                <span class="nav-text">News</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-desktop fa-lg"></i>
                <span class="nav-text">Technology</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-plane fa-lg"></i>
                <span class="nav-text">Travel</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-shopping-cart"></i>
                <span class="nav-text">Shopping</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-microphone fa-lg"></i>
                <span class="nav-text">Film & Music</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-flask fa-lg"></i>
                <span class="nav-text">Web Tools</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-picture-o fa-lg"></i>
                <span class="nav-text">Art & Design</span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-align-left fa-lg"></i>
                <span class="nav-text">Magazines
                </span>
            </a>
        </li>

        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-gamepad fa-lg"></i>
                <span class="nav-text">Games</span>
            </a>
        </li>
        <li class="darkerli">
            <a href="http://startific.com">
                <i class="fa fa-glass fa-lg"></i>
                <span class="nav-text">Life & Style
                </span>
            </a>
        </li>
        <li class="darkerlishadowdown">
            <a href="http://startific.com">
                <i class="fa fa-rocket fa-lg"></i>
                <span class="nav-text">Fun</span>
            </a>
        </li>
    </ul>
    <li>
        <a href="http://startific.com">
            <i class="fa fa-question-circle fa-lg"></i>
            <span class="nav-text">Help</span>
        </a>
    </li>   
    <ul class="logout">
        <li>
         <a href="http://startific.com">
           <i class="fa fa-lightbulb-o fa-lg"></i>
           <span class="nav-text">
            BLOG 
        </span>

    </a>
</li>  
</ul>
</nav> -->


  <div class="preloader">
    <div class="preloader-body">
      <div class="cssload-container"><span></span><span></span><span></span><span></span>
      </div>
    </div>
  </div>
  <div class="page">
    <div id="home">

      <!-- Page Header-->
      <header class="section page-header">
        <!-- RD Navbar-->
        <div class="rd-navbar-wrap">
          <nav class="rd-navbar rd-navbar-classic" data-layout="rd-navbar-fixed" data-sm-layout="rd-navbar-fixed" data-md-layout="rd-navbar-fixed" data-md-device-layout="rd-navbar-fixed" data-lg-layout="rd-navbar-static" data-lg-device-layout="rd-navbar-fixed" data-xl-layout="rd-navbar-static" data-xl-device-layout="rd-navbar-static" data-xxl-layout="rd-navbar-static" data-xxl-device-layout="rd-navbar-static" data-lg-stick-up-offset="46px" data-xl-stick-up-offset="46px" data-xxl-stick-up-offset="46px" data-lg-stick-up="true" data-xl-stick-up="true" data-xxl-stick-up="true">
            <div class="rd-navbar-main-outer">
              <div class="rd-navbar-main">
                <!-- RD Navbar Panel-->
                <div class="rd-navbar-panel">
                  <!-- RD Navbar Toggle-->
                  <button class="rd-navbar-toggle" data-rd-navbar-toggle=".rd-navbar-nav-wrap"><span></span></button>
                  <!-- RD Navbar Brand-->
                  <div class="rd-navbar-brand"><a class="brand" href="index.html"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/logo_n_final.jpg" alt="" width="223" height="50" /></a></div>
                </div>
                <div class="rd-navbar-main-element">
                  <div class="rd-navbar-nav-wrap">
                    <!-- RD Navbar Share-->
                    <div class="rd-navbar-share fl-bigmug-line-share27" data-rd-navbar-toggle=".rd-navbar-share-list">
                      <ul class="list-inline rd-navbar-share-list">
                        <li class="rd-navbar-share-list-item"><a class="icon fa fa-facebook" href="#"></a></li>
                        <li class="rd-navbar-share-list-item"><a class="icon fa fa-twitter" href="#"></a></li>
                        <li class="rd-navbar-share-list-item"><a class="icon fa fa-google-plus" href="#"></a></li>
                        <li class="rd-navbar-share-list-item"><a class="icon fa fa-instagram" href="#"></a></li>
                      </ul>
                    </div>
                    <ul class="rd-navbar-nav">
                      <li class="rd-nav-item active"><a class="rd-nav-link" href="#home">Home</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#services">VIP</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#projects">Servicios</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#team">Patrocinantes</a></li>
                      <li class="rd-nav-item"><a class="rd-nav-link" href="#contacts">Contacts</a></li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
      <!-- Swiper-->
      <section class="section swiper-container swiper-slider swiper-slider-classic" data-loop="true" data-autoplay="4859" data-simulate-touch="true" data-direction="vertical" data-nav="false">
        <div class="swiper-wrapper text-center">


          <?php

          if (is_array($dataview->banners)) {
            foreach ($dataview->banners as $key => $row) {
              $resultado = str_replace(" ", "_", $row->imagen);
          ?>
              <div class="swiper-slide" data-slide-bg="<?php echo get_assets_url() . $resultado; ?>">
                <div class="swiper-slide-caption section-md">
                  <div class="container">
                    <!--  <h1 data-caption-animate="fadeInLeft" data-caption-delay="0">Mobile App Development</h1>
        <p class="text-width-large" data-caption-animate="fadeInRight" data-caption-delay="100">Since our establishment, we have been delivering high-quality and sustainable software solutions for corporate purposes of worldwide businesses.</p><a class="button button-primary button-ujarak" href="#modalCta" data-toggle="modal" data-caption-animate="fadeInUp" data-caption-delay="200">Get in touch</a> -->
                  </div>
                </div>
              </div>
          <?php
            }
          }
          ?> </div>
        <!-- Swiper Pagination-->
        <div class="swiper-pagination__module">
          <div class="swiper-pagination__fraction"><span class="swiper-pagination__fraction-index">00</span><span class="swiper-pagination__fraction-divider">/</span><span class="swiper-pagination__fraction-count">00</span></div>
          <div class="swiper-pagination__divider"></div>
          <div class="swiper-pagination"></div>
        </div>
      </section>

      <!-- See all services-->
      <!--  <section class="text-center" id="services" style="background: linear-gradient(0deg, rgba(10,20,32,1) 0%, rgba(26,34,57,1) 35%, rgba(10,20,32,1) 100%);">
       -->
      <section class="text-center" id="services">
        <div class="intro-header">
          <div class="bg-overlay">
            <div class="container container2">
              <div class="row">
                <div class="col-lg-12">

                  <h2 class="wow fadeInLeft title-vip"><i class="fa fa-star"></i><i class="fa fa-star"></i>ZONA VIP<i class="fa fa-star"></i><i class="fa fa-star"></i></h2>
                  <div style="overflow: hidden">

                    <div class="slider-video">
                      <div class="slider-video-aspect">
                        <div class="slider-video-scroll">

                          <div class="slider-video-item">
                            <div class="slider-video-pic" style="background-image: url('https://img.youtube.com/vi/DblEwHkde8U/mqdefault.jpg')">
                              <div class="slider-video-caption">
                                Batman
                              </div>
                            </div>
                            <div class="slider-video-play">
                              <button class="slider-video-close">&times;</button>
                              <iframe width="420" height="315" src="https://www.youtube.com/embed/DblEwHkde8U?enablejsapi=1&rel=0">
                              </iframe>
                            </div>
                          </div>

                          <div class="slider-video-item">
                            <div class="slider-video-pic" style="background-image: url('https://img.youtube.com/vi/zSWdZVtXT7E/mqdefault.jpg')">
                              <div class="slider-video-caption">
                                Interstellar
                              </div>
                            </div>
                            <div class="slider-video-play">
                              <button class="slider-video-close">&times;</button>
                              <iframe width="420" height="315" src="https://www.youtube.com/embed/zSWdZVtXT7E?enablejsapi=1">
                              </iframe>
                            </div>
                          </div>


                          <div class="slider-video-item">
                            <div class="slider-video-pic" style="background-image: url('https://img.youtube.com/vi/1Q8fG0TtVAY/mqdefault.jpg')">
                              <div class="slider-video-caption">
                                Wonder Women
                              </div>

                            </div>
                            <div class="slider-video-play">
                              <button class="slider-video-close">&times;</button>
                              <iframe width="420" height="315" src="https://www.youtube.com/embed/1Q8fG0TtVAY?enablejsapi=1&rel=0">
                              </iframe>
                            </div>
                          </div>
                          <div class="slider-video-item">
                            <div class="slider-video-pic" style="background-image: url('https://img.youtube.com/vi/kl8F-8tR8to/mqdefault.jpg')">
                              <div class="slider-video-caption">
                                Kingsman
                              </div>
                            </div>
                            <div class="slider-video-play">
                              <button class="slider-video-close">&times;</button>
                              <iframe width="420" height="315" src="https://www.youtube.com/embed/kl8F-8tR8to?enablejsapi=1&rel=0">
                              </iframe>
                            </div>
                          </div>
                        </div>
                      </div>
                      <button class="slider-video-left"></button>
                      <button class="slider-video-right"></button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

      <!-- Cta-->
      <section class="section section-fluid bg-default">
        <div class="parallax-container" data-parallax-img="<?php echo get_assets_url(); ?>assets/theme_chevere/images/parallax-1.jpg">
          <div class="parallax-content section-xl context-dark bg-overlay-68 bg-mobile-overlay">
            <div class="container">
              <div class="row row-30 justify-content-end text-right">
                <div class="col-sm-7">
                  <h3 class="wow fadeInLeft">Publica con nosotros!</h3>
                  <p>Do you need a unique software solution for your company? We know how to help you!</p>
                  <div class="group-sm group-middle group justify-content-end"><a class="button button-primary button-ujarak" href="#modalCta" data-toggle="modal">Get in Touch</a><a class="button button-white-outline button-ujarak" href="#">Learn More</a></div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>




    </div>

    <!-- Latest Projects-->
    <section class="section section-sm section-fluid bg-default text-center" id="projects">
      <div class="container-fluid">
        <h2 class="wow fadeInLeft">Servicios</h2>
        <!--  <p class="quote-jean wow fadeInRight" data-wow-delay=".1s">In our portfolio, you can browse the latest products developed for our clients for different corporate purposes. Our qualified team of interface designers and software developers is always ready to create something unique for you.</p> -->
        <div class="isotope-filters isotope-filters-horizontal">
          <button class="isotope-filters-toggle button button-md button-icon button-icon-right button-default-outline button-wapasha" data-custom-toggle="#isotope-3" data-custom-toggle-hide-on-blur="true" data-custom-toggle-disable-on-blur="true"><span class="icon fa fa-caret-down"></span>Filter</button>
          <ul class="isotope-filters-list" id="isotope-3">
            <li><a class="active" href="#" data-isotope-filter="*" data-isotope-group="gallery">All</a></li>
            <li><a href="#" data-isotope-filter="Type 1" data-isotope-group="gallery">mobile Apps</a></li>
            <li><a href="#" data-isotope-filter="Type 2" data-isotope-group="gallery">Custom Software</a></li>
            <li><a href="#" data-isotope-filter="Type 3" data-isotope-group="gallery">QA & Testing</a></li>
            <li><a href="#" data-isotope-filter="Type 4" data-isotope-group="gallery">UX and Design</a></li>
          </ul>
        </div>
        <div class="row row-30 isotope" data-isotope-layout="fitRows" data-isotope-group="gallery" data-lightgallery="group">
          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 4">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-1-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-1-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-1-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">FinStep</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 1" data-wow-delay=".1s">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-2-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-2-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-2-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">Mobile Finance</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 2" data-wow-delay=".2s">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-3-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-3-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-3-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">Q-Manage</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInRight" data-filter="Type 3" data-wow-delay=".3s">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-4-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-4-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-4-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">WeatherCast</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>
          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 3">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-5-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-5-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-5-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">Home Calendar</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>

          <div class="col-sm-6 col-lg-4 col-xxl-3 isotope-item wow fadeInLeft" data-filter="Type 3" data-wow-delay=".3s">
            <!-- Thumbnail Classic-->
            <article class="thumbnail thumbnail-classic thumbnail-md">
              <div class="thumbnail-classic-figure"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-8-420x350.jpg" alt="" width="420" height="350" />
              </div>
              <div class="thumbnail-classic-caption">
                <div class="thumbnail-classic-title-wrap"><a class="icon fl-bigmug-line-zoom60" href="<?php echo get_assets_url(); ?>assets/theme_chevere/images/grid-gallery-8-1200x800-original.jpg" data-lightgallery="item"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/fullwidth-gallery-8-420x350.jpg" alt="" width="420" height="350" /></a>
                  <h5 class="thumbnail-classic-title"><a href="#">WiseMoney</a></h5>
                </div>
                <p class="thumbnail-classic-text">We work hard on every app to deliver top-notch features with great UI that you won’t find anywhere else.</p>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>

    <!-- Years of experience-->
    <section class="section section-sm bg-default">
      <div class="container">
        <div class="row row-30 row-xl-24 justify-content-center align-items-center align-items-lg-start text-left">
          <div class="col-md-6 col-lg-5 col-xl-4 text-center"><a class="text-img" href="#">
              <div id="particles-js"></div><span class="counter">5</span>
            </a></div>
          <div class="col-sm-8 col-md-6 col-lg-5 col-xl-4">
            <div class="text-width-extra-small offset-top-lg-24 wow fadeInUp">
              <h3 class="title-decoration-lines-left">Years of Experience</h3>
              <p class="text-gray-500">RatherApp is a team of highly experienced app designers and developers creating unique software for you.</p><a class="button button-secondary button-pipaluk" href="#">Get in touch</a>
            </div>
          </div>
          <div class="col-sm-10 col-md-8 col-lg-6 col-xl-4 wow fadeInRight" data-wow-delay=".1s">
            <div class="row justify-content-center border-2-column offset-top-xl-26">
              <div class="col-9 col-sm-6">
                <div class="counter-amy">
                  <div class="counter-amy-number"><span class="counter">2</span><span class="symbol">k</span>
                  </div>
                  <h6 class="counter-amy-title">apps developed</h6>
                </div>
              </div>
              <div class="col-9 col-sm-6">
                <div class="counter-amy">
                  <div class="counter-amy-number"><span class="counter">40</span>
                  </div>
                  <h6 class="counter-amy-title">Consultants</h6>
                </div>
              </div>
              <div class="col-9 col-sm-6">
                <div class="counter-amy">
                  <div class="counter-amy-number"><span class="counter">12</span>
                  </div>
                  <h6 class="counter-amy-title">Awards won</h6>
                </div>
              </div>
              <div class="col-9 col-sm-6">
                <div class="counter-amy">
                  <div class="counter-amy-number"><span class="counter">160</span>
                  </div>
                  <h6 class="counter-amy-title">Employees</h6>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 col-xl-12 align-self-center">
            <div class="row row-30 justify-content-center">
              <div class="col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft"><a class="clients-classic" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/clients-9-270x117.png" alt="" width="270" height="117" /></a></div>
              <div class="col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".1s"><a class="clients-classic" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/clients-10-270x117.png" alt="" width="270" height="117" /></a></div>
              <div class="col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".2s"><a class="clients-classic" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/clients-3-270x117.png" alt="" width="270" height="117" /></a></div>
              <div class="col-sm-6 col-md-5 col-lg-6 col-xl-3 wow fadeInLeft" data-wow-delay=".3s"><a class="clients-classic" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/clients-11-270x117.png" alt="" width="270" height="117" /></a></div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <!-- Meet The Team-->
    <section class="section section-sm section-fluid bg-default" id="team">
      <div class="container-fluid">
        <h2>Patrocinantes</h2>
        <div class="row row-sm row-30 justify-content-center">
          <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight">
            <!-- Team Classic-->
            <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/team-11-420x424.jpg" alt="" width="420" height="424" /></a>
              <div class="team-classic-caption">
                <h4 class="team-classic-name"><a href="#">Ryan Wilson</a></h4>
                <p class="team-classic-status">Director of Production</p>
              </div>
            </article>
          </div>
          <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight" data-wow-delay=".1s">
            <!-- Team Classic-->
            <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/team-12-420x424.jpg" alt="" width="420" height="424" /></a>
              <div class="team-classic-caption">
                <h4 class="team-classic-name"><a href="#">Jill Peterson</a></h4>
                <p class="team-classic-status">UI Designer</p>
              </div>
            </article>
          </div>
          <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight" data-wow-delay=".2s">
            <!-- Team Classic-->
            <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/team-13-420x424.jpg" alt="" width="420" height="424" /></a>
              <div class="team-classic-caption">
                <h4 class="team-classic-name"><a href="#">Sam Robinson</a></h4>
                <p class="team-classic-status">Senior Developer</p>
              </div>
            </article>
          </div>
          <div class="col-md-6 col-lg-5 col-xl-3 wow fadeInRight" data-wow-delay=".3s">
            <!-- Team Classic-->
            <article class="team-classic team-classic-lg"><a class="team-classic-figure" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/team-14-420x424.jpg" alt="" width="420" height="424" /></a>
              <div class="team-classic-caption">
                <h4 class="team-classic-name"><a href="#">Mary Lee</a></h4>
                <p class="team-classic-status">Software Developer</p>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section>

    <!-- You dream — we embody-->
    <section class="section section-sm bg-default text-md-left">
      <div class="container">
        <div class="row row-50 align-items-center justify-content-center justify-content-xl-between">
          <div class="col-lg-6 col-xl-5 wow fadeInLeft">
            <h2>Get More With Us</h2>
            <!-- Bootstrap tabs-->
            <div class="tabs-custom tabs-horizontal tabs-line tabs-line-big text-center text-md-left" id="tabs-6">
              <!-- Nav tabs-->
              <ul class="nav nav-tabs">
                <li class="nav-item" role="presentation"><a class="nav-link nav-link-big active" href="#tabs-6-1" data-toggle="tab">01</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link nav-link-big" href="#tabs-6-2" data-toggle="tab">02</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link nav-link-big" href="#tabs-6-3" data-toggle="tab">03</a></li>
                <li class="nav-item" role="presentation"><a class="nav-link nav-link-big" href="#tabs-6-4" data-toggle="tab">04</a></li>
              </ul>
              <!-- Tab panes-->
              <div class="tab-content">
                <div class="tab-pane fade show active" id="tabs-6-1">
                  <h5 class="font-weight-normal">FREE APPS</h5>
                  <p>We regularly upload new free apps to our website, which is fully accessible to our clients and subscribers. You can also find out about free apps in our blog.</p>
                  <div class="group-sm group-middle"><a class="button button-secondary button-pipaluk" href="#modalCta" data-toggle="modal">Get in touch</a><a class="button button-default-outline button-wapasha" href="#">Learn More</a></div>
                </div>
                <div class="tab-pane fade" id="tabs-6-2">
                  <h5 class="font-weight-normal">GET SOCIAL</h5>
                  <p>Every app we develop has built-in social support that allows you to stay connected to your accounts on Facebook, Instagram, Twitter and other networks.</p>
                  <div class="group-sm group-middle"><a class="button button-secondary button-pipaluk" href="#modalCta" data-toggle="modal">Get in touch</a><a class="button button-default-outline button-wapasha" href="#">Learn More</a></div>
                </div>
                <div class="tab-pane fade" id="tabs-6-3">
                  <h5 class="font-weight-normal">CUSTOMER SERVICE</h5>
                  <p>Every customer of RatherApp can get access to our friendly and qualified 24/7 support via chat or phone. Fell free to ask us any question!</p>
                  <div class="group-sm group-middle"><a class="button button-secondary button-pipaluk" href="#modalCta" data-toggle="modal">Get in touch</a><a class="button button-default-outline button-wapasha" href="#">Learn More</a></div>
                </div>
                <div class="tab-pane fade" id="tabs-6-4">
                  <h5 class="font-weight-normal">GREAT USABILITY</h5>
                  <p>All our apps are designed to have great usability in order to easily operate our applications. That is why our software has high ratings and lots of awards.</p>
                  <div class="group-sm group-middle"><a class="button button-secondary button-pipaluk" href="#modalCta" data-toggle="modal">Get in touch</a><a class="button button-default-outline button-wapasha" href="#">Learn More</a></div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-lg-6 text-center wow fadeInUp" data-wow-delay=".1s">
            <div class="owl-carousel owl-style-1" data-items="2" data-stage-padding="0" data-loop="true" data-margin="0" data-mouse-drag="true" data-autoplay="true"><a class="box-device" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/index-4-313x580.png" alt="" width="313" height="580" /></a><a class="box-device" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/index-5-313x580.png" alt="" width="313" height="580" /></a><a class="box-device" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/index-4-313x580.png" alt="" width="313" height="580" /></a><a class="box-device" href="#"><img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/index-5-313x580.png" alt="" width="313" height="580" /></a></div>
          </div>
        </div>
      </div>
    </section>

    <!-- Contact information-->
   <!--  <section class="section section-sm bg-default">
      <div class="container">
        <div class="row row-30 justify-content-center">
          <div class="col-sm-8 col-md-6 col-lg-4">
            <article class="box-contacts">
              <div class="box-contacts-body">
                <div class="box-contacts-icon fl-bigmug-line-cellphone55"></div>
                <div class="box-contacts-decor"></div>
                <p class="box-contacts-link"><a href="tel:#">+1 323-913-4688</a></p>
                <p class="box-contacts-link"><a href="tel:#">+1 323-888-4554</a></p>
              </div>
            </article>
          </div>
          <div class="col-sm-8 col-md-6 col-lg-4">
            <article class="box-contacts">
              <div class="box-contacts-body">
                <div class="box-contacts-icon fl-bigmug-line-up104"></div>
                <div class="box-contacts-decor"></div>
                <p class="box-contacts-link"><a href="#">4730 Crystal Springs Dr, Los Angeles, CA 90027</a></p>
              </div>
            </article>
          </div>
          <div class="col-sm-8 col-md-6 col-lg-4">
            <article class="box-contacts">
              <div class="box-contacts-body">
                <div class="box-contacts-icon fl-bigmug-line-chat55"></div>
                <div class="box-contacts-decor"></div>
                <p class="box-contacts-link"><a href="mailto:#">mail@demolink.org</a></p>
                <p class="box-contacts-link"><a href="mailto:#">info@demolink.org</a></p>
              </div>
            </article>
          </div>
        </div>
      </div>
    </section> -->

    <!-- Contact Form-->
    <section class="section section-sm section-last bg-default text-left" id="contacts">
      <div class="container">
        <article class="title-classic">
          <div class="title-classic-title">
            <h3>Contactanos</h3>
          </div>
        </article>
        <form class="rd-form rd-form-variant-2 rd-mailform" data-form-output="form-output-global" data-form-type="contact" method="post" action="bat/rd-mailform.php">
          <div class="row row-14 gutters-14">
            <div class="col-md-4">
              <div class="form-wrap">
                <input class="form-input" id="contact-your-name-2" type="text" name="name" data-constraints="@Required">
                <label class="form-label" for="contact-your-name-2">Your Name</label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-wrap">
                <input class="form-input" id="contact-email-2" type="email" name="email" data-constraints="@Email @Required">
                <label class="form-label" for="contact-email-2">E-mail</label>
              </div>
            </div>
            <div class="col-md-4">
              <div class="form-wrap">
                <input class="form-input" id="contact-phone-2" type="text" name="phone" data-constraints="@Numeric">
                <label class="form-label" for="contact-phone-2">Phone</label>
              </div>
            </div>
            <div class="col-12">
              <div class="form-wrap">
                <label class="form-label" for="contact-message-2">Message</label>
                <textarea class="form-input textarea-lg" id="contact-message-2" name="message" data-constraints="@Required"></textarea>
              </div>
            </div>
          </div>
          <button class="button button-primary button-pipaluk" type="submit">Send Message</button>
        </form>
      </div>
    </section>
    <a href="https://api.whatsapp.com/send?phone=519621234567&text=Hola mi nombre es... " class="whatsapp-button" target="_blank" style="position: fixed;  right: 15px; bottom: 105px;">
      <img src="<?php echo get_assets_url(); ?>assets/theme_chevere/images/whatsapp-button.png" alt="whatsapp">
    </a>

    <!-- Page Footer-->
    <footer class="section section-fluid footer-minimal context-dark">
      <div class="bg-gray-15">
        <div class="container-fluid">
<!--           <div class="footer-minimal-inset oh">
            <ul class="footer-list-category-2">
              <li><a href="#">UI Design</a></li>
              <li><a href="#">Windows/Mac OS Apps</a></li>
              <li><a href="#">Android/iOS Apps</a></li>
              <li><a href="#">Cloud Solutions</a></li>
              <li><a href="#">Customer Support</a></li>
            </ul>
          </div> -->
          <div class="footer-minimal-bottom-panel text-sm-left">
            <div class="row row-10 align-items-md-center">
              <div class="col-sm-6 col-md-4 text-sm-right text-md-center">
                <div>
                  <ul class="list-inline list-inline-sm footer-social-list-2">
                    <li><a class="icon fa fa-facebook" href="#"></a></li>
                    <li><a class="icon fa fa-twitter" href="#"></a></li>
                    <li><a class="icon fa fa-google-plus" href="#"></a></li>
                    <li><a class="icon fa fa-instagram" href="#"></a></li>
                  </ul>
                </div>
              </div>
              <div class="col-sm-6 col-md-4 order-sm-first">
                <!-- Rights-->
                <p class="rights"><span>&copy;&nbsp;</span><span class="copyright-year"></span> <span>RatherApp</span>
                </p>
              </div>
              <div class="col-sm-6 col-md-4 text-md-right"><span>All rights reserved.</span> <span>Design&nbsp;by&nbsp;<a href="https://www.templatemonster.com">TemplateMonster</a></span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>

    <div class="modal fade" id="modalCta" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h4>Contact Us</h4>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
          </div>
          <div class="modal-body">
            <form class="rd-form rd-form-variant-2 rd-mailform" data-form-output="form-output-global" data-form-type="contact-modal" method="post" action="bat/rd-mailform.php">
              <div class="row row-14 gutters-14">
                <div class="col-12">
                  <div class="form-wrap">
                    <input class="form-input" id="contact-modal-name" type="text" name="name" data-constraints="@Required">
                    <label class="form-label" for="contact-modal-name">Your Name</label>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-wrap">
                    <input class="form-input" id="contact-modal-email" type="email" name="email" data-constraints="@Email @Required">
                    <label class="form-label" for="contact-modal-email">E-mail</label>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-wrap">
                    <input class="form-input" id="contact-modal-phone" type="text" name="phone" data-constraints="@Numeric">
                    <label class="form-label" for="contact-modal-phone">Phone</label>
                  </div>
                </div>
                <div class="col-12">
                  <div class="form-wrap">
                    <label class="form-label" for="contact-modal-message">Message</label>
                    <textarea class="form-input textarea-lg" id="contact-modal-message" name="message" data-constraints="@Required"></textarea>
                  </div>
                </div>
              </div>
              <button class="button button-primary button-pipaluk" type="submit">Send Message</button>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- Global Mailform Output-->
  <div class="snackbars" id="form-output-global"></div>
  <!-- Javascript-->

</body>

</html>
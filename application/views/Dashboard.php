   <!-- page content -->
     <div class="right_col" role="main">
          <div class="">
            <div class="row top_tiles">
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-mortar-board"></i></div>
                  <div class="count"><?php echo $count[0]->num; ?></div>
                  <h3>Candidatos</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-users"></i></div>
                  <div class="count"><?php echo $count_corregidos[0]->num; ?></div>
                  <h3>Reclutas</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-folder-open"></i></div>
                  <div class="count"><?php echo $count_solicitudes[0]->num; ?></div>
                  <h3>Solicitudes</h3>
                </div>
              </div>
              <div class="animated flipInY col-lg-3 col-md-3 col-sm-3 col-xs-12">
                <div class="tile-stats">
                  <div class="icon"><i class="fa fa-envelope"></i></div>
                  <div class="count"><?php echo $count_mensajes; ?></div>
                  <h3>Mensajes</h3>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ultimos Candidatos </h2>
                    <div class="clearfix"></div>
                  </div>
                     <div class="x_content">
                  <?php
                        if(is_array($data->rows )){
                                foreach($data->rows as $key =>$row){ ?>
                                    <article class="media event">
                                        <a class="pull-left date">
                                          <p class="month"><?php
                                          $mydate = $row->fecha;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $month = date("M", strtotime($resultado));
                                          echo $month;
                                           ?></p>
                                          <p class="day"><?php
                                          $mydate = $row->fecha;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $day = date("d", strtotime($resultado));
                                          echo $day;
                                           ?></p>
                                        </a>
                                        <div class="media-body">
                                          <a class="title" href="<?php echo get_site_url('candidatos/preview/'.$row->idcandidato)?>" style="color: #6A6A6A;"><?php echo $row->Nombre.' '.$row->Apellido?></a>
                                          <p class="text-left"><?php echo $row->Correo ?></p>
                                        </div>
                                      </article>
                                            <?php
                                        }
                            }
                    ?>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="x_panel">
                  <div class="x_title">
                    <h2>Ultimas Solicitudes </h2>
                    <div class="clearfix"></div>
                  </div>
                     <div class="x_content">
                  <?php
                        if(is_array($last_solicitudes)){
                                foreach($last_solicitudes as $key =>$row){ ?>

                                    <article class="media event">
                                        <a class="pull-left date">
                                          <p class="month"><?php
                                          $mydate = $row->fecha;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $month = date("M", strtotime($resultado));
                                          echo $month;
                                           ?></p>
                                          <p class="day"><?php
                                          $mydate = $row->fecha;
                                          $resultado = str_replace("/", "-", $mydate);
                                          $day = date("d", strtotime($resultado));
                                          echo $day;
                                           ?></p>
                                        </a>
                                        <div class="media-body">
                                          <a class="title" href="<?php echo get_site_url('candidatos/preview/'.$row->idsolicitud)?>" style="color: #6A6A6A;"><?php echo $row->Puesto_buscar?></a>
                                          <p class="text-left"><?php echo $row->Descripcion_puesto ?></p>
                                        </div>
                                      </article>
                                            <?php
                                        }
                            }
                    ?>
                  </div>
                </div>
              </div>
            </div>
           </div>
          </div>
        <!-- /page content -->
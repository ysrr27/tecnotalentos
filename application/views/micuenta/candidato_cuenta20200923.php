<br><br><br>
<div class="my-goomo-shimmer">
    <div class="mg-user-header">
        <div class="wrp">
            <div class="usr-img gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar">
                <?php }else{ ?>

                    <div class="image-upload">

                      <label for="">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar ">
                    </label>

                    <input id="file-input" type="file"/>
                </div>
                <?php } ?>
            </div>

            <div  id="h1first2" class="mg-dtls">
                <div  class="mg-ttl">
                    <h4><?php echo $nombre." ".$apellido ?></h4>
                </div>
                <div class="mg-ttl correo">


                    <h4><i><?php echo $correo ?></i></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wrap">
        <div class="mg-shim-main">

            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-th-large"></i> Resumen</a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-newspaper-o"></i> Mi curriculum</a>
                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab"> 
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                       
                                                <!-- start recent activity -->
                                                <ul class="messages">
                                                    <li>
                                    

                                                        <div class="message_wrapper">
                                                            <h4 class="heading">Estatus de tu curriculum</h4>
                                                            <div class="emojis">
                                                               <?php echo $candidato->icono?>
                                                            </div>
                                                            <br>
                                                            <br>
                                                            <blockquote class="message">
                                                                <?php echo $candidato->descripcion?></blockquote>
                                                            <br />
                                                            <p class="url">
                                                                <span class="fs1 text-info" aria-hidden="true" data-icon=""></span>
                                                            </p>
                                                        </div>
                                                    </li>

                                                </ul>
                                                <!-- end recent activity -->
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-12 profile_left">
                                                <div class="history-tl-container">
                                                    <ul class="tl">
                                                        <li class="tl-item" ng-repeat="item in retailer_history">
                                                            <div class="timestamp">
                                                                <b> Imagen profesional </b>
                                                            </div>
                                                            <div class="item-detail">Preséntate con una foto de perfil que transmita tu profesionalidad</div>
                                                        </li>
                                                        <li class="tl-item" ng-repeat="item in retailer_history">
                                                            <div class="timestamp">
                                                                <b>Currículum al día</b>
                                                            </div>
                                                            <div class="item-detail">Actualiza tu CV regularmente y revisa todos los detalles</div>
                                                        </li>
                                                        <li class="tl-item" ng-repeat="item in retailer_history">
                                                            <div class="timestamp">
                                                                <b>Tus puntos fuertes</b>
                                                            </div>
                                                            <div class="item-detail">Agrega tus fortalezas y virtudes</div>
                                                        </li>
                                                        <li class="tl-item" ng-repeat="item in retailer_history">
                                                            <div class="timestamp">
                                                                <b>Destino...</b>
                                                            </div>
                                                            <div class="item-detail">¡El trabajo de tus sueños!</div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
        <div class="col-md-12 col-sm-12 col-xs-12">


            <!--CTA1 START-->
            <div  id="step-1" class="cta-1">
                <div class="container container2">
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10 text-center">
                            <H4 class="h1first2 titulo-candidato" >
                                ¡Completa tu CV y te ayudaremos a encontrar el trabajo de tus sueños!
                            </H4>
                        </div>
                    </div>
                </div>
            </div>
            <!--CTA1 END-->
            <div id="loading-wrapper">
                <div id="loading-text">Cargando...</div>
                <div id="loading-content"></div>
            </div>


            <div class="container container2" id="about-section">
                <div class="row">
                    <div id="widfom" class="col-md-offset-1 col-md-10">
                        <!-- Smart Wizard -->
                        <form action="<?php echo get_site_url("candidatos/candatoajax")?>" id="formcandidato" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
                            <!-- SmartWizard html -->

                            <?php 
                            $iduser = $id;
                            $id = $this->uri->segment(4);
                            if($id > 0){
                                $var = 'done';
                            }else{
                                $var = '';
                            } 
                            ?>

                            <div id="candidato" class="form_wizard wizard_verticle ">
                                <ul>
                                    <?php
                                    if(is_array($data->pasos)){
                                        foreach( $data->pasos as $paso){ ?>
                                            <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                                <a href="#step-<?php echo $paso->valor_paso?>">Paso <?php echo $paso->valor_paso?><br /></a>
                                            </li>
                                        <?php } } ?>
                                    </ul>
                                    <p>*Obligatorio</p>
                                    <div>

                                        <!-- paso 1 -->

                                        <div id="step-<?php echo $data->pasos[0]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[0]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b><?php echo $data->pasos[0]->titulo_paso;?></b></H4>
                                                    <hr>

                                                <div class="col-md-2">
                                                    <div class="user_forms-signup cambiar-foto" style="margin-top: -23px;margin-left: -10px;">
                                                            <div id='img_contain'>
                                                                <?php if (!empty($foto)) { ?>
                                                                    <img id="blah" align='middle' src="<?php echo get_assets_url().$foto;?>" alt="Imagen de perfil" title=''/>
                                                                <?php }else{ ?>
                                                                    <img id="blah" align='middle' src="<?php echo get_assets_url();?>assets/img/img.png" alt="Imagen de perfil" title=''/>
                                                                <?php } ?>

                                                            </div> 
                                                            <div class="input-group"> 
                                                                <div class="custom-file">
                                                                    <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                                                    <label class="sube_foto" for="inputGroupFile01"><i class="fa fa-camera"></i> sube tu foto</label>
                                                                </div>
                                                            </div>
                                                    </div>
                                                </div>



                                                    <div class="col-md-10">
                                                        <input type="text" name="iduser" id="iduser" value="<?php echo (!empty($candidato->iduser) && $candidato->iduser > '') ? $candidato->iduser : $iduser; ?>">
                                                        <input type="hidden" name="idcandidato" id="idcandidato" value="<?php echo (!empty($candidato->idcandidato) && $candidato->idcandidato > '') ? $candidato->idcandidato : ''; ?>">
                                                    
                                                        <input type="hidden" name="iddatospersonales" id="iddatospersonales" value="<?php echo (!empty($candidato->iddatospersonales) && $candidato->iddatospersonales > '') ? $candidato->iddatospersonales : ''; ?>">
                                                        <input type="hidden" name="idrrss" id="idrrss" value="<?php echo (!empty($candidato->idrrss) && $candidato->idrrss > '') ? $candidato->idrrss : ''; ?>">
                                                        <input type="hidden" name="iddgeografico" id="iddgeografico" value="<?php echo (!empty($candidato->iddgeografico) && $candidato->iddgeografico > '') ? $candidato->iddgeografico : ''; ?>">
                                                        <input type="hidden" name="idaptitudes" id="idaptitudes" value="<?php echo (!empty($candidato->idaptitudes) && $candidato->idaptitudes > '') ? $candidato->idaptitudes : ''; ?>">
                                                        <input type="hidden" name="idexperiencia" id="idexperiencia" value="<?php echo (!empty($candidato->idexperiencia) && $candidato->idexperiencia > '') ? $candidato->idexperiencia : ''; ?>">

                                                        <input type="hidden" name="file" id="file" value="<?php echo (!empty($candidato->file) && $candidato->file > '') ? $candidato->file : ''; ?>">
                                                        <input type="hidden" name="cv_file" id="cv_file" value="<?php echo (!empty($candidato->cv_file) && $candidato->cv_file > '') ? $candidato->cv_file : ''; ?>">

                                                        <div class="row">
                                                            <div class="form-group col-md-12">
                                                                <label for="Correo">Correo: *</label>
                                                                <input type="correo" class="form-control" name="Correo" id="Correo" placeholder="" required value="<?php echo (!empty($candidato->Correo) && $candidato->Correo > '') ? $candidato->Correo : $correo; ?>">
                                                                <div class="help-block with-errors"></div>
                                                            </div> 
                                                            <div class="form-group col-md-12">
                                                                <label for="Nombre">Nombre: *</label>
                                                                <input type="text" class="form-control" name="Nombre" id="Nombre" placeholder="" required value="<?php echo (!empty($candidato->Nombre) && $candidato->Nombre > '') ? $candidato->Nombre : $nombre; ?>">
                                                                <div class="help-block with-errors"></div>
                                                            </div> 
                                                            <div class="form-group col-md-12">
                                                                <label for="Apellido">Apellido: *</label>
                                                                <input type="text" class="form-control" name="Apellido" id="Apellido" placeholder="" required value="<?php echo (!empty($candidato->Apellido) && $candidato->Apellido > '') ? $candidato->Apellido : $apellido; ?>">
                                                                <div class="help-block with-errors"></div>
                                                            </div> 
                                                            <div class="form-group col-md-12">
                                                                <label for="Telefono">Teléfono: *</label>
                                                                <input type="text" class="form-control" name="Telefono" id="Telefono" placeholder="" required value="<?php echo (!empty($candidato->Telefono) && $candidato->Telefono > '') ? $candidato->Telefono : ''; ?>">
                                                                <div class="help-block with-errors"></div>
                                                            </div> 
                                                            <div class="form-group col-md-12">
                                                                <label for="Fecha_nacimiento">Fecha de nacimiento: *</label>
                                                                <div class="control-group"><div class="controls"><div class="xdisplay_inputx form-group has-feedback"> <input type="text" class="form-control has-feedback-left" id="single_cal1" name="Fecha_nacimiento" placeholder="" aria-describedby="inputSuccess2Status1" value="<?php echo (!empty($candidato->Fecha_nacimiento) && $candidato->Fecha_nacimiento > '') ? $candidato->Fecha_nacimiento : ''; ?>"><span class="fa fa-calendar-o form-control-feedback left" aria-hidden="true"></span><span id="inputSuccess2Status1" class="sr-only">(success)</span></div></div></div>
                                                                <div class="help-block with-errors"></div>
                                                            </div> 
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                        <!-- paso 2 -->
                                        <div id="step-<?php echo $data->pasos[1]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[1]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b><?php echo $data->pasos[1]->titulo_paso;?></b></H4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="Skype">Skype:</label>
                                                            <input type="text" class="form-control" name="Skype" id="Skype" placeholder="" value="<?php echo (!empty($candidato->Skype) && $candidato->Skype > '') ? $candidato->Skype : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Youtube">Blog / canal de Youtube u otro perfil donde transmitas tus conocimientos:</label>
                                                            <input type="text" class="form-control" name="Youtube" id="Youtube" placeholder="" value="<?php echo (!empty($candidato->Youtube) && $candidato->Youtube > '') ? $candidato->Youtube : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Facebook">Perfil de Facebook (indica la url completa):</label>
                                                            <input type="text" class="form-control" name="Facebook" id="Facebook" placeholder="" value="<?php echo (!empty($candidato->Facebook) && $candidato->Facebook > '') ? $candidato->Facebook : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Twitter">Perfil de Twitter (indica la url completa):</label>
                                                            <input type="text" class="form-control" name="Twitter" id="Twitter" placeholder="" value="<?php echo (!empty($candidato->Twitter) && $candidato->Twitter > '') ? $candidato->Twitter : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Instagram">Perfil de Instagram (indica la url completa):</label>
                                                            <input type="text" class="form-control" name="Instagram" id="Instagram" placeholder="" value="<?php echo (!empty($candidato->Instagram) && $candidato->Instagram > '') ? $candidato->Instagram : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Linkedin">Perfil de Linkedin (indica la url completa):</label>
                                                            <input type="text" class="form-control" name="Linkedin" id="Linkedin" placeholder="" value="<?php echo (!empty($candidato->Linkedin) && $candidato->Linkedin > '') ? $candidato->Linkedin : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- paso 3 -->
                                        <div id="step-<?php echo $data->pasos[2]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[2]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b><?php echo $data->pasos[2]->titulo_paso;?></b></H4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="Ciudad_Nacimiento">Ciudad de Nacimiento:</label>
                                                            <input type="text" class="form-control" name="Ciudad_Nacimiento" id="Ciudad_Nacimiento" placeholder="" value="<?php echo (!empty($candidato->Ciudad_Nacimiento) && $candidato->Ciudad_Nacimiento > '') ? $candidato->Ciudad_Nacimiento : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Pais_Nacimiento">País de Nacimiento:</label>
                                                            <input type="text" class="form-control" name="Pais_Nacimiento" id="Pais_Nacimiento" placeholder=""  value="<?php echo (!empty($candidato->Pais_Nacimiento) && $candidato->Pais_Nacimiento > '') ? $candidato->Pais_Nacimiento : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Ciudad_Residencia">Ciudad de Residencia:</label>
                                                            <input type="text" class="form-control" name="Ciudad_Residencia" id="Ciudad_Residencia" placeholder="" value="<?php echo (!empty($candidato->Ciudad_Residencia) && $candidato->Ciudad_Residencia > '') ? $candidato->Ciudad_Residencia : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Pais_Residencia">País de Residencia:</label>
                                                            <input type="text" class="form-control" name="Pais_Residencia" id="Pais_Residencia" placeholder=""  value="<?php echo (!empty($candidato->Pais_Residencia) && $candidato->Pais_Residencia > '') ? $candidato->Pais_Residencia : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Direccion_Completa">Dirección Completa:</label>
                                                            <input type="text" class="form-control" name="Direccion_Completa" id="Direccion_Completa" placeholder="" value="<?php echo (!empty($candidato->Direccion_Completa) && $candidato->Direccion_Completa > '') ? $candidato->Direccion_Completa : ''; ?>">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- paso 4 -->
                                        <div id="step-<?php echo $data->pasos[3]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[3]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b><?php echo $data->pasos[3]->titulo_paso;?></b></H4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="titulo_cv">Cargo o título breve del Currículum *:</label>
                                                            <input type="text" id="titulo_cv" class="form-control" name="titulo_cv" value="<?php echo (!empty($candidato->titulo_cv) && $candidato->titulo_cv > '') ? $candidato->titulo_cv : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Disponibilidad">Disponibilidad:</label>
                                                            <select id="Disponibilidad" name="Disponibilidad" class="form-control" required>
                                                                <?php echo (!empty($candidato->Disponibilidad) && $candidato->Disponibilidad > '') ? "<option value='".$candidato->Disponibilidad."' selected>".$candidato->Disponibilidad."</option>" : ''; ?>
                                                                <option value="">Seleccione</option>
                                                                <option value="Tiempo completo">Tiempo completo</option>
                                                                <option value=" Medio tiempo"> Medio tiempo</option>
                                                                <option value="A distancia">A distancia</option>
                                                            </select>
                                                        </div>
                                                        <div class="form-group col-md-12 tdistancia">
                                                            <label for="Equipo_trabajar_dis">Cuentas con equipo para trabajar (mencione especificaciones técnicas del equipo):</label>
                                                            <input type="text" id="Equipo_trabajar_dis" class="form-control" name="Equipo_trabajar_dis" value="<?php echo (!empty($candidato->Equipo_trabajar_dis) && $candidato->Equipo_trabajar_dis > '') ? $candidato->Equipo_trabajar_dis : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12 tdistancia">
                                                            <label for="conexion_estable">Cuentas con conexión estable a Internet:</label>
                                                            <select id="conexion_estable" name="conexion_estable" class="form-control tdistancia" required>
                                                                <?php echo (!empty($candidato->conexion_estable) && $candidato->conexion_estable > '') ? "<option value='".$candidato->conexion_estable."' selected>".$candidato->conexion_estable."</option>" : ''; ?>
                                                                <option value="">Seleccione</option>
                                                                <option value="Si">Si</option>
                                                                <option value="No">No</option>
                                                            </select>
                                                        </div>                        
                                                        <div class="form-group col-md-12 tdistancia">
                                                            <label for="Proveedores_Red">Mencione sus proveedores de red:</label>
                                                            <input type="text" id="Proveedores_Red" class="form-control" name="Proveedores_Red" value="<?php echo (!empty($candidato->Proveedores_Red) && $candidato->Proveedores_Red > '') ? $candidato->Proveedores_Red : ''; ?>">
                                                        </div>                    
                                                        <div class="form-group col-md-12 tdistancia">
                                                            <label for="Velocidad_conexion">Velocidad de conexión:</label>
                                                            <input type="text" id="Velocidad_conexion" class="form-control" name="Velocidad_conexion" value="<?php echo (!empty($candidato->Velocidad_conexion) && $candidato->Velocidad_conexion > '') ? $candidato->Velocidad_conexion : ''; ?>">
                                                        </div>                 
                                                        <div class="form-group col-md-12">
                                                            <label for="Proyectos_trabajado">Proyectos en los que hayas trabajado (Un plus si existen enlaces de esos proyectos en producción):</label>
                                                            <input type="text" id="Proyectos_trabajado" class="form-control" name="Proyectos_trabajado" value="<?php echo (!empty($candidato->Proyectos_trabajado) && $candidato->Proyectos_trabajado > '') ? $candidato->Proyectos_trabajado : ''; ?>">
                                                        </div>           
                                                        <div class="form-group col-md-12">
                                                            <label for="git">Posee repositorio git personal (Dejar el enlace a un proyecto para evaluar forma de programación en caso de ser afirmativo):</label>
                                                            <input type="text" id="git" class="form-control" name="git" value="<?php echo (!empty($candidato->git) && $candidato->git > '') ? $candidato->git : ''; ?>">
                                                        </div>      
                                                        <div class="form-group col-md-12">
                                                            <label for="gran_profesional">Cuéntanos, en breves palabras, lo que te hace un gran profesional:</label>
                                                            <input type="text" id="gran_profesional" class="form-control" name="gran_profesional" value="<?php echo (!empty($candidato->gran_profesional) && $candidato->gran_profesional > '') ? $candidato->gran_profesional : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Cargo_postulas">Cargo al que te postulas:</label>
                                                            <input type="text" id="Cargo_postulas" class="form-control" name="Cargo_postulas" value="<?php echo (!empty($candidato->Cargo_postulas) && $candidato->Cargo_postulas > '') ? $candidato->Cargo_postulas : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Lenguajes_programacion">Lenguajes de programación que dominas:</label> <br>
                                                            <small>* Agrega el lenguaje y presiona enter</small>
                                                            <ul class="tags lenguajes"><li class="tagAdd taglist"><input type="text" id="lenguajes" class="search-field" onkeypress="keypresstags($(this).val(), this.id)"></li></ul>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="fortalezas">Mencione al menos 5 fortalezas:</label> <br>
                                                            <small>* Agrega la fortalezas y presiona enter</small>
                                                            <ul class="tags fortalezas"><li class="tagAdd taglist"><input type="text" id="fortalezas" class="search-field" onkeypress="keypresstags($(this).val(), this.id)"></li></ul>
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="debilidades">Mencione al menos 3 debilidades:</label> <br>
                                                            <small>* Agrega la debilidades y presiona enter</small>
                                                            <ul class="tags debilidades"><li class="tagAdd taglist"><input type="text" id="debilidades" class="search-field" onkeypress="keypresstags($(this).val(), this.id)"></li></ul>
                                                        </div>
                                                         <div class="form-group col-md-12">
                                                                <label for="Aspiracion">Aspiración Salarial:</label>
                                                                <input type="text" class="form-control" name="Aspiracion" id="Aspiracion" placeholder=""  value="<?php echo (!empty($candidato->Aspiracion) && $candidato->Aspiracion > '') ? $candidato->Aspiracion : ''; ?>">
                                                            </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>


                                        <!-- paso 5 -->
                                        <div id="step-<?php echo $data->pasos[4]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[4]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b><?php echo $data->pasos[4]->titulo_paso;?></b></H4>
                                                    <hr>
                                                    <div class="row">
                                                        <div class="form-group col-md-12">
                                                            <label for="Empresa_exp1">Empresa:</label>
                                                            <input type="text" class="form-control" name="Empresa_exp1" id="Empresa_exp1" placeholder="" value="<?php echo (!empty($candidato->Empresa_exp1) && $candidato->Empresa_exp1 > '') ? $candidato->Empresa_exp1 : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Destacarias_empresa_exp1">Qué destacarías de la empresa:</label>
                                                            <input type="text" class="form-control" name="Destacarias_empresa_exp1" id="Destacarias_empresa_exp1" placeholder=""  value="<?php echo (!empty($candidato->Destacarias_empresa_exp1) && $candidato->Destacarias_empresa_exp1 > '') ? $candidato->Destacarias_empresa_exp1 : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Mejorarias_empresa_exp1">Qué mejorarías de la empresa:</label>
                                                            <input type="text" class="form-control" name="Mejorarias_empresa_exp1" id="Mejorarias_empresa_exp1" placeholder="" value="<?php echo (!empty($candidato->Mejorarias_empresa_exp1) && $candidato->Mejorarias_empresa_exp1 > '') ? $candidato->Mejorarias_empresa_exp1 : ''; ?>">
                                                        </div>
                                                        <div class="form-group col-md-12">
                                                            <label for="Sector_empresa_exp1">Sector de empresa:</label>
                                                            <select id="Sector_empresa_exp1" name="Sector_empresa_exp1" class="form-control">
                                                                 <?php echo (!empty($candidato->Sector_empresa_exp1) && $candidato->Sector_empresa_exp1 > '') ? "<option value='".$candidato->Sector_empresa_exp1."' selected>".$candidato->Sector_empresa_exp1."</option>" : ''; ?>
                                                                <option value="Agricultura / Pesca / Ganadería">Agricultura / Pesca / Ganadería</option>
                                                                <option value="Construcción / obras">Construcción / obras</option>
                                                                <option value="Educación">Educación</option>
                                                                <option value="Energía">Energía</option>
                                                                <option value="Entretenimiento / Deportes">Entretenimiento / Deportes</option>
                                                                <option value="Fabricación">Fabricación</option>
                                                                <option value="Finanzas / Banca">Finanzas / Banca</option>
                                                                <option value="Gobierno / No Lucro">Gobierno / No Lucro</option>
                                                                <option value="Hostelería / Turismo">Hostelería / Turismo</option>
                                                                <option value="Informática / Hardware">Informática / Hardware</option>
                                                                <option value="Informática / Software">Informática / Software</option>
                                                                <option value="Internet">Internet</option>
                                                                <option value="Legal / Asesoría">Legal / Asesoría</option>
                                                                <option value="Materias Primas">Materias Primas</option>
                                                                <option value="Medios de Comunicación">Medios de Comunicación</option>
                                                                <option value="Publicidad / RRPP">Publicidad / RRPP</option>
                                                                <option value="RRHH / Personal">RRHH / Personal</option>
                                                                <option value="Salud / Medicina">Salud / Medicina</option>
                                                                <option value="Servicios Profesionales">Servicios Profesionales</option><option value="Telecomunicaciones">Telecomunicaciones</option>
                                                                <option value="Transporte">Transporte</option>
                                                                <option value="Venta al consumidor">Venta al consumidor</option>
                                                                <option value="Venta al por mayor">Venta al por mayor</option>
                                                            </select>
                                                        </div>            
                                                        <div class="form-group col-md-12">
                                                            <label for="Cargo_empresa_exp1">Cargo:</label>
                                                            <input type="text" class="form-control" name="Cargo_empresa_exp1" id="Cargo_empresa_exp1" placeholder="" value="<?php echo (!empty($candidato->Cargo_empresa_exp1) && $candidato->Cargo_empresa_exp1 > '') ? $candidato->Cargo_empresa_exp1 : ''; ?>">
                                                        </div>     
                                                        <div class="form-group col-md-12">
                                                            <label for="Area_empresa_exp1">Área:</label>
                                                            <select id="Area_empresa_exp1" name="Area_empresa_exp1" class="form-control">
                                                                <?php echo (!empty($candidato->Area_empresa_exp1) && $candidato->Area_empresa_exp1 > '') ? "<option value='".$candidato->Area_empresa_exp1."' selected>".$candidato->Area_empresa_exp1."</option>" : ''; ?>
                                                                <option value="Administración / Oficina">Administración / Oficina</option>
                                                                <option value="Almacén / Logística / Transporte">Almacén / Logística / Transporte</option>
                                                                <option value="Atención a clientes">Atención a clientes</option>
                                                                <option value="CallCenter / Telemercadeo">CallCenter / Telemercadeo</option>
                                                                <option value="Compras / Comercio Exterior">Compras / Comercio Exterior</option>
                                                                <option value="Construccion y obra">Construccion y obra</option>
                                                                <option value="Contabilidad / Finanzas">Contabilidad / Finanzas</option><option value="Dirección / Gerencia">Dirección / Gerencia</option>
                                                                <option value="Diseño / Artes gráficas">Diseño / Artes gráficas</option><option value="Docencia">Docencia</option>
                                                                <option value="Hostelería / Turismo">Hostelería / Turismo</option>
                                                                <option value="Informática / Telecomunicaciones">Informática / Telecomunicaciones</option>
                                                                <option value="Ingeniería">Ingeniería</option>
                                                                <option value="Investigación y Calidad">Investigación y Calidad</option><option value="Legal / Asesoría">Legal / Asesoría</option>
                                                                <option value="Mantenimiento y Reparaciones Técnicas">Mantenimiento y Reparaciones Técnicas</option>
                                                                <option value="Medicina / Salud">Medicina / Salud</option>
                                                                <option value="Mercadotécnia / Publicidad / Comunicación">Mercadotécnia / Publicidad / Comunicación</option>
                                                                <option value="Producción / Operarios / Manufactura">Producción / Operarios / Manufactura</option>
                                                                <option value="Recursos Humanos">Recursos Humanos</option>
                                                                <option value="Servicios Generales, Aseo y Seguridad">Servicios Generales, Aseo y Seguridad</option>
                                                                <option value="Ventas">Ventas</option>
                                                            </select>
                                                        </div>          
                                                        <div class="form-group col-md-12">
                                                            <label for="Funciones_logros_empresa_exp1">Funciones y logros del cargo:</label>
                                                            <textarea id="Funciones_logros_empresa_exp1" class="form-control" name="Funciones_logros_empresa_exp1" required><?php echo (!empty($candidato->Funciones_logros_empresa_exp1) && $candidato->Funciones_logros_empresa_exp1 > '') ? $candidato->Funciones_logros_empresa_exp1 : ''; ?></textarea>
                                                        </div>     
                                                        <div class="form-group col-md-12">
                                                            <label for="Periodo_empresa_exp1">Periodo:</label> <br>
                                                            <div class="col-md-2 col-sm-3 col-xs-12 form-group has-feedback">
                                                                  <select class="form-control" id="mes_desde_exp1" name="mes_desde_exp1">
                                                                    <?php echo (!empty($candidato->mes_desde_exp1) && $candidato->mes_desde_exp1 > '') ? "<option value='".$candidato->mes_desde_exp1."' selected>".$candidato->mes_desde_exp1."</option>" : ''; ?>
                                                                        <option value="">Mes</option>
                                                                        <option value="Enero">Enero</option> 
                                                                        <option value="Febrero">Febrero</option> 
                                                                        <option value="Marzo">Marzo</option> 
                                                                        <option value="Abril">Abril</option> 
                                                                        <option value="Mayo">Mayo</option> 
                                                                        <option value="Junio">Junio</option> 
                                                                        <option value="Julio">Julio</option> 
                                                                        <option value="Agosto">Agosto</option> 
                                                                        <option value="Septiembre">Septiembre</option> 
                                                                        <option value="Octubre">Octubre</option> 
                                                                        <option value="Noviembre">Noviembre</option> 
                                                                        <option value="Diciembre">Diciembre</option> 
                                                                  </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-2 col-xs-12 form-group has-feedback">
                                                                  <select class="form-control" id="ano_desde_exp1" name="ano_desde_exp1">
                                                                     <?php echo (!empty($candidato->ano_desde_exp1) && $candidato->ano_desde_exp1 > '') ? "<option value='".$candidato->ano_desde_exp1."' selected>".$candidato->ano_desde_exp1."</option>" : ''; ?>
                                                                        <option value="0">Año</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2014">2014</option>
                                                                        <option value="2013">2013</option>
                                                                        <option value="2012">2012</option>
                                                                        <option value="2011">2011</option>
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                        <option value="1994">1994</option>
                                                                        <option value="1993">1993</option>
                                                                        <option value="1992">1992</option>
                                                                        <option value="1991">1991</option>
                                                                        <option value="1990">1990</option>
                                                                        <option value="1989">1989</option>
                                                                        <option value="1988">1988</option>
                                                                        <option value="1987">1987</option>
                                                                        <option value="1986">1986</option>
                                                                        <option value="1985">1985</option>
                                                                        <option value="1984">1984</option>
                                                                        <option value="1983">1983</option>
                                                                        <option value="1982">1982</option>
                                                                        <option value="1981">1981</option>
                                                                        <option value="1980">1980</option>
                                                                        <option value="1979">1979</option>
                                                                        <option value="1978">1978</option>
                                                                        <option value="1977">1977</option>
                                                                        <option value="1976">1976</option>
                                                                        <option value="1975">1975</option>
                                                                        <option value="1974">1974</option>
                                                                        <option value="1973">1973</option>
                                                                        <option value="1972">1972</option>
                                                                        <option value="1971">1971</option>
                                                                        <option value="1970">1970</option>
                                                                        <option value="1969">1969</option>
                                                                        <option value="1968">1968</option>
                                                                        <option value="1967">1967</option>
                                                                        <option value="1966">1966</option>
                                                                        <option value="1965">1965</option>
                                                                        <option value="1964">1964</option>
                                                                        <option value="1963">1963</option>
                                                                        <option value="1962">1962</option>
                                                                        <option value="1961">1961</option>
                                                                        <option value="1960">1960</option>
                                                                        <option value="1959">1959</option>
                                                                        <option value="1958">1958</option>
                                                                        <option value="1957">1957</option>
                                                                        <option value="1956">1956</option>
                                                                        <option value="1955">1955</option>
                                                                        <option value="1954">1954</option>
                                                                        <option value="1953">1953</option>
                                                                        <option value="1952">1952</option>
                                                                        <option value="1951">1951</option>
                                                                        <option value="1950">1950</option>
                                                                        <option value="1949">1949</option>
                                                                        <option value="1948">1948</option>
                                                                        <option value="1947">1947</option>
                                                                        <option value="1946">1946</option>
                                                                        <option value="1945">1945</option>
                                                                        <option value="1944">1944</option>
                                                                        <option value="1943">1943</option>
                                                                        <option value="1942">1942</option>
                                                                        <option value="1941">1941</option>
                                                                        <option value="1940">1940</option>
                                                                        <option value="1939">1939</option>
                                                                        <option value="1938">1938</option>
                                                                        <option value="1937">1937</option>
                                                                        <option value="1936">1936</option>
                                                                        <option value="1935">1935</option>
                                                                        <option value="1934">1934</option>
                                                                        <option value="1933">1933</option>
                                                                        <option value="1932">1932</option>
                                                                        <option value="1931">1931</option>
                                                                        <option value="1930">1930</option>
                                                                        <option value="1929">1929</option>
                                                                        <option value="1928">1928</option>
                                                                        <option value="1927">1927</option>
                                                                        <option value="1926">1926</option>
                                                                        <option value="1925">1925</option>
                                                                        <option value="1924">1924</option>
                                                                        <option value="1923">1923</option>
                                                                        <option value="1922">1922</option>
                                                                        <option value="1921">1921</option>
                                                                        <option value="1920">1920</option>
                                                                  </select>
                                                            </div>
                                                            <p class="col-md-1" for="Área exp1">  </p>
                                                            <div class="col-md-2 col-sm-3 col-xs-12 form-group has-feedback">
                                                                  <select class="form-control" id="mes_hasta_exp1" name="mes_hasta_exp1">
                                                                    <?php echo (!empty($candidato->mes_hasta_exp1) && $candidato->mes_hasta_exp1 > '') ? "<option value='".$candidato->mes_hasta_exp1."' selected>".$candidato->mes_hasta_exp1."</option>" : ''; ?>
                                                                        <option value="">Mes</option>
                                                                        <option value="Enero">Enero</option> 
                                                                        <option value="Febrero">Febrero</option> 
                                                                        <option value="Marzo">Marzo</option> 
                                                                        <option value="Abril">Abril</option> 
                                                                        <option value="Mayo">Mayo</option> 
                                                                        <option value="Junio">Junio</option> 
                                                                        <option value="Julio">Julio</option> 
                                                                        <option value="Agosto">Agosto</option> 
                                                                        <option value="Septiembre">Septiembre</option> 
                                                                        <option value="Octubre">Octubre</option>
                                                                        <option value="Noviembre">Noviembre</option> 
                                                                        <option value="Diciembre">Diciembre</option> 
                                                                  </select>
                                                            </div>
                                                            <div class="col-md-2 col-sm-3 col-xs-12 form-group has-feedback">
                                                                  <select class="form-control" id="ano_hasta_exp1" name="ano_hasta_exp1">
                                                                    <?php echo (!empty($candidato->ano_hasta_exp1) && $candidato->ano_hasta_exp1 > '') ? "<option value='".$candidato->ano_hasta_exp1."' selected>".$candidato->ano_hasta_exp1."</option>" : ''; ?>
                                                                        <option value="0">Año</option>
                                                                        <option value="2020">2020</option>
                                                                        <option value="2019">2019</option>
                                                                        <option value="2018">2018</option>
                                                                        <option value="2017">2017</option>
                                                                        <option value="2016">2016</option>
                                                                        <option value="2015">2015</option>
                                                                        <option value="2014">2014</option>
                                                                        <option value="2013">2013</option>
                                                                        <option value="2012">2012</option>
                                                                        <option value="2011">2011</option>
                                                                        <option value="2010">2010</option>
                                                                        <option value="2009">2009</option>
                                                                        <option value="2008">2008</option>
                                                                        <option value="2007">2007</option>
                                                                        <option value="2006">2006</option>
                                                                        <option value="2005">2005</option>
                                                                        <option value="2004">2004</option>
                                                                        <option value="2003">2003</option>
                                                                        <option value="2002">2002</option>
                                                                        <option value="2001">2001</option>
                                                                        <option value="2000">2000</option>
                                                                        <option value="1999">1999</option>
                                                                        <option value="1998">1998</option>
                                                                        <option value="1997">1997</option>
                                                                        <option value="1996">1996</option>
                                                                        <option value="1995">1995</option>
                                                                        <option value="1994">1994</option>
                                                                        <option value="1993">1993</option>
                                                                        <option value="1992">1992</option>
                                                                        <option value="1991">1991</option>
                                                                        <option value="1990">1990</option>
                                                                        <option value="1989">1989</option>
                                                                        <option value="1988">1988</option>
                                                                        <option value="1987">1987</option>
                                                                        <option value="1986">1986</option>
                                                                        <option value="1985">1985</option>
                                                                        <option value="1984">1984</option>
                                                                        <option value="1983">1983</option>
                                                                        <option value="1982">1982</option>
                                                                        <option value="1981">1981</option>
                                                                        <option value="1980">1980</option>
                                                                        <option value="1979">1979</option>
                                                                        <option value="1978">1978</option>
                                                                        <option value="1977">1977</option>
                                                                        <option value="1976">1976</option>
                                                                        <option value="1975">1975</option>
                                                                        <option value="1974">1974</option>
                                                                        <option value="1973">1973</option>
                                                                        <option value="1972">1972</option>
                                                                        <option value="1971">1971</option>
                                                                        <option value="1970">1970</option>
                                                                        <option value="1969">1969</option>
                                                                        <option value="1968">1968</option>
                                                                        <option value="1967">1967</option>
                                                                        <option value="1966">1966</option>
                                                                        <option value="1965">1965</option>
                                                                        <option value="1964">1964</option>
                                                                        <option value="1963">1963</option>
                                                                        <option value="1962">1962</option>
                                                                        <option value="1961">1961</option>
                                                                        <option value="1960">1960</option>
                                                                        <option value="1959">1959</option>
                                                                        <option value="1958">1958</option>
                                                                        <option value="1957">1957</option>
                                                                        <option value="1956">1956</option>
                                                                        <option value="1955">1955</option>
                                                                        <option value="1954">1954</option>
                                                                        <option value="1953">1953</option>
                                                                        <option value="1952">1952</option>
                                                                        <option value="1951">1951</option>
                                                                        <option value="1950">1950</option>
                                                                        <option value="1949">1949</option>
                                                                        <option value="1948">1948</option>
                                                                        <option value="1947">1947</option>
                                                                        <option value="1946">1946</option>
                                                                        <option value="1945">1945</option>
                                                                        <option value="1944">1944</option>
                                                                        <option value="1943">1943</option>
                                                                        <option value="1942">1942</option>
                                                                        <option value="1941">1941</option>
                                                                        <option value="1940">1940</option>
                                                                        <option value="1939">1939</option>
                                                                        <option value="1938">1938</option>
                                                                        <option value="1937">1937</option>
                                                                        <option value="1936">1936</option>
                                                                        <option value="1935">1935</option>
                                                                        <option value="1934">1934</option>
                                                                        <option value="1933">1933</option>
                                                                        <option value="1932">1932</option>
                                                                        <option value="1931">1931</option>
                                                                        <option value="1930">1930</option>
                                                                        <option value="1929">1929</option>
                                                                        <option value="1928">1928</option>
                                                                        <option value="1927">1927</option>
                                                                        <option value="1926">1926</option>
                                                                        <option value="1925">1925</option>
                                                                        <option value="1924">1924</option>
                                                                        <option value="1923">1923</option>
                                                                        <option value="1922">1922</option>
                                                                        <option value="1921">1921</option>
                                                                        <option value="1920">1920</option>
                                                                  </select>
                                                            </div>
                                                        </div> 
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                        <!-- paso 6 -->
                                        <div id="step-<?php echo $data->pasos[5]->valor_paso;?>"> 
                                            <div class="row">
                                                <div id="form-step-<?php echo intval($data->pasos[5]->valor_paso) -1;?>" role="form" data-toggle="validator">
                                                    <hr>                                               
                                                    <H4 class="text-center"><b><?php echo $data->pasos[5]->titulo_paso;?></b></H4>
                                                    <hr>
                                                    <div class="custom-file">
                                                        <input type="file" id="archivo" name="archivo_cv" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" accept="application/pdf, .doc, .docx, .odf" style="display: none">
                                                       <!--  <label for="archivo" id="text-file_cv"> -->
                                                          <?php 

                                                          if  (!empty($candidato->cv_file) && $candidato->cv_file) {
                                                            $cv_str = substr($candidato->cv_file, 19);
                                                            echo '<i class="fa fa-file-text-o"></i> '.$cv_str.'<br><br><label for="archivo" id="text-file_cv">
                                                           <i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i> CAMBIAR ARCHIVO <br><small style="margin-left: 74px;"> (PDF,WORD,JPG,PNG)</small></label>';
                                                        }else{
                                                           echo '<label for="archivo" id="text-file_cv">
                                                           <i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i> SUBE TU CURRÍCULUM <br><small style="margin-left: 74px;"> (PDF,WORD,JPG,PNG)</small></label>';
                                                       }
                                                       ?>


  
                                                        <!-- </label> -->
                                                    </div>
                                                    <br><br>


                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>

                    </div> 

                </div>
<!--End About-->


</div>
                                        <div class="col-md-2 col-sm-2 col-xs-12">
                                        </div>
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>
<section class="user">
    <div class="user_options-container">
        <div class="user_options-text">
            <div class="user_options-unregistered">
                <h2 class="user_unregistered-title"></h2>
                <p class="user_unregistered-text"></p>
            </div>

            <div class="user_options-registered">
                <h2 class="user_registered-title">Tienes una cuenta?</h2>
                <p class="user_registered-text">Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap.</p>
            </div>
        </div>

        <div class="user_options-forms user_options-perfile" id="user_options-forms">
            <div class="user_forms-login">
                <h2 class="forms_title"><i class="fa fa-edit"></i> EDITAR MI CUENTA
                </h2>
                <form class="forms_form" action="<?php echo get_site_url("reclutas/actualizar_datos")?>" id="perfil_recluta" role="form" name="perfil_recluta" method="post" accept-charset="utf-8">
                    <input type="hidden" class="form-control" name="file" id="file" value="">
                    <input type="hidden" class="form-control" name="id" id="id" value="<?php echo (!empty($datauser->id) && $datauser->id > '') ? $datauser->id : ''; ?>">
                    <fieldset class="forms_fieldset">
                        <div class="forms_field">
                            <input type="text" placeholder="Nombre y Apellido" class="forms_field-input" name="nombre" id="nombre" required  value="<?php echo (!empty($nombre) && $nombre > '') ? $nombre : ''; ?>" />
                        </div>
                        <div class="forms_field">
                            <input type="text" placeholder="Teléfono" class="forms_field-input" name="telefono" id="telefono" required  value="<?php echo (!empty($datauser->telefono) && $datauser->telefono > '') ? $datauser->telefono : ''; ?>" />
                        </div>
                        <div class="forms_field">
                            <input type="email" placeholder="Correo" class="forms_field-input" name="correo" id="correo" required  value="<?php echo (!empty($correo) && $correo > '') ? $correo : ''; ?>"/>
                        </div>
                        <div class="forms_field">
                            <input type="password" placeholder="Contraseña" class="forms_field-input" name="contrasena" id="contrasena" required value="<?php echo (!empty($datauser->contrasena) && $datauser->contrasena > '') ? $datauser->contrasena : ''; ?>" />
                        </div>
                    </fieldset>
                    <div class="forms_buttons">
                        <button type="button" class="forms_buttons-forgot"></button>
                        <button type="submit" id="actualizar_datos" class="forms_buttons-action" name="actualizar_datos"> Guardar</button>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <div class="user_options-forms bounceLeft user_options-perfile" id="user_options-forms"  style="margin-left: -4px;">

            
            <div class="user_forms-signup cambiar-foto">
                <h2 class="forms_title"><i class="fa fa-image"></i> Sube tu foto</h2>
                <form id="form1" runat="server">
                    <div class="alert"></div>
                    <div id='img_contain'>
                    <?php if (!empty($foto)) { ?>
                         <img id="blah" align='middle' src="<?php echo get_assets_url().$foto;?>" alt="Imagen de perfil" title=''/>
                    <?php }else{ ?>
                        <img id="blah" align='middle' src="<?php echo get_assets_url();?>assets/img/img.png" alt="Imagen de perfil" title=''/>
                    <?php } ?>

                    </div> 
                    <div class="input-group"> 
                        <div class="custom-file">
                            <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                            <label class="forms_buttons-action btn-cambiar-foto" for="inputGroupFile01">Cambiar foto</label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>


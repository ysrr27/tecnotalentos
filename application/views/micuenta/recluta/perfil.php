
<br><br><br>
<div class="my-goomo-shimmer_rec">
    <div class="mg-user-header_rec">
        <div class="wrp_rec">
            <div class="usr-img_rec gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_rec profile-photo-small">
                <?php }else{ ?>
                    <div class="image-upload">
                      <label for="file-input">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_rec profile-photo-small">
                    </label>
                    <input id="file-input" type="file"/>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="wrp_rec_right">
            <div class="btn-group">
                <a href="<?php echo get_site_url("reclutas/solicitud")?>" title="PUBLICAR AVISO" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-edit"></i></a>
                <a href="<?php echo get_site_url("reclutas/buzon")?>" title="BANDEJA DE SOLICITUDES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-bullhorn"></i></a>
                <a href="<?php echo get_site_url("reclutas/correo")?>" title="MENSAJES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-envelope-o"></i></a>
            </div>
        </div>
    </div>
    <div class="main-wrap container">
        <div class="mg-shim-main">
            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-md-12 descripcionuser"><br>
                              <h1>PERFIL DE EMPRESA </h1>
                              <br><br><br>
                        </div>   
                    </div>
             


                     <div class="row">
                        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 content-perfil-empresa">
                            <div class="wrp_rec_center">
                                <div id="card-photo-empresa" class="usr-img_rec <?php echo (empty($dataperfil->rows_perfil)) ? 'card-perfile-empresa' : ''; ?>">
                                    <?php if (!empty($foto)) {?>
                                        <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_rec">
                                    <?php }else{ ?>
                                        <div class="image-upload">
                                            <label for="file-input">
                                                <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_rec ">
                                            </label>
                                            <input id="file-input" type="file"/>
                                        </div>
                                    <?php } ?>
                                </div>


                                <div id="card-edit-photo-empresa" class="user_forms-signup cambiar-foto <?php echo (!empty($dataperfil->rows_perfil)) ? 'card-perfile-empresa' : ''; ?>">
                                    <form id="form1" runat="server">
                                        <div class="alert"></div>
                                        <div id='img_contain'>
                                            <?php if (!empty($foto)) { ?>
                                               <img id="blah" align='middle' src="<?php echo get_assets_url().$foto;?>" alt="Imagen de perfil" title=''/>
                                           <?php }else{ ?>
                                            <img id="blah" align='middle' src="<?php echo get_assets_url();?>assets/img/img.png" alt="Imagen de perfil" title=''/>
                                        <?php } ?>

                                    </div> 
                                    <div class="input-group"> 
                                        <div class="custom-file">
                                            <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                            <label class="forms_buttons-action btn-cambiar-foto" for="inputGroupFile01"><i class="fa fa-image"></i></label>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            </div><br><br><br>
                            <div id="card-perfile-empresa" class="col-md-12 col-sm-12 col-xs-12 <?php echo (empty($dataperfil->rows_perfil)) ? 'card-perfile-empresa' : ''; ?> tb_padig"> 
                                <div class="pull-right">
                                    <a href="#" id="edit_perfile_empresa">
                                        <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                    </a>
                                </div><br><br>
                                <div class="row">
                                    <div id="e_data" class="col-md-12 col-sm-12 col-xs-12 descripcionuser">            
                                        <p><?php echo (!empty($dataperfil->rows_perfil->Razon_social_empresa)) ? $dataperfil->rows_perfil->Razon_social_empresa : ''; ?></p> 
                                        <p><?php echo (!empty($dataperfil->rows_perfil->Rif_empresa)) ? $dataperfil->rows_perfil->Rif_empresa : ''; ?></p>

                                        <p>Descripción: <?php echo (!empty($dataperfil->rows_perfil->Descripcion_empresa)) ? $dataperfil->rows_perfil->Descripcion_empresa : ''; ?></p>
                                        
                                        <p>Ubicación Actual: <?php echo (!empty($dataperfil->rows_perfil->Estado_empresa)) ? $dataperfil->rows_perfil->Estado_empresa : ''; ?> <?php echo (!empty($dataperfil->rows_perfil->Ciudad_empresa)) ? $dataperfil->rows_perfil->Ciudad_empresa : ''; ?></p>
                                        <p><?php echo (!empty($dataperfil->rows_perfil->Direccion_empresa)) ? $dataperfil->rows_perfil->Direccion_empresa : ''; ?></p>

                                        <p>Email: <?php echo (!empty($dataperfil->rows_perfil->Email_contacto)) ? $dataperfil->rows_perfil->Email_contacto : ''; ?></p><br>
                                        <hr>
                                    </div>   
                                </div>
                            </div>
                            <div id="card-edit-perfile-empresa" class="col-md-12 col-sm-12 col-xs-12 <?php echo (!empty($dataperfil->rows_perfil)) ? 'card-perfile-empresa' : ''; ?>">
                                <br><br>
                                <form action="<?php echo get_site_url("reclutas/perfil_empresa")?>" class="" id="perfil_empresa" role="form" data-toggle="validator" name="perfil_empresa" method="post" accept-charset="utf-8">
                                    <div class="form-group row">
                                        <input type="hidden" name="idempresa" id="idempresa" value="<?php echo (!empty($dataperfil->rows_perfil->idempresa) && $dataperfil->rows_perfil->idempresa > '') ? $dataperfil->rows_perfil->idempresa : ''; ?>">

                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label for="Razon_social_empresa">Razón social *:</label>
                                            <input type="text" class="form-control" name="Razon_social_empresa" pattern="^[a-zA-Z0-9\s]{5,}$" maxlength="15"  id="Razon_social_empresa" placeholder="" required value="<?php echo (!empty($dataperfil->rows_perfil->Razon_social_empresa) && $dataperfil->rows_perfil->Razon_social_empresa > '') ? $dataperfil->rows_perfil->Razon_social_empresa : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div> 
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label for="Descripcion_empresa">Descripción: *<br></label>
                                            <textarea id="Descripcion_empresa" required="required" class="form-control" name="Descripcion_empresa" minlength="10" maxlength="55" pattern="^[a-zA-Z0-9\s]{5,}$">
                                                <?php echo (!empty($dataperfil->rows_perfil->Descripcion_empresa) && $dataperfil->rows_perfil->Descripcion_empresa > '') ? $dataperfil->rows_perfil->Descripcion_empresa : ''; ?>
                                            </textarea>
                                            <div class="textarea help-block with-errors"></div>
                                        </div> 
                                    </div>
                                    <div class="row">
                                        <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label for="Rif_empresa">Rif *:</label>
                                            <input type="text" class="form-control" name="Rif_empresa" id="Rif_empresa" placeholder="" data-inputmask="'mask' : '[9-]A-99999999-9'" required value="<?php echo (!empty($dataperfil->rows_perfil->Rif_empresa) && $dataperfil->rows_perfil->Rif_empresa > '') ? $dataperfil->rows_perfil->Rif_empresa : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                         <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                            <label for="Email_contacto">E-mail de contacto *: </label>
                                            <input type="email" class="form-control" name="Email_contacto" id="Email_contacto" placeholder="" required value="<?php echo (!empty($dataperfil->rows_perfil->Email_contacto) && $dataperfil->rows_perfil->Email_contacto > '') ? $dataperfil->rows_perfil->Email_contacto : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>  
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label for="Estado_empresa">Estado *:</label>
                                            <input type="text" class="form-control" name="Estado_empresa" id="Estado_empresa" placeholder="" pattern="^[a-zA-Z0-9\s]{5,}$" maxlength="15" required value="<?php echo (!empty($dataperfil->rows_perfil->Estado_empresa) && $dataperfil->rows_perfil->Estado_empresa > '') ? $dataperfil->rows_perfil->Estado_empresa : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                         <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label for="Ciudad_empresa">Ciudad *:</label>
                                            <input type="text" class="form-control" name="Ciudad_empresa" id="Ciudad_empresa" placeholder="" pattern="^[a-zA-Z0-9\s]{5,}$" maxlength="15" required value="<?php echo (!empty($dataperfil->rows_perfil->Ciudad_empresa) && $dataperfil->rows_perfil->Ciudad_empresa > '') ? $dataperfil->rows_perfil->Ciudad_empresa : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>  
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label for="Direccion_empresa">Dirección *:</label>
                                            <input type="text" class="form-control" name="Direccion_empresa" pattern="^[a-zA-Z0-9\s]{5,}$" maxlength="55" id="Direccion_empresa" placeholder="" required value="<?php echo (!empty($dataperfil->rows_perfil->Direccion_empresa) && $dataperfil->rows_perfil->Direccion_empresa > '') ? $dataperfil->rows_perfil->Direccion_empresa : ''; ?>">
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                            <button class="btn btn-primary" id="guardar_perfil_empresa" type="button">Guardar</button>
                                          <button class="btn btn-primary" id="reset_perfil_empresa" type="reset">Descartar</button>
                                      </div><br><br>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>

<br><br><br>
<div class="my-goomo-shimmer_rec">
    <div class="mg-user-header_rec">
        <div class="wrp_rec">
            <div class="usr-img_rec gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_rec">
                <?php }else{ ?>
                    <div class="image-upload">
                        <label for="file-input">
                            <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_rec ">
                        </label>
                        <input id="file-input" type="file"/>
                    </div>
                <?php } ?>
            </div>
        </div>
        <div class="wrp_rec_right">
            <div class="btn-group">
                <a href="<?php echo get_site_url("reclutas/solicitud")?>" title="PUBLICAR AVISO" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-edit"></i></a>
                <a href="<?php echo get_site_url("reclutas/buzon")?>" title="BANDEJA DE SOLICITUDES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-bullhorn"></i></a>
                <a href="<?php echo get_site_url("reclutas/correo")?>" title="MENSAJES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-envelope-o"></i></a>
            </div>
        </div>
    </div>
    <div class="main-wrap container">
        <div class="mg-shim-main">
            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 descripcionuser"><br>
                            <h1>BANDEJA DE CORREO</h1>
                            <br><br><br>                         
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12 content-perfil-empresa content-correo-empresa">
                            <br><br>
                            <div class="col-md-offset-1 col-md-10 col-sm-12 col-xs-12">
                                <br>
                              <!--   <a id="volver_sms" href="#"><i class="fa fa-reply"></i> Volver</a> -->
                                <div class="text-right text-large">
                                    <a href="#" id="volver_sms" class="social-button btn-primary m_disable2">
                                        <span class="fa fa-lg fa-reply"></span>
                                    </a>
                                </div>
                                <div class="x_content">
                           <?php 
                           if(is_array($datasolicitudes->rows_mensajes )){ ?>
                                        <div class="new">
                                            <form action="<?php echo get_site_url('reclutas/mensajeajax')?>" class="" id="new_mensaje" role="form" data-toggle="validator" name="new_mensaje" method="post" accept-charset="utf-8">
                                                <input type="hidden" class="form-control" name="idmensaje_respuesta" id="idmensaje_respuesta" value="">
                                                <input type="hidden" class="form-control" name="useridfrom_respuesta" id="useridfrom_respuesta" value="">
                                                <input type="hidden" class="form-control" name="file" id="file" value="">


                                                <div class="new-mail">
                                                    <div class="new-mail__top">
                                                        <div class="new-mail__title">
                                                            <span id="rpd">Responder:</span>

                                                        </div>
                                                        <i class="new-mail__close new-mail__toggle fa fa-close"></i>
                                                    </div>
                                                    <div class="new-mail-exp">
                                                    </div>
                                                    <div class="new-mail__content">
                                                        <textarea id="fullmensaje" name="fullmensaje" 
                                                        class="new-mail__message" autofocus></textarea>
                                                    </div>
                                                    <div class="new-mail-foot">
                                                        <div class="new-mail-foot__insert">
                                                            <div class="input-group"> 
                                                                <div class="custom-file">
                                                                    <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                                                    <label class="custom-file-label" for="inputGroupFile01"><i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="new-mail-foot__actions">
                                                            <button id="btn-cancel" class="new-mail__toggle preview-foot__button">Cancelar</button>
                                                            <button class="button--primary preview-foot__button" id="send-message">
                                                                <i class="fa fa-paper-plane"></i></button>                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <div class="mails">
                                            <div class="message-list scrollable">
                                                <div class="scrollable__target">

                                                    <?php 
                                                    if(is_array($datasolicitudes->rows_mensajes )){
                                                        foreach($datasolicitudes->rows_mensajes as $key =>$row){
                                                            if ($key === 0) {
                                                                ?>

                                                                <div class="message<?php echo $row->idmensaje ?> message message--active <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                                                <?php }else{ ?>
                                                                    <div class="message<?php echo $row->idmensaje ?> message <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                                                    <?php } ?>

                                                                    <div class="message-tags">
                                                                        <span class="dot dot--green"></span>
                                                                    </div>
                                                                    <div class="message__actions"></div>
                                                                    <div class="message__content">
                                                                        <div class="message__exp">
                                                                            <div><?php echo $row->nombre." ".$row->apellido; ?></div>
                                                                            <div class="date"><?php echo date( "d/m/Y H:i:s", $row->fecha); ?></div>
                                                                        </div>
                                                                        <div class="message__title">
                                                                            <?php echo $row->subject ?>
                                                                        </div>
                                                                        <div class="message__expr">
                                                                            « <?php echo $row->smallmessage ?> »
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php

                                                            }
                                                        }?>
                                                        

                                                    </div>
                                                </div>                                     
                                            <div class="preview m_disable">
                                               <?php 
                                               if(is_array($datasolicitudes->rows_ultimo_mensaje )){
                                                  foreach($datasolicitudes->rows_ultimo_mensaje as $key =>$row){
                                                    if ($key === 0) {?>
                                                    
                                                    <div class="preview-top" id="<?php echo $row->idmensaje; ?>"  data-rowId = "<?php echo $row->useridfrom; ?>">
                                                        <div class="preview__title"><?php echo $row->subject; ?></div>
                                                    </div>
                                                    <div class="scrollable">
                                                        <div class="preview-content scrollable__target">
                                                <?php } ?>

                                                <div class="preview-respond">
                                                    <div class="preview-respond__head">
                                                        <div class="profile-head">
                                                            <div class="profile-head__id">

                                                                <?php echo (!empty($row->foto) && $row->foto > '') ? '<img class="profile-head__avatar"
                                                                src="'.get_assets_url().$row->foto.'"
                                                                alt="">' : '<img class="profile-head__avatar"
                                                                src="'.get_assets_url().'assets/img/img.png"
                                                                alt="">'; ?>
                                                                <div>
                                                                    <div class="profile-head__name"><?php echo $row->usuario; ?></div>
                                                                    <div class="profile-head__mail"><?php echo $row->correo_mensaje; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="date"><?php echo date( "d/m/Y H:i:s", $row->timecreate); ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="preview-respond__content">
                                                        <p class="paragraph"><?php echo $row->fullmensaje; ?></p>

                                                        <?php echo (!empty($row->file) && $row->file > '') ? '<a href="'.get_assets_url().'uploads/'.$row->file.'" download><b><i class="fa fa-paperclip"></i> '.$row->file.' </b></a>' : ''; ?> 
                                                    </div>
                                                </div>

                                                    <?php
                                                }
                                            }                          
                                            ?>
                                                    </div>
                                                </div>
                                                <div class="preview-foot">
                                                    <button class="new-mail__toggle btn preview-foot__button"><i class="fa fa-mail-reply"></i> Responder</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>                              
                                <?php  }else{ echo "<br><p>No hay mensajes</p><br><br>"; } ?>
                                    <!-- aqui termina -->

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br>
    <footer class="site-footer" role="contentinfo">
        <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
    </footer>
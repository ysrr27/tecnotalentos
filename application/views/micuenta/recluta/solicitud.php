<p class="load-circle disabled">
    <span class="ouro ouro3">
        <span class="left"><span class="anim"></span></span>
        <span class="right"><span class="anim"></span></span>
    </span>
</p>
<br><br><br>
<div class="my-goomo-shimmer_rec">
    <div class="mg-user-header_rec">
        <div class="wrp_rec">
            <div class="usr-img_rec gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_rec">
                <?php }else{ ?>
                    <div class="image-upload">
                      <label for="file-input">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_rec ">
                    </label>
                    <input id="file-input" type="file"/>
                </div>
                <?php } ?>
            </div>
        </div>
        <div class="wrp_rec_right">
            <div class="btn-group">
                <a href="<?php echo get_site_url("reclutas/solicitud")?>" title="CREAR SOLICITUD" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-edit"></i></a>
                <a href="<?php echo get_site_url("reclutas/buzon")?>" title="BANDEJA DE SOLICITUDES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-bullhorn"></i></a>
                <a href="<?php echo get_site_url("reclutas/correo")?>" title="MENSAJES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-envelope-o"></i></a>
            </div>
        </div>
    </div>
    <div class="main-wrap container">
        <div class="mg-shim-main">
            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-md-12 descripcionuser"><br>
                              <h1>PUBLICAR AVISO</h1>
                              <br><br><br>
                        </div>   
                    </div>

               

                     <div class="row">
                        <div class="col-md-offset-1 col-md-10 content-perfil-empresa">
                            <br><br>
                            <h1>CUÉNTANOS: ¿EN QUÉ PODEMOS AYUDARTE?</h1>
                            <div class="col-md-offset-1 col-md-10">
                                <br><br>
                                <form action="<?php echo get_site_url("reclutas/solicitudajax")?>" class="" id="solicitud" role="form" data-toggle="validator" name="solicitud" method="post" accept-charset="utf-8">
                                    <div id="form-step-0" role="form" data-toggle="validator">
                                    <div class="form-group row">
                                        <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Puesto a buscar*
                                        </label>
                                        <div class="col-md-9 col-sm-6 col-xs-12">
                                            <input type="hidden" name="cv_file" id="cv_file" value="">

                                            <input type="text" id="Puesto_buscar" required="required" name="Puesto_buscar" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div><br> <hr class="hr_gris">

                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <h4 for="Descripcion_puesto">DESCRIPCIÓN <br></h4> <br>
                                            <small>Detalle las tareas específicas asignadas al cargo:*</small>
                                            <textarea id="Descripcion_puesto" required="required" class="form-control" name="Descripcion_puesto"></textarea>
                                            <div class="help-block with-errors"></div>
                                        </div> 
                                    </div>
                                    <p class="text-center">- o -</p>
                                    <div id="text-file-solicitd" class="col-md-offset-4 col-md-4 alert alert-success alert-dismissible fade in" role="alert">
                                    <div class="custom-file">
                                        <input type="file" id="archivo" name="archivo_cv" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01" accept="application/pdf, .doc, .docx, .odf" style="display: none">
                                        <label for="archivo" id="text-file_cv">Subir Archivo 
                                           <img src="<?php echo get_assets_url();?>assets/img/PDF.png" width="30" class="bclick">
                                           <img src="<?php echo get_assets_url();?>assets/img/WORD.png" width="30" class="bclick">
                                       </label>
                                   </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">               
                                            <hr class="hr_gris">
                                            <h4 for="Descripcion_empresa">INFORMACIÓN REFERENCIAL DEL CARGO  <br></h4>
                                        </div> <br>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">País:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                               <input type="text" id="Pais" name="Pais" required="required" class="form-control col-md-7 col-xs-12">
                                               <div class="help-block with-errors"></div>
                                           </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Región:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" id="Region" name="Region" required="required" class="form-control col-md-7 col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Ciudad:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" id="Ciudad" name="Ciudad" required="required" class="form-control col-md-7 col-xs-12">
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div><br>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Jornada laboral:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                 <select id="Jornada_laboral" name="Jornada_laboral" class="form-control" required>
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Full Time">Full Time</option>
                                                    <option value="Part Time"> Part Time</option>
                                                    <option value="Pasantia">Pasantía</option>
                                                    <option value="Por horas">Por horas</option>
                                                    <option value="Teletrabajo">Teletrabajo</option>
                                                    <option value="Fines de semana">Fines de semana</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de contrato:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select id="Tipo_contrato" name="Tipo_contrato" class="form-control especifique_otros" required>
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Por obra">Por obra</option>
                                                    <option value="A tiempo determinado">A tiempo determinado</option>
                                                    <option value="A tiempo indeterminado">A tiempo indeterminado</option>
                                                    <option value="Otro">Otro</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div id="especifique_Tipo_contrato" class="form-group col-md-offset-1 col-md-11 disabled">
                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                                </label>
                                                <div class="col-md-10 col-sm-5 col-xs-12">
                                                    <input class="form-control col-md-7  col-sm-12 col-xs-12" type="text" id="Vacantes" name="especifique_Tipo_contrato"> 
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div><br><br><br>
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label for="Estado_empresa">Salario (neto mensual):*</label>
                                            <fieldset>
                                                <label for="name"></label>
                                                <div class="col-md-offset-1 col-md-11">
                                                    <div class="radiobutton">
                                                        <input type="radio" name="Salario" id="Moneda Nacional" value="Moneda Nacional" class="solvencia_inmueble" <?php echo (!empty($data->Salario) && $data->Salario == 'Moneda Nacional') ? 'checked' : ''; ?>  />
                                                        <label for="Moneda Nacional">Moneda Nacional</label>
                                                    </div>
                                                    <div class="radiobutton">
                                                        <input type="radio" name="Salario" id="Moneda Extranjera" value="Moneda Extranjera" class="solvencia_inmueble" <?php echo (!empty($data->estatus_actual) && $data->estatus_actual == 'Moneda Extranjera') ? 'checked' : ''; ?>/>
                                                        <label for="Moneda Extranjera">Moneda Extranjera</label>
                                                    </div>
                                                    <div class="radiobutton">
                                                        <input type="radio" name="Salario" id="Mixto" value="Mixto" class="solvencia_estatus_actual" <?php echo (!empty($data->estatus_actual) && $data->estatus_actual == 'Mixto') ? 'checked' : ''; ?>/>
                                                        <label for="Mixto">Mixto</label>
                                                    </div>
                                                </div>
                                            </fieldset>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <label class="control-label col-md-2 col-sm-2 col-xs-12">N° de Vacantes:*
                                        </label>
                                        <div class="col-md-5 col-sm-5 col-xs-12">
                                            <input class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" id="Vacantes" data-inputmask="'mask' : '999'" name="Vacantes"> 
                                            <div class="help-block with-errors"></div>
                                        </div><br><br><br>
                                    </div><hr class="hr_gris"/>

                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12"><hr class="hr_gris">
                                            <h4 for="Descripcion_empresa">CARACTERÍSTICAS DEL CANDIDATO IDEAL   <br></h4>
                                        </div> <br>
                                    </div>
                                     <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Jerarquía:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select id="tipo_contratoJerarquia" name="Jerarquia" class="form-control especifique_otros" required>
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Pasante">Pasante</option>
                                                    <option value="Junior">Junior</option>
                                                    <option value="Semi Senior">Semi Senior</option>
                                                    <option value="Senior">Senior</option>
                                                    <option value="Jefe/ Supervisor/ Coordinador">Jefe/ Supervisor/ Coordinador</option>
                                                    <option value="Gerente/Director">Gerente/Director</option>
                                                    <option value="Otro">Otro</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-4 col-sm-4 col-xs-12">Antigüedad en cargo o similares:*</label>
                                            <div class="col-md-8 col-sm-8 col-xs-12">
                                                <select class="form-control" name="Experiencia" id="Experiencia">
                                                    <option>- Seleccione -</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="o Más">o Más</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div id="especifique_tipo_contratoJerarquia" class="form-group col-md-offset-1 col-md-11 disabled">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                            </label>
                                            <div class="col-md-10 col-sm-5 col-xs-12">
                                                <input class="form-control col-md-7  col-sm-12 col-xs-12" type="text" id="Vacantes" name="especifique_tipo_contratoJerarquia"> 
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><br><br><br>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-2 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="first-name">Edad:*
                                            </label>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Min.</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" class="form-control" name="Edad_minima" id="Edad_minima" placeholder="" data-inputmask="'mask' : '99'" value="<?php echo (!empty($data->telefono_1) && $data->telefono_1 > '') ? $data->telefono_1 : ''; ?>" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Max.</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                  <input type="text" class="form-control" name="Edad_maxima" id="Edad_maxima" placeholder="" data-inputmask="'mask' : '99'"required>
                                                  <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Estudios Mínimos:</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select id="Estudios_minimos" name="Estudios_minimos" class="form-control" required>
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Basico">Básico</option>
                                                    <option value="Bachiller">Bachiller</option>
                                                    <option value="Estudiante universitario">Estudiante universitario</option>
                                                    <option value="TSU">TSU</option>
                                                    <option value="Universitario titulado">Universitario titulado</option>
                                                    <option value="Universitario titulado con estudios superiores">Universitario titulado con estudios superiores</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Idioma:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                 <select id="Idiomas_requerido" name="Idiomas_requerido" class="form-control" required>
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Espanol">Español</option>
                                                    <option value="Ingles">Inglés</option>
                                                    <option value="Frances">Frances</option>
                                                    <option value="Portugues">Portugués</option>
                                                    <option value="Italiano">Italiano</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="Nivel" id="Nivel">
                                                    <option>- Seleccione -</option>
                                                    <option value="Nativo">Nativo </option>
                                                    <option value="Avanzado">Avanzado </option>
                                                    <option value="Intermedio">Intermedio </option>
                                                    <option value="Basico">Básico</option>
                                                </select>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group row"><br><br>
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        <hr class="hr_gris">
                                            <h4 for="Descripcion_empresa">DESTREZAS Y CONOCIMIENTOS  <br></h4>
                                        </div> <br>
                                    </div>
                                    <div class="form-group col-md-offset-1 col-md-11">
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Área:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="Area" id="Area" onchange="cargos();">
                                                    <option value="">- Seleccione -</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel del Cargo:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="Nivel_cargo" id="cargo">
                                                  <option value="">- Seleccione -</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><br>
                                    <div id="disabled" class="form-group col-md-offset-1 col-md-11 disabled">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                        <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                        </label>
                                        <div class="col-md-10 col-sm-5 col-xs-12">
                                            <input class="date-picker form-control col-md-7 col-xs-12" type="text" id="Vacantes" name="Otro_cargo"> 
                                            <div class="help-block with-errors"></div>
                                        </div>
                                    </div><br><br><br>
                                    </div>
                                    <div id="especificacion" class="form-group col-md-offset-1 col-md-11 disabled">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Especificación:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="especificacion" id="especificacion">
                                                    <option>- Seleccione -</option>
                                                    <option value="Frontend">Frontend</option>
                                                    <option value="Backend">Backend</option>
                                                    <option value="Full Stack">Full Stack</option>
                                                </select>
                                            </div>
                                        </div><br><br>
                                    </div><br>
                                      <div class="form-group col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado de especialización:*</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="Grado_especializacion" id="Grado_especializacion">
                                                    <option>- Seleccione -</option>
                                                    <option value="Pasantías">Pasantías</option>
                                                    <option value="Junior">Junior</option>
                                                    <option value="Semisenior">Semisenior</option>
                                                    <option value="Programacion">Senior</option>
                                                </select>
                                            </div>
                                        </div><br><br>
                                    </div><br>
                                    <label for="tipo_establecimiento">Conocimientos Requeridos:*</label><br>
                                    <div id="output"></div>
                                    <div class="form-group col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <select id="Conocimientos_requeridos" data-placeholder="- Seleccione -" name="Conocimientos_requeridos[]" multiple class="chosen-select form-control especifique_otros">
                                                <option value="CSS">CSS</option>
                                                <option value="JavaScript">JavaScript</option>
                                                <option value="HTML">HTML</option>
                                                <option value="React">React</option>
                                                <option value="Redux">Redux</option>
                                                <option value="Angular">Angular</option>
                                                <option value="Bootstrap">Bootstrap</option>
                                                <option value="Foundation">Foundation</option>
                                                <option value="LESS">LESS</option>
                                                <option value="Sass">Sass</option>
                                                <option value="Stylus">Stylus</option>
                                                <option value="PostCSS">PostCSS</option>
                                                <option value="Python">Python</option>
                                                <option value="PHP">PHP</option>
                                                <option value="Ruby">Ruby</option>
                                                <option value="C#">C#</option>
                                                <option value="Java">Java</option>
                                                <option value="linux">linux</option>
                                                <option value="Flutter">Flutter</option>
                                                <option value="Kottlin (Android)">Kottlin (Android)</option>
                                                <option value="Git">Git</option>
                                                <option value="Django">Django</option>
                                                <option value="Go">Go</option>
                                                <option value="Ruby on Rails">Ruby on Rails</option>
                                                <option value="Azure Dep Ops">Azure Dep Ops</option>
                                                <option value="Mongo DB">Mongo DB</option>
                                                <option value="NodeJs">NodeJs</option>
                                                <option value="PostgreSql">PostgreSql</option>
                                                <option value="SpingBoot">SpingBoot</option>
                                                <option value="Maven Empaquedor Java">Maven Empaquedor Java</option>
                                                <option value="Jenkins Integración Continua">Jenkins Integración Continua</option>
                                                <option value="JSF - Java Framework">JSF - Java Framework</option>
                                                <option value="JSP - Java Framework">JSP - Java Framework</option>
                                                <option value="Rest API">Rest API</option>
                                                <option value="Soap UI">Soap UI</option>
                                                <option value="XML">XML</option>
                                                <option value="WSDL">WSDL</option>
                                                <option value="Docker">Docker</option>
                                                <option value="C++">C++</option>
                                                <option value="React Native">React Native</option>
                                                <option value="FlexBox Grid">FlexBox Grid</option>
                                                <option value="Typescript">Typescript</option>
                                                <option value="RXJS">RXJS</option>
                                                <option value="Pearl">Pearl</option>
                                                <option value="SQL Server">SQL Server</option>
                                                <option value="Windows Server">Windows Server</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div id="especifique_Conocimientos_requeridos" class="form-group col-md-offset-1 col-md-11 disabled">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                            </label>
                                            <div class="col-md-10 col-sm-5 col-xs-12">
                                                <input class="form-control col-md-7  col-sm-12 col-xs-12" type="text" name="especifique_Conocimientos_requeridos"> 
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><br><br><br>
                                    </div>
                                    <br><label for="tipo_establecimiento">Conocimientos Deseables:</label>
                                    <br>
                                    <div id="output"></div>
                                    <div class="form-group col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <select id="Conocimientos_deseables" data-placeholder="- Seleccione -" name="Conocimientos_deseables[]" multiple class="chosen-select form-control especifique_otros">
                                                <option value="CSS">CSS</option>
                                                <option value="JavaScript">JavaScript</option>
                                                <option value="HTML">HTML</option>
                                                <option value="React">React</option>
                                                <option value="Redux">Redux</option>
                                                <option value="Angular">Angular</option>
                                                <option value="Bootstrap">Bootstrap</option>
                                                <option value="Foundation">Foundation</option>
                                                <option value="LESS">LESS</option>
                                                <option value="Sass">Sass</option>
                                                <option value="Stylus">Stylus</option>
                                                <option value="PostCSS">PostCSS</option>
                                                <option value="Python">Python</option>
                                                <option value="PHP">PHP</option>
                                                <option value="Ruby">Ruby</option>
                                                <option value="C#">C#</option>
                                                <option value="Java">Java</option>
                                                <option value="linux">linux</option>
                                                <option value="Flutter">Flutter</option>
                                                <option value="Kottlin (Android)">Kottlin (Android)</option>
                                                <option value="Git">Git</option>
                                                <option value="Django">Django</option>
                                                <option value="Go">Go</option>
                                                <option value="Ruby on Rails">Ruby on Rails</option>
                                                <option value="Azure Dep Ops">Azure Dep Ops</option>
                                                <option value="Mongo DB">Mongo DB</option>
                                                <option value="NodeJs">NodeJs</option>
                                                <option value="PostgreSql">PostgreSql</option>
                                                <option value="SpingBoot">SpingBoot</option>
                                                <option value="Maven Empaquedor Java">Maven Empaquedor Java</option>
                                                <option value="Jenkins Integración Continua">Jenkins Integración Continua</option>
                                                <option value="JSF - Java Framework">JSF - Java Framework</option>
                                                <option value="JSP - Java Framework">JSP - Java Framework</option>
                                                <option value="Rest API">Rest API</option>
                                                <option value="Soap UI">Soap UI</option>
                                                <option value="XML">XML</option>
                                                <option value="WSDL">WSDL</option>
                                                <option value="Docker">Docker</option>
                                                <option value="C++">C++</option>
                                                <option value="React Native">React Native</option>
                                                <option value="FlexBox Grid">FlexBox Grid</option>
                                                <option value="Typescript">Typescript</option>
                                                <option value="RXJS">RXJS</option>
                                                <option value="Pearl">Pearl</option>
                                                <option value="SQL Server">SQL Server</option>
                                                <option value="Windows Server">Windows Server</option>
                                                <option value="Otro">Otro</option>
                                            </select>
                                        </div>
                                    </div>
                                    <br>
                                    <div id="especifique_Conocimientos_deseables" class="form-group col-md-offset-1 col-md-11 disabled">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                            </label>
                                            <div class="col-md-10 col-sm-5 col-xs-12">
                                                <input class="form-control col-md-7  col-sm-12 col-xs-12" type="text" name="especifique_Conocimientos_deseables"> 
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div><br><br><br>
                                    </div>
                                    <div class="form-group row">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                          <hr class="hr_gris">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Disponibilidad de viaje:</label>
                                            <div class="col-md-4 col-sm-4 col-xs-12">
                                                <select class="form-control" name="Disponibilidad_viaje" id="Disponibilidad_viaje">
                                                    <option value="">- Seleccione -</option>
                                                    <option value="Si">Si</option>
                                                    <option value="No">No</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div><br>                                        
                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-4">
                                            <button class="btn btn-primary" id="enviar_solicitud" type="button" name="Generar Solicitud">Generar Solicitud</button>
                                          <button class="btn btn-primary" type="reset">Cancelar</button>
                                      </div><br><br>
                                  </div>
                                  </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>
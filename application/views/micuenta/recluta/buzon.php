
<br><br><br>
<div class="my-goomo-shimmer_rec">
    <div class="mg-user-header_rec">
        <div class="wrp_rec">
            <div class="usr-img_rec gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_rec">
                <?php }else{ ?>
                    <div class="image-upload">
                      <label for="file-input">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_rec ">
                    </label>
                    <input id="file-input" type="file"/>
                </div>
            <?php } ?>
        </div>
    </div>
    <div class="wrp_rec_right">
        <div class="btn-group">
            <a href="<?php echo get_site_url("reclutas/solicitud")?>" title="PUBLICAR AVISO" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-edit"></i></a>
            <a href="<?php echo get_site_url("reclutas/buzon")?>" title="BANDEJA DE SOLICITUDES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-bullhorn"></i></a>
            <a href="<?php echo get_site_url("reclutas/correo")?>" title="MENSAJES" class="btn btn-default button-recluta-perfil" type="button"><i class="fa fa-envelope-o"></i></a>
        </div>
    </div>
</div>
<div class="main-wrap container">
    <div class="mg-shim-main">
        <div class="mg-rst">
            <div class="col-md-12 col-sm-12 col-xs-12">
               <div class="row">
                <div class="col-md-12 descripcionuser"><br>
                  <h1>BANDEJA DE SOLICITUDES</h1>
                  <br><br><br>
              </div>   
          </div>
          <div class="row">
            <div class="col-md-offset-1 col-md-10 content-perfil-empresa">
                <br><br>
                <div class="col-md-offset-1 col-md-10">
                    <br><br>
                    <div class="x_content">
                        <table id="table_solicitudes" class="table table-striped table-bordered">
                          <thead>
                            <tr>
                              <th>ID</th>
                              <th>TÍTULO DE SOLICITUD</th>
                              <th>FECHA</th>
                              <th>ESTADO DE SOLICITUD</th>
                              <th>ACCIONES</th>
                          </tr>
                      </thead>
                      <tbody>
                       <?php
                        if(is_array($dataperfil->rows )){
                          foreach(  $dataperfil->rows as $key =>$row){
                            ?>
                                <tr align="center">
                                  <td><?php echo $row->idsolicitud?></td>
                                  <td><?php echo $row->Puesto_buscar?></td>
                                  <td><?php echo  date('d/m/Y',$row->fecha)?></td>
                                  <td> 
                                    <!-- <img src="<?php echo get_assets_url();?>assets/img/<?php echo $row->estatus?>.png" width="25"> -->
                                     <img src="<?php echo get_assets_url();?>assets/img/<?php echo $row->estatus?>.png" width="200">

                                  </td>
                                  <td>     
                                    <div class="btn-group  btn-group-sm">
                                     <!--    <a href="<?php echo get_site_url('usuarios/editar/'.$row->idsolicitud)?>" class="" type="button"><i class="fa fa-pencil"></i></a> -->
                                        <button class="" name="Activar" data-toggle="tooltip" data-placement="right" id="<?php echo  $row->idsolicitud?>"><i class="fa fa-trash-o"></i></button> 
                                    </div>
                                </td>
                              </tr>
                            <?php
                           }
                        }                          
                        ?>
                  </tbody>
              </table>
          </div>
      </div>
  </div>
</div>
</div>
</div>
</div>
</div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>
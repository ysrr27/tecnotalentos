
<br><br><br>
<div class="my-goomo-shimmer">
    <div class="mg-user-header">
        <div class="wrp">
            <div class="usr-img gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar">
                <?php }else{ ?>

                    <div class="image-upload">

                      <label for="file-input">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar ">
                    </label>

                    <input id="file-input" type="file"/>
                </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="main-wrap">
        <div class="mg-shim-main">
            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                     <div class="row">
                        <div class="col-md-12 descripcionuser">
                            <p><?php echo $nombre." ".$apellido ?></p>
                            <p>Fecha de Nacimiento: AAAA-MM-DD</p>
                            <p>Ubicación Actual:Av.Lorem Ipsum, Edif. Ipsum, Caracas</p>
                            <p>Teléfono: (+58)414-000 00 00</p>
                            <p>Email: <?php echo $correo ?></p><br>
                     <!--        <div class="col-md-offset-5 col-md-7">
                                <button type="button" class="btn btn-default btn-lg text-center">Descargar CV        <i class="fa fa-file-pdf-o btn-dow-d"></i> <i class="fa fa-file-word-o btn-dow-d"></i></button>
                            </div> -->
                            <div class="col-md-offset-4 col-md-4 alert alert-success alert-dismissible fade in" role="alert">
                                <span class="dscv">Descargar CV</span>
                                <a href="">
                                     <!-- <i class="fa fa-file-pdf-o btn-dow-d"></i> -->
                                     <img src="<?php echo get_assets_url();?>assets/img/PDF.png" width="30">
                                </a>
                                <a href="">
                                    <img src="<?php echo get_assets_url();?>assets/img/WORD.png" width="30">
                                </a>
                            </div>
                        </div>   
                    </div>
                     <div class="row">
                        <div class="col-md-12 content-cv"> <hr>
                            <div class="col-md-6">
                                <h4>ACERCA DE</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                                    Nunc et felis quis nulla facilisis aliquet suscipit id risus. In
                                    congue tellus nec libero finibus condimentum nisi gravida.
                                    Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed
                                diam nonummy nibh euismod tincidunt ut </p>
                                <br>
                                <h4>EDUCACIÓN</h4>
                                <div class="dashboard-widget-content">

                                    <ul class="list-unstyled timeline widget">
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <span class="count_top">2009 - 2012</span>
                                                    <h2 class="title text-left">
                                                        Licenciado por Lorem Ipsum
                                                    </h2>
                                                    <p class="excerpt text-left">Universidad de Lorem Ipsum
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <span class="count_top">2009 - 2012</span>
                                                    <h2 class="title text-left">
                                                        Licenciado por Lorem Ipsum
                                                    </h2>
                                                    <p class="excerpt text-left">Universidad de Lorem Ipsum
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <span class="count_top">2009 - 2012</span>
                                                    <h2 class="title text-left">
                                                        Licenciado por Lorem Ipsum
                                                    </h2>
                                                    <p class="excerpt text-left">Universidad de Lorem Ipsum
                                                    </p>
                                                </div>
                                            </div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 timeline">
                                <div class="block_experiencia">

                                <h4>EXPERIENCA</h4>
                                <br>
                                <span class="count_top">2019 - Presente</span>
                                <h3 class="text-left">Nombre de la empresa</h3>
                                <span>Nombre/función del cargo</span>
                                <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                                    et felis quis nulla facilisis aliquet suscipit id risus. In congue
                                tellus nec libero finibus condimentum nisi gravida.</p>
                                <br>
                                <span class="count_top">2019 - Presente</span>
                                <h3 class="text-left">Nombre de la empresa</h3>
                                <span>Nombre/función del cargo</span>
                                <p class="text-left">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                                    et felis quis nulla facilisis aliquet suscipit id risus. In congue
                                tellus nec libero finibus condimentum nisi gravida.</p>
                                <p class="text-left">
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc
                                    et felis quis nulla facilisis aliquet suscipit id risus. In congue
                                </p>

                            </div>
                            </div>
                        </div>
                    </div><hr>
                    <div class="row content-cv">
                        <div class="col-md-12">
                            <h4 class="text-center">IDIOMAS</h4>
                            <div class="row">
                                <div class="col-md-4">
                                     <h3 class="text-left">Español</h3>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                    <h3 class="text-left">Ingles</h3>
                                    <div class="progress progress_sm">
                                        <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="70"></div>
                                    </div>
                                </div>
                                <div class="col-md-4">
                                 <h3 class="text-left">Chino Mandarín</h3>
                                 <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="30"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- <footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer> -->
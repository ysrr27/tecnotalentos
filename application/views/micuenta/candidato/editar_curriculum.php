<br><br><br>
<div class="my-goomo-shimmer">
    <div class="mg-user-header">
        <div class="wrp">
            <div class="usr-img gm_animated-background">
                <form id="form1" runat="server">
                    <div class="alert ed_cv-alr"></div>
                    <div id='img_contain'>
                        <?php if (!empty($foto)) { ?>
                            <img id="blah" align='middle' src="<?php echo get_assets_url().$foto;?>" alt="Imagen de perfil" title='' style="width: 200px !important;height: 200px !important;max-height: 200px !important;"/>
                        <?php }else{ ?>
                            <img id="blah" align='middle' src="<?php echo get_assets_url();?>assets/img/img.png" alt="Imagen de perfil" title='' style="width: 200px !important;height: 200px !important;max-height: 200px !important;"/>
                        <?php } ?>
                        <small>Foto (200 x 200)px</small>
                    </div> 
                    <div class="input-group"> 
                        <div class="custom-file">
                            <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                            <label class="forms_buttons-action btn-cambiar-foto" for="inputGroupFile01"><i class="fa fa-image"></i></label>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <br><br>
    <form action="<?php echo get_site_url("candidatos/candatoajax")?>" id="formcandidato" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
        <div class="main-wrap">
            <div class="mg-shim-main">
                <div class="mg-rst">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="row p_usrm">
                            <div class="col-md-12  col-sm-12 col-xs-12 descripcionuser">

                                <input type="hidden" name="idcandidato" id="idcandidato" value="<?php echo (!empty($datacandidatos->rows->idcandidato) && $datacandidatos->rows->idcandidato > '') ? $datacandidatos->rows->idcandidato : ''; ?>">
                                <input type="hidden" name="idexperiencia" id="idexperiencia" value="<?php echo (!empty($datacandidatos->rows->idexperiencia) && $datacandidatos->rows->idexperiencia > '') ? $datacandidatos->rows->idexperiencia : ''; ?>">
                                <input type="hidden" name="iddatospersonales" id="iddatospersonales" value="<?php echo (!empty($datacandidatos->rows->iddatospersonales) && $datacandidatos->rows->iddatospersonales > '') ? $datacandidatos->rows->iddatospersonales : ''; ?>">
                                <input type="hidden" name="ideducacion" id="ideducacion" value="<?php echo (!empty($datacandidatos->rows->ideducacion) && $datacandidatos->rows->ideducacion > '') ? $datacandidatos->rows->ideducacion : ''; ?>">


                                <fieldset class="forms_fieldset">
                                    <div class="col-md-offset-5 col-md-7 col-sm-6 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <input type="text" placeholder="Nombre" class="forms_field-input" name="Nombre" id="Nombre" required autofocus value="<?php echo (!empty($datacandidatos->rows->Nombre) && $datacandidatos->rows->Nombre > '') ? $datacandidatos->rows->Nombre : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="col-md-offset-5 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <input type="text" placeholder="Apellido" class="forms_field-input" name="Apellido" id="Apellido" required autofocus value="<?php echo (!empty($datacandidatos->rows->Apellido) && $datacandidatos->rows->Apellido > '') ? $datacandidatos->rows->Apellido : ''; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-5 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <input type="text" placeholder="Fecha nacimiento" class="forms_field-input" name="Fecha_nacimiento" id="Fecha_nacimiento" required autofocus data-inputmask="'mask': '99/99/9999'" value="<?php echo (!empty($datacandidatos->rows->Fecha_nacimiento) && $datacandidatos->rows->Fecha_nacimiento > '') ? $datacandidatos->rows->Fecha_nacimiento : ''; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-5 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <textarea name="Direccion"  id="Direccion" class="orms_field-input" rows="10" cols="50" style="background: transparent;border: 1px solid #efeded;">Dirección: <?php echo (!empty($datacandidatos->rows->Direccion) && $datacandidatos->rows->Direccion > '') ? $datacandidatos->rows->Direccion : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-5 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <input type="text" placeholder="Teléfono" class="forms_field-input" name="Telefono" id="correo_session" required autofocus value="<?php echo (!empty($datacandidatos->rows->Telefono) && $datacandidatos->rows->Telefono > '') ? $datacandidatos->rows->Telefono : ''; ?>"/>
                                        </div>
                                    </div>
                                    <div class="col-md-offset-5 col-md-6 col-sm-12 col-xs-12 form-group">
                                        <div class="forms_field">
                                            <input type="text" placeholder="Email" class="forms_field-input" name="Correo" id="Correo" required autofocus  data-inputmask-alias="email" data-val="true" data-val-required="Required" value="<?php echo (!empty($datacandidatos->rows->Correo) && $datacandidatos->rows->Correo > '') ? $datacandidatos->rows->Correo : ''; ?>" />
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="pull-right">
                                    <a href="#." class="btn_edit">
                                        <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                    </a>
                                    <a href="#" class="save_curriculum">
                                        <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                    </a>
                                </div>
                                <br>
                            </div>   
                        </div>
                        <div class="row">
                            <div class="col-md-12 content-cv"> <hr>
                                <div class="col-md-6">
                                    <h4>ACERCA DE MI</h4>
                                    <div class="textarea" id="textarea">
                                        <textarea id="Acerca_de_mi" required="required" class="form-control" name="Acerca_de_mi" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="" data-parsley-validation-threshold="10"><?php echo (!empty($datacandidatos->rows->Acerca_de_mi) && $datacandidatos->rows->Acerca_de_mi > '') ? $datacandidatos->rows->Acerca_de_mi : ''; ?>
                                    </textarea>
                                </div>
                                <div class="pull-right">
                                    <a href="#." class="btn_edit">
                                        <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                    </a>
                                    <a href="#" class="save_curriculum">
                                        <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                    </a>
                                </div><br><br>
                                <br>
                                <h4>EDUCACIÓN</h4>
                                <div class="dashboard-widget-content">
                                    <ul class="list-unstyled timeline widget">
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="desde1" id="desde1">
                                                                <?php echo (!empty($datacandidatos->rows->desde1) && $datacandidatos->rows->desde1 > '') ? "<option value='".$datacandidatos->rows->desde1."' selected>".$datacandidatos->rows->desde1."</option>" : ''; 
                                                                $selectyear .= '<option value="'.date("Y").'">'.date("Y").'</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="hasta1" id="hasta1">
                                                                <?php echo (!empty($datacandidatos->rows->hasta1) && $datacandidatos->rows->hasta1 > '') ? "<option value='".$datacandidatos->rows->hasta1."' selected>".$datacandidatos->rows->hasta1."</option>" : '';  
                                                                $selectyear2 .= '<option value="Presente">Presente</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear2 .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear2 .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear2;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div><br>
                                                    <h2 class="title text-left">
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Titulo" name="titulo1" id="titulo1" value="<?php echo (!empty($datacandidatos->rows->titulo1) && $datacandidatos->rows->titulo1 > '') ? $datacandidatos->rows->titulo1 : ''; ?>">
                                                        </div>
                                                    </h2>
                                                    <p class="excerpt text-left"><br>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Institución" name="institucion1" id="institucion1" value="<?php echo (!empty($datacandidatos->rows->institucion1) && $datacandidatos->rows->institucion1 > '') ? $datacandidatos->rows->institucion1 : ''; ?>">
                                                        </div>
                                                    </p>
                                                </div><br><br>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="desde2" id="desde2">
                                                                <?php echo (!empty($datacandidatos->rows->desde2) && $datacandidatos->rows->desde2 > '') ? "<option value='".$datacandidatos->rows->desde2."' selected>".$datacandidatos->rows->desde2."</option>" : ''; 
                                                                $selectyear3 .= '<option value="'.date("Y").'">'.date("Y").'</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear3 .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear3 .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear3;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="hasta2" id="hasta2">
                                                                <?php echo (!empty($datacandidatos->rows->hasta2) && $datacandidatos->rows->hasta2 > '') ? "<option value='".$datacandidatos->rows->hasta2."' selected>".$datacandidatos->rows->hasta2."</option>" : ''; 
                                                                $selectyear4 .= '<option value="Presente">Presente</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear4 .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear4 .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear4;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div><br>
                                                    <h2 class="title text-left">
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Titulo" name="titulo2" id="titulo2"  value="<?php echo (!empty($datacandidatos->rows->titulo2) && $datacandidatos->rows->titulo2 > '') ? $datacandidatos->rows->titulo2 : ''; ?>">
                                                        </div>
                                                    </h2>
                                                    <p class="excerpt text-left"><br>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Institución" name="institucion2" id="institucion2" value="<?php echo (!empty($datacandidatos->rows->institucion2) && $datacandidatos->rows->institucion2 > '') ? $datacandidatos->rows->institucion2 : ''; ?>">
                                                        </div>
                                                    </p>
                                                </div><br><br>
                                            </div>
                                        </li>
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <div class="row">
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="desde3" id="desde3">
                                                                <?php echo (!empty($datacandidatos->rows->desde3) && $datacandidatos->rows->desde3 > '') ? "<option value='".$datacandidatos->rows->desde3."' selected>".$datacandidatos->rows->desde3."</option>" : ''; 
                                                                $selectyear5 .= '<option value="'.date("Y").'">'.date("Y").'</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear5 .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear5 .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear5;
                                                                ?>
                                                            </select>
                                                        </div>
                                                        <div class="col-md-4">
                                                            <select class="form-control" name="hasta3" id="hasta3">
                                                                <?php echo (!empty($datacandidatos->rows->hasta3) && $datacandidatos->rows->hasta3 > '') ? "<option value='".$datacandidatos->rows->hasta3."' selected>".$datacandidatos->rows->hasta3."</option>" : '';
                                                                $selectyear6 .= '<option value="Presente">Presente</option>';
                                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                                    if($year == $years){
                                                                        $selectyear6 .=  "<option value='$years' selected>$years</option>";
                                                                    }else{
                                                                        $selectyear6 .=  "<option value='$years'>$years</option>";
                                                                    }
                                                                }  
                                                                echo $selectyear6;
                                                                ?>
                                                            </select>
                                                        </div>
                                                    </div><br>
                                                    <h2 class="title text-left">
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Titulo" name="titulo3" id="titulo3" value="<?php echo (!empty($datacandidatos->rows->titulo3) && $datacandidatos->rows->titulo3 > '') ? $datacandidatos->rows->titulo3 : ''; ?>">
                                                        </div>
                                                    </h2>
                                                    <p class="excerpt text-left"><br>
                                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                                            <input type="text" class="form-control" placeholder="Institución" name="institucion3" id="institucion3" value="<?php echo (!empty($datacandidatos->rows->institucion3) && $datacandidatos->rows->institucion3 > '') ? $datacandidatos->rows->institucion3 : ''; ?>">
                                                        </div>
                                                    </p>
                                                </div>
                                            </div>  
                                            <div class="pull-right bt_stud">
                                                <a href="#." data-name="name" data-type="text"  class="btn_edit" data-pk="randomID+">
                                                    <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                                </a>
                                                <a href="#" class="save_curriculum">
                                                    <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                                </a>
                                            </div><br><br>
                                            <br>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-md-6 timeline">
                                <div class="block_experiencia">

                                    <h4>EXPERIENCIA</h4>
                                    <br>
                                    <div class="row">
                                        <div class="col-md-4">
                                            <select class="form-control" name="e_desde1" id="e_desde1">
                                                <?php echo (!empty($datacandidatos->rows->e_desde1) && $datacandidatos->rows->e_desde1 > '') ? "<option value='".$datacandidatos->rows->e_desde1."' selected>".$datacandidatos->rows->e_desde1."</option>" : ''; 
                                                $selectyearempresa .= '<option value="'.date("Y").'">'.date("Y").'</option>';
                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                    if($year == $years){
                                                        $selectyearempresa .=  "<option value='$years' selected>$years</option>";
                                                    }else{
                                                        $selectyearempresa .=  "<option value='$years'>$years</option>";
                                                    }
                                                }  
                                                echo $selectyearempresa;
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="form-control" name="e_hasta1" id="e_hasta1">
                                                <?php echo (!empty($datacandidatos->rows->e_hasta1) && $datacandidatos->rows->e_hasta1 > '') ? "<option value='".$datacandidatos->rows->e_hasta1."' selected>".$datacandidatos->rows->e_hasta1."</option>" : ''; 
                                                $selectyearempresa2 .= '<option value="Presente">Presente</option>';
                                                for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                    if($year == $years){
                                                        $selectyearempresa2 .=  "<option value='$years' selected>$years</option>";
                                                    }else{
                                                        $selectyearempresa2 .=  "<option value='$years'>$years</option>";
                                                    }
                                                }  
                                                echo $selectyearempresa2;
                                                ?>
                                            </select>
                                        </div>
                                    </div><br>
                                    <h3 class="text-left">
                                        <div class="col-md-9 col-sm-9 col-xs-12">
                                            <input type="text" class="form-control" placeholder="Nombre de la empresa" name="empresa1" id="empresa1" value="<?php echo (!empty($datacandidatos->rows->empresa1) && $datacandidatos->rows->empresa1 > '') ? $datacandidatos->rows->empresa1 : ''; ?>">
                                        </div><br>
                                    </h3>
                                    <span><div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" placeholder="función del cargo" name="funcion_cargo1" id="funcion_cargo1" value="<?php echo (!empty($datacandidatos->rows->funcion_cargo1) && $datacandidatos->rows->funcion_cargo1 > '') ? $datacandidatos->rows->funcion_cargo1 : ''; ?>">
                                    </div><br></span>
                                    <p class="text-left"><br>
                                        <textarea id="descripcion_cargo1" required="required" class="form-control" name="descripcion_cargo1" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="" data-parsley-validation-threshold="10"><?php echo (!empty($datacandidatos->rows->descripcion_cargo1) && $datacandidatos->rows->descripcion_cargo1 > '') ? $datacandidatos->rows->descripcion_cargo1 : 'Descripción'; ?>
                                    </textarea>
                                </p>
                                <br><br><br>
                                <div class="row">
                                    <div class="col-md-4">
                                        <select class="form-control" name="e_desde2" id="e_desde2">
                                            <?php echo (!empty($datacandidatos->rows->e_desde2) && $datacandidatos->rows->e_desde2 > '') ? "<option value='".$datacandidatos->rows->e_desde2."' selected>".$datacandidatos->rows->e_desde2."</option>" : ''; 
                                            $selectyearempresa3 .= '<option value="'.date("Y").'">'.date("Y").'</option>';
                                            for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                if($year == $years){
                                                    $selectyearempresa3 .=  "<option value='$years' selected>$years</option>";
                                                }else{
                                                    $selectyearempresa3 .=  "<option value='$years'>$years</option>";
                                                }
                                            }  
                                            echo $selectyearempresa3;
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <select class="form-control" name="e_hasta2" id="e_hasta2">
                                            <?php echo (!empty($datacandidatos->rows->e_hasta2) && $datacandidatos->rows->e_hasta2 > '') ? "<option value='".$datacandidatos->rows->e_hasta2."' selected>".$datacandidatos->rows->e_hasta2."</option>" : ''; 
                                            $selectyearempresa4 .= '<option value="Presente">Presente</option>';
                                            for ($years=1990; $years<=date("Y",strtotime(date("Y")."- 1 year")); $years++){
                                                if($year == $years){
                                                    $selectyearempresa4 .=  "<option value='$years' selected>$years</option>";
                                                }else{
                                                    $selectyearempresa4 .=  "<option value='$years'>$years</option>";
                                                }
                                            }  
                                            echo $selectyearempresa4;
                                            ?>
                                        </select>
                                    </div>
                                </div><br>
                                <h3 class="text-left">
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="text" class="form-control" placeholder="Nombre de la empresa" name="empresa2" id="empresa2" value="<?php echo (!empty($datacandidatos->rows->empresa2) && $datacandidatos->rows->empresa2 > '') ? $datacandidatos->rows->empresa2 : ''; ?>">
                                    </div><br>
                                </h3>
                                <span><div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" class="form-control" placeholder="función del cargo" name="funcion_cargo2" id="funcion_cargo2" value="<?php echo (!empty($datacandidatos->rows->funcion_cargo2) && $datacandidatos->rows->funcion_cargo2 > '') ? $datacandidatos->rows->funcion_cargo2 : ''; ?>">
                                </div><br></span>
                                <p class="text-left"><br>
                                    <textarea id="descripcion_cargo2" required="required" class="form-control" name="descripcion_cargo2" data-parsley-trigger="keyup" data-parsley-minlength="20" data-parsley-maxlength="100" data-parsley-minlength-message="" data-parsley-validation-threshold="10"><?php echo (!empty($datacandidatos->rows->descripcion_cargo2) && $datacandidatos->rows->descripcion_cargo2 > '') ? $datacandidatos->rows->descripcion_cargo2 : 'Descripción'; ?>
                                </textarea>
                            </p>
                            <div class="pull-right">
                                <a href="#." data-name="name" data-type="text"  class="btn_edit" data-pk="randomID+">
                                    <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                </a>
                                <a href="#" class="save_curriculum">
                                    <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                </a>
                            </div><br><br>
                            <br>
                        </div>
                    </div>
                </div>
            </div><hr>
            <div class="row content-cv">
                <div class="col-md-12">
                    <h4 class="text-center">IDIOMAS</h4>
                    <div class="row">
                        <div class="col-md-4">
                            <h3 class="text-left">Seleccione el idioma</h3>
                            <select class="form-control" name="Idiomas_requerido" id="Idiomas_requerido">
                                <?php echo (!empty($datacandidatos->rows->Idiomas_requerido) && $datacandidatos->rows->Idiomas_requerido > '') ? "<option value='".$datacandidatos->rows->Idiomas_requerido."' selected>".$datacandidatos->rows->Idiomas_requerido."</option>" : ''; ?>
                                <option value="">- Seleccione -</option>
                                <option value="Espanol">Español</option>
                                <option value="Ingles">Inglés</option>
                                <option value="Frances">Frances</option>
                                <option value="Portugues">Portugués</option>
                                <option value="Italiano">Italiano</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <h3 class="text-left">Nivel</h3>
                            <select class="form-control" name="Nivel" id="Nivel">
                                <?php echo (!empty($datacandidatos->rows->Nivel) && $datacandidatos->rows->Nivel > '') ? "<option value='".$datacandidatos->rows->Nivel."' selected>".$datacandidatos->rows->Nivel."</option>" : ''; ?>
                                <option>- Seleccione -</option>
                                <option value="Nativo">Nativo </option>
                                <option value="Avanzado">Avanzado </option>
                                <option value="Intermedio">Intermedio </option>
                                <option value="Basico">Básico</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            <div class="">
                                <h3 class="text-left">.</h3>

                                <a href="#." class="btn_edit">
                                    <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                </a>
                                <a href="#" class="save_curriculum">
                                    <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                </a>
                            </div><br><br>
                        </div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row content-cv">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h4 class="text-center">CONOCIMIENTOS Y HABILIDADES</h4>
                    <div class="row">
                        <div class="form-group col-md-offset-1 col-md-11">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Área:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="Area" id="Area" onchange="cargos();">
                                        <?php echo (!empty($datacandidatos->rows->Area) && $datacandidatos->rows->Area > '') ? "<option value='".$datacandidatos->rows->Area."' selected>".$datacandidatos->rows->Area."</option>" : ''; ?>
                                        <option value="">- Seleccione -</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Nivel del Cargo:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="Nivel_cargo" id="cargo">
                                        <?php echo (!empty($datacandidatos->rows->Nivel_cargo) && $datacandidatos->rows->Nivel_cargo > '') ? "<option value='".$datacandidatos->rows->Nivel_cargo."' selected>".$datacandidatos->rows->Nivel_cargo."</option>" : ''; ?>
                                        <option value="">- Seleccione -</option>
                                    </select>
                                </div>
                            </div>
                        </div><br>
                        <div id="disabled" class="form-group col-md-offset-1 col-md-11 disabled">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                </label>
                                <div class="col-md-10 col-sm-5 col-xs-12">
                                    <input class="date-picker form-control col-md-7 col-xs-12" type="text" id="Otro_cargo" name="Otro_cargo" value="<?php echo (!empty($datacandidatos->rows->Otro_cargo) && $datacandidatos->rows->Otro_cargo > '') ? $datacandidatos->rows->Otro_cargo : ''; ?>"> 
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div><br><br><br>
                        </div>
                        <div id="especificacion" class="form-group col-md-offset-1 col-md-11 disabled">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Especificación:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="especificacion" id="especificacion">
                                        <?php echo (!empty($datacandidatos->rows->especificacion) && $datacandidatos->rows->especificacion > '') ? "<option value='".$datacandidatos->rows->especificacion."' selected>".$datacandidatos->rows->especificacion."</option>" : ''; ?>
                                        <option>- Seleccione -</option>
                                        <option value="Frontend">Frontend</option>
                                        <option value="Backend">Backend</option>
                                        <option value="Full Stack">Full Stack</option>
                                    </select>
                                </div>
                            </div><br><br>
                        </div><br>
                        <div class="form-group col-md-offset-1 col-md-11 col-sm-12 col-xs-12">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Grado de especialización:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select class="form-control" name="Grado_especializacion" id="Grado_especializacion">
                                        <?php echo (!empty($datacandidatos->rows->Grado_especializacion) && $datacandidatos->rows->Grado_especializacion > '') ? "<option value='".$datacandidatos->rows->Grado_especializacion."' selected>".$datacandidatos->rows->Grado_especializacion."</option>" : ''; ?>
                                        <option>- Seleccione -</option>
                                        <option value="Pasantías">Pasantías</option>
                                        <option value="Junior">Junior</option>
                                        <option value="Semisenior">Semisenior</option>
                                        <option value="Programacion">Senior</option>
                                    </select>
                                </div>
                            </div><br><br>
                        </div><br>
                        <br>
                        <div class="form-group col-md-offset-1 col-md-11">
                            <label class="control-label col-md-2 col-sm-2 col-xs-12">Conocimientos y habilidades adquiridas:*
                            </label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <input class="date-picker form-control col-md-7 col-xs-12" required="required" type="text" id="Conocimientos_habilidades" name="Conocimientos_habilidades" value="<?php echo (!empty($datacandidatos->rows->Conocimientos_habilidades) && $datacandidatos->rows->Conocimientos_habilidades > '') ? $datacandidatos->rows->Conocimientos_habilidades : ''; ?>"> 
                                <div class="help-block with-errors"></div>
                            </div><br><br><br>
                        </div>
                        <div class="pull-right">
                            <a href="#." data-name="name" data-type="text"  class="btn_edit" data-pk="randomID+">
                                <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                            </a>
                            <a href="#" class="save_curriculum">
                                <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                            </a>
                        </div><br><br>
                        <br>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row content-cv">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <h4 class="text-center">CUENTANOS, SEGÚN TUS CONOCIMIENTOS Y HABILIDADES,<br><br> ¿CÓMO SERÍA EL EMPLEO DE TUS SUEÑOS?</h4><br>
                    <div class="row">
                        <div class="form-group col-md-11">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">País:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="Pais" name="Pais" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo (!empty($datacandidatos->rows->Pais) && $datacandidatos->rows->Pais > '') ? $datacandidatos->rows->Pais : ''; ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Región:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="Region" name="Region" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo (!empty($datacandidatos->rows->Region) && $datacandidatos->rows->Region > '') ? $datacandidatos->rows->Region : ''; ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-11">
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Ciudad:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <input type="text" id="Ciudad" name="Ciudad" required="required" class="form-control col-md-7 col-xs-12" value="<?php echo (!empty($datacandidatos->rows->Ciudad) && $datacandidatos->rows->Ciudad > '') ? $datacandidatos->rows->Ciudad : ''; ?>">
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                        </div><br>
                        <div class="form-group col-md-11">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jornada laboral:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select id="Jornada_laboral" name="Jornada_laboral" class="form-control" required>
                                        <?php echo (!empty($datacandidatos->rows->Jornada_laboral) && $datacandidatos->rows->Jornada_laboral > '') ? "<option value='".$datacandidatos->rows->Jornada_laboral."' selected>".$datacandidatos->rows->Jornada_laboral."</option>" : ''; ?>
                                        <option value="">- Seleccione -</option>
                                        <option value="Full Time">Full Time</option>
                                        <option value="Part Time"> Part Time</option>
                                        <option value="Pasantia">Pasantía</option>
                                        <option value="Por horas">Por horas</option>
                                        <option value="Teletrabajo">Teletrabajo</option>
                                        <option value="Fines de semana">Fines de semana</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Tipo de contrato:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select id="Tipo_contrato" name="Tipo_contrato" class="form-control especifique_otros" required>
                                        <?php echo (!empty($datacandidatos->rows->Tipo_contrato) && $datacandidatos->rows->Tipo_contrato > '') ? "<option value='".$datacandidatos->rows->Tipo_contrato."' selected>".$datacandidatos->rows->Tipo_contrato."</option>" : ''; ?>
                                        <option value="">- Seleccione -</option>
                                        <option value="Por obra">Por obra</option>
                                        <option value="A tiempo determinado">A tiempo determinado</option>
                                        <option value="A tiempo indeterminado">A tiempo indeterminado</option>
                                        <option value="Otro">Otro</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div id="especifique_Tipo_contrato" class="form-group col-md-offset-1 col-md-11 disabled">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    <label class="control-label col-md-2 col-sm-3 col-xs-12">Especifíque:*
                                    </label>
                                    <div class="col-md-10 col-sm-5 col-xs-12">
                                        <input class="form-control col-md-7  col-sm-12 col-xs-12" type="text" id="especifique_Tipo_contrato" name="especifique_Tipo_contrato" value="<?php echo (!empty($datacandidatos->rows->especifique_Tipo_contrato) && $datacandidatos->rows->especifique_Tipo_contrato > '') ? $datacandidatos->rows->especifique_Tipo_contrato : ''; ?>"> 
                                        <div class="help-block with-errors"></div>
                                    </div>
                                </div><br><br><br>
                            </div>
                        </div><br>
                        <div class="form-group row">
                            <div id="output"></div>
                            <div class="col-md-12 col-sm-12 col-xs-12"><br>
                                <label for="Estado_empresa" style="margin-left: 30px">Salario (neto mensual):*</label>
                                <fieldset>
                                    <label for="name"></label>
                                    <div class="col-md-offset-1 col-md-11">
                                        <div class="radiobutton">
                                            <input type="radio" name="Salario" id="Moneda Nacional" value="Moneda Nacional" class="solvencia_inmueble" <?php echo (!empty($datacandidatos->rows->Salario) && $datacandidatos->rows->Salario == 'Moneda Nacional') ? 'checked' : ''; ?>  />
                                            <label for="Moneda Nacional">Moneda Nacional</label>
                                        </div>
                                        <div class="radiobutton">
                                            <input type="radio" name="Salario" id="Moneda Extranjera" value="Moneda Extranjera" class="solvencia_inmueble" <?php echo (!empty($datacandidatos->rows->Salario) && $datacandidatos->rows->Salario == 'Moneda Extranjera') ? 'checked' : ''; ?>/>
                                            <label for="Moneda Extranjera">Moneda Extranjera</label>
                                        </div>
                                        <div class="radiobutton">
                                            <input type="radio" name="Salario" id="Mixto" value="Mixto" class="solvencia_estatus_actual" <?php echo (!empty($datacandidatos->rows->Salario) && $datacandidatos->rows->Salario == 'Mixto') ? 'checked' : ''; ?>/>
                                            <label for="Mixto">Mixto</label>
                                        </div>
                                    </div>
                                </fieldset>
                                <div class="help-block with-errors"></div>
                            </div>
                        </div>
                        <div class="form-group col-md-12" style="margin-left: 10px">
                            <label class="control-label col-md-1 col-sm-2 col-xs-12">Monto:*
                            </label>
                            <div class="col-md-5 col-sm-5 col-xs-12">
                                <input class="date-picker form-control col-md-7 col-xs-12" required="required" type="number" id="Monto" name="Monto" value="<?php echo (!empty($datacandidatos->rows->Monto) && $datacandidatos->rows->Monto > '') ? $datacandidatos->rows->Monto : ''; ?>"> 
                                <div class="help-block with-errors"></div>
                            </div><br><br><br>
                        </div>
                        <div class="form-group col-md-12">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Jerarquía:*</label>
                                <div class="col-md-9 col-sm-9 col-xs-12">
                                    <select id="tipo_contratoJerarquia" name="Jerarquia" class="form-control especifique_otros" required>
                                        <?php echo (!empty($datacandidatos->rows->Jerarquia) && $datacandidatos->rows->Jerarquia > '') ? "<option value='".$datacandidatos->rows->Tipo_contrato."' selected>".$datacandidatos->rows->Jerarquia."</option>" : ''; ?>
                                        <option value="">- Seleccione -</option>
                                        <option value="Pasante">Pasante</option>
                                        <option value="Junior">Junior</option>
                                        <option value="Semi Senior">Semi Senior</option>
                                        <option value="Senior">Senior</option>
                                        <option value="Jefe/ Supervisor/ Coordinador">Jefe/ Supervisor/ Coordinador</option>
                                        <option value="Gerente/Director">Gerente/Director</option>
                                        <option value="Otro">Otro</option>
                                    </select>
                                    <div class="help-block with-errors"></div>
                                </div>
                            </div>
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <label class="control-label col-md-4 col-sm-4 col-xs-12">Antigüedad en cargo o similares:*</label>
                                <div class="col-md-8 col-sm-8 col-xs-12">
                                    <select class="form-control" name="Experiencia" id="Experiencia">
                                        <?php echo (!empty($datacandidatos->rows->Experiencia) && $datacandidatos->rows->Experiencia > '') ? "<option value='".$datacandidatos->rows->Experiencia."' selected>".$datacandidatos->rows->Experiencia."</option>" : ''; ?>
                                        <option>- Seleccione -</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="o Más">o Más</option>
                                    </select>
                                </div>
                            </div>
                            <div class="pull-right">
                                <a href="#." data-name="name" data-type="text"  class="btn_edit" data-pk="randomID+">
                                    <img src="<?php echo get_assets_url();?>assets/img/Editar1.png" width="25">
                                </a>
                                <a href="#" class="save_curriculum">
                                    <img src="<?php echo get_assets_url();?>assets/img/Guardar1.png" width="25">
                                </a>
                            </div><br><br>
                            <br>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</form>
</div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>

<!-- Modal contacto -->
<div id="cv_guardado" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content modal-color">
            <div class="modal-header modal-color">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 id="menss" class="text-center">¡TU CURRICULUM HA SIDO CARGADO EXITOSAMENTE!</h3><br>
                <P class="parr">¡Felicitaciones! acabas de dar el primer paso para encontrar el empleo de tus sueños. Encenderemos nuestros motores de búsqueda y te contactaremos apenas consigamos la vacante ideal para tu perfil; mantente atento a tu bandeja de entrada</P>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- /modals -->
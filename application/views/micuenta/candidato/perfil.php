
<br><br><br>
<div class="my-goomo-shimmer">
    <div class="mg-user-header">
        <div class="wrp">
            <div class="usr-img gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar">
                <?php }else{ ?>

                    <div class="image-upload">

                        <label for="file-input">
                            <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar ">
                        </label>

                        <input id="file-input" type="file"/>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <div class="main-wrap container">
        <div class="mg-shim-main">
            <div class="mg-rst row">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 descripcionuser">
                            <p><?php echo $nombre." ".$apellido ?></p>
                            <p>Fecha de Nacimiento: <?php echo (!empty($datacandidatos->rows->Fecha_nacimiento) && $datacandidatos->rows->Fecha_nacimiento > '') ? $datacandidatos->rows->Fecha_nacimiento : ''; ?></p>
                            <p>Ubicación Actual:<?php echo (!empty($datacandidatos->rows->Direccion) && $datacandidatos->rows->Direccion > '') ? $datacandidatos->rows->Direccion : ''; ?></p>
                            <p>Teléfono: <?php echo (!empty($datacandidatos->rows->Telefono) && $datacandidatos->rows->Telefono > '') ? $datacandidatos->rows->Telefono : ''; ?></p>
                            <p>Email: <?php echo (!empty($datacandidatos->rows->Correo) && $datacandidatos->rows->Correo > '') ? $datacandidatos->rows->Correo : ''; ?> </p><br>
                            <div class="col-md-offset-4 col-md-4 col-sm-12 col-xs-12 alert alert-success alert-dismissible fade in" role="alert">
                                <span class="dscv">Descargar CV</span>
                                <a href="<?php echo get_site_url('/pdfx/cv_candidato')?>">
                                    <img src="<?php echo get_assets_url();?>assets/img/PDF.png" width="30">
                                </a>
                                <a href="<?php echo get_site_url('/pdfx/cv_candidato')?>">
                                    <img src="<?php echo get_assets_url();?>assets/img/WORD.png" width="30">
                                </a>
                            </div>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-md-12 col-sm-12 col-xs-12 content-cv"> <hr>
                            <div class="col-md-6 col-sm-12 col-xs-12">
                                <h4>ACERCA DE MI</h4>
                                <p><?php echo (!empty($datacandidatos->rows->Acerca_de_mi) && $datacandidatos->rows->Acerca_de_mi > '') ? $datacandidatos->rows->Acerca_de_mi : ''; ?></p>
                                <br>
                                <h4>EDUCACIÓN</h4>
                                <div class="dashboard-widget-content">

                                    <ul class="list-unstyled timeline widget">
                                        <li>
                                            <div class="block">
                                                <div class="block_content">
                                                    <span class="count_top"><?php echo (!empty($datacandidatos->rows->desde1) && $datacandidatos->rows->desde1 > '') ? $datacandidatos->rows->desde1 : ''; ?> - <?php echo (!empty($datacandidatos->rows->hasta1) && $datacandidatos->rows->hasta1 > '') ? $datacandidatos->rows->hasta1 : ''; ?>
                                                </span>
                                                <h2 class="title text-left">
                                                    <?php echo (!empty($datacandidatos->rows->titulo1) && $datacandidatos->rows->titulo1 > '') ? $datacandidatos->rows->titulo1 : ''; ?>
                                                </h2>
                                                <p class="excerpt text-left">
                                                    <?php echo (!empty($datacandidatos->rows->institucion1) && $datacandidatos->rows->institucion1 > '') ? $datacandidatos->rows->institucion1 : ''; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block">
                                            <div class="block_content">
                                                <span class="count_top">
                                                    <?php echo (!empty($datacandidatos->rows->desde2) && $datacandidatos->rows->desde2 > '') ? $datacandidatos->rows->desde2 : ''; ?> -
                                                    <?php echo (!empty($datacandidatos->rows->hasta2) && $datacandidatos->rows->hasta2 > '') ? $datacandidatos->rows->hasta2 : ''; ?>
                                                </span>
                                                <h2 class="title text-left">
                                                    <?php echo (!empty($datacandidatos->rows->titulo2) && $datacandidatos->rows->titulo2 > '') ? $datacandidatos->rows->titulo2 : ''; ?>
                                                </h2>
                                                <p class="excerpt text-left">  
                                                    <?php echo (!empty($datacandidatos->rows->institucion2) && $datacandidatos->rows->institucion2 > '') ? $datacandidatos->rows->institucion2 : ''; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                    <li>
                                        <div class="block">
                                            <div class="block_content">
                                                <span class="count_top">

                                                    <?php echo (!empty($datacandidatos->rows->desde3) && $datacandidatos->rows->desde3 > '') ? $datacandidatos->rows->desde3 : ''; ?> - 
                                                    <?php echo (!empty($datacandidatos->rows->hasta3) && $datacandidatos->rows->hasta3 > '') ? $datacandidatos->rows->hasta3 : ''; ?>

                                                </span>
                                                <h2 class="title text-left">
                                                    <?php echo (!empty($datacandidatos->rows->titulo3) && $datacandidatos->rows->titulo3 > '') ? $datacandidatos->rows->titulo3 : ''; ?>
                                                </h2>
                                                <p class="excerpt text-left">
                                                    <?php echo (!empty($datacandidatos->rows->institucion3) && $datacandidatos->rows->institucion3 > '') ? $datacandidatos->rows->institucion3 : ''; ?>
                                                </p>
                                            </div>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-6 timeline">
                            <div class="block_experiencia">

                                <h4>EXPERIENCIA</h4>
                                <br>
                                <span class="count_top">
                                    <?php echo (!empty($datacandidatos->rows->e_desde1) && $datacandidatos->rows->e_desde1 > '') ? $datacandidatos->rows->e_desde1 : ''; ?> - 
                                    <?php echo (!empty($datacandidatos->rows->e_hasta1) && $datacandidatos->rows->e_hasta1 > '') ? $datacandidatos->rows->e_hasta1 : ''; ?>
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->empresa1) && $datacandidatos->rows->empresa1 > '') ? $datacandidatos->rows->empresa1 : ''; ?>
                                </h3>
                                <span>
                                    <?php echo (!empty($datacandidatos->rows->funcion_cargo1) && $datacandidatos->rows->funcion_cargo1 > '') ? $datacandidatos->rows->funcion_cargo1 : ''; ?>
                                </span>
                                <p class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->descripcion_cargo1) && $datacandidatos->rows->descripcion_cargo1 > '') ? $datacandidatos->rows->descripcion_cargo1 : ''; ?>
                                </p>
                                <br>
                                <span class="count_top">
                                    <?php echo (!empty($datacandidatos->rows->descripcion_cargo1e_desde2)) ? $datacandidatos->rows->e_desde2 : ''; ?> - 
                                    <?php echo (!empty($datacandidatos->rows->e_hasta2)) ? $datacandidatos->rows->e_hasta2 : ''; ?>
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->empresa2)) ? $datacandidatos->rows->empresa2 : ''; ?>
                                </h3>
                                <span>
                                    <?php echo (!empty($datacandidatos->rows->funcion_cargo2)) ? $datacandidatos->rows->funcion_cargo2 : ''; ?>
                                </span>
                                <p class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->descripcion_cargo2)) ? $datacandidatos->rows->descripcion_cargo2 : ''; ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row content-cv">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h4 class="text-center">IDIOMAS</h4>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Idiomas_requerido)) ? $datacandidatos->rows->Idiomas_requerido : ''; ?>
                                </h3>
                                <div class="progress progress_sm">
                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row content-cv">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h4 class="text-center">CONOCIMIENTOS Y HABILIDADES</h4><br>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Área
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Area) && $datacandidatos->rows->Area > '') ? $datacandidatos->rows->Area : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Nivel del Cargo
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Nivel_cargo) && $datacandidatos->rows->Nivel_cargo > '') ? $datacandidatos->rows->Nivel_cargo : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Grado de especialización
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Grado_especializacion) && $datacandidatos->rows->Grado_especializacion > '') ? $datacandidatos->rows->Grado_especializacion : ''; ?>
                                </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <span class="count_top">Conocimientos y habilidades adquiridas
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Conocimientos_habilidades) && $datacandidatos->rows->Conocimientos_habilidades > '') ? $datacandidatos->rows->Conocimientos_habilidades : ''; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row content-cv">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <h4 class="text-center">CUENTANOS, SEGÚN TUS CONOCIMIENTOS Y HABILIDADES,<br><br>¿CÓMO SERÍA EL EMPLEO DE TUS SUEÑOS?</h4><br><br>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">País
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Pais) && $datacandidatos->rows->Pais > '') ? $datacandidatos->rows->Pais : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Región
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Region) && $datacandidatos->rows->Region > '') ? $datacandidatos->rows->Region : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Ciudad
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Ciudad) && $datacandidatos->rows->Ciudad > '') ? $datacandidatos->rows->Ciudad : ''; ?>
                                </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Jornada laboral
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Jornada_laboral) && $datacandidatos->rows->Jornada_laboral > '') ? $datacandidatos->rows->Jornada_laboral : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Tipo de contrato
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Tipo_contrato) && $datacandidatos->rows->Tipo_contrato > '') ? $datacandidatos->rows->Tipo_contrato : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Salario (neto mensual)
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Salario) && $datacandidatos->rows->Salario > '') ? $datacandidatos->rows->Salario : ''; ?>
                                </h3>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Monto
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Monto) && $datacandidatos->rows->Monto > '') ? $datacandidatos->rows->Monto : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Jerarquía
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Jerarquia) && $datacandidatos->rows->Jerarquia > '') ? $datacandidatos->rows->Jerarquia : ''; ?>
                                </h3>
                            </div>
                            <div class="col-md-4 col-sm-12 col-xs-12">
                                <span class="count_top">Antigüedad en cargo o similares
                                </span>
                                <h3 class="text-left">
                                    <?php echo (!empty($datacandidatos->rows->Experiencia) && $datacandidatos->rows->Experiencia > '') ? $datacandidatos->rows->Experiencia : ''; ?>
                                </h3>
                            </div>
                        </div>
                    </div>
                </div>         
            </div>
        </div>
    </div>
</div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>
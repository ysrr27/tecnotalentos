
<br><br><br>
<div class="my-goomo-shimmer_rec">
    <div class="mg-user-header_rec" style="position: absolute !important;">
        <div class="wrp_rec wrp_rec_setting">
            <div class="usr-img_setting gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar_setting">
                <?php }else{ ?>
                    <div class="image-upload">
                        <label for="file-input">
                            <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar_setting ">
                        </label>
                        <input id="file-input" type="file"/>
                    </div>
                <?php } ?>
            </div>
            <div class="saludo">
                <h1 id="sal_user">¡HOLA, <?php echo strtoupper($datacandidatos->user->nombre)." ".strtoupper($datacandidatos->user->apellido) ?>!</h1>
            </div>
        </div>
    </div><br><br><br><br><br>
    <div class="main-wrap container pdfor">
        <div class="mg-shim-main">
            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-12 descripcionuser"><br>
                        </div>   
                    </div>
                    <div class="row">
                        <div class="col-md-offset-1 col-md-10">
                            <br><br>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="col-md-12">
                                            <br>
                                            <div class="x_content">
                                                <br />
                                                <form class="form-horizontal form-label-left input_mask" action="<?php echo get_site_url("candidatos/settins")?>" id="settins_user" role="form" name="session_recluta" method="post" accept-charset="utf-8">
                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control has-feedback-left" placeholder="Nombre" name="nombre" id="nombre" value="<?php echo (!empty($datacandidatos->user->nombre) && $datacandidatos->user->nombre > '') ? $datacandidatos->user->nombre : ''; ?>">
                                                        <span class="fa fa-user form-control-feedback left" aria-hidden="true"></span>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Apellido" value="<?php echo (!empty($datacandidatos->user->apellido) &&$datacandidatos->user->apellido > '') ? $datacandidatos->user->apellido : ''; ?>">
                                                        <span class="fa fa-user form-control-feedback right" aria-hidden="true"></span>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control has-feedback-left" placeholder="Correo" name="correo" id="correo" value="<?php echo (!empty($datacandidatos->user->correo) && $datacandidatos->user->correo > '') ? $datacandidatos->user->correo : ''; ?>">
                                                        <span class="fa fa-envelope-o form-control-feedback left" aria-hidden="true"></span>
                                                    </div>

                                                    <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                        <input type="text" class="form-control" id="telefono" name="telefono" placeholder="Telefono"  value="<?php echo (!empty($datacandidatos->user->telefono) && $datacandidatos->user->telefono > '') ? $datacandidatos->user->telefono : ''; ?>">
                                                        <span class="fa fa-mobile form-control-feedback right" aria-hidden="true"></span>
                                                    </div>
                                                    <div class="col-md-12"><br>
                                                         <a href="#" id="cambiar-contrasena"><i class="fa fa-lock"></i> Cambiar contraseña</a><br><br>
                                                    </div>
                                                    <div class="col-md-12 form_register">
                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                            <input type="password" class="form-control has-feedback-left" name="clave_actual" id="clave_actual" placeholder="Clave actual">
                                                            <span class="fa fa-key form-control-feedback left" aria-hidden="true"></span>
                                                        </div>

                                                        <div class="col-md-6 col-sm-6 col-xs-12 form-group has-feedback">
                                                            <input type="password" class="form-control" id="nueva_clave" name="nueva_clave" placeholder="Nueva clave">
                                                            <span class="fa fa-key form-control-feedback right" aria-hidden="true"></span>
                                                        </div>
                                                    </div>
                                                    <div class="ln_solid"></div>
                                                    <div class="form-group">
                                                        <div class="col-md-7 col-sm-12 col-xs-12 col-md-offset-5">
                                                            <button class="btn btn-primary" type="reset">Reset</button>
                                                            <button type="submit" class="btn btn-primary" id="guardar-settins">Guardar</button>
                                                        </div>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <hr>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
</div><br><br><br><br><br>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy"><label id="ctl00_Master_CopyrightFooter">© Copyright 2020 TECNO-RED Soluciones 14 C.A</label></p>
</footer>
<!-- Modal contacto -->
<div id="datosguardados" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content modal-color">
            <div class="modal-header modal-color">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <h4 id="menss" class="text-center">¡TUS DATOS HAN SIDO ACTUALIZADO EXITOSAMENTE!</h4>
                <br>
            </div>
        </div>
    </div>
</div>
<!-- /modals -->
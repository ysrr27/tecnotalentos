
<br><br><br>
<div class="my-goomo-shimmer">
    <div class="mg-user-header">
        <div class="wrp">
            <div class="usr-img gm_animated-background">
                <?php if (!empty($foto)) {?>
                    <img src="<?php echo get_assets_url().$foto?>" alt="" class="landing-icon perfil-avatar">
                <?php }else{ ?>

                    <div class="image-upload">

                      <label for="file-input">
                        <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar ">
                    </label>

                    <input id="file-input" type="file"/>
                </div>
                <?php } ?>
            </div>

            <div  id="h1first2" class="mg-dtls">
                <div  class="mg-ttl">
                    <h4><?php echo $nombre." ".$apellido ?></h4>
                </div>
                <div class="mg-ttl correo">


                    <h4><i><?php echo $correo ?></i></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="main-wrap">
        <div class="mg-shim-main">

            <div class="mg-rst">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#tab_content1" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false"><i class="fa fa-folder-open"></i><spam class="m_tab"> Solicitudes</spam></a>
                                    </li>
                                    <li role="presentation">
                                        <a href="#tab_content2" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true"><i class="fa fa-info-circle"></i> <spam class="m_tab">Ayuda</spam></a>
                                    </li>
                                    <li role="presentation" class=""><a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false"><i class="fa fa-envelope"></i><spam class="m_tab"> Mensajes </spam>
                                        <?php if($mensajes_nuevos > 0 ) {?>
                                            <div id="cant_mensajes">
                                                <span class="badge bg-green btn-notif"><?php echo $mensajes_nuevos?> </span>  
                                            </div>

                                        <?php } ?>
                                    </a>

                                    </li>
                                </ul>
                                <div id="myTabContent" class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="profile-tab">

                                    <div class="row">
                                      <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                          <div class="x_title">
                                            <h2>Responsive example</h2>
                                            <div class="clearfix"></div>
                                          </div>
                                          <div class="x_content">
                                            <table id="datatable-responsive" class="table table-striped dt-responsive nowrap row-border" cellspacing="0" width="100%">
                                                <thead>
                                                    <tr>
                                                        <th>First name</th>
                                                        <th>Last name</th>
                                                        <th>Position</th>
                                                        <th>Office</th>
                                                        <th>Age</th>
                                                        <th>Start date</th>
                                                        <th>Salary</th>
                                                        <th>Extn.</th>
                                                        <th>E-mail</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td>Tiger</td>
                                                        <td>Nixon</td>
                                                        <td>System Architect</td>
                                                        <td>Edinburgh</td>
                                                        <td>61</td>
                                                        <td>2011/04/25</td>
                                                        <td>$320,800</td>
                                                        <td>5421</td>
                                                        <td>t.nixon@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Garrett</td>
                                                        <td>Winters</td>
                                                        <td>Accountant</td>
                                                        <td>Tokyo</td>
                                                        <td>63</td>
                                                        <td>2011/07/25</td>
                                                        <td>$170,750</td>
                                                        <td>8422</td>
                                                        <td>g.winters@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Ashton</td>
                                                        <td>Cox</td>
                                                        <td>Junior Technical Author</td>
                                                        <td>San Francisco</td>
                                                        <td>66</td>
                                                        <td>2009/01/12</td>
                                                        <td>$86,000</td>
                                                        <td>1562</td>
                                                        <td>a.cox@datatables.net</td>
                                                    </tr>
                                                    <tr>
                                                        <td>Cedric</td>
                                                        <td>Kelly</td>
                                                        <td>Senior Javascript Developer</td>
                                                        <td>Edinburgh</td>
                                                        <td>22</td>
                                                        <td>2012/03/29</td>
                                                        <td>$433,060</td>
                                                        <td>6224</td>
                                                        <td>c.kelly@datatables.net</td>
                                                    </tr>

                                                </tbody>
                                            </table>
                                      </div>
                                    </div>
                               
                                </div>
                                <!-- /page content -->
                              </div>
                          
                         


                                    </div>



                                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="home-tab"> 
                                        <h1>¿En qué podemos ayudarte?</h1> 
                                        <div id="messages" style="display: none;"></div>
                                        <div id="service" class="section-padding">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-md-offset-1 col-md-10">
                                                        <p>*Obligatorio</p>
                                                        <form action="<?php echo get_site_url("reclutas/solicitudajax")?>" class="" id="solicitud" role="form" data-toggle="validator" name="solicitud" method="post" accept-charset="utf-8">
                                                            <div id="form-step-0" role="form" data-toggle="validator">
                                                                <div class="form-group row">
                                                                     <input type="hidden" name="idrecluta" id="idrecluta" value="<?php echo (!empty($idrecluta) && $idrecluta > '') ? $idrecluta : ''; ?>">

                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Puesto_buscar">¿Qué perfil buscas?: *</label>
                                                                        <input type="text" class="form-control" name="Puesto_buscar" id="Puesto_buscar" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Descripcion_puesto">Descripción: *<br></label>
                                                                        <textarea id="Descripcion_puesto" required="required" class="form-control" name="Descripcion_puesto"></textarea>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                </div>
                                                                <div class="form-group row">
                                                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Detalle_tareas">Detalle las tareas que debe realizar: *<br></label>
                                                                        <textarea id="Detalle_tareas" required="required" class="form-control" name="Detalle_tareas"></textarea>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                </div>
                                                                 <hr><h4><i class="fa fa-graduation-cap"></i> INFORMACIÓN REFERENCIAL DEL CARGO</h4> <hr>
                                                                 <div class="row">
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="empresa_recluta">Empresa: *</label>
                                                                        <input type="text" class="form-control" name="empresa_recluta" id="empresa_recluta" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Pais">País: *</label>
                                                                        <input type="text" class="form-control" name="Pais" id="Pais" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Region">Región: *</label>
                                                                        <input type="text" class="form-control" name="Region" id="Region" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Ciudad">Ciudad: *</label>
                                                                        <input type="text" class="form-control" name="Ciudad" id="Ciudad" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Jornada_laboral">Jornada laboral: *</label>
                                                                        <select id="Jornada_laboral" name="Jornada_laboral" class="form-control" required>
                                                                            <option value="">Seleccione</option>
                                                                            <option value="Full Time">Full Time</option>
                                                                            <option value="Part Time"> Part Time</option>
                                                                            <option value="Pasantia">Pasantía</option>
                                                                            <option value="Por horas">Por horas</option>
                                                                            <option value="Teletrabajo">Teletrabajo</option>
                                                                            <option value="Fines de semana">Fines de semana</option>
                                                                        </select>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Tipo_contrato">Tipo de contrato: *</label>
                                                                        <select id="Tipo_contrato" name="Tipo_contrato" class="form-control" required>
                                                                            <option value="">Seleccione</option>
                                                                            <option value="Por obra">Por obra</option>
                                                                            <option value="A tiempo determinado">A tiempo determinado</option>
                                                                            <option value="A tiempo indeterminado">A tiempo indeterminado</option>
                                                                            <option value="Otro">Otro</option>
                                                                        </select>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 

                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Salario">Salario (neto mensual): *</label><br>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Salario"  id="Salario0" value="Moneda nacional">
                                                                            <label for="Salario0">Moneda nacional</label>
                                                                        </div>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Salario" id="Salario1" value="Moneda extranjera">
                                                                            <label for="Salario1">Moneda extranjera</label>
                                                                        </div>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Salario" id="Salario2" value="Mixto"><label for="Salario2">Mixto</label>
                                                                        </div>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>

                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Tipo_contratacion">Tipo de contratación: *</label>
                                                                        <input type="text" class="form-control" name="Tipo_contratacion" id="Tipo_contratacion" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Vacantes">Cantidad de vacantes: *</label>
                                                                        <input type="text" class="form-control" name="Vacantes" id="Vacantes" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>  

                                                                </div>

                                                                <div class="row">
                                                                     <hr><h4><i class="fa fa-child"></i> CARACTERÍSTICAS DEL CANDIDATO IDEAL</h4> <hr>
                                                                     <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Jerarquia">Jerarquía: *</label>
                                                                        <select id="tipo_contratoJerarquia" name="Jerarquia" class="form-control" required>
                                                                            <option value="">Seleccione</option>
                                                                            <option value="Pasante">Pasante</option>
                                                                            <option value="Junior">Junior</option>
                                                                            <option value="Semi Senior">Semi Senior</option>
                                                                            <option value="Senior">Senior</option>
                                                                            <option value="Jefe/ Supervisor/ Coordinador">Jefe/ Supervisor/ Coordinador</option>
                                                                            <option value="Gerente/Director">Gerente/Director</option>
                                                                            <option value="Otro">Otro</option>
                                                                        </select>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 

                                                                    <div class="form-group row">
                                                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                                                                <label for="edad_maxima">Antigüedad en el cargo o cargos similares:</label>
                                                                            </div>
                                                                        <div class="col-md-4">
                                                                            <div class="col-md-7">
                                                                                <input type="text" class="form-control" name="Edad_minima" id="Edad_minima" placeholder="Edad minima" data-inputmask="'mask' : '99'" value="<?php echo (!empty($data->telefono_1) && $data->telefono_1 > '') ? $data->telefono_1 : ''; ?>" required>
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>

                                                                        </div> 
                                                                        <div class="col-md-4">
                                                                            <div class="col-md-7">
                                                                                <input type="text" class="form-control" name="Edad_maxima" id="Edad_maxima" placeholder="Edad máxima" data-inputmask="'mask' : '99'"required>
                                                                                <div class="help-block with-errors"></div>
                                                                            </div>
                                                                        </div> 
                                                                    </div>

                                                                     <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Estudios_minimos:">Estudios mínimos:: *</label>
                                                                        <select id="Estudios_minimos" name="Estudios_minimos" class="form-control" required>
                                                                            <option value="">Seleccione</option>
                                                                            <option value="Basico">Básico</option>
                                                                            <option value="Bachiller">Bachiller</option>
                                                                            <option value="Estudiante universitario">Estudiante universitario</option>
                                                                            <option value="TSU">TSU</option>
                                                                            <option value="Universitario titulado">Universitario titulado</option>
                                                                            <option value="Universitario titulado con estudios superiores">Universitario titulado con estudios superiores</option>
                                                                        </select>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>

                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Idiomas_requerido">Idiomas requerido: *</label>
                                                                        <input type="text" class="form-control" name="Idiomas_requerido" id="Idiomas_requerido" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div> 
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Nivel">Nivel: *</label><br>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Nivel"  id="Nivel0" value="Nativo">
                                                                            <label for="Nivel0">Nativo</label>
                                                                        </div>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Nivel" id="Nivel1" value="Moneda extranjera">
                                                                            <label for="Nivel1">Avanzado</label>
                                                                        </div>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Nivel" id="Nivel2" value="Intermedio"><label for="Nivel2">Intermedio</label>
                                                                        </div>
                                                                        <div class="radiobutton">
                                                                            <input type="radio" required name="Nivel" id="Nivel3" value="Basico"><label for="Nivel3">Básico</label>
                                                                        </div>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>

                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Destrezas_conocimientos">Destrezas y conocimientos: *</label>
                                                                        <input type="text" class="form-control" name="Destrezas_conocimientos" id="Destrezas_conocimientos" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>  
                                                                    <div class="form-group col-md-12 col-sm-12 col-xs-12">
                                                                        <label for="Disponibilidad_viaje">Disponibilidad de viaje: *</label>
                                                                        <input type="text" class="form-control" name="Disponibilidad_viaje" id="Disponibilidad_viaje" placeholder="" required>
                                                                        <div class="help-block with-errors"></div>
                                                                    </div>    
                                                                </div>

                                                                <div class="row form-group">
                                                                    <div class="col-md-7 col-sm-9 col-xs-12 col-md-offset-5">
                                                                        <button type="submit" id="enviar_solicitud" class="btn sw-btn-next" style="display: block !important;" name="enviar"><i class="fa fa-envelope-o"></i> Enviar</button>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </form>

                                                    </div>

                                                </div>
                                            </div> 

                                        </div>
                                    </div>
                                    
                                    <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                           <?php 
                           if(is_array($datasolicitudes->rows_mensajes )){ ?>
                                        <div class="new">
                                            <form action="<?php echo get_site_url('reclutas/mensajeajax')?>" class="" id="new_mensaje" role="form" data-toggle="validator" name="new_mensaje" method="post" accept-charset="utf-8">
                                                <input type="hidden" class="form-control" name="idmensaje_respuesta" id="idmensaje_respuesta" value="">
                                                <input type="hidden" class="form-control" name="useridfrom_respuesta" id="useridfrom_respuesta" value="">
                                                <input type="hidden" class="form-control" name="file" id="file" value="">


                                                <div class="new-mail">
                                                    <div class="new-mail__top">
                                                        <div class="new-mail__title">
                                                            <span id="rpd">Responder:</span>

                                                        </div>
                                                        <i class="new-mail__close new-mail__toggle fa fa-close"></i>
                                                    </div>
                                                    <div class="new-mail-exp">
                                                    </div>
                                                    <div class="new-mail__content">
                                                        <textarea id="fullmensaje" name="fullmensaje" 
                                                        class="new-mail__message" autofocus></textarea>
                                                    </div>
                                                    <div class="new-mail-foot">
                                                        <div class="new-mail-foot__insert">
                                                            <div class="input-group"> 
                                                                <div class="custom-file">
                                                                    <input type="file" id="inputGroupFile01" name="archivo" class="imgInp custom-file-input" aria-describedby="inputGroupFileAddon01">
                                                                    <label class="custom-file-label" for="inputGroupFile01"><i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i>
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="new-mail-foot__actions">
                                                            <button id="btn-cancel" class="new-mail__toggle preview-foot__button">Cancelar</button>
                                                            <button class="button--primary preview-foot__button" id="send-message">
                                                                <i class="fa fa-paper-plane"></i></button>                   
                                                            </div>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        <div class="mails">
                                            <div class="message-list scrollable">
                                                <div class="scrollable__target">

                                                    <?php 
                                                    if(is_array($datasolicitudes->rows_mensajes )){
                                                        foreach($datasolicitudes->rows_mensajes as $key =>$row){
                                                            if ($key === 0) {
                                                                ?>

                                                                <div class="message<?php echo $row->idmensaje ?> message message--active <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                                                <?php }else{ ?>
                                                                    <div class="message<?php echo $row->idmensaje ?> message <?php echo $row->status ?>" id="<?php echo $row->idmensaje ?>" onclick="detalle_mensaje(this.id);">
                                                                    <?php } ?>

                                                                    <div class="message-tags">
                                                                        <span class="dot dot--green"></span>
                                                                    </div>
                                                                    <div class="message__actions"></div>
                                                                    <div class="message__content">
                                                                        <div class="message__exp">
                                                                            <div><?php echo $row->nombre." ".$row->apellido; ?></div>
                                                                            <div class="date"><?php echo date( "d/m/Y H:i:s", $row->fecha); ?></div>
                                                                        </div>
                                                                        <div class="message__title">
                                                                            <?php echo $row->subject ?>
                                                                        </div>
                                                                        <div class="message__expr">
                                                                            « <?php echo $row->smallmessage ?> »
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                                <?php

                                                            }
                                                        }?>
                                                        

                                                    </div>
                                                </div>                                     
                                            <div class="preview">
                                               <?php 
                                               if(is_array($datasolicitudes->rows_ultimo_mensaje )){
                                                  foreach($datasolicitudes->rows_ultimo_mensaje as $key =>$row){
                                                    if ($key === 0) {?>
                                                    
                                                    <div class="preview-top" id="<?php echo $row->idmensaje; ?>"  data-rowId = "<?php echo $row->useridfrom; ?>">
                                                        <div class="preview__title"><?php echo $row->subject; ?></div>
                                                    </div>
                                                    <div class="scrollable">
                                                        <div class="preview-content scrollable__target">
                                                <?php } ?>

                                                <div class="preview-respond">
                                                    <div class="preview-respond__head">
                                                        <div class="profile-head">
                                                            <div class="profile-head__id">

                                                                <?php echo (!empty($row->foto) && $row->foto > '') ? '<img class="profile-head__avatar"
                                                                src="'.get_assets_url().$row->foto.'"
                                                                alt="">' : '<img class="profile-head__avatar"
                                                                src="'.get_assets_url().'assets/img/img.png"
                                                                alt="">'; ?>
                                                                <div>
                                                                    <div class="profile-head__name"><?php echo $row->usuario; ?></div>
                                                                    <div class="profile-head__mail"><?php echo $row->correo_mensaje; ?></div>
                                                                </div>
                                                            </div>
                                                            <div class="date"><?php echo date( "d/m/Y H:i:s", $row->timecreate); ?></div>
                                                        </div>
                                                    </div>
                                                    <div class="preview-respond__content">
                                                        <p class="paragraph"><?php echo $row->fullmensaje; ?></p>

                                                        <?php echo (!empty($row->file) && $row->file > '') ? '<a href="'.get_assets_url().'uploads/'.$row->file.'" download><b><i class="fa fa-paperclip"></i> '.$row->file.' </b></a>' : ''; ?> 
                                                    </div>
                                                </div>

                                                    <?php
                                                }
                                            }                          
                                            ?>
                                                    </div>
                                                </div>
                                                <div class="preview-foot">
                                                    <button class="new-mail__toggle btn preview-foot__button"><i class="fa fa-mail-reply"></i> Responder</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php  }else{ echo "<br><p>No hay mensajes</p><br><br>"; } ?>
                                    <!-- aqui termina -->
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>
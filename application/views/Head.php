<!DOCTYPE html>
<html lang="en">
  <head>
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- <meta name="viewport" content="width=device-width, initial-scale=1">  -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">


    <meta name="author" content="" />
    <meta name="description" content="<?php echo (!empty($data->rows->description) && $data->rows->description > '') ? $data->rows->description : ''; ?>" />
        <?php 
        if (!isset($count_mensajes)) {        
            $countmsn = 0;
        }else{
           $countmsn = intval($count_mensajes);
        }?>
    <title><?php echo (!empty($countmsn) && $countmsn > 0) ? '('.$countmsn.') ' : ''; ?><?php echo (!empty($data->rows->title) && $data->rows->title > '') ? $data->rows->title : 'Cheverecars'; ?></title>
    <meta property="og:type" content="<?php echo (!empty($data->rows->type) && $data->rows->type > '') ? $data->rows->type : ''; ?>" />
    <meta property="og:title" content="<?php echo (!empty($data->rows->title) && $data->rows->title > '') ? $data->rows->title : ''; ?>" />
    <meta property="og:url" content="<?php echo (!empty($data->rows->url) && $data->rows->url > '') ? $data->rows->url : ''; ?>" />
    <meta property="og:image" content="<?php echo get_assets_url();?>revenuemanagementworld/assets/img/Barcelona.png" />
    <meta property="og:description" content="<?php echo (!empty($data->rows->description) && $data->rows->description > '') ? $data->rows->description : ''; ?>" />
    <meta property="og:site_name" content="<?php echo (!empty($data->rows->site_name) && $data->rows->site_name > '') ? $data->rows->site_name : ''; ?>" />
    <meta property="og:locale" content="<?php echo (!empty($data->rows->locale) && $data->rows->locale > '') ? $data->rows->locale : ''; ?>" />
    <meta name="keywords" content="<?php echo (!empty($data->rows->keywords) && $data->rows->keywords > '') ? $data->rows->keywords : ''; ?>" />

    <!-- #FAVICONS -->
    <link rel="shortcut icon" href="<?php echo get_assets_url();?>assets/theme_chevere/images/fav.png" type="image/x-icon">
    <link rel="icon" href="<?php echo get_assets_url();?>assets/theme_chevere/images/fav.png" type="image/x-icon">

    <?php 
    $url = $this->uri->segment(1);
if (empty($url)) { ?>

 <!-- Bootstrap -->
<!--     <link href="<?php echo get_assets_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet"> -->
<!-- Font Awesome -->
    <link href="<?php echo get_assets_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
 <!-- Custom Theme Style -->
    <!-- <link href="<?php echo get_assets_url();?>assets/css/custom.css" rel="stylesheet"> -->


    <!-- Animate.css -->

    <!-- Modernizr JS -->
    <!-- <script src="<?php echo get_assets_url();?>assets/theme_chevere/js/modernizr-2.6.2.min.js"></script> -->

        <link rel="stylesheet" type="text/css" href="//fonts.googleapis.com/css?family=Poppins:400,500,600%7CTeko:300,400,500%7CMaven+Pro:500">
    <link rel="stylesheet" href="<?php echo get_assets_url();?>assets/theme_chevere/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_assets_url();?>assets/theme_chevere/css/fonts.css">
    <link rel="stylesheet" href="<?php echo get_assets_url();?>assets/theme_chevere/css/style.css">
   <!--    <link rel="stylesheet" href="<?php echo get_assets_url();?>assets/theme_chevere/css/style2.css">
 -->

 <?php  }else{ ?>


    <link href="<?php echo get_assets_url();?>assets/css/home.css" rel="stylesheet">
 <!--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">
 -->
    <!-- Bootstrap -->
    <link href="<?php echo get_assets_url();?>assets/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <link href="<?php echo get_assets_url();?>assets/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo get_assets_url();?>assets/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo get_assets_url();?>assets/css/custom.css" rel="stylesheet">
    <!-- iCheck -->
    <link href="<?php echo get_assets_url();?>assets/iCheck/skins/flat/green.css" rel="stylesheet">


    <link href="<?php echo get_assets_url();?>assets/jQuery-Smart-Wizard/dist/css/smart_wizard.css" rel="stylesheet" type="text/css" />


    <link href="<?php echo get_assets_url();?>assets/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet">


    <!-- SmartWizard theme -->

    <!-- Optional SmartWizard theme -->
    <link href="<?php echo get_assets_url();?>assets/jQuery-Smart-Wizard/dist/css/smart_wizard_theme_circles.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo get_assets_url();?>assets/jQuery-Smart-Wizard/dist/css/smart_wizard_theme_arrows.css" rel="stylesheet" type="text/css" />

    <!-- Datatables -->
    <link href="<?php echo get_assets_url();?>assets/datatables.net-bs/css/dataTables.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_assets_url();?>assets/datatables.net-buttons-bs/css/buttons.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_assets_url();?>assets/datatables.net-fixedheader-bs/css/fixedHeader.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_assets_url();?>assets/datatables.net-responsive-bs/css/responsive.bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo get_assets_url();?>assets/datatables.net-scroller-bs/css/scroller.bootstrap.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_assets_url();?>assets/alertify/themes/alertify.core.css">
    <link rel="stylesheet" type="text/css" media="screen" href="<?php echo get_assets_url();?>assets/alertify/themes/alertify.bootstrap.css">
    <link rel="stylesheet" href="<?php echo get_assets_url();?>assets/css/jquery-ui.css">
<!--     <script src='https://www.google.com/recaptcha/api.js'></script>
 -->
    <!-- Switchery -->
    <link href="<?php echo get_assets_url();?>assets/switchery/dist/switchery.min.css" rel="stylesheet">
<link rel='stylesheet' href='<?php echo get_assets_url();?>assets/chosen/chosen.css'>
 <?php } ?>
         <!-- Global site tag (gtag.js) - Google Analytics -->
       <!--   <script async src="https://www.googletagmanager.com/gtag/js?id=UA-144725970-1"></script>
         <script>
          window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);}
          gtag('js', new Date());

          gtag('config', 'UA-144725970-1');
        </script> -->
  </head>
  <?php  

  if($menu){
    ?>
    <body class="nav-sm">
        <div class="container body">
            <div class="main_container">
              <?php        $this->load->view('Menu', $nombre); 
          }else{

            $admin = $this->uri->segment(1);
            if ($admin == "admin") {
            }else{         ?>
            <header class="site-header" role="banner">
               <!--  <div class="topbar" id="navbar-fixed-top"> -->
                    <!-- <h1 class="site-title">
                        <div class="navbar" style="border: 0;">
                            <a href="" class=""><img src="<?php echo get_assets_url();?>assets/img/TecnoRed.png" alt="..." class="img-logo-admin"></a>
                        </div>
                    </h1> -->
                    <nav class="top-navigation" role="navigation" style="display:none">
                        <ul class="inline-list padded-list">                       
                                <?php  if($logueado){  ?>
                                    <li class="">
                                        <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false" style="color: #6a6a6a !important;">

                                            <?php if (!empty($foto)) {?>
                                                <img class="profile-photo-small" src="<?php echo get_assets_url().$foto?>" alt="">
                                            <?php }else{ ?>
                                                <img class="profile-photo-small" src="<?php echo get_assets_url();?>assets/img/img.png" alt="">
                                            <?php } ?>
                                            <span class="usr">
                                            <?php echo $nombre." ".$apellido ?></span>

                                            <span class=" fa fa-angle-down"></span>
                                        </a>
                                        <?php $url = $this->uri->segment(1);?>
                                    <ul class="dropdown-menu dropdown-usermenu pull-right"> 
                                            <?php if($rol == 'recluta'){  ?>
                                                <li><a href="<?php echo get_site_url($url."/micuenta")?>">Perfil de empresa</a></li>
                                                <li><a href="<?php echo get_site_url($url."/configuracion")?>">Configuración</a></li>
                                            <?php } ?>
                                            <?php if($rol == 'candidato'){  ?>
                                              <li><a href="<?php echo get_site_url($url."/micuenta")?>">Ver Perfil</a></li>
                                              <li><a href="<?php echo get_site_url($url."/editar_curriculum")?>">Editar Currículum</a></li>
                                              <li><a href="<?php echo get_site_url($url."/mensajes")?>">Mensajes</a></li>
                                               <li><a href="<?php echo get_site_url($url."/configuracion")?>">Configuración</a></li>
                                            <?php } ?>
                                            <li><a href="<?php echo get_site_url("/login/cerrar_sesion_recluta")?>"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>
                                        </ul>
                                    </li>

                                <?php }else{ 
                                    $urlact = $this->uri->segment(3);?>

                                 <!--  <li><a href="<?php echo get_site_url("/")?>" class="m_btn <?php echo (empty($urlact)) ? 'm_btn_actv' : '';  ?>">INICIO</a></li>
                                  <li><a href="<?php echo get_site_url("/login/sign_in/reclutas")?>" class="m_btn <?php echo (!empty($urlact) && $urlact=='reclutas') ? 'm_btn_actv' : '';  ?>">RECLUTADOR</a></li>
                                  <li><a href="<?php echo get_site_url("/login/sign_in/candidatos")?>" class="m_btn <?php echo (!empty($urlact) && $urlact=='candidatos') ? 'm_btn_actv' : '';  ?>">CANDIDATO</a></li> -->
                                  <ul class="nav navbar-nav navbar-right" style="margin-top: -9px;">
                                <?php }
                            } ?>
                            </ul>          
                        </ul>
                    </nav>
                    <a href="#m_movil" id="toggle"><span></span></a>
                    <div id="m_movil">
                        <ul>
                            <?php  if($logueado){  ?>
                                <?php if($rol == 'recluta'){  ?>
                                    <li><a href="<?php echo get_site_url($url."/micuenta")?>">Perfil de empresa</a></li>
                                    <li><a href="<?php echo get_site_url($url."/configuracion")?>">Configuración</a></li>
                                <?php } ?>
                                <?php if($rol == 'candidato'){  ?>
                                  <li><a href="<?php echo get_site_url($url."/micuenta")?>">Ver Perfil</a></li>
                                  <li><a href="<?php echo get_site_url($url."/editar_curriculum")?>">Editar Currículum</a></li>
                                  <li><a href="<?php echo get_site_url($url."/mensajes")?>">Mensajes</a></li>
                                  <li><a href="<?php echo get_site_url($url."/configuracion")?>">Configuración</a></li>
                                <?php } ?>
                              <li><a href="<?php echo get_site_url("/login/cerrar_sesion_recluta")?>"><i class="fa fa-sign-out pull-right"></i> Cerrar Sesión</a></li>


                            <?php }else{?>   

                               <!--  <li><a href="<?php echo get_site_url("/")?>">INICIO</a></li>
                                <li><a href="<?php echo get_site_url("/login/sign_in/reclutas")?>">RECLUTADOR</a></li>
                                <li><a href="<?php echo get_site_url("/login/sign_in/candidatos")?>">CANDIDATO</a></li>  -->
                            <?php } ?>
                        </ul>
                    </div>
                <!-- </div> -->
            </header>
              <body>
              <?php } ?>

           

<div class="col-md-3 left_col">
          <div class="left_col scroll-view">
            <div class="navbar nav_title" style="border: 0;">
              <a href="<?php echo base_url() ?>dashboard" class="site_title">
               <img src="<?php echo get_assets_url();?>assets/theme_chevere/images/fav.png" id="img-perf-head" alt="..." class="img-circle profile_img">
              </a>
            </div>
            <br>

            <div class="clearfix"></div>

            <!-- menu profile quick info -->
            <div class="profile clearfix">
              <div class="profile_pic">
                <img src="<?php echo get_assets_url();?>assets/img/img.png" alt="..." class="img-circle profile_img">
              </div>
              <div class="profile_info">
                <span>Bienvenido</span>
                <h2> <?php echo $nombre; ?></h2>
              </div>
            </div>
            <!-- /menu profile quick info -->
            <br />
            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                <ul class="nav side-menu">

                  <?php if (!empty($rol) && $rol > '' && $rol =="Administrador") {?>
                    <li>
                   <li><a href="<?php echo get_site_url('/meta/home')?>"><i class="fa fa-code"></i> Meta </a>
                   </li>
                 <!--   <li><a href="<?php echo get_site_url('/formulario/editar/1')?>"><i class="fa fa-list"></i> Formulario </a>
                   </li> -->

                   <li><a href="<?php echo get_site_url('/usuarios/home')?>"><i class="fa fa-user"></i> Usuarios </a>
                  </li>
                   <li><a href="<?php echo get_site_url('/banners/home')?>"><i class="fa fa-image"></i> Banners </a>
                  </li>
                  <li><a href="<?php echo get_site_url('/reclutas/home')?>"><i class="fa fa-thumbs-up"></i> VIP </a>
                  </li>
                  <li><a href="<?php echo get_site_url('/servicios/home')?>"><i class="fa fa-gears"></i> Servicios </a>
                  </li>
                  <li><a href="<?php echo get_site_url('/patrocinantes/home')?>"><i class="fa fa-slideshare"></i> Patrocinantes </a>
                  </li>
                 <?php   } ?>
                </ul>
              </div>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
   
            </div>
            <!-- /menu footer buttons -->
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle" style="color: #f7f1f1;"><i class="fa fa-bars"></i></a>
              </div>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                    <img src="<?php echo get_assets_url();?>assets/img/img.png" alt="">
                    <?php echo $nombre; ?>
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li><a href="<?php echo base_url() ?>index.php/login/cerrar_sesion"><i class="fa fa-sign-out pull-right"></i>Salir</a></li>
                  </ul>
                </li>
                  <li role="presentation" class="dropdown">
                  <a href="<?php echo get_site_url('/mensajes/home')?>" class="dropdown-toggle info-number">
                    <i class="fa fa-envelope-o"></i>
                    <?php 
                    $countmsn = intval($count_mensajes); 
                    if ($countmsn > 0) { 
                        echo '<span class="badge bg-green">'.$countmsn.'</span>';
                    }?>
                  </a>
                </li>
              </ul>
            </nav>
          </div>
        </div>
        <!-- /top navigation -->
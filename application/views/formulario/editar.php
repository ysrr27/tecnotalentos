<!--page content -->


        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="breadcrumb">
                  <a>Home</a>
                  Formulario
                </div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Formulario Candidatos</h2> <br>
                        <div class="clearfix"></div>
                    </div>
                         <ul class="nav navbar-right panel_toolbox">
                                    <li>
                                        <a class="btn btn-primary sw-btn-next" id="addpregunta">
                                            <i class="fa fa-plus"></i> Agregar pregunta
                                        </a> 

                                        <div class="btn-group closet">

                                            <button class="btn btn-sm btn-default " id="cerrar" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-close"></i></button>
                                        </div>
                                    </li>

                                </ul>

                  <div class="x_content">
                      <form id="form_pregutnas" name="form_pregutnas" class="form-horizontal form-label-left input_mask addpregunta" action="<?php echo get_site_url("formulario/update")?>" method="post">  

                        <div class="row">
                            <div class="col-md-offset-1 col-md-9">
                               <div class="x_panel">
                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                            <input type="text" class="form-control" name="pregunta" id="pregunta" placeholder="Pregunta">
                                            <input type="hidden" name="formato_pregunta" id="formato_pregunta">
                                            <input type="hidden" name="opciones" id="opciones">
                                            <input type="hidden" name="idpregunta" id="idpregunta">
                                            <p class="text-danger" id="err"></p>

                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 form-group">
                                            <select class="form-control" name="tipo_pregunta" id="tipo_pregunta">
                                                <option>Tipo de Pregunta</option>
                                                <option value="text"><i class="fa fa-align-left"></i> Respuesta corta</option>
                                                <option value="textarea"><i class="fa fa-align-justify"></i> Párrafo</option>
                                                <option value="select"><i class="fa fa-align-justify"></i> Desplegable</option>
                                                <option value="checkbox"><i class="fa fa-check-square"></i> Selección múltiple</option>
                                                <option value="radio"><i class="fa fa-dot-circle-o"></i> Casillas de verificación</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-7 col-sm-9 col-xs-12">
                                                <div class="">
                                                  <div class="col-md-12">
                                                    <input type="checkbox" value="required" name="requerido" id="requerido" checked>
                                                    <label class="mouse" for='requerido' tabindex="-1">
                                                        <span class="check"></span>
                                                        Obligatorio
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 form-group">
                                            <select class="form-control" name="paso" id="paso">
                                                <option>Paso</option>
                                                <?php
                                                if(is_array($data->pasos)){
                                                    foreach( $data->pasos as $paso){ ?>
                                                        <option value="<?php echo $paso->valor_paso?>">Paso <?php echo $paso->valor_paso?></option>

                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="optiondisplay">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="options">
                                                    <div class="col-md-12 col-sm-11 col-xs-11 input-group  form-group has-feedback" id="1">
                                                        <input type="text" class="form-control has-feedback-left option1" id="radio1" placeholder="Opción 1">
                                                        <span class="fa fa-dot-circle-o form-control-feedback left" aria-hidden="true"></span>
                                                    </div>     
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion" id="1" href="#."><i class="fa fa-dot-circle-o"></i> Añadir opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="chekdisplay">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="checks">
                                                    <div class="col-md-12 col-sm-11 col-xs-11 input-group  form-group has-feedback" id="1">
                                                        <input type="text" class="form-control has-feedback-left option1" id="checkbox1" placeholder="Opción 1">
                                                        <span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>
                                                    </div>     
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion" id="1" href="#."><i class="fa fa-check-square-o"></i> Añadir opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="selectdisplay">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="selects">
                                                    <div class="col-md-12 col-sm-11 col-xs-11 input-group  form-group has-feedback" id="1">
                                                        <input type="text" class="form-control has-feedback-left option1" id="select1" placeholder="Opción 1">
                                                        <span class="fa fa-chevron-down form-control-feedback left" aria-hidden="true"></span>
                                                    </div>     
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion" id="1" href="#."><i class="fa fa-chevron-down"></i> Añadir opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="row">
                                <div class="btn-group-vertical btn-group-preguntas">
                                    <button type="submit" class="btn btn-default btn-pregutas" id="agregar_pregunta" title="Añadir pregunta"><i class="fa fa-plus-circle"></i></button>
                                         <!--    <a class="btn btn-default btn-pregutas" id="agregar_titulo" title="Añadir titulo"><i class="fa fa-text-width"></i></a>
                                            <a class="btn btn-default btn-pregutas" id="agregar_paso" data="<?php echo get_site_url('formulario/pasos/')?>" title="Añadir paso"><i class="fa fa-th-list"></i></a> -->
                                        </div>
                                    </div>
                                </div>
                            </div>



                            <br>
                            <div class="form-group">
                                <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-5">
                                  <!--         <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button> -->
                              </div>
                          </div>

                      </form>

<?php 
$json = '{"1":"a","2":"b","3":"c","4":"d","5":"e"}';
 $obj = json_decode($json, TRUE);

foreach($obj as $key => $value) 
{
//echo '<br> Your key is: '.$key.' and the value of the key is:'.$value;
} ?>



                      <!-- editar pregunta -->
                    <form id="form_editar_pregutnas" name="form_editar_pregutnas" class="form-horizontal form-label-left input_mask editpregunta" action="<?php echo get_site_url("formulario/update")?>" method="post">  

                        <div id="editar" class="row">
                            <div class="col-md-offset-1 col-md-9">
                               <div class="x_panel">
                                <div class="x_content">
                                    <div class="row">
                                        <div class="col-md-7 col-sm-7 col-xs-12 form-group">
                                            <input type="text" class="form-control" name="pregunta" id="edit_pregunta" placeholder="Pregunta">
                                            <input type="hidden" name="formato_pregunta" id="edit_formato_pregunta">
                                            <input type="hidden" name="opciones" id="edit_opciones">
                                            <input type="hidden" name="idpregunta" id="edit_idpregunta">
                                            <input type="hidden" name="tipo_pregunta" id="edit_tipo_pregunta">
                                            <input type="hidden" name="requerido" id="edit_requerido">
                                            <p class="text-danger" id="err"></p>

                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 form-group">
                                            <span class="view-text" id="tipopregunta"></span>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="form-group">
                                            <div class="col-md-7 col-sm-9 col-xs-12">
                                                <div class="">
                                                  <div class="col-md-12">
                                                    <input type="checkbox" value="required" name="requerido" id="edit_requerido" checked>
                                                    <label class="mouse" for='requerido' tabindex="-1">
                                                        <span class="check"></span>
                                                        Obligatorio
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-5 col-sm-5 col-xs-12 form-group">
                                            <select class="form-control" name="paso" id="edit_paso">
                                                <option>Paso</option>
                                                <?php
                                                if(is_array($data->pasos)){
                                                    foreach( $data->pasos as $paso){ ?>
                                                        <option value="<?php echo $paso->valor_paso?>">Paso <?php echo $paso->valor_paso?></option>

                                                    <?php } } ?>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="opciones_editar">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="editar_options">    
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion_editar" id="1" href="#."><i class="fa fa-check-square-o"></i> agregar opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> 
                                    <div class="chekdisplay">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="checks">
                                                    <div class="col-md-12 col-sm-11 col-xs-11 input-group  form-group has-feedback" id="1">
                                                        <input type="text" class="form-control has-feedback-left option1" id="checkbox1" placeholder="Opción 1">
                                                        <span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>
                                                    </div>     
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion" id="1" href="#."><i class="fa fa-check-square-o"></i> Añadir opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="selectdisplay">
                                        <div>
                                            <label>Opciones: </label>
                                            <div class="form-group">
                                                <div id="selects">
                                                    <div class="col-md-12 col-sm-11 col-xs-11 input-group  form-group has-feedback" id="1">
                                                        <input type="text" class="form-control has-feedback-left option1" id="select1" placeholder="Opción 1">
                                                        <span class="fa fa-chevron-down form-control-feedback left" aria-hidden="true"></span>
                                                    </div>     
                                                </div> 
                                                <div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">
                                                    <p><a class="colorgris agregar_opcion" id="1" href="#."><i class="fa fa-chevron-down"></i> Añadir opción</a></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>            
                                </div>
                            </div>
                        </div>
                        <div class="col-md-1">
                            <div class="row">
                                <div class="btn-group-vertical btn-group-preguntas">
                                    <button type="submit" class="btn btn-default btn-pregutas" id="editar_pregunta" title="Guardar"><i class="fa fa-save"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            

                            
                            <br>
                            <div class="form-group">
                                <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-5">
                                  <!--         <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button> -->
                              </div>
                          </div>

                      </form>
                    <div id="listadepreguntas" class="row">
                        <!-- NEW COL START -->
                        <article class="col-sm-12 col-md-12 col-lg-12 pasos">
                            <ul id="image-list1" class="sortable-list" data-rowId = "">
                             <?php 
                            if(is_array($data->rows)){

                                foreach( $data->pasos as $paso){
                                    echo '<hr><h4 class="text-center"><b>Paso '.$paso->valor_paso.'</b></h4> <hr>';
                                      foreach( $data->rows as $key =>$row){
                                           if ($row->paso == $paso->valor_paso) {
                                            
                                                ?>
                                                <a href="">
                                                <li id="<?php echo $row->idpregunta?>">
                                                  <div class="form-group row">
                                                      <div class="col-md-11">
                                                          <label for="sector"> 
                                                            <?php echo $row->pregunta?> 
                                                          </label>
                                                          <span class="view-text"><?php echo $row->opciones?> </span>
                                                      </div> 
                                                      <div class="col-md-1"><br>
                      
                                                          <div class="btn-group">
                                                            <a href="#editar" class="btn btn-sm btn-default edit" type="button" data-placement="top" data-toggle="tooltip"  data-rowId ="<?php echo $row->idpregunta?>" data="<?php echo get_site_url('formulario/preguntas/')?>"data-original-title="Print"><i class="fa fa-edit"></i></a>
                                                            <button class="btn btn-sm btn-default eliminar_pregunta" id="<?php echo $row->idpregunta?>" type="button" data-placement="top" data-toggle="tooltip" data-original-title="Trash"><i class="fa fa-trash-o"></i></button>
                                                        </div>
                                                      </div>
                                                  </div> 
                                              </li>
                                              </a>
                                        <?php
                                           }
                                        }

                                    }

                                 }
                                                      
                            ?>
                            </ul>
                        </article>
                       
                      <div class="ln_solid"></div>
    
                    </div>
                    <br>
                    <br>
                    <br>
                    <br>
                  
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content
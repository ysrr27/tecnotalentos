<?php  $usr = $this->uri->segment(3);?>
<br><br><br>
       <div class="x_panel user_options-forms">
              <div class="x_content">
                <br />
                <div class="row">
                    <div class="col-md-10 col-sm-12 col-xs-12 col-md-offset-1 cart-conten-form">
                        <div class="col-md-6 content-cohete">
                            <img src="<?php echo get_assets_url();?>/assets/img/Cohete-8.png">
                        </div>
                        <div class="col-md-6 col-sm-12 col-xs-12 content-forms">
                            <br><br>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel content-tabs">
                                    <div class="x_content">
                                        <div class="" role="tabpanel" data-example-id="togglable-tabs">
                                            <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                                                <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">ENTRAR</a>
                                                </li>
                                                <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">REGISTRARSE</a>
                                                </li>
                                            </ul>
                                            <div id="myTabContent" class="tab-content">
                                                <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                                                    <form class="forms_form" action="<?php echo get_site_url("login/iniciar_sesion_recluta")?>" id="session_recluta" role="form" name="session_recluta" method="post" accept-charset="utf-8">
                                                        <fieldset class="forms_fieldset">
                                                            <div class="forms_field">
                                                                <input type="email" placeholder="E-MAIL" class="forms_field-input" name="correo_session" id="correo_session" required autofocus />
                                                            </div>
                                                            <div class="forms_field">
                                                                <input type="password" placeholder="CONTRASEÑA" class="forms_field-input" name="contrasena_session" id="contrasena_session" required />
                                                            </div>
                                                        </fieldset>
                                                        <div class="forms_buttons">
                                                            <button type="button" class="Termino_uso">¿Se te olvidó tu contraseña?</button>
                                                            <button type="submit" id="ingresa_session" class="forms_buttons-action" name="ingresa_session"> Ingresar</button>
                                                        </div>
                                                    </form>
                                                </div>
                                                <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                                                    <form class="forms_form" action="<?php echo get_site_url("login/registro")?>" id="registrate" role="form" name="registrate" method="post" accept-charset="utf-8">
                                                        <fieldset class="forms_fieldset">
                                                            <div class="forms_field">
                                                                <input type="hidden" id="user" name="user" value="<?php echo $usr; ?>">
                                                                <input type="text" placeholder="NOMBRES" class="forms_field-input" name="nombre" id="nombre" required />
                                                            </div>
                                                            <div class="forms_field">
                                                                <input type="text" placeholder="APELLIDOS" class="forms_field-input" name="apellido" id="apellido" required />
                                                            </div>
                                                            <div class="forms_field">
                                                                <input type="email" placeholder="E-MAIL" class="forms_field-input" name="correo" id="correo" required />
                                                            </div>
                                                            <div class="forms_field">
                                                                <input type="password" placeholder="CONTRASEÑA" class="forms_field-input" name="contrasena" id="contrasena" required />
                                                            </div>
                                                            <div class="form-group row"><br>
                                                                <div class="checkbox">
                                                                    <input type="checkbox" name="Termino" id="Termino" value="Termino" class="flat Termino" />
                                                                    <label for="Termino" class="Termino_uso">He leído y acepto los <a href="<?php echo get_site_url("politicas")?>" style="color: #3c62a0;">Términos y Condiciones</a> de uso.</label>
                                                                </div>
                                                            </div>
                                                        </fieldset>
                                                        <div class="forms_buttons col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                           <button type="submit" id="envar_registro" class="forms_buttons-action" name="envar_registro"> REGISTRARSE</button>
                                                       </div>
                                                   </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


              </div>
            </div>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>


  <div>
      <a class="hiddenanchor" id="signup"></a>
      <a class="hiddenanchor" id="signin"></a>

      <div class="login_wrapper">
        <div class="animate form login_form">
          <section class="login_content">
            <form method="post" action="<?php echo base_url() ?>index.php/login/iniciar_sesion_post">

              <h1>Acceder</h1>
              <div>
                <input type="text" name="correo" class="form-control" placeholder="Usuario" required="" />
              </div>
              <div>
                <input type="password" name="contrasena" class="form-control" placeholder="Contraseña" required="" />
              </div>
               <div class="col-md-4 col-md-offset-2">
                <input type="submit" class="btn btn-default submit sw-btn-next" value="Entrar" />
              </div>
               <div class="col-md-6">
                <a class="reset_pass" href="<?php echo get_site_url('/usuarios/recover')?>" style="color:#333 !important;">¿olvido su contraña?</a>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                <div class="clearfix"></div>
                <br />

                <div>
                  <p> Política y Privacidad  © Copyright 2019 Copyright.es - Todos los Derechos Reservados </p>
                </div>
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
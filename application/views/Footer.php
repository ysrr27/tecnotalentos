    </div>
    </div>
  <!-- footer content -->

<?php if($menu){?>    
        <footer>
          <div class="pull-right">
           © Copyright 2020 TECNO-RED Soluciones 14 C.A
          </div>
          <div class="clearfix"></div>
        </footer>
<?php } ?>
        <!-- /footer content -->
      </div>
    </div>

  <?php 
    $url = $this->uri->segment(1);
if (empty($url)) { ?>
<!--     <script src="<?php echo get_assets_url();?>assets/jquery/dist/jquery.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/home.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/menu.js"></script> -->


   <script src="<?php echo get_assets_url();?>assets/theme_chevere/js/core.min.js"></script>

<script>
(function ($) {
    $.fn.sliderVideo = function (options) {
        options = $.extend({
            scrollerSelector: '.slider-video-scroll',
            itemSelector: '.slider-video-item',
            aspectSelect: '.slider-video-aspect',
            navLeftSelector: '.slider-video-left',
            navRightSelecto: '.slider-video-right',
            scrollTime: 500,
            animationTime: 300,
            visibleItems: 3,
            itemsGap: 10,
            aspectRation: 0.625
        }, options);

        var $slider = $(this);
        var $scroller = $slider.find(options.scrollerSelector);
        var $aspect = $slider.find(options.aspectSelect);
        var $items = $slider.find(options.itemSelector);
        var $navLeft = $slider.find(options.navLeftSelector);
        var $navRight = $slider.find(options.navRightSelecto);
        $items.bind('click', onItemClick);
        $navLeft.bind('click', onNavLeftClick);
        $navRight.bind('click', onNavRightClick);
        $slider.bind('scrollToIndex', function (event, index) {
            scrollToIndex(index);
        });

        initializeSize();
        initializeOrigin();
        initializeNavigation();

        function initializeSize() {
            $aspect.height($aspect.width() * options.aspectRation);
            $scroller.width((50 * $items.length) + '%');

            var itemWidth = $aspect.width() / options.visibleItems;
            var itemHeight = itemWidth * options.aspectRation;
            var positionTop = ($aspect.height() - itemHeight) / 2;
            $items.each(function (i) {
                $(this).css({
                    top: positionTop,
                    left: i * (itemWidth + options.itemsGap),
                    width: itemWidth - options.itemsGap + 'px',
                    height: itemHeight + 'px'
                })
            });
        }

        function initializeOrigin() {
            var aspectScrollLeft = $aspect.scrollLeft()
            $items.each(function (i) {
                var $item = $(this);
                if ($item.position().left > aspectScrollLeft) {
                    $item.css({
                        'transform-origin': '100% 50%'
                    });
                } else {
                    $item.css({
                        'transform-origin': '0 50%'
                    });
                }
            })
        }

        function initializeNavigation() {
            var visibleItems = getVisibleItems();

            $navLeft.toggle(visibleItems.first().prev().length > 0);
            $navRight.toggle(visibleItems.last().next().length > 0);
        }

        function onItemClick() {
            var $item = $(this);

            if ($item.is(".active")) {
                $item.removeClass('active');
                toggleVideo($item, false);
                initializeSize();

                $item.addClass('active-out');
                setTimeout(function () {
                    $item.removeClass('active-out');
                }, options.animationTime)
            }
            else {
                $item.css({
                    width: $aspect.width(),
                    height: $aspect.height(),
                    left: $aspect.scrollLeft(),
                    top: 0
                });
                $item.addClass('active');
                setTimeout(function () {
                    toggleVideo($item, true);
                }, 300)
            }
        }

        function onNavRightClick() {
            scrollTo(1);
        }

        function onNavLeftClick() {
            scrollTo(-1);
        }


        function toggleVideo($item, status) {
            $item.find('.slider-video-pic').toggle(!status);
            $item.find('.slider-video-play').toggle(status);
            var frame = $item.find('iframe')[0];
            frame.contentWindow.postMessage('{"event":"command","func":"' + (status ? 'playVideo' : 'pauseVideo') + '","args":""}', '*')
        }

        function scrollTo(direction) {

            if ($items.is('.active')) {
                var $active = $('.slider-video-item').filter('.active');
                var $next = direction > 0 ? $active.next() : $active.prev();
                if ($next.length) {
                    scrollToIndex($next.index());
                    $active.trigger('click');
                    setTimeout(function () {
                        $next.trigger('click');
                        initializeNavigation();
                    }, options.scrollTime)
                }
            } else {
                var visibleItems = getVisibleItems();

                if (direction > 0) {
                    scrollToIndex(visibleItems.last().next().index());
                } else {
                    scrollToIndex(visibleItems.first().prev().index());
                }
            }
        }

        function scrollToIndex(index) {
            var $item = $items.eq(index);
            if ($item.length) {
                var itemWidth = $aspect.width() / options.visibleItems;

                var itemLeft = $item.position().left;
                var targetPosition = null;

                if (itemLeft > $aspect.scrollLeft() + $aspect.width()) {
                    targetPosition = itemLeft - itemWidth - options.itemsGap;
                } else if (itemLeft < $aspect.scrollLeft()) {
                    targetPosition = itemLeft;
                }

                if (targetPosition != null) {
                    $aspect.animate({ scrollLeft: targetPosition + 'px' }, options.scrollTime, function () {
                        initializeOrigin();
                        initializeNavigation();
                    });
                }
            }
        }

        function getVisibleItems() {
            return $items.filter(function (i) {
                var posLeft = $(this).position().left;
                return posLeft >= $aspect.scrollLeft() && posLeft < $aspect.scrollLeft() + $aspect.width();
            });
        }

    };
})(jQuery);

$(function() {
  $('.slider-video').sliderVideo();
})
</script>

    <script src="<?php echo get_assets_url();?>assets/theme_chevere/js/script.js"></script>
    <!-- coded by Himic-->


 <?php  }else{ ?>


    <!-- jQuery -->
    <script src="<?php echo get_assets_url();?>assets/jquery/dist/jquery.min.js"></script>
    <!-- Bootstrap -->
    <script src="<?php echo get_assets_url();?>assets/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src='<?php echo get_assets_url();?>assets/chosen/chosen.jquery.js'></script>

    <!-- FastClick -->
    <script src="<?php echo get_assets_url();?>assets/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo get_assets_url();?>assets/iCheck/icheck.js"></script>
    <script src="<?php echo get_assets_url();?>assets/nprogress/nprogress.js"></script>

    <script src="<?php echo get_assets_url();?>assets/bootstrap-progressbar/bootstrap-progressbar.min.js"></script> 
    <script src="<?php echo get_assets_url();?>assets/editable/jquery.editable.js"></script>

    <!-- Custom Theme Scripts -->
    <script src="<?php echo get_assets_url();?>assets/js/custom.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/preguntas.js"></script>

    <script src="<?php echo get_assets_url();?>assets/bootstrap-validator/validator.min.js"></script>

    <script type="text/javascript" src="<?php echo get_assets_url();?>assets/jQuery-Smart-Wizard/dist/js/jquery.smartWizard.js"></script>
     <script src="<?php echo get_assets_url();?>assets/datatables.net/js/jquery.dataTables.min.js"></script>

    <script src="<?php echo get_assets_url();?>assets/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>

    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/dataTables.buttons.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons-bs/js/buttons.bootstrap.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.flash.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.html5.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-buttons/js/buttons.print.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-keytable/js/dataTables.keyTable.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-responsive/js/dataTables.responsive.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-responsive-bs/js/responsive.bootstrap.js"></script>
    <script src="<?php echo get_assets_url();?>assets/datatables.net-scroller/js/dataTables.scroller.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/libs/1.11.4-jquery-ui.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/micuenta.js"></script>
    <!-- iCheck -->
    <!-- jquery.inputmask -->
    <script src="<?php echo get_assets_url();?>assets/jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>

    <script src="<?php echo get_assets_url();?>assets/alertify/lib/alertify.min.js"></script> 

    <script src="<?php echo get_assets_url();?>assets/switchery/dist/switchery.min.js"></script>
    <script src="<?php echo get_assets_url();?>assets/moment/min/moment.min.js"></script>

    <script src="<?php echo get_assets_url();?>assets/bootstrap-daterangepicker/daterangepicker.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/jquery.validate.min.js"></script>

    <!-- Registro -->
    <script src="<?php echo get_assets_url();?>assets/js/registro.js"></script>
    <script src="<?php echo get_assets_url();?>assets/js/solicitudes.js"></script>

    <script src="<?php echo get_assets_url();?>assets/js/menu.js"></script>

      <script type="text/javascript">
      

var width = $( window ).width();



/*
ocultar la barra del candidato
 $(function() {
              $(".wrp_rec").animate({"height":$(window).height()-0},300);

          $(document).scroll(function () {
           var $nav2 = $(".mg-user-header_rec");
           console.log($nav2.height()) ;
            $nav2.toggleClass('distancia', $(this).scrollTop() > $nav2.height());

          });
        });*/


      //ajax para recuperar contraseña
   $('#recover').submit(function(e) {
        e.preventDefault();
        var form = $("#recover");
        var url = form.attr('action');
        var simple = '<?php echo get_site_url("/admin")?>';

 $("#loading-text").empty();
        $("#loading-text").append("<strong>Enviando...</strong>");
        $("#loading-wrapper").fadeIn(1500);

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
               var data = data;

               console.log(data)
               if(data != 1) {
                $(".login_content").empty();
                $(".login_content").html('<br><br><br><br><br><br><br><br><br><br><div class="" style="border: 0;"><a href="'+simple+'" class="site_title" style="font-size: 68px;margin: -58px 43px 30px 5px;color: #6a6a6a !important;height: 116px;"><i class="fa fa-envelope" style="padding: 21px 23px;"></i></a></div><p><b>Se recuperó la cuenta correctamente</b></p><p>Ingresa a tu correo electrónico. te enviaremos los pasos para recuperar el acceso a tu cuenta.</p><hr><a href="'+simple+'" class="btn btn-primary sw-btn-next"><i class="fa fa-unlock"></i> Acceder</a><div>');
                console.log(data);
                 $("#loading-wrapper").fadeOut(1500);

                 //   window.location.replace(simple+"/"+data);
               }else{
                 $("#error").html('<div class="alert alert-danger alert-dismissible fade in" role="alert"><strong>El cuenta de correo no existe.</strong> </div>');
                  $("#loading-wrapper").fadeOut(1500);

               }
          }
     });
   });

      
    function keypresstags(valor,clase) {

        if (event.which == '13') {
          var contains = "."+clase;
          var nameinput = clase+"[]";

            if ((valor != '') && ($(contains+ ".addedTag:contains('" + valor + "') ").length == 0 ))  {
                    $('<li class="addedTag">' + valor + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + valor + '" name="'+nameinput+'"></li>').insertBefore('.'+clase+' .tagAdd');
                    $(".search-field").val('');

            } else {
                $(".search-field").val('');

            }
        }

    }
      
      

  $(".sortable-list").sortable({
        delay: 150,
        stop: function() {
           var changedList = "#"+this.id;
            var selectedData = new Array();
            $(changedList+'>li').each(function() {
                selectedData.push($(this).attr("id"));
            });
            updateOrder(selectedData);
        }
    });


    function updateOrder(data) {
       console.log(data);
        $.ajax({
            url:"<?php echo get_assets_url();?>/formulario/ajaxPro/",
            type:'post',
            data:{position:data},
            success:function(){
            }
        })
    }



        $(function() {

          $(document).scroll(function () {
            var $nav = $("#navbar-fixed-top");
            $nav.toggleClass('scrolled', $(this).scrollTop() > $nav.height());
          });
        });

        $(document).ready(function() {
          $( window ).load(function() {
            $('#enviar').removeAttr('disabled');
            $("#enviar").removeClass("disabled");
          });



          $(window).scroll( function(){

            $('.fadeInBlock').each( function(i){

              var bottom_of_object = $(this).offset().top + $(this).outerHeight();
              var bottom_of_window = $(window).scrollTop() + $(window).height();
              if( bottom_of_window > bottom_of_object ){

                $(this).animate({'opacity':'1'},500);

              }

            }); 

          });

        });

        $(function(){
          $('.fadeInPage').hide().fadeIn('slow');
        });
        

        window.addEventListener("keypress", function(event){
          if (event.keyCode == 13){
            event.preventDefault();
          }
        }, false);
     


        $(document).ready(function() {

  
   $('#myForm').submit(function(e) {
        e.preventDefault();
        var form = $("#myForm");
        var url = form.attr('action');
        var simple = '<?php echo get_site_url("candidatos/preview")?>';

        $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
               $data = data;
               if ($data != "false") {

                console.log(data);
                    window.location.replace(simple+"/"+data);
               }
          }
     });
   });


   $("input[type='radio'][name='Acepto la política de privacidad ']").change(function() {

     if (this.value == "No") {
      $(".enviarformulario").addClass("disabled");
      $(".polit").empty();
      $('.polit').append('<ul class="list-unstyled"><li>Debe aceptar la politica de privacidad.</li></ul>');
      $('.polit').show();
     }  
     if (this.value == "Si") {
      $(".enviarformulario").removeClass("disabled");
      $(".polit").hide();
      $(".polit").empty();
     }       

   });

   //Validar formato de correo
    function isEmail(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }


    $('#contacto').submit(function(e) {
        e.preventDefault();

        var form = $("#contacto");
        var url = form.attr('action');
        var simple = '<?php echo get_site_url("candidatos/preview")?>';


        var empty = true;
        $('input[type="text"]').each(function(){
            if($(this).val()==""){
                empty =false;
                return false;
            }
            if($('input[name="Nombre_empresa"]').is(':checked')) {
                empty =true;
            }else{
                empty =false;
            }
        });
        var response = grecaptcha.getResponse();
        if(response.length == 0){
            alert("Debe completar los datos");
            $("#capcha").empty();
            $( "#capcha" ).append( "<p class='text-danger'>Debe completar el reCAPTCHA</p>" );
            $("#loading-wrapper").fadeOut(1500);

        }else{
            if (empty) {
                var email =  $('#Correo_recluta').val();
                if (email != "" && !isEmail(email)) {
                    alert("correo incorrecto");
                    $("#form-step-0").focus();
                }else{
                    $("#loading-text").empty();
                    $("#loading-text").append("<strong>Enviando...</strong>");
                    $("#loading-wrapper").fadeIn(500);
                    $("#modalcontacto").modal('show');
                    $("#loading-wrapper").fadeOut(2000);
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), 
                        success: function(data){
                            var data = data;
                            $("#loading-wrapper").fadeOut(1500);

                            console.log(data);
                        }
                    });
                }
            }else{
                console.log("error validate!");
                $("#loading-wrapper").fadeOut(1500);
                $("#loading-text").empty();

                if ($('#Termino').is(':checked')) {
                    $('#enviar').removeAttr('disabled');
                    $('#enviar').removeClass('disabled');
                    $('#enviar').removeAttr("disabled");
                    $("#enviar").css({"display": "block", "opacity": "1"});
                } else {
                    $('#enviar').attr('disabled', 'disabled');
                }
                return false;
            }
        }
    });

    $( "#btnclose" ).click(function() {
       window.location.href = '<?php echo get_site_url("/")?>';
   });



     $( "#menu_toggle" ).click(function(e) {
        e.preventDefault();
        var clase = $("#img-perf-head").hasClass("img-circle");

        if (clase) {
          $("#img-perf-head").attr("src","http://localhost/tecnotalentos/assets/img/TecnoRed.png").removeClass("img-circle");
        }else{
          $("#img-perf-head").attr("src","http://localhost/tecnotalentos/assets/img/favicon.png").addClass("img-circle");

        }

     });


            // Smart Wizard
            $('#smartwizard').smartWizard({
                  selected: 0,  // Initial selected step, 0 = first step 
                  keyNavigation:false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                  contentCache:false,
                  autoAdjustHeight:true, // Automatically adjust content height
                  cycleSteps: false, // Allows to cycle the navigation of steps
                  backButtonSupport: true, // Enable the back button support
                  useURLhash: true, // Enable selection of the step based on url hash
                  lang: {  // Language variables
                      next: 'Siguiente', 
                      previous: 'Atras'
                  },
                  toolbarSettings: {
                      toolbarPosition: 'bottom', // none, top, bottom, both
                      toolbarButtonPosition: 'right', // left, right
                      showNextButton: true, // show/hide a Next button
                      showPreviousButton: true, // show/hide a Previous button

                      toolbarExtraButtons: [
                  $('<button></button>').text('Enviar')
                                .addClass('btn btn-info enviarformulario')
                      
                                .on('click', function(){ 

                                }),
                  $('').text('')
                                .addClass('btn btn-danger')
                                .on('click', function(){ 
                              alert('Cancel button click');                            
                                })
                            ]
                  }, 
                  anchorSettings: {
                      anchorClickable: true, // Enable/Disable anchor navigation
                      enableAllAnchors: false, // Activates all anchors clickable all times
                      markDoneStep: true, // add done css
                      enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                  },            
                  contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
                  disabledSteps: [],    // Array Steps disabled
                  errorSteps: [],    // Highlight step with errors
                  theme: 'circles',
                  transitionEffect: 'fade', // Effect on navigation, none/slide/fade
                  transitionSpeed: '400'
            });

          
            $("#smartwizard").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                var elmForm = $("#form-step-" + stepNumber);

                var env = $("#enviar").val();

                var url = $(location).attr('href').split("/").splice(6, 9).join("/");
                var url2 = url.split('#')[1].substring(5, 6);

                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    if(elmErr && elmErr.length > 0){
                        // Form validation failed
                        return false;
                    }
                }
                return true;
            });

            //candidato

            //funcion load
            setTimeout(function() {
              $("#loading-wrapper").fadeOut(1500);
            },100);



            // Smart Wizard

            $('#candidato').smartWizard({
                  selected: 0,  // Initial selected step, 0 = first step 
                  keyNavigation:false, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
                  contentCache:false,
                  autoAdjustHeight:true, // Automatically adjust content height
                  cycleSteps: false, // Allows to cycle the navigation of steps
                  backButtonSupport: true, // Enable the back button support
                  useURLhash: true, // Enable selection of the step based on url hash
                  lang: {  // Language variables
                      next: 'Siguiente', 
                      previous: 'Atras'
                  },
                  toolbarSettings: {
                      toolbarPosition: 'bottom', // none, top, bottom, both
                      toolbarButtonPosition: 'right', // left, right
                      showNextButton: true, // show/hide a Next button
                      showPreviousButton: true, // show/hide a Previous button

                      toolbarExtraButtons: [
                  $('<button></button>').text('Enviar')
                                .addClass('btn btn-info enviarformulario')
                      
                                .on('click', function(){ 

                                }),
                  $('').text('')
                                .addClass('btn btn-danger')
                                .on('click', function(){ 
                              alert('Cancel button click');                            
                                })
                            ]
                  }, 
                  anchorSettings: {
                      anchorClickable: true, // Enable/Disable anchor navigation
                      enableAllAnchors: false, // Activates all anchors clickable all times
                      markDoneStep: true, // add done css
                      enableAnchorOnDoneStep: true // Enable/Disable the done steps navigation
                  },            
                  contentURL: null, // content url, Enables Ajax content loading. can set as data data-content-url on anchor
                  disabledSteps: [],    // Array Steps disabled
                  errorSteps: [],    // Highlight step with errors
                  theme: 'circles',
                  transitionEffect: 'fade', // Effect on navigation, none/slide/fade
                  transitionSpeed: '400'
            });



  


          
            $("#candidato").on("leaveStep", function(e, anchorObject, stepNumber, stepDirection) {
                var elmForm = $("#form-step-" + stepNumber);




                /*var env = $("#enviar").val();

                var url = $(location).attr('href').split("/").splice(6, 9).join("/");
                var url2 = url.split('#')[1].substring(5, 6);*/

               if(stepNumber == 2){
                 var check = $("input[name='Predisposición para trabajar en otro paises']:checked").length;
                 console.log(check);
                 $(".errCheck").empty();
                 $('.errCheck').append('<ul class="list-unstyled"><li>Debe seleecionar una opcion.</li></ul>');
                 $('.errCheck').show();
                 $('.errCheck').attr('id', '1');
                 $('.checkbox').animate({
                   scrollTop: 0
                 }, 'slow');

               }

               if(stepNumber == 3){
                 var check = $("input[name='¿Qué buscas? (puedes seleccionar varias)']:checked").length;
                 console.log(check);
                 $(".errCheck").empty();
                 $('.errCheck').append('<ul class="list-unstyled"><li>Debe seleecionar una opcion.</li></ul>');
                 $('.errCheck').show();
                 $('.errCheck').attr('id', '1');
                 $('.checkbox').animate({
                   scrollTop: 0
                 }, 'slow');

               }




                if(stepDirection === 'forward' && elmForm){
                    elmForm.validator('validate');
                    var elmErr = elmForm.children('.has-error');
                    var elmErr2 = $(".errCheck").attr('id');
                    if(elmErr && elmErr.length > 0){
                        // Form validation failed
                         $('.errCheck').attr('id', '0');
                         $(".errCheck").fadeOut(10000);

                        return false;
                    }
                }

                $(".errCheck").empty();
                return true;
            });




            //dataTable
           $('#example').DataTable( {
           "initComplete": function(settings, json) {
          $("#example").removeClass( "display" );
          },
            dom: 'Bfrtip',
            order:[1,"desc"],
              /* 'columnDefs': [{ 
                            'targets': [1], 
                            'visible': false, 
                        }],*/
            buttons: [
                'excelHtml5',
                'pdfHtml5'
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        } );

                   //dataTable
                   $('#table_banner').DataTable( {
           "initComplete": function(settings, json) {
          $("#table_banner").removeClass( "display" );
          },
            dom: 'Bfrtip',
            order:[1,"desc"],
              /* 'columnDefs': [{ 
                            'targets': [1], 
                            'visible': false, 
                        }],*/
            buttons: [
                'excelHtml5',
                'pdfHtml5'
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        } );

        //table banner
        $('#table_banner tbody').on( 'click', 'button', function () {
      
      var table =  $('#table_banner').DataTable();

      var id=$(this).attr('id');

      var idbanner = jQuery(this).attr("id");


      var rows = $(this).parents('tr')

        alertify.confirm("¿Esta seguro que desea eliminar este banner?",
          function (e) {
              if (e) {
                  $.ajax({
                      url: '<?php echo get_site_url('banners/delete')?>',
                      type: 'get',
                      data: { 'idbanner' : idbanner},
                      success: function (data) {
                          console.log(data);
                          alertify.success('banner Eliminado');
                          table.row(rows).remove().draw();


                      },error: function (err) {
                          console.log(err);
                      }
                  });
                  return false;
              } else {
                  alertify.error("Usted ha cancelado la solicitud");

              }
          },
          function () {
              var error = alertify.error('Cancel');
          });
       
       } );

            //dataTable
           $('#meta').DataTable( {
           "initComplete": function(settings, json) {
          $("#example").removeClass( "display" );
          },
            dom: 'Bfrtip',
            order:[1,"desc"],
               'columnDefs': [{ 
                            'targets': [1,3,4], 
                            'visible': false, 
                        }],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        } );


        $('#example tbody').on( 'click', 'button', function () {
      
        var table =  $('#example').DataTable();

        var id=$(this).attr('id');

        var idusuario = jQuery(this).attr("id");

        console.log(idusuario);


        var rows = $(this).parents('tr')

          alertify.confirm("¿Esta seguro que desea eliminar este usuario?",
            function (e) {
                if (e) {
                    $.ajax({
                        url: '<?php echo get_site_url('usuarios/delete')?>',
                        type: 'get',
                        data: { 'id' : idusuario},
                        success: function (data) {
                            console.log(data);
                            alertify.success('Usuario Eliminado');
                            table.row(rows).remove().draw();

  
                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
                } else {
                    alertify.error("Usted ha cancelado la solicitud");

                }
            },
            function () {
                var error = alertify.error('Cancel');
            });
         
         } );


         //dataTable
           $('#censo').DataTable({
           "initComplete": function(settings, json) {
          $("#censo").removeClass( "display" );
          },
            dom: 'Bfrtip',
            order:[1,"desc"],
               'columnDefs': [{ 
                            'targets': [1], 
                            'visible': false, 
                        }],
            buttons: [
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        });


        $('#censo tbody').on( 'click', 'button', function () {
      
        var table =  $('#censo').DataTable();

        var id=$(this).attr('id');

        var idusuario = jQuery(this).attr("id");

        var rows = $(this).parents('tr')

          alertify.confirm("¿Esta seguro que desea eliminar este candidato?",
            function (e) {
                if (e) {
                    $.ajax({
                        url: '<?php echo get_site_url('candidatos/delete/')?>',
                        type: 'get',
                        data: { 'id' : idusuario},
                        success: function (data) {
                            console.log(data);
                            alertify.success('Candidato Eliminado');
                            table.row(rows).remove().draw();
                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
                } else {
                    alertify.error("Usted ha cancelado la solicitud");

                }
            },
            function () {
                var error = alertify.error('Cancel');
            });
         
         });


        //Reclutas
         //dataTable
           $('#reclutas').DataTable({
           "initComplete": function(settings, json) {
          $("#reclutas").removeClass( "display" );
          },
            dom: 'Bfrtip',
            order:[1,"desc"],
               'columnDefs': [{ 
                            'targets': [1], 
                            'visible': false, 
                        }],
            buttons: [
            ],
            language: {
                search: "Buscar",
                paginate: {
                    first:      "Primero",
                    previous:   "Anterior",
                    next:       "Siguiente",
                    last:       "Último"
                },
                info:           "",
                scrollY: 1300,
                "lengthMenu":     "_MENU_"
            }
        });


        $('#reclutas tbody').on( 'click', 'button', function () {
      
        var table =  $('#reclutas').DataTable();

        var id=$(this).attr('id');

        var idusuario = jQuery(this).attr("id");

        var rows = $(this).parents('tr')

          alertify.confirm("¿Esta seguro que desea eliminar este recluta?",
            function (e) {
                if (e) {
                    $.ajax({
                        url: '<?php echo get_site_url('usuarios/delete')?>',
                        type: 'get',
                        data: { 'id' : idusuario},
                        success: function (data) {
                            console.log(data);
                            alertify.success('Recluta Eliminado');
                            table.row(rows).remove().draw();
                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
                } else {
                    alertify.error("Usted ha cancelado la solicitud");

                }
            },
            function () {
                var error = alertify.error('Cancel');
            });
         
         });

        }); 
      </script>
   <?php } ?>





  </body>
</html>

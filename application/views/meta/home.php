<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                  <a>Home</a>
                  Usuarios
                </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><i class="fa fa-code"></i> Metadatos</small></h2>
                          
                    <div class="clearfix"></div>
                  </div>

                  <div class="x_content">

                  <table id="meta" class="row-border" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>Pagína</th>
                              <th>Titulo</th>
                              <th>Tipo</th>
                              <th>Url</th>
                              <th>Descripcion</th>
                              <th>Nombre sitio</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                if(is_array($data->rows )){
                  foreach(  $data->rows as $key =>$row){
                            ?>
                          <tr>
                              <td>
                                <div class="btn-group  btn-group-sm">
                                  <a href="<?php echo get_site_url('meta/editar/'.$row->idmeta)?>" class="btn btn-default sw-btn-next" type="button"><i class="fa fa-edit"></i></a> 
                                </div>
                              </td>
                              <td><?php echo  $row->pagina ?></td>
                              <td><?php echo  $row->title ?></td>
                              <td><?php echo  $row->type ?></td>
                              <td><?php echo  $row->url ?></td>
                              <td><?php echo  $row->description ?></td>
                              <td><?php echo  $row->site_name ?></td>
                          </tr>
                          <?php

                        }
                      }                          
                      ?>
                      </tbody>
                      <tfoot>
                          <tr>
                            <td colspan="7"></td>
                          </tr>
                      </tfoot>
                  </table>

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


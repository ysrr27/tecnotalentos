
        <div class="right_col" role="main">
          <div class="">
            <div class="clearfix"></div>
            <div class="row">
              <div class="col-md-12 col-xs-12">
                <div class="breadcrumb">
                  <a>Home</a>
                  Metadatos
                </div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2>Metadatos</h2>
                        <div class="clearfix"></div>
                    </div>

                  <div class="x_content">
                    <br />
                    <?php echo validation_errors(); ?>
                    <form class="form-horizontal form-label-left input_mask" action="<?php echo get_site_url("meta/update")?>" method="post">  
                      <input type="hidden" id="idmeta" name="idmeta" value="<?php echo (!empty($data->idmeta) && $data->idmeta > '') ? $data->idmeta : ''; ?>">
                      <input type="hidden" id="pagina" name="pagina" value="<?php echo (!empty($data->pagina) && $data->pagina > '') ? $data->pagina : ''; ?>">
                    <br>
                      <div class="row">
                          <div class="col-md-12">
                            <label for="title">Titulo:</label>
                            <input type="text" class="form-control" name="title" id="title" placeholder="" value="<?php echo (!empty($data->title) && $data->title > '') ? $data->title : ''; ?>" required>
                            <div class="help-block with-errors"></div>
                          </div>      
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <label for="Mesaje_recluta">Descriptión</label>
                            <textarea id="description" required="required" class="form-control" name="description"><?php echo (!empty($data->description) && $data->description > '') ? $data->description : ''; ?></textarea>
                            <div class="help-block with-errors"></div>
                          </div>      
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <label for="Mesaje_recluta">keywords</label>
                            <textarea id="keywords" class="form-control" name="keywords"><?php echo (!empty($data->keywords) && $data->keywords > '') ? $data->keywords : ''; ?></textarea>
                            <div class="help-block with-errors"></div>
                          </div>      
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <label for="Nombre">Nombre del sitio:</label>
                            <input type="text" class="form-control" name="site_name" id="site_name" placeholder="" value="<?php echo (!empty($data->site_name) && $data->site_name > '') ? $data->site_name : ''; ?>" required>
                            <div class="help-block with-errors"></div>
                          </div>      
                      </div>
                      <div class="row">
                          <div class="col-md-12">
                            <label for="Nombre">Localización:</label>
                            <input type="text" class="form-control" name="locale" id="locale" placeholder="" value="<?php echo (!empty($data->locale) && $data->locale > '') ? $data->locale : ''; ?>" required>
                            <div class="help-block with-errors"></div>
                          </div>      
                      </div>
                      <br>
                      <div class="ln_solid"></div>
                      <div class="form-group">
                        <div class="col-md-7 col-sm-7 col-xs-12 col-md-offset-5">
                          <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button>
                        </div>
                      </div>

                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content
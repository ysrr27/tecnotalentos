<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb">
                        <a>Home</a>
                        <a>Candidatos</a>
                        Editar
                   </div>
                   <div id="loading-wrapper"  ng-show="dataloading === 'true'">
                        <div id="loading-text">Cargando...</div>
                        <div id="loading-content"></div>
                    </div>
                <div class="x_panel">
                
                  <div class="x_content">


                    <!-- Smart Wizard -->
                  <form action="<?php echo get_site_url("candidatos/candidatos/update")?>" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
                        <!-- SmartWizard html -->

                            <?php 
                             $id = $this->uri->segment(4);
                             if($id > 0){
                                    $var = 'done';
                                  }else{
                                    $var = '';
                                  } 
                             ?>
                        <div id="smartwizard" class="form_wizard wizard_verticle">
                            <ul>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-1">Paso 1<br /></a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-2">Paso 2<br /></a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-3">Paso 3<br /></small></a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-4">Paso 4<br /></a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-5">Paso 5 <br></a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-6">Paso 6<br></small>
                                    </a>
                                </li>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                    <a href="#step-7">Step 7<br></small>
                                    </a>
                                </li>  
                            </ul>

                            <div>

                                <div id="step-1">
                                    <div id="form-step-0" role="form" data-toggle="validator">
                                        <br>
                                         <input type="hidden" class="form-control" name="id_datos" id="id_datos" value="<?php echo (!empty($data->id_datos) && $data->id_datos > '') ? $data->id_datos : ''; ?>">
                                         
                                         <input type="hidden" class="form-control" name="id_geograficos" id="id_geograficos" value="<?php echo (!empty($data->id_geograficos) && $data->id_geograficos > '') ? $data->id_geograficos : ''; ?>">
                                        
                                        <input type="hidden" class="form-control" name="id_aptitudes" id="id_aptitudes" value="<?php echo (!empty($data->id_aptitudes) && $data->id_aptitudes > '') ? $data->id_aptitudes : ''; ?>">

                                         <input type="hidden" class="form-control" name="id_datos_academicos" id="id_datos_academicos" value="<?php echo (!empty($data->id_datos_academicos) && $data->id_datos_academicos > '') ? $data->id_datos_academicos : ''; ?>">

                                         <input type="hidden" class="form-control" name="id_otra" id="id_otra" value="<?php echo (!empty($data->id_otra) && $data->id_otra > '') ? $data->id_otra : ''; ?>">

                                         <input type="hidden" class="form-control" name="id_candidadto" id="id_candidadto" value="<?php echo (!empty($data->id_candidadto) && $data->id_candidadto > '') ? $data->id_candidadto : ''; ?>">

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="rif">Dirección de correo electrónico *:</label>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">

                                                <div class="col-md-6">
                                                    <label for="Correo">Correo:</label>
                                                    <input type="text" class="form-control" name="Correo" id="Correo" placeholder="" value="<?php echo (!empty($data->Correo) && $data->Correo > '') ? $data->Correo : ''; ?>" required>
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <hr>
                                            <h4 class="text-center"><b>DATOS PERSONALES</b></h4>
                                            <hr>  
                                            <div class="col-md-6">
                                                <label for="Nombre">Nombre:</label>
                                                <input type="text" class="form-control" name="Nombre" id="Nombre" placeholder="" value="<?php echo (!empty($data->Nombre) && $data->Nombre > '') ? $data->Nombre : ''; ?>" required>
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Apellido"> Apellido:</label>
                                                <input type="text" class="form-control" name="Apellido" id="Apellido" placeholder="" value="<?php echo (!empty($data->Apellido) && $data->Apellido > '') ? $data->Apellido : ''; ?>" required>
                                                <div class="help-block with-errors"></div> 
                                            </div>      
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="Telefono">Teléfono:</label>
                                                <input type="text" class="form-control" name="Telefono" id="Telefono" placeholder="" value="<?php echo (!empty($data->Telefono) && $data->Telefono > '') ? $data->Telefono : ''; ?>" required>
                                                <div class="help-block with-errors"></div>
                                            </div> 
                                            <div class="col-md-6">
                                                <label for="Fecha_nacimiento">Fecha de nacimiento:</label>
                                                <input type="text" class="form-control" name="Fecha_nacimiento" id="Fecha_nacimiento" placeholder="" value="<?php echo (!empty($data->Fecha_nacimiento) && $data->Fecha_nacimiento > '') ? $data->Fecha_nacimiento : ''; ?>" required>
                                                <div class="help-block with-errors"></div>
                                            </div> 
                                        </div>
                                    </div>
                                </div>
                                <div id="step-2">
                                   <div id="form-step-1" role="form" data-toggle="validator">
                                       <hr><h4 class="text-center"><b>DATOS 2.0</b></h4><hr>  
                  
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                   <label for="Youtube">Blog / canal de Youtube u otro perfil donde transmitas tus conocimientos:</label>
                                                   <input type="text"  class="form-control" name="Youtube" id="Youtube" placeholder="" value="<?php echo (!empty($data->Youtube) && $data->Youtube > '') ? $data->Youtube : ''; ?>"  required>
                                                   <div class="help-block with-errors"></div>
                                              </div> 
                                              <div class="col-md-6">
                                                   <label for="Facebook">Perfil de Facebook <br> (indica la url completa):</label> 
                                                   <input type="text" class="form-control " name="Facebook" id="Facebook" value="<?php echo (!empty($data->Facebook) && $data->Facebook > '') ? $data->Facebook : ''; ?>" required>
                                                   <div class="help-block with-errors"></div>
                                              </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                   <label for="Twitter">Perfil de Twitter (indica la url completa):</label>
                                                   <input type="text"  class="form-control" name="Twitter" id="Twitter" placeholder="" value="<?php echo (!empty($data->Twitter) && $data->Twitter > '') ? $data->Twitter : ''; ?>"  required>
                                                   <div class="help-block with-errors"></div>
                                              </div> 
                                              <div class="col-md-6">
                                                   <label for="Instagram">Perfil de Instagram (indica la url completa):</label>
                                                   <input type="text" class="form-control" name="Instagram" id="Instagram" placeholder="" value="<?php echo (!empty($data->Instagram) && $data->Instagram > '') ? $data->Instagram : ''; ?>" required>
                                                   <div class="help-block with-errors"></div>
                                              </div> 
                                        </div>
                                         <div class="form-group row">
                                            <div class="col-md-6">
                                                   <label for="Linkedin">Perfil de Linkedin (indica la url completa):</label>
                                                   <input type="text"  class="form-control" name="Linkedin" id="Linkedin" placeholder="" value="<?php echo (!empty($data->Linkedin) && $data->Linkedin > '') ? $data->Linkedin : ''; ?>"  required>
                                                   <div class="help-block with-errors"></div>
                                              </div> 
                                        </div>
                                      </div>
                                </div>
                                <div id="step-3">
                                    <div id="form-step-2" role="form" data-toggle="validator">
                                    <hr><h4 class="text-center"><b>DATOS GEOGRÁFICOS</b></h4><hr> 
                                        <div class="row">
                                             <div class="col-md-6">
                                                 <label for="Ciudad_Nacimiento">Ciudad de Nacimiento:</label>
                                                 <input type="text" class="form-control" name="Ciudad_Nacimiento" id="Ciudad_Nacimiento" placeholder="" value="<?php echo (!empty($data->Ciudad_Nacimiento) && $data->Ciudad_Nacimiento > '') ? $data->Ciudad_Nacimiento : ''; ?>">
                                                 <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                 <label for="Pais_Nacimiento">País de Nacimiento:</label>
                                                 <input type="text" class="form-control" name="Pais_Nacimiento" id=" Pais_Nacimiento" placeholder="" value="<?php echo (!empty($data->Pais_Nacimiento) && $data->Pais_Nacimiento > '') ? $data->Pais_Nacimiento : ''; ?>">
                                                 <div class="help-block with-errors"></div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                 <label for="Ciudad_Residencia">Ciudad de Residencia:</label>
                                                 <input type="text" class="form-control" name="Ciudad_Residencia" id="Ciudad_Residencia" placeholder="" value="<?php echo (!empty($data->Ciudad_Residencia) && $data->Ciudad_Residencia > '') ? $data->Ciudad_Residencia : ''; ?>">
                                                 <div class="help-block with-errors"></div>
                                            </div>   
                                             <div class="col-md-6">
                                                 <label for="Pais_Residencia">País de Residencia:</label>
                                                 <input type="text" class="form-control" name="Pais_Residencia" id="Pais_Residencia" placeholder="" value="<?php echo (!empty($data->Pais_Residencia) && $data->Pais_Residencia > '') ? $data->Pais_Residencia : ''; ?>">
                                                 <div class="help-block with-errors"></div>
                                            </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="Ciudades_Paises_gustaria_trabajar">Ciudades y Países en las que te gustaría trabajar:</label>
                                                <input type="text" class="form-control" name="Ciudades_Paises_gustaria_trabajar" id="Ciudades_Paises_gustaria_trabajar" placeholder="" value="<?php echo (!empty($data->Ciudades_Paises_gustaria_trabajar) && $data->Ciudades_Paises_gustaria_trabajar > '') ? $data->Ciudades_Paises_gustaria_trabajar : ''; ?>">
                                                <div class="help-block with-errors"></div>
                                            </div>   
                                        </div>
                                        <div class="row">
                                             <div class="col-md-12">
                                                   <label for="Trabajar_otro_paises">Predisposición para trabajar en otro paises:</label>
                                                      <?php 
                                                     $prubea = (!empty($data->Trabajar_otro_paises)) ? json_decode($data->Trabajar_otro_paises) : '';
                                                         ?>
                                                 <fieldset>
                                                   <label for="name"></label>
                                                    <div class="row">

                                                        <div class="col-md-6">
                                                            <div class="checkbox"><label for="resido">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="resido" value="Solo por el país en el que resido"  <?php if (!empty($prubea)){
                                                                  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Solo por el país en el que resido') ? 'checked' : ''; }
                                                                }?> />Solo por el país en el que resido</label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" id="Europa" value="Europa" class="flat" <?php if (!empty($prubea)){ 
                                                                  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Europa') ? 'checked' : ''; }
                                                                }?>/>
                                                                <label for="Europa">Europa</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                   <div class="row">
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                              <label for="Option 9">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="Option 9" value="Option 9" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Option 9') ? 'checked' : ''; }}?>/>
                                                                Option 9</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="Asia" value="Asia" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Asia') ? 'checked' : '';} }?>/>
                                                                <label for="Asia">Asia</label>
                                                            </div>
                                                       </div>
                                                   </div>

                                                   <div class="row">
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                              <label for="AUTOREPUESTOS">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="AUTOREPUESTOS" value="AUTOREPUESTOS" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'AUTOREPUESTOS') ? 'checked' : '';} }?>/>
                                                                Autorepuestos</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="Africa" value="Africa" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Africa') ? 'checked' : '';} }?>/>
                                                                <label for="Africa">África</label>
                                                            </div>
                                                       </div>
                                                   </div>
                                                   
                                                   <div class="row">
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                              <label for="América Del Norte">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="América Del Norte" value="América Del Norte" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'América Del Norte') ? 'checked' : '';} }?>/>
                                                                América Del Norte</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="America Central" value="America Central" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'America Central') ? 'checked' : '';} }?>/>
                                                                <label for="America Central">América Central</label>
                                                            </div>
                                                       </div>
                                                   </div>

                                                   <div class="row">
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                              <label for="Sudamerica">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]" class="flat" id="Sudamerica" value="Sudamerica"  <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Sudamerica') ? 'checked' : '';} }?>/>
                                                                Sudamérica</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-6">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Trabajar_otro_paises[]"  class="flat" id="Oceania" value="Oceania" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Oceania') ? 'checked' : '';} }?>/>
                                                                <label for="Oceania">Oceanía</label>
                                                            </div>
                                                       </div>
                                                   </div>
                                              </fieldset>      
                                            </div> 
                                       </div>
                                    </div>
                                </div>
                                <div id="step-4" class="">
                                     <div id="form-step-3" role="form" data-toggle="validator">
                                        <hr>
                                    <h4 class="text-center"><b>DATOS SOBRE TUS APTITUDES PROFESIONALES</b></h4>
                                     <hr>  
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Resumen">Cuéntanos, en breves palabras, lo que te hace un gran profesional:</label>
                                                <input type="text" class="form-control licencia_licores" name="Resumen" id="Resumen" placeholder=""  value="<?php echo (!empty($data->Resumen) && $data->Resumen > '') ? $data->Resumen : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                            <div class="form-group row">
                                              <div class="col-md-12">
                                                   <label for="Que_buscas">¿Qué buscas?</label>
                                                      <?php 
                                                     $prubea = (!empty($data->Que_buscas)) ? json_decode($data->Que_buscas) : '';


                                                         ?>
                                                   <br>
                                                 <fieldset>
                                                   <label for="name"></label>
                                                    <div class="row">

                                                        <div class="col-md-3">
                                                            <div class="checkbox"><label for="Practicas de Revenue Manager">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Practicas de Revenue Manager" value="Practicas de Revenue Manager"  <?php if (!empty($prubea)){
                                                                  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Practicas de Revenue Manager') ? 'checked' : ''; }
                                                                }?> /> Prácticas de Revenue Manager</label>

                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" id="Practicas en Marketing Hotelero" value="Practicas en Marketing Hotelero" class="flat" <?php if (!empty($prubea)){ 
                                                                  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Practicas en Marketing Hotelero') ? 'checked' : ''; }
                                                                }?>/>
                                                                <label for="Practicas en Marketing Hotelero">Prácticas en Marketing Hotelero</label>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]"class="flat" id="Practicas en Distribucion Hotelera" value="Practicas en Distribucion Hotelera" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Practicas en Distribucion Hotelera') ? 'checked' : ''; }}?>/>
                                                                <label for="Practicas en Distribucion Hotelera">Prácticas en Distribución Hotelera</label>
                                                            </div>
                                                        </div>
                                                        <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo Junior de Revenue Manager" value="Trabajo Junior de Revenue Manager" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Junior de Revenue Manager') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo Junior de Revenue Manager">Trabajo Junior de Revenue Manager</label>
                                                            </div>
                                                        </div>
                                                    </div>
                                                   
                                                   <div class="row">
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                              <label for="Trabajo Senior de Revenue Manager">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo Senior de Revenue Manager" value="Trabajo Senior de Revenue Manager" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Senior de Revenue Manager') ? 'checked' : ''; }}?>/>
                                                                Trabajo Senior de Revenue Manager</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo Junior de Marketing Hotelero" value="Trabajo Junior de Marketing Hotelero" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Junior de Marketing Hotelero') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo Junior de Marketing Hotelero">Trabajo Junior de Marketing Hotelero</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo Senior de Marketing Hotelero" value="Trabajo Senior de Marketing Hotelero" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Senior de Marketing Hotelero') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo Senior de Marketing Hotelero">Trabajo Senior de Marketing Hotelero</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Proyectos de Revenue Manager freelance (importante ser autonomo)" value="Proyectos de Revenue Manager freelance (importante ser autonomo)" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Proyectos de Revenue Manager freelance (importante ser autonomo)') ? 'checked' : '';} }?>/>
                                                                <label for="Proyectos de Revenue Manager freelance (importante ser autonomo)">Proyectos de Revenue Manager freelance (importante ser autónomo)</label>
                                                            </div>
                                                       </div>
                                                   </div>

                                                   <div class="row">
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                              <label for="Proyectos de Marketing Hotelero freelance (importante ser autonomo)">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Proyectos de Marketing Hotelero freelance (importante ser autonomo)" value="Proyectos de Marketing Hotelero freelance (importante ser autonomo)" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Proyectos de Marketing Hotelero freelance (importante ser autonomo)') ? 'checked' : '';} }?>/>
                                                                Proyectos de Marketing Hotelero freelance (importante ser autónomo)</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="ESTETICA" value="Trabajo Junior en Distribucion Hotelera" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Junior en Distribucion Hotelera') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo Junior en Distribucion Hotelera">Trabajo Junior en Distribución Hotelera</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo Senior en Distribucion Hotelera" value="Trabajo Senior en Distribucion Hotelera" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo Senior en Distribucion Hotelera') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo Senior en Distribucion Hotelera">Trabajo Senior en Distribución Hotelera</label>
                                                            </div>
                                                       </div>
                                                       <div class="col-md-3">
                                                            <div class="checkbox">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Trabajo en Rentals / apartamentos Turísticos / Viviendas de uso Vacacional" value="Trabajo en Rentals / apartamentos Turísticos / Viviendas de uso Vacacional" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'Trabajo en Rentals / apartamentos Turísticos / Viviendas de uso Vacacional') ? 'checked' : '';} }?>/>
                                                                <label for="Trabajo en Rentals / apartamentos Turísticos / Viviendas de uso Vacacional">Trabajo en Rentals / apartamentos Turísticos / Viviendas de uso Vacacional</label>
                                                            </div>
                                                       </div>
                                                   </div>

                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <div class="checkbox">
                                                              <label for="Otros">
                                                                <input type="checkbox" name="Que_buscas[]" class="flat" id="Otros" value="OTROS" <?php if (!empty($prubea)){  foreach($prubea as $selected){ echo (!empty($selected) && $selected == 'OTROS') ? 'checked' : '';} }?>/>
                                                                Otros</label>
                                                            </div>
                                                        </div>
                                                    </div>         
                                              </fieldset>   
                                              </div> 
                                         </div>
                                         <hr>
                                    <h4 class="text-center"><b>DATOS ESPECÍFICOS DE CADA AREA DE EXPERTISE</b></h4>
                                     <hr>     
                                        <div class="arrow-container">
                                            <div class="arrow arrow-right">
                                                <div class="arrow-text">1. Revenue Management</div>
                                            </div>
                                        </div>
                                        <br> <br> <br>    

                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="Exp_mercado_1">Años de experiencia totales Mercado 1: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_mercado_1" id="Exp_mercado_1" placeholder=""  value="<?php echo (!empty($data->Exp_mercado_1) && $data->Exp_mercado_1 > '') ? $data->Exp_mercado_1 : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Exp_mercado_2">Años de experiencia totales Mercado 2: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_mercado_2" id="Exp_mercado_2" placeholder=""  value="<?php echo (!empty($data->Exp_mercado_2) && $data->Exp_mercado_2 > '') ? $data->Exp_mercado_2 : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="Exp_mercado_3">Años de experiencia totales Mercado 3: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_mercado_3" id="Exp_mercado_3" placeholder=""  value="<?php echo (!empty($data->Exp_mercado_3) && $data->Exp_mercado_3 > '') ? $data->Exp_mercado_3 : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Exp_mercado_4">Años de experiencia totales Mercado 4: Indica los países y ciudades de los hoteles donde has gestionado las estrategias de Revenue y los años de experiencia:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_mercado_4" id="Exp_mercado_4" placeholder=""  value="<?php echo (!empty($data->Exp_mercado_4) && $data->Exp_mercado_4 > '') ? $data->Exp_mercado_4 : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="Exp_segmento_urbano">Indica tu experiencia en el segmento Urbano:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_segmento_urbano" id="Exp_segmento_urbano" placeholder=""  value="<?php echo (!empty($data->Exp_segmento_urbano) && $data->Exp_segmento_urbano > '') ? $data->Exp_segmento_urbano : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Exp_segmento_Vacacional">Indica tu experiencia en el segmento Vacacional:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_segmento_Vacacional" id="Exp_segmento_Vacacional" placeholder=""  value="<?php echo (!empty($data->Exp_segmento_Vacacional) && $data->Exp_segmento_Vacacional > '') ? $data->Exp_segmento_Vacacional : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-6">
                                                <label for="Exp_otros_segmentos">Indica tu experiencia en Otros segmentos:<br></label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_otros_segmentos" id="Exp_otros_segmentos" placeholder=""  value="<?php echo (!empty($data->Exp_otros_segmentos) && $data->Exp_otros_segmentos > '') ? $data->Exp_otros_segmentos : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Exp_rentals_apartamentos_turisticos">Indica tu experiencia en Rentals / Apartamentos turísticos / Viviendas de uso vacacional:</label>
                                                <input type="text" class="form-control licencia_licores" name="Exp_rentals_apartamentos_turisticos" id="Exp_rentals_apartamentos_turisticos" placeholder=""  value="<?php echo (!empty($data->Exp_rentals_apartamentos_turisticos) && $data->Exp_rentals_apartamentos_turisticos > '') ? $data->Exp_rentals_apartamentos_turisticos : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Logro_destacar">Cuéntanos aquello que te hace grande en este campo ¿algún logro que quieras destacar?:</label>
                                                <input type="text" class="form-control licencia_licores" name="Logro_destacar" id="Logro_destacar" placeholder=""  value="<?php echo (!empty($data->Logro_destacar) && $data->Logro_destacar > '') ? $data->Logro_destacar : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                         <hr>     
                                        <div class="arrow-container">
                                            <div class="arrow arrow-right">
                                                <div class="arrow-text">2. Distribución Hotelera</div>
                                            </div>
                                        </div>
                                        <br> <br> <br>  

                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Paises_gestionado_estrategias_distribucion">Indica los países donde has gestionado las estrategias de distribución y los años de experiencia:</label>
                                                <input type="text" class="form-control" name="Paises_gestionado_estrategias_distribucion" id="Paises_gestionado_estrategias_distribucion" placeholder=""  value="<?php echo (!empty($data->Paises_gestionado_estrategias_distribucion) && $data->Paises_gestionado_estrategias_distribucion > '') ? $data->Paises_gestionado_estrategias_distribucion : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">Experiencia en distribución Online (nivel):</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="0" value="0" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '0') ? 'checked' : ''; ?> />
                                                             <label for="0" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="1" value="1" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="2" value="2" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '2') ? 'checked' : ''; ?> />
                                                             <label for="2" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="3" value="3" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="4" value="4" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="5" value="5" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '5') ? 'checked' : ''; ?> />
                                                             <label for="5" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="6" value="6" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="7" value="7" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '7') ? 'checked' : ''; ?> />
                                                             <label for="7" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="8" value="8" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="9" value="9" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_online" id="10" value="10" <?php echo (!empty($data->Nivel_distribucion_online) && $data->Nivel_distribucion_online == '10') ? 'checked' : ''; ?>  />
                                                             <label for="10" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Exp_distribucion_online">Si lo deseas cuéntanos más sobre tu experiencia en distribución online:</label>
                                                <input type="text" class="form-control" name="Exp_distribucion_online" id="Exp_distribucion_online" placeholder=""  value="<?php echo (!empty($data->Exp_distribucion_online) && $data->Exp_distribucion_online > '') ? $data->Exp_distribucion_online : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                             <div class="col-md-12">
                                                <label for="name">Experiencia en distribución Offline:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="0seo" value="0" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '0') ? 'checked' : ''; ?> />
                                                             <label for="0seo" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="1seo" value="1" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1seo" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="2seo" value="2" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '2') ? 'checked' : ''; ?> />
                                                             <label for="2seo" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="3seo" value="3" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3seo" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="4seo" value="4" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4seo" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="5seo" value="5" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '5') ? 'checked' : ''; ?> />
                                                             <label for="5seo" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="6seo" value="6" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6seo" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="7seo" value="7" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '7') ? 'checked' : ''; ?> />
                                                             <label for="7seo" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="8seo" value="8" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8seo" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="9seo" value="9" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9seo" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Nivel_distribucion_offline" id="10seo" value="10" <?php echo (!empty($data->Nivel_distribucion_offline) && $data->Nivel_distribucion_offline == '10') ? 'checked' : ''; ?>  />
                                                             <label for="10seo" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Exp_distribucion_offline">Si lo deseas cuéntanos más sobre tu experiencia en distribución offline:</label>
                                                <input type="text" class="form-control" name="Exp_distribucion_offline" id="Exp_distribucion_offline" placeholder=""  value="<?php echo (!empty($data->Exp_distribucion_offline) && $data->Exp_distribucion_offline > '') ? $data->Exp_distribucion_offline : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                        <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Revenue">¿Has hecho Revenue para otros sectores? (campos de golf, restaurantes, lineas aéreas, spas, salones...) ¡Cuéntanos tu experiencia! :</label>
                                                <input type="text" class="form-control" name="Revenue" id="Revenue" placeholder=""  value="<?php echo (!empty($data->Revenue) && $data->Revenue > '') ? $data->Revenue : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                         <hr>     
                                        <div class="arrow-container">
                                            <div class="arrow arrow-right">
                                                <div class="arrow-text">3. Marketing Hotelero</div>
                                            </div>
                                        </div>
                                        <br> <br> <br> 
                                        <p><i>Indica las disciplinas que controlas y tu nivel al respecto</i></p><br>

                                        <div class="form-group row">
                                             <div class="col-md-12">
                                                <label for="name">SEO Técnico:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="0off" value="0" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '0') ? 'checked' : ''; ?> />
                                                             <label for="0off" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="1off" value="1" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1off" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="2off" value="2" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '2') ? 'checked' : ''; ?> />
                                                             <label for="2off" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="3off" value="3" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3off" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="4off" value="4" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4off" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="5off" value="5" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '5') ? 'checked' : ''; ?> />
                                                             <label for="5off" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="6off" value="6" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6off" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="7off" value="7" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '7') ? 'checked' : ''; ?> />
                                                             <label for="7off" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="8off" value="8" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8off" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="9off" value="9" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9off" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_tecnico" id="10off" value="10" <?php echo (!empty($data->SEO_tecnico) && $data->SEO_tecnico == '10') ? 'checked' : ''; ?>  />
                                                             <label for="10off" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">SEO Offpage:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="0offpage" value="0" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '0') ? 'checked' : ''; ?> />
                                                             <label for="0offpage" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="1offpage" value="1" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1offpage" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="2offpage" value="2" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '2') ? 'checked' : ''; ?> />
                                                             <label for="2offpage" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="3offpage" value="3" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3offpage" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="4offpage" value="4" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4offpage" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="5offpage" value="5" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '5') ? 'checked' : ''; ?> />
                                                             <label for="5offpage" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="6offpage" value="6" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6offpage" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="7offpage" value="7" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '7') ? 'checked' : ''; ?> />
                                                             <label for="7offpage" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="8offpage" value="8" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8offpage" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="9offpage" value="9" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9offpage" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEO_offpage" id="1offpage0" value="10" <?php echo (!empty($data->SEO_offpage) && $data->SEO_offpage == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1offpage0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">SEM:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="0SEM" value="0" <?php echo (!empty($data->SEM) && $data->SEM == '0') ? 'checked' : ''; ?> />
                                                             <label for="0SEM" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="1SEM" value="1" <?php echo (!empty($data->SEM) && $data->SEM == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1SEM" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="2SEM" value="2" <?php echo (!empty($data->SEM) && $data->SEM == '2') ? 'checked' : ''; ?> />
                                                             <label for="2SEM" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="3SEM" value="3" <?php echo (!empty($data->SEM) && $data->SEM == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3SEM" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="4SEM" value="4" <?php echo (!empty($data->SEM) && $data->SEM == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4SEM" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="5SEM" value="5" <?php echo (!empty($data->SEM) && $data->SEM == '5') ? 'checked' : ''; ?> />
                                                             <label for="5SEM" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="6SEM" value="6" <?php echo (!empty($data->SEM) && $data->SEM == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6SEM" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="7SEM" value="7" <?php echo (!empty($data->SEM) && $data->SEM == '7') ? 'checked' : ''; ?> />
                                                             <label for="7SEM" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="8SEM" value="8" <?php echo (!empty($data->SEM) && $data->SEM == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8SEM" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="9SEM" value="9" <?php echo (!empty($data->SEM) && $data->SEM == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9SEM" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="SEM" id="1SEM0" value="10" <?php echo (!empty($data->SEM) && $data->SEM == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1SEM0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">RRSS:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="0RRSS" value="0" <?php echo (!empty($data->RRSS) && $data->RRSS == '0') ? 'checked' : ''; ?> />
                                                             <label for="0RRSS" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="1RRSS" value="1" <?php echo (!empty($data->RRSS) && $data->RRSS == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1RRSS" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="2RRSS" value="2" <?php echo (!empty($data->RRSS) && $data->RRSS == '2') ? 'checked' : ''; ?> />
                                                             <label for="2RRSS" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="3RRSS" value="3" <?php echo (!empty($data->RRSS) && $data->RRSS == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3RRSS" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="4RRSS" value="4" <?php echo (!empty($data->RRSS) && $data->RRSS == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4RRSS" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="5RRSS" value="5" <?php echo (!empty($data->RRSS) && $data->RRSS == '5') ? 'checked' : ''; ?> />
                                                             <label for="5RRSS" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="6RRSS" value="6" <?php echo (!empty($data->RRSS) && $data->RRSS == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6RRSS" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="7RRSS" value="7" <?php echo (!empty($data->RRSS) && $data->RRSS == '7') ? 'checked' : ''; ?> />
                                                             <label for="7RRSS" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="8RRSS" value="8" <?php echo (!empty($data->RRSS) && $data->RRSS == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8RRSS" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="9RRSS" value="9" <?php echo (!empty($data->RRSS) && $data->RRSS == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9RRSS" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="RRSS" id="1RRSS0" value="10" <?php echo (!empty($data->RRSS) && $data->RRSS == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1RRSS0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">Email Marketing:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="0Email_marketing" value="0" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '0') ? 'checked' : ''; ?> />
                                                             <label for="0Email_marketing" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="1Email_marketing" value="1" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1Email_marketing" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="2Email_marketing" value="2" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '2') ? 'checked' : ''; ?> />
                                                             <label for="2Email_marketing" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="3Email_marketing" value="3" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3Email_marketing" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="4Email_marketing" value="4" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4Email_marketing" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="5Email_marketing" value="5" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '5') ? 'checked' : ''; ?> />
                                                             <label for="5Email_marketing" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="6Email_marketing" value="6" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6Email_marketing" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="7Email_marketing" value="7" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '7') ? 'checked' : ''; ?> />
                                                             <label for="7Email_marketing" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="8Email_marketing" value="8" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8Email_marketing" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="9Email_marketing" value="9" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9Email_marketing" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Email_marketing" id="1Email_marketing0" value="10" <?php echo (!empty($data->Email_marketing) && $data->Email_marketing == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1Email_marketing0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                          <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">Redacción de contenidos (Blogging y copywriting):</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="0Redaccion" value="0" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '0') ? 'checked' : ''; ?> />
                                                             <label for="0Redaccion" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="1Redaccion" value="1" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1Redaccion" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="2Redaccion" value="2" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '2') ? 'checked' : ''; ?> />
                                                             <label for="2Redaccion" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="3Redaccion" value="3" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3Redaccion" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="4Redaccion" value="4" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4Redaccion" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="5Redaccion" value="5" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '5') ? 'checked' : ''; ?> />
                                                             <label for="5Redaccion" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="6Redaccion" value="6" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6Redaccion" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="7Redaccion" value="7" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '7') ? 'checked' : ''; ?> />
                                                             <label for="7Redaccion" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="8Redaccion" value="8" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8Redaccion" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="9Redaccion" value="9" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9Redaccion" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Redaccion" id="1Redaccion0" value="10" <?php echo (!empty($data->Redaccion) && $data->Redaccion == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1Redaccion0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                             <div class="row">
                                            <div class="col-md-12">
                                                <label for="name">Diseño de Imágenes:</label>
                                                   <br>
                                                   <fieldset>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="0Diseno" value="0" <?php echo (!empty($data->Diseno) && $data->Diseno == '0') ? 'checked' : ''; ?> />
                                                             <label for="0Diseno" >0</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="1Diseno" value="1" <?php echo (!empty($data->Diseno) && $data->Diseno == '1') ? 'checked' : ''; ?>  />
                                                             <label for="1Diseno" >1</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="2Diseno" value="2" <?php echo (!empty($data->Diseno) && $data->Diseno == '2') ? 'checked' : ''; ?> />
                                                             <label for="2Diseno" >2</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="3Diseno" value="3" <?php echo (!empty($data->Diseno) && $data->Diseno == '3') ? 'checked' : ''; ?>  />
                                                             <label for="3Diseno" >3</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="4Diseno" value="4" <?php echo (!empty($data->Diseno) && $data->Diseno == '4') ? 'checked' : ''; ?>  />
                                                             <label for="4Diseno" >4</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="5Diseno" value="5" <?php echo (!empty($data->Diseno) && $data->Diseno == '5') ? 'checked' : ''; ?> />
                                                             <label for="5Diseno" >5</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="6Diseno" value="6" <?php echo (!empty($data->Diseno) && $data->Diseno == '6') ? 'checked' : ''; ?>  />
                                                             <label for="6Diseno" >6</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="7Diseno" value="7" <?php echo (!empty($data->Diseno) && $data->Diseno == '7') ? 'checked' : ''; ?> />
                                                             <label for="7Diseno" >7</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="8Diseno" value="8" <?php echo (!empty($data->Diseno) && $data->Diseno == '8') ? 'checked' : ''; ?>  />
                                                             <label for="8Diseno" >8</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="9Diseno" value="9" <?php echo (!empty($data->Diseno) && $data->Diseno == '9') ? 'checked' : ''; ?>  />
                                                             <label for="9Diseno" >9</label>
                                                        </div>
                                                        <div class="radiobutton">
                                                             <input type="radio" name="Diseno" id="1Diseno0" value="10" <?php echo (!empty($data->Diseno) && $data->Diseno == '10') ? 'checked' : ''; ?>  />
                                                             <label for="1Diseno0" >10</label>
                                                        </div>
                                                   </fieldset>
                                              </div> 
                                        </div>
                                        <div class="row">
                                           <div class="col-md-12">
                                               <label for="name">Embudos de Conversión:</label>
                                                  <br>
                                                  <fieldset>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="0Embudos" value="0" <?php echo (!empty($data->Embudos) && $data->Embudos == '0') ? 'checked' : ''; ?> />
                                                            <label for="0Embudos" >0</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="1Embudos" value="1" <?php echo (!empty($data->Embudos) && $data->Embudos == '1') ? 'checked' : ''; ?>  />
                                                            <label for="1Embudos" >1</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="2Embudos" value="2" <?php echo (!empty($data->Embudos) && $data->Embudos == '2') ? 'checked' : ''; ?> />
                                                            <label for="2Embudos" >2</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="3Embudos" value="3" <?php echo (!empty($data->Embudos) && $data->Embudos == '3') ? 'checked' : ''; ?>  />
                                                            <label for="3Embudos" >3</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="4Embudos" value="4" <?php echo (!empty($data->Embudos) && $data->Embudos == '4') ? 'checked' : ''; ?>  />
                                                            <label for="4Embudos" >4</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="5Embudos" value="5" <?php echo (!empty($data->Embudos) && $data->Embudos == '5') ? 'checked' : ''; ?> />
                                                            <label for="5Embudos" >5</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="6Embudos" value="6" <?php echo (!empty($data->Embudos) && $data->Embudos == '6') ? 'checked' : ''; ?>  />
                                                            <label for="6Embudos" >6</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="7Embudos" value="7" <?php echo (!empty($data->Embudos) && $data->Embudos == '7') ? 'checked' : ''; ?> />
                                                            <label for="7Embudos" >7</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="8Embudos" value="8" <?php echo (!empty($data->Embudos) && $data->Embudos == '8') ? 'checked' : ''; ?>  />
                                                            <label for="8Embudos" >8</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="9Embudos" value="9" <?php echo (!empty($data->Embudos) && $data->Embudos == '9') ? 'checked' : ''; ?>  />
                                                            <label for="9Embudos" >9</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Embudos" id="1Embudos0" value="10" <?php echo (!empty($data->Embudos) && $data->Embudos == '10') ? 'checked' : ''; ?>  />
                                                            <label for="1Embudos0" >10</label>
                                                       </div>
                                                  </fieldset>
                                             </div> 
                                        </div>
                                        <div class="row">
                                           <div class="col-md-12">
                                               <label for="name">Marketing Tradicional (relación con medios, radio, prensa, cartelería):</label>
                                                  <br>
                                                  <fieldset>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="0Marketing_tradicional" value="0" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '0') ? 'checked' : ''; ?> />
                                                            <label for="0Marketing_tradicional" >0</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="1Marketing_tradicional" value="1" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '1') ? 'checked' : ''; ?>  />
                                                            <label for="1Marketing_tradicional" >1</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="2Marketing_tradicional" value="2" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '2') ? 'checked' : ''; ?> />
                                                            <label for="2Marketing_tradicional" >2</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="3Marketing_tradicional" value="3" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '3') ? 'checked' : ''; ?>  />
                                                            <label for="3Marketing_tradicional" >3</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="4Marketing_tradicional" value="4" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '4') ? 'checked' : ''; ?>  />
                                                            <label for="4Marketing_tradicional" >4</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="5Marketing_tradicional" value="5" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '5') ? 'checked' : ''; ?> />
                                                            <label for="5Marketing_tradicional" >5</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="6Marketing_tradicional" value="6" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '6') ? 'checked' : ''; ?>  />
                                                            <label for="6Marketing_tradicional" >6</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="7Marketing_tradicional" value="7" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '7') ? 'checked' : ''; ?> />
                                                            <label for="7Marketing_tradicional" >7</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="8Marketing_tradicional" value="8" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '8') ? 'checked' : ''; ?>  />
                                                            <label for="8Marketing_tradicional" >8</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="9Marketing_tradicional" value="9" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '9') ? 'checked' : ''; ?>  />
                                                            <label for="9Marketing_tradicional" >9</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Marketing_tradicional" id="1Marketing_tradicional0" value="10" <?php echo (!empty($data->Marketing_tradicional) && $data->Marketing_tradicional == '10') ? 'checked' : ''; ?>  />
                                                            <label for="1Marketing_tradicional0" >10</label>
                                                       </div>
                                                  </fieldset>
                                             </div> 
                                        </div>
                                        <div class="row">
                                           <div class="col-md-12">
                                               <label for="name">Planificación de estrategias de Marketing Digital:</label>
                                                  <br>
                                                  <fieldset>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="0Planificacion_marketing_digital" value="0" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '0') ? 'checked' : ''; ?> />
                                                            <label for="0Planificacion_marketing_digital" >0</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="1Planificacion_marketing_digital" value="1" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '1') ? 'checked' : ''; ?>  />
                                                            <label for="1Planificacion_marketing_digital" >1</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="2Planificacion_marketing_digital" value="2" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '2') ? 'checked' : ''; ?> />
                                                            <label for="2Planificacion_marketing_digital" >2</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="3Planificacion_marketing_digital" value="3" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '3') ? 'checked' : ''; ?>  />
                                                            <label for="3Planificacion_marketing_digital" >3</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="4Planificacion_marketing_digital" value="4" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '4') ? 'checked' : ''; ?>  />
                                                            <label for="4Planificacion_marketing_digital" >4</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="5Planificacion_marketing_digital" value="5" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '5') ? 'checked' : ''; ?> />
                                                            <label for="5Planificacion_marketing_digital" >5</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="6Planificacion_marketing_digital" value="6" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '6') ? 'checked' : ''; ?>  />
                                                            <label for="6Planificacion_marketing_digital" >6</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="7Planificacion_marketing_digital" value="7" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '7') ? 'checked' : ''; ?> />
                                                            <label for="7Planificacion_marketing_digital" >7</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="8Planificacion_marketing_digital" value="8" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '8') ? 'checked' : ''; ?>  />
                                                            <label for="8Planificacion_marketing_digital" >8</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="9Planificacion_marketing_digital" value="9" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '9') ? 'checked' : ''; ?>  />
                                                            <label for="9Planificacion_marketing_digital" >9</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Planificacion_marketing_digital" id="1Planificacion_marketing_digital0" value="10" <?php echo (!empty($data->Planificacion_marketing_digital) && $data->Planificacion_marketing_digital == '10') ? 'checked' : ''; ?>  />
                                                            <label for="1Planificacion_marketing_digital0" >10</label>
                                                       </div>
                                                  </fieldset>
                                             </div> 
                                        </div>

                                        <hr>     
                                        <div class="arrow-container">
                                            <div class="arrow arrow-right">
                                                <div class="arrow-text">4. Control de Gestión y costes</div>
                                            </div>
                                        </div>
                                        <br> <br> <br>    
                    
                                        <div class="row">
                                           <div class="col-md-12">
                                               <label for="name">Nivel:</label>
                                                  <br>
                                                  <fieldset>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="0Control_gestion_nivel" value="0" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '0') ? 'checked' : ''; ?> />
                                                            <label for="0Control_gestion_nivel" >0</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="1Control_gestion_nivel" value="1" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '1') ? 'checked' : ''; ?>  />
                                                            <label for="1Control_gestion_nivel" >1</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="2Control_gestion_nivel" value="2" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '2') ? 'checked' : ''; ?> />
                                                            <label for="2Control_gestion_nivel" >2</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="3Control_gestion_nivel" value="3" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '3') ? 'checked' : ''; ?>  />
                                                            <label for="3Control_gestion_nivel" >3</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="4Control_gestion_nivel" value="4" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '4') ? 'checked' : ''; ?>  />
                                                            <label for="4Control_gestion_nivel" >4</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="5Control_gestion_nivel" value="5" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '5') ? 'checked' : ''; ?> />
                                                            <label for="5Control_gestion_nivel" >5</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="6Control_gestion_nivel" value="6" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '6') ? 'checked' : ''; ?>  />
                                                            <label for="6Control_gestion_nivel" >6</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="7Control_gestion_nivel" value="7" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '7') ? 'checked' : ''; ?> />
                                                            <label for="7Control_gestion_nivel" >7</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="8Control_gestion_nivel" value="8" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '8') ? 'checked' : ''; ?>  />
                                                            <label for="8Control_gestion_nivel" >8</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="9Control_gestion_nivel" value="9" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '9') ? 'checked' : ''; ?>  />
                                                            <label for="9Control_gestion_nivel" >9</label>
                                                       </div>
                                                       <div class="radiobutton">
                                                            <input type="radio" name="Control_gestion_nivel" id="1Control_gestion_nivel0" value="10" <?php echo (!empty($data->Control_gestion_nivel) && $data->Control_gestion_nivel == '10') ? 'checked' : ''; ?>  />
                                                            <label for="1Control_gestion_nivel0" >10</label>
                                                       </div>
                                                  </fieldset>
                                             </div> 
                                        </div>
                                         <div class="form-group row">
                                            <div class="col-md-12">
                                                <label for="Logro">Cuéntanos aquello que te hace grande en este campo ¿algún logro que quieras destacar?:</label>
                                                <input type="text" class="form-control" name="Logro" id="Logro" placeholder=""  value="<?php echo (!empty($data->Logro) && $data->Logro > '') ? $data->Logro : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-12">
                                                <label for="Conferencias">¿Has dado conferencias y/o escrito en medios profesionales del sector Hotelero? (en caso afirmativo indica todo lo que pueda ayudarnos a conocerte mejor):</label>
                                                <input type="text" class="form-control" name="Conferencias" id="Conferencias" placeholder=""  value="<?php echo (!empty($data->Conferencias) && $data->Conferencias > '') ? $data->Conferencias : ''; ?>" >
                                                <div class="help-block with-errors"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div id="step-5" class="">
                                    <div id="form-step-4" role="form" data-toggle="validator">
                                        <hr><h4 class="text-center"><b>DATOS ACADÉMICOS</b></h4><hr>   <div class="form-group row">
                                                <div class="col-md-12">
                                                    <label for="Revenue_management">¿Donde has aprendido Revenue Management? (en caso de no tener formación sobre esta materia indícalo):</label>
                                                    <input type="text" class="form-control" name="Revenue_management" id="Revenue_management" placeholder=""  value="<?php echo (!empty($data->Revenue_management) && $data->Revenue_management > '') ? $data->Revenue_management : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="Marketing_hotelero">¿Donde has aprendido Marketing Hotelero? (en caso de no tener formación sobre esta materia indícalo) :</label>
                                                    <input type="text" class="form-control" name="Marketing_hotelero" id="Marketing_hotelero" placeholder=""  value="<?php echo (!empty($data->Marketing_hotelero) && $data->Marketing_hotelero > '') ? $data->Marketing_hotelero : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                                <div class="col-md-12">
                                                    <label for="Control_gestio">¿Donde has aprendido Control de gestión? (en caso de no tener formación sobre esta materia indícalo) :</label>
                                                    <input type="text" class="form-control" name="Control_gestio" id="Control_gestio" placeholder=""  value="<?php echo (!empty($data->Control_gestio) && $data->Control_gestio > '') ? $data->Control_gestio : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <hr>     
                                            <div class="arrow-container">
                                                <div class="arrow arrow-right">
                                                    <div class="arrow-text">IDIOMAS</div>
                                                </div>
                                            </div>
                                            <br> <br> <br>
                                            <div class="row">
                                               <div class="col-md-12">
                                                   <label for="name">Inglés:</label>
                                                      <br>
                                                      <fieldset>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_ingles" id="0Idioma_ingles" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_ingles) && $data->Idioma_ingles == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="0Idioma_ingles" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_ingles" id="1Idioma_ingles" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_ingles) && $data->Idioma_ingles == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)') ? 'checked' : ''; ?>  />
                                                                <label for="1Idioma_ingles" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_ingles" id="2Idioma_ingles" value="Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)" <?php echo (!empty($data->Idioma_ingles) && $data->Idioma_ingles == 'Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="2Idioma_ingles" >Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_ingles" id="3Idioma_ingles" value="Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)" <?php echo (!empty($data->Idioma_ingles) && $data->Idioma_ingles == 'Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)') ? 'checked' : ''; ?>  />
                                                                <label for="3Idioma_ingles" >Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)</label>
                                                           </div> <br>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_ingles" id="4Idioma_ingles" value="No tengo ningún concimiento" <?php echo (!empty($data->Idioma_ingles) && $data->Idioma_ingles == 'No tengo ningún concimiento') ? 'checked' : ''; ?>  />
                                                                <label for="4Idioma_ingles" >No tengo ningún concimiento</label>
                                                           </div>
                                                      </fieldset>
                                                 </div> 
                                            </div>
                                            <div class="row">
                                               <div class="col-md-12">
                                                   <label for="name">Francés:</label>
                                                      <br>
                                                      <fieldset>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_frances" id="0Idioma_frances" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_frances) && $data->Idioma_frances == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="0Idioma_frances" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_frances" id="1Idioma_frances" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_frances) && $data->Idioma_frances == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)') ? 'checked' : ''; ?>  />
                                                                <label for="1Idioma_frances" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_frances" id="2Idioma_frances" value="Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)" <?php echo (!empty($data->Idioma_frances) && $data->Idioma_frances == 'Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="2Idioma_frances" >Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_frances" id="3Idioma_frances" value="Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)" <?php echo (!empty($data->Idioma_frances) && $data->Idioma_frances == 'Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)') ? 'checked' : ''; ?>  />
                                                                <label for="3Idioma_frances" >Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)</label>
                                                           </div><br>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_frances" id="4Idioma_frances" value="No tengo ningún concimiento" <?php echo (!empty($data->Idioma_frances) && $data->Idioma_frances == 'No tengo ningún concimiento') ? 'checked' : ''; ?>  />
                                                                <label for="4Idioma_frances" >No tengo ningún concimiento</label>
                                                           </div>   
                                                       </fieldset>
                                                 </div> 
                                            </div>
                                            <div class="row">
                                               <div class="col-md-12">
                                                   <label for="name">Alemán:</label>
                                                      <br>
                                                      <fieldset>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_aleman" id="0Idioma_aleman" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_aleman) && $data->Idioma_aleman == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="0Idioma_aleman" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura y he vivido en un país en el que se habla esta lengua durante más de 5 meses (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_aleman" id="1Idioma_aleman" value="Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)" <?php echo (!empty($data->Idioma_aleman) && $data->Idioma_aleman == 'Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)') ? 'checked' : ''; ?>  />
                                                                <label for="1Idioma_aleman" >Soy capaz de mantener una conversación y comunicarme por email con mucha soltura (nivel 8-10 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_aleman" id="2Idioma_aleman" value="Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)" <?php echo (!empty($data->Idioma_aleman) && $data->Idioma_aleman == 'Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)') ? 'checked' : ''; ?> />
                                                                <label for="2Idioma_aleman" >Soy capaz de mantener una conversación y comunicarme por email con soltura moderada (nivel 5 sobre 10)</label>
                                                           </div>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_aleman" id="3Idioma_aleman" value="Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)No tengo ningún concimiento" <?php echo (!empty($data->Idioma_aleman) && $data->Idioma_aleman == 'Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)No tengo ningún concimiento') ? 'checked' : ''; ?>  />
                                                                <label for="3Idioma_aleman" >Tengo dificultades en comunicarme en este idioma (nivel 1-4 sobre 10)No tengo ningún concimiento</label>
                                                           </div><br>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="Idioma_aleman" id="4Idioma_aleman" value="No tengo ningún concimiento" <?php echo (!empty($data->Idioma_aleman) && $data->Idioma_aleman == 'No tengo ningún concimiento') ? 'checked' : ''; ?>  />
                                                                <label for="4Idioma_aleman" >No tengo ningún concimiento</label>
                                                           </div>
                                                      </fieldset>
                                                 </div> 
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Otro_idioma">Indica Conocimientos en otros idiomas (o explicaciones extra sobre los anteriores tales como titulaciones etc...):</label>
                                                    <input type="text" class="form-control" name="Otro_idioma" id="Otro_idioma" placeholder=""  value="<?php echo (!empty($data->Otro_idioma) && $data->Otro_idioma > '') ? $data->Otro_idioma : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            
                                             <hr>     
                                            <div class="arrow-container">
                                                <div class="arrow arrow-right">
                                                    <div class="arrow-text">HERRAMIENTAS DE SOFTWARE</div>
                                                </div>
                                            </div>
                                            <br> <br> <br>

                                            <div class="row">
                                                <div class="col-md-6"><br>
                                                    <label for="Software_excel">Excel</label>
                                                    <select id="Software_excel" name="Software_excel" class="form-control solvencia_inmueble" >
                                                        <?php echo (!empty($data->Software_excel) && $data->Software_excel > '') ? "<option value='".$data->Software_excel."' selected>".$data->Software_excel."</option>" : '<option selected hidden>Seleccione</option>'; ?>
                                                        <option value="No tengo conocimientos">No tengo conocimientos</option> 
                                                        <option value="Tengo conocimientos básicos (dar formato)">Tengo conocimientos básicos (dar formato)</option> 
                                                        <option value="Tengo conocimientos intermedios (trabajar con fórmulas de cálculo)">Tengo conocimientos intermedios (trabajar con fórmulas de cálculo)</option> 
                                                        <option value="Tengo conocimientos avanzados (trabajar con tablas dinámicas y macros)">Tengo conocimientos avanzados (trabajar con tablas dinámicas y macros)</option> 
                                                    </select> 
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_PMS">PMS (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una):</label>
                                                    <input type="text" class="form-control" name="Software_PMS" id="Software_PMS" placeholder=""  value="<?php echo (!empty($data->Software_PMS) && $data->Software_PMS > '') ? $data->Software_PMS : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_channel_manager">Channel Manager (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una):</label>
                                                    <input type="text" class="form-control" name="Software_channel_manager" id="Software_channel_manager" placeholder=""  value="<?php echo (!empty($data->Software_channel_manager) && $data->Software_channel_manager > '') ? $data->Software_channel_manager : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_RMS">RMS(Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una):</label>
                                                    <input type="text" class="form-control" name="Software_RMS" id="Software_RMS" placeholder=""  value="<?php echo (!empty($data->Software_RMS) && $data->Software_RMS > '') ? $data->Software_RMS : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_Reviewer">Reviewer (reputación online) (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una) :</label>
                                                    <input type="text" class="form-control" name="Software_Reviewer" id="Software_Reviewer" placeholder=""  value="<?php echo (!empty($data->Software_Reviewer) && $data->Software_Reviewer > '') ? $data->Software_Reviewer : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_Shopper">Shopper (Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una):</label>
                                                    <input type="text" class="form-control" name="Software_Shopper" id="Software_Shopper" placeholder=""  value="<?php echo (!empty($data->Software_Shopper) && $data->Software_Shopper > '') ? $data->Software_Shopper : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_Power_Bi">Power Bi (Indica tu nivel de conocimiento):</label>
                                                    <input type="text" class="form-control" name="Software_Power_Bi" id="Software_Power_Bi" placeholder=""  value="<?php echo (!empty($data->Software_Power_Bi) && $data->Software_Power_Bi > '') ? $data->Software_Power_Bi : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_CRM">CRM(Indica qué herramientas de este tipo conoces y tu nivel de conocimiento en cada una):</label>
                                                    <input type="text" class="form-control" name="Software_CRM" id="Software_CRM" placeholder=""  value="<?php echo (!empty($data->Software_CRM) && $data->Software_CRM > '') ? $data->Software_CRM : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_GDS">GDS (Indica cuáles conoces y tu nivel de conocimiento en cada uno):</label>
                                                    <input type="text" class="form-control" name="Software_GDS" id="Software_GDS" placeholder=""  value="<?php echo (!empty($data->Software_GDS) && $data->Software_GDS > '') ? $data->Software_GDS : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <label for="Software_Python">Python, R u otras herramaientas de análisis de datos:</label>
                                                    <input type="text" class="form-control" name="Software_Python" id="Software_Python" placeholder=""  value="<?php echo (!empty($data->Software_Python) && $data->Software_Python > '') ? $data->Software_Python : ''; ?>" >
                                                    <div class="help-block with-errors"></div>
                                                </div>
                                            </div>
                                    </div>
                                </div>
                                <div id="step-6" class="">
                                   <div id="form-step-5" role="form" data-toggle="validator">
                                    <hr><h4 class="text-center"><b>OTRA INFORMACIÓN</b></h4><hr>
                                        <div class="formgroup row">
                                            <div class="col-md-6">
                                                <label for="Carnet_Conducir">Carnet de conducir</label>
                                                <select id="Carnet_Conducir" name="Carnet_Conducir" class="form-control solvencia_inmueble" >
                                                    <?php echo (!empty($data->Carnet_Conducir) && $data->Carnet_Conducir > '') ? "<option value='".$data->Carnet_Conducir."' selected>".$data->Carnet_Conducir."</option>" : '<option selected hidden>Seleccione</option>'; ?>
                                                    <option value="SI">SI</option> 
                                                    <option value="NO">NO</option> 
                                                </select> 
                                                <div class="help-block with-errors"></div>
                                            </div>
                                            <div class="col-md-6">
                                                <label for="Vehiculo_propio">Vehículo propio</label>
                                                <select id="Vehiculo_propio" name="Vehiculo_propio" class="form-control solvencia_inmueble" >
                                                    <?php echo (!empty($data->Vehiculo_propio) && $data->Vehiculo_propio > '') ? "<option value='".$data->Vehiculo_propio."' selected>".$data->Vehiculo_propio."</option>" : '<option selected hidden>Seleccione</option>'; ?>
                                                    <option value="SI">SI</option> 
                                                    <option value="NO">NO</option> 
                                                </select> 
                                                <div class="help-block with-errors"></div>
                                            </div>  
                                        </div>    
                                 </div>
                                </div>
                                <div id="step-7" class="">
                                    <div id="form-step-6" role="form" data-toggle="validator">
                                     <hr>     
                                     <div class="arrow-container">
                                        <div class="arrow arrow-right">
                                            <div class="arrow-text">LOPD</div>
                                        </div>
                                    </div>
                                    <br> <br> <br>
                                        <div class="form-group row">
                                            <div class="col-xs-12">
                                              <p class="text-muted well well-sm no-shadow">
                                                Los datos de carácter personal que me proporciones rellenando el presente formulario serán tratados por Jaime López-Chicheri Mirecki. como responsable del formulario. Finalidad de la recogida y tratamiento de los datos personales: gestionar la solicitud que realizas en este formulario de contacto. Legitimación: Consentimiento del interesado.Destinatarios: Reclutadores (siempre con tu consentimiento expreso previo para cada candidatura), Infusion Soft (para el envío de correos relacionados con el fin de este formulario), Google, Nerion Networks S.L. (proveedor de hosting) dentro de la UE. Ver política Privacidad Nerion Networks S.L.. Podrás ejercer tus derechos de acceso, rectificación, limitación y suprimir los datos en <a href="jaime@revenuemanagementworld.com.com" style="color: #d8a445;">jaime@revenuemanagementworld.com.com </a> así como el derecho a presentar una reclamación ante una autoridad de control.Puedes consultar la información adicional y detallada sobre Protección de Datos en mi página web:<a href="https://erevenuemasters.com/politica-de-privacidad/" style="color: #d8a445;"> https://erevenuemasters.com/politica-de-privacidad/</a>
                                            </p>
                                        </div>
                                            <div class="row">
                                               <div class="col-md-12">
                                                   <label for="name">Acepto la política de privacidad:</label>
                                                      <br>
                                                      <fieldset>
                                                           <div class="radiobutton">
                                                                <input type="radio" name="LOPD" id="0LOPD" value="SI" <?php echo (!empty($data->LOPD) && $data->LOPD == 'SI') ? 'checked' : ''; ?> />
                                                                <label for="0LOPD" >SI</label>
                                                           </div> 
                                                           <div class="radiobutton">
                                                                <input type="radio" name="LOPD" id="1LOPD" value="NO" <?php echo (!empty($data->LOPD) && $data->LOPD == 'NO') ? 'checked' : ''; ?>  />
                                                                <label for="1LOPD" >NO</label>
                                                           </div>
                                                      </fieldset>
                                                 </div> 
                                            </div>    
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <!-- End SmartWizard Content -->

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


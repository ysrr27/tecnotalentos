

<!--CTA1 START-->
  <div  id="step-1" class="cta-1">
    <div class="container container2">
        <br>
      <div class="row"></br>
        <div class="col-md-offset-1 col-md-10 text-center">
            <H2 class="h1first2 titulo-candidato" ><br>
               
                ¡Completa el formulario y te ayudaremos <br> a encontrar el trabajo de tus sueños!
            </H2>
        </div>
      </div>
    </div>
  </div>
  <!--CTA1 END-->
<div id="loading-wrapper"  ng-show="dataloading === 'true'">
    <div id="loading-text">Cargando...</div>
    <div id="loading-content"></div>
</div>


<div class="container container2" id="about-section">
    <div class="row">
        <div id="widfom" class="col-md-offset-1 col-md-10">
                <!-- Smart Wizard -->
                  <form action="<?php echo get_site_url("candidatos/candatoajax")?>" id="formcandidato" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
                        <!-- SmartWizard html -->

                            <?php 
                             $id = $this->uri->segment(4);
                             if($id > 0){
                                    $var = 'done';
                                  }else{
                                    $var = '';
                                  } 

                                 // var_dump($data);
                             ?>

                        <div id="candidato" class="form_wizard wizard_verticle ">
                            <ul>
                             <?php
                             if(is_array($data->pasos)){
                              foreach( $data->pasos as $paso){ ?>
                                <li class="<?php  echo ($id != "" or $id != "0") ? $var : ''; ?>">
                                  <a href="#step-<?php echo $paso->valor_paso?>">Paso <?php echo $paso->valor_paso?><br /></a>
                                </li>
                              <?php } } ?>
                            </ul>
                            <p>*Obligatorio</p>
                            <div>
                             <?php 
                            if(is_array($data->preguntas)){
                                foreach( $data->pasos as $paso){
                                      echo '<div id="step-'.$paso->valor_paso.'"> <div class="row"><div id="form-step-'.($paso->valor_paso - 1).'" role="form" data-toggle="validator">
                                                    <hr>
                                                    <H4 class="text-center"><b>'.$paso->titulo_paso.'</b></H4>
                                                    <hr>

                                                      ';
                                                       
                                                      if ($paso->valor_paso == 7) {
                                                        echo '  <div class="col-xs-12">
                                              <p class="text-muted well well-sm no-shadow">
                                                Los datos de carácter personal que me proporciones rellenando el presente formulario serán tratados por Jaime López-Chicheri Mirecki. como responsable del formulario. Finalidad de la recogida y tratamiento de los datos personales: gestionar la solicitud que realizas en este formulario de contacto. Legitimación: Consentimiento del interesado.Destinatarios: Reclutadores (siempre con tu consentimiento expreso previo para cada candidatura), Infusion Soft (para el envío de correos relacionados con el fin de este formulario), Google, Nerion Networks S.L. (proveedor de hosting) dentro de la UE. Ver política Privacidad Nerion Networks S.L.. Podrás ejercer tus derechos de acceso, rectificación, limitación y suprimir los datos en <a href="jaime@revenuemanagementworld.com.com" style="color: #d8a445;">jaime@revenuemanagementworld.com.com </a> así como el derecho a presentar una reclamación ante una autoridad de control.Puedes consultar la información adicional y detallada sobre Protección de Datos en mi página web:<a href="https://trabajorevenuemanager.com/politicas" style="color: #d8a445;"> politica de privacidad</a>
                                            </p>
                                        </div><div class="polit"></div> ';
                                                      }

                                        foreach( $data->preguntas as $key =>$preguntas){
                                             if ($preguntas->paso == $paso->valor_paso) {
                                              
                                                  ?>
                                          <div class="<?php echo (!empty($preguntas->requerido) && $preguntas->requerido > '') ? 'form-group' : ''; ?>">
                                              <div class="col-md-12 col-sm-12 col-xs-11">
                                                  <div class="col-md-12">

                                                      <label class="<?php echo 'class_'.$key?>" for="<?php echo $preguntas->pregunta?>"><?php echo $preguntas->pregunta?>  <?php echo (!empty($preguntas->requerido) && $preguntas->requerido > '') ? '*' : ''; ?>:</label>

                                                
                                            <div>
                                              <?php echo $preguntas->formato_pregunta?>
                                            </div>

                                        
                                                      <div class="help-block with-errors"></div>
                                                      <?php echo ($preguntas->tipo_pregunta == 'checkbox') ? '<div class="errCheck" id="1"></div>' : ''; ?>
                                                  </div>
                                              </div>
                                          </div>



                                          <?php
                                             }
                                          }
                                        /*  if ($paso->valor_paso == 5) {
                                            echo ' <a class="pull-right btn btn-primary sw-btn-next" id="addexperiencia"><i class="fa fa-plus"></i> Agregar experiencia</a> ';
                                          }*/
                                      echo "</div></div></div>";
                                    }
                                 }                   
                            ?>
                          </div>
                          </div>
                  </form>
        </div>

    </div> 

</div>
<!--End About-->
    <!-- Modal candidato -->

                  <div id="modalcandidato" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-sm">
                        <div class="modal-content">

                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span>
                                </button>
                                <H4 class="modal-title" id="myModalLabel2"><i class="fa fa-check"></i> Mensaje Enviado</H4>
                            </div>
                            <div class="modal-body">
                                <H4>Datos enviado con exito, Pronto nos comunicaremos contigo.</H4>
                    
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="btnclose" class="btn btn-default sw-btn-next" data-dismiss="modal">Cerrar</button>
                            </div>

                        </div>
                    </div>
                </div>
                  <!-- /modals -->


<!--Footer-->
<footer class="footer">
    <p class="text-center" id="copy"> <a href="<?php echo get_site_url("politicas")?>" target="_blank">Política y Privacidad&nbsp;</a> <label id="ctl00_Master_CopyrightFooter">© Copyright 2019 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>




 <style type="text/css">
     .display{
          display: none;
     }
</style>

<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                        <a>Home</a>
                        <a>Candidatos</a>
                        Lista
                   </div>
                <div class="x_panel">
                  <div class="x_title">
                    <h2><small><i class="fa fa-mortar-board"></i> CANDIDATOS</small></h2>
                            <ul class="nav navbar-right panel_toolbox">
                                <li>
                                    <a href="<?php echo get_site_url("candidatos/export/")?>" class="btn btn-primary sw-btn-next" type="submit">
                                        <i class="fa fa-download"></i> Descargar en Excel
                                    </a> 
                                </li>

                            </ul>
                    <div class="clearfix"></div>
                  </div>
                  <div id="loading-wrapper">
                      <div id="loading-text">Cargando...</div>
                      <div id="loading-content"></div>
                  </div>

                  <div class="x_content">
                  <table id="censo" class="row-border display" style="width:100%">
                      <thead>
                          <tr>
                              <th>Acción</th>
                              <th>id</th>
                              <th>Nombres y Apellidos</th>
                              <th>Fecha</th>
                              <th>Estatus</th>
                              <th>Curriculum</th>
                          </tr> 
                      </thead>
                      <tbody>
                      <?php 
                        if(is_array($data->rows )){
                          foreach(  $data->rows as $key =>$row){
                                    ?>
                                  <tr>
                                      <td>
                                        <div class="btn-group  btn-group-sm">
                                          <a href="<?php echo get_site_url('candidatos/preview/'.$row->iduser)?>" class="btn btn-primary" type="button" title=”VER”><i class="fa fa-eye"></i></a>
                                          <button class="btn btn-xs btn-danger btn-action" name="Activar" data-toggle="tooltip" title=”Borrar” data-placement="right" id="<?php echo  $row->iduser?>"><i class="fa fa-times"></i></button> 
                                        </div>         
                                      </td>
                                      <td><?php echo $row->idcandidato?></td>
                                      <td><?php echo $row->Nombre." ".$row->Apellido?></td>
                                      <td><?php echo $row->fecha?></td>
                                      <td>
                                        <select id="<?php echo $row->idcandidato?>" name="ranking" class="form-control ranking" style="width: 75%">
                                         <?php echo (!empty($row->estatus) && $row->estatus > '') ? "<option value='".$row->estatus."' selected>".$row->estatus."</option>" : ''; ?> 
                                        <option value="Enviado">Enviado</option>
                                        <option value="Recibido">Recibido</option>
                                        <option value="Evaluado">Evaluado</option>
                                        <option value="Preseleccionado">Preseleccionado</option>
                                        <option value="Seleccionado">Seleccionado</option>
                                      </select>
                                    </td>
                                      <td> 
                                        <div class="btn-group  btn-group-sm">
                                          <?php echo (!empty($row->cv_file) && $row->cv_file > '') ? '<a href="<?php echo get_assets_url($row->cv_file)?>" class="btn btn-default" type="button" title=”CV_EMPRESA” download><i class="fa fa-download"></i></a>' : ''; ?>

                                          <a href="<?php echo get_site_url('pdfx/cv_export_candidato/'.$row->iduser)?>" class="btn btn-default" type="button" title=”CV” target="_blank"><i class="fa fa-file-pdf-o"></i></a>
                                        </div>
                                    </td>
                                  </tr>
                                  <?php

                                }
                              }                          
                        ?>
                      </tbody>
                  </table>
                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


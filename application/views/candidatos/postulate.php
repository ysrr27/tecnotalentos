<?php  $usr = $this->uri->segment(1); ?>
<section class="user">
    <div class="user_options-container">
        <div class="user_options-text">
            <div class="user_options-unregistered">
                <h2 class="user_unregistered-title">Siguenos en:</h2><br>
     <!-- Social -->
                        <section class="fh5co-social">
                            <div class="site-container">
                                <div class="social">
                                    <h5></h5>
                                    <div class="social-icons">
                                        <a href="#" target="_blank"><i class="fa fa-facebook"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-instagram"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-twitter"></i></a>
                                        <a href="#" target="_blank"><i class="fa fa-linkedin"></i></a>
                                    </div>
                                    <h5></h5>
                                </div>
                            </div>
                            <br>
                            <br>
                        </section>
                        <!-- Social -->                  
                     <!--    <button class="user_unregistered-signup" id="signup-button">Regístrate</button> -->
            </div>

            <div class="user_options-registered">
                <div class="boxings">
                    <!-- Formulario de login -->
                    <div class="form_login"> 
                        <form class="forms_form" action="<?php echo get_site_url("login/iniciar_sesion_recluta")?>" id="session_recluta" role="form" name="session_recluta" method="post" accept-charset="utf-8">
                            <h2 class="forms_title">INICIAR SESIÓN</h2>
                            <div class="input-container">
                                <input type="email" name="correo_session" id="correo_session" required autofocus />
                                <label>Correo</label>        
                            </div>
                            <div class="input-container">       
                                <input type="password" name="contrasena_session" id="contrasena_session" required />
                                <label>Contraseña</label>
                            </div>
                            <button type="submit" id="ingresa_session" class="btn-action-login" name="ingresa_session" style="width: 100%;"> Ingresar</button>

                            <div class="forms_field"><br>
                                <span for="register" class="lkn">¿Olvidaste tu contraseña? <a href="#">Reiniciar</a></span> <br>
                                <span for="register" class="lkn">¿Aun no estas registrado? <a href="#" id="create_account">Crea una cuenta</a></span>
                            </div>
                        </form> 
                    </div>
                    <!-- Formulario de registro -->
                    <div class="form_register"> 
                         <form class="forms_form" action="<?php echo get_site_url("reclutas/registro")?>" id="registrate" role="form" name="registrate" method="post" accept-charset="utf-8">
                            <h2 class="forms_title">REGÍSTRATE</h2>
                            <div class="input-container">
                                <input type="hidden" id="user" name="user" value="<?php echo $usr; ?>">
                                <input type="text" name="nombre" id="nombre" required />
                                <label>Nombre y Apellido</label>        
                            </div>

                            <div class="input-container">
                                <input type="email" name="correo" id="correo" required autofocus />
                                <label>Correo</label>        
                            </div>
                            <div class="input-container">       
                                <input type="password" name="contrasena" id="contrasena" required />
                                <label>Contraseña</label>
                            </div>

                              <button type="submit" id="envar_registro" class="btn-action-login" name="envar_registro" style="width: 100%;"> Regístrate</button>

                            <div class="forms_field"><br>
                                <span for="register" class="lkn">¿Ya estas registrado? <a href="#" id="reset">Ingresar</a></span>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="site-footer" role="contentinfo">
    <p class="text-center" id="copy">Política y Privacidad&nbsp; <label id="ctl00_Master_CopyrightFooter">© Copyright 2020 Copyright.es - Todos los Derechos Reservados&nbsp;</label></p>
</footer>


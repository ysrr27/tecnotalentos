<!-- page content -->
        <div class="right_col" role="main">
          <div class="">
  
            <div class="clearfix"></div>

            <div class="row">

                <div class="col-md-12 col-sm-12 col-xs-12">
                    <div class="breadcrumb">
                        <a>Home</a>
                        <a>Candidatos</a>
                        Editar
                   </div>
                   <div id="loading-wrapper"  ng-show="dataloading === 'true'">
                        <div id="loading-text">Cargando...</div>
                        <div id="loading-content"></div>
                    </div>
                <div class="x_panel">
                
                  <div class="x_content">


                    <!-- Smart Wizard -->
                  <form action="<?php echo get_site_url("candidatos/update")?>" id="myForm" role="form" data-toggle="validator" method="post" accept-charset="utf-8">
                    <!-- SmartWizard html -->

                    <div id="listadepreguntas" class="row">
                        <!-- NEW COL START -->

                        <input type="hidden" name="idcandidatos" value="<?php echo $data->idcandidatos?> ">

                        <article class="col-sm-12 col-md-12 col-lg-12 pasos">
                                 <?php 
                                      $obj = json_decode($data->respuestas, TRUE);

                                      foreach($obj as $key => $value) 
                                      {?>

                                             <fieldset class="">
                                                  <div class="form-group row">
                                                      <div class="col-md-12">
                                                          <label for="<?php echo $key?>" class="<?php echo $key?>"> 
                                                            <?php 
                                                            $pregunta = str_replace("_", " ", $key);
                                                            echo $pregunta?> 
                                                          </label>
                                                          <input type="text" id="<?php echo $key?>" class="form-control" name="<?php echo $key?>" value="<?php echo $value?>">
                                                      </div> 
                                                  </div> 
                                              </fieldset>




                                       <?php 

                                      }?>

                        </article>
                   <!--    <div class="ln_solid"></div>
     -->
                    </div>


                                <div class="form-group">
                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-4">
                                        <a href="<?php echo get_site_url('candidatos/home/')?>" class="btn btn-primary sw-btn-next"><i class="fa fa-reply"></i> Volver</a>
                                        <button type="submit" class="btn btn-success sw-btn-next"><i class="fa fa-save"></i> Guardar</button>
                                    </div>
                              </div>


                    <!-- End SmartWizard Content -->

                  </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- /page content -->


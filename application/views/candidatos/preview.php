<!-- page content -->
<div class="right_col" role="main">
    <div class="">
        <div class="clearfix"></div>
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="breadcrumb">
                    <a>Home</a>
                    <a>Candidatos</a>
                    Ver
                </div>
                <div class="x_panel">
                    <div class="x_title">
                        <h2><b><small><i class="fa fa-mortar-board"></i> CANDIDATO</small></b></h2>
                        <div class="clearfix"></div>
                    </div>
                    <div class="x_content">


                        <div class="">
                            <div class="mg-user-header">
                                <div class="wrp">
                                    <div class="usr-img gm_animated-background_prew">
                                        <?php if (!empty($data->foto)) {?>
                                            <img src="<?php echo get_assets_url().$data->foto?>" alt="" class="landing-icon perfil-avatar">
                                        <?php }else{ ?>

                                            <div class="image-upload">

                                                <label for="file-input">
                                                    <img src="<?php echo get_assets_url();?>assets/img/img.png" width="100%" alt="" class="landing-icon perfil-avatar ">
                                                </label>

                                                <input id="file-input" type="file"/>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="main-wrap">
                                <div class="mg-shim-main">
                                    <div class="mg-rst">
                                        <div class="col-md-12 col-sm-12 col-xs-12">
                                            <div class="row">
                                                <div class="col-md-12 descripcionuser"><br>
                                                    <p><?php echo $data->nombre." ".$data->apellido ?></p>
                                                    <p>Fecha de Nacimiento: <?php echo $data->Fecha_nacimiento ?></p>
                                                    <p>Ubicación Actual:<?php echo $data->Direccion ?></p>
                                                    <p>Teléfono: <?php echo $data->Direccion ?></p>
                                                    <p>Email: <?php echo $data->Telefono ?></p><br>
                                                    <div class="col-md-offset-4 col-md-4 alert alert-success alert-dismissible fade in" role="alert">
                                                        <span class="dscv">Descargar CV</span>
                                                        <a href="<?php echo get_site_url('pdfx/cv_export_candidato/'.$data->iduser)?>" target="_blank">
                                                            <img src="<?php echo get_assets_url();?>assets/img/PDF.png" width="30">
                                                        </a>
                                                        <a href="<?php echo get_site_url('pdfx/cv_export_candidato/'.$data->iduser)?>" target="_blank">
                                                            <img src="<?php echo get_assets_url();?>assets/img/WORD.png" width="30">
                                                        </a>
                                                    </div>
                                                </div>   
                                            </div>
                                            <div class="row">
                                                <div class="col-md-12 content-cv"> <hr>
                                                    <div class="col-md-6">
                                                        <h4>ACERCA DE MI</h4>
                                                        <p><?php echo $data->Acerca_de_mi ?></p>
                                                        <br>
                                                        <h4>EDUCACIÓN</h4>
                                                        <div class="dashboard-widget-content">

                                                            <ul class="list-unstyled timeline widget">
                                                                <li>
                                                                    <div class="block">
                                                                        <div class="block_content">
                                                                            <span class="count_top"><?php echo $data->desde1 ?> - <?php echo $data->hasta1 ?></span>
                                                                            <h2 class="title text-left">
                                                                                <?php echo $data->titulo1 ?>
                                                                            </h2>
                                                                            <p class="excerpt text-left"><?php echo $data->institucion1 ?>
                                                                        </p>
                                                                    </div>
                                                                </div>
                                                            </li>
                                                            <li>
                                                                <div class="block">
                                                                    <div class="block_content">
                                                                        <span class="count_top"><?php echo $data->desde2 ?> - <?php echo $data->hasta2 ?></span>
                                                                        <h2 class="title text-left">
                                                                            <?php echo $data->titulo2 ?>
                                                                        </h2>
                                                                        <p class="excerpt text-left">  <?php echo $data->institucion2 ?>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                        </li>
                                                        <li>
                                                            <div class="block">
                                                                <div class="block_content">
                                                                    <span class="count_top"><?php echo $data->desde3 ?> - <?php echo $data->hasta3 ?></span>
                                                                    <h2 class="title text-left">
                                                                        <?php echo $data->titulo3 ?>
                                                                    </h2>
                                                                    <p class="excerpt text-left"><?php echo $data->institucion3 ?>
                                                                </p>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                        </div>
                                        <div class="col-md-6 timeline">
                                            <div class="block_experiencia">

                                                <h4>EXPERIENCIA</h4>
                                                <br>
                                                <span class="count_top"><?php echo $data->e_desde1 ?> - <?php echo $data->e_hasta1 ?></span>
                                                <h3 class="text-left"><?php echo $data->empresa1 ?></h3>
                                                <span><?php echo $data->funcion_cargo1 ?></span>
                                                <p class="text-left"><?php echo $data->descripcion_cargo1 ?></p>
                                                <br>
                                                <span class="count_top"><?php echo $data->e_desde2 ?> - <?php echo $data->e_hasta2 ?></span>
                                                <h3 class="text-left"><?php echo $data->empresa2 ?></h3>
                                                <span><?php echo $data->funcion_cargo2 ?></span>
                                                <p class="text-left"><?php echo $data->descripcion_cargo2 ?></p>
                                            </div>
                                        </div>
                                    </div>
                                </div><hr>
                                <div class="row content-cv">
                                    <div class="col-md-12">
                                        <h4 class="text-center">IDIOMAS</h4>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <h3 class="text-left"><?php echo $data->Idiomas_requerido ?></h3>
                                                <div class="progress progress_sm">
                                                    <div class="progress-bar bg-green" role="progressbar" data-transitiongoal="50"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><br><br><br><br><br>
            </div>
        </div>
    </div>
</div>
</div>
</div>
</div>
<!-- /page content -->


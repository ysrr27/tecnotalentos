<?php
class Vip_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }

   public function session($correo, $contrasena){

     
      $this->db->select('id, nombre, apellido,rol,foto,correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $this->db->where('contrasena', $contrasena);
      $consulta = $this->db->get();
      $resultado = $consulta->row();

      return $resultado;
   }

   public function addbanner($objBanner){

      if( !isset($objBanner->idbanner) or $objBanner->idbanner =="0" or $objBanner->idbanner==""){
            $this->db->insert('c_banners', $objBanner );             
            $resultado = $this->db->insert_id();
      
        }else{
            $this->db->update('c_banners' , $objBanner, array('idbanner' =>  $objBanner->idbanner));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
        return $resultado;

   }


   public function updateuser($objActualizardatos){

    $this->db->update('c_usuarios' , $objActualizardatos, array('id' =>  $objActualizardatos->id));
    $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 

    return $resultado;

    }

        public function pass($id){
          $this->db->select('contrasena');
          $this->db->from('c_usuarios');
          $this->db->where('id', $id);
          $consulta = $this->db->get();
          $resultado = $consulta->row();
          return $resultado;
      }

   public function get_publications(){
      $sql = "SELECT * FROM c_zona_vip";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function get_users_reclutas(){
      $sql = "SELECT * FROM c_usuarios WHERE rol='recluta'";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function checkEmail($correo){
      $this->db->select('correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
      return $resultado;
  }

   public function get_byIdBanner($id){

      if( $id > 0){
         $response = new StdClass();         
    
         $sql = " SELECT * FROM c_banners  WHERE  idbanner = ".$id."    LIMIT 1 ";
         $query = $this->db->query(  $sql  ); 
         $response = $query->row();
    
         return $response;
        }

   }

   public function deleteBanner($idbanner){
            $this->db->where('idbanner',$idbanner);
            $this->db->delete('c_banners');

            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
   }
   
   public function recoverypassword($objUsuario){
        $this->db->set('contrasena',$objUsuario->contrasena);
        $this->db->where('correo',$objUsuario->correo);
        $this->db->update('c_usuarios');
        $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 

        return $resultado;

  }

  public function update_photo_profile($idusario,$file){

        $this->db->set('foto', $file); 
        $this->db->where('id', $idusario);
        $this->db->update('c_usuarios');

        $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        return $resultado;
}

}
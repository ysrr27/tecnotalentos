<?php
class Preguntas_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }


   public function addPreguntas($objPreguntas){

      if( !isset($objPreguntas->idpregunta) or $objPreguntas->idpregunta =="0" or $objPreguntas->idpregunta==""){

            $this->db->insert('rmw_preguntas', $objPreguntas );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('rmw_preguntas' , $objPreguntas, array('idpregunta' =>  $objPreguntas->idpregunta));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

   public function addPaso($objPasos){
            $this->db->insert('rmw_pasos', $objPasos );             
            $resultado = $this->db->insert_id();
        return $resultado;
   }

   public function order($i,$v){

    $this->db->set('orden', $i);
    $this->db->where('idpregunta',$v);
    $this->db->update('rmw_preguntas');



    return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
  }

   

   public function get_Metas(){
      $sql = "SELECT * FROM rmw_metadatos";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function checkEmail($correo){
      $this->db->select('correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
      return $resultado;
  }

   public function get_byPregunta($idpregunta){

      if( $idpregunta > 0){
         $response = new StdClass();         
    
         $sql = " SELECT * FROM rmw_preguntas  WHERE  idpregunta = ".$idpregunta."    LIMIT 1 ";
         $query = $this->db->query(  $sql  ); 
         $response = $query->row();
    
         return $response;
        }

   }

   public function get_preguntas(){
    $sql = "SELECT * 
    FROM rmw_preguntas as pre
    INNER JOIN rmw_pasos pa ON (pre.paso = pa.valor_paso)
    ORDER BY pre.orden+0 ASC";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

  public function get_pasos(){
      $sql = "SELECT * FROM rmw_pasos";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   

   public function get_byNameMeta($metaname){

         $response = new StdClass();         
    
         $sql = " SELECT * FROM rmw_metadatos  WHERE  pagina = '".$metaname."'    LIMIT 1 ";
         $query = $this->db->query(  $sql  ); 
         $response = $query->row();
    
         return $response;

   }

   public function deletePregunta($idpregunta){
            $this->db->where('idpregunta',$idpregunta);
            $this->db->delete('rmw_preguntas');
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

}
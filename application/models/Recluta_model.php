<?php
class Recluta_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }



  public function addrecluta($objRecluta){

      if( !isset($objRecluta->id_recluta) or $objRecluta->id_recluta =="0" or $objRecluta->id_recluta==""){

            $this->db->insert('rmw_recluta', $objRecluta );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('rmw_recluta' , $objRecluta, array('id_recluta' =>  $objRecluta->id_recluta));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

    public function addempresa($objEmpresa){

        if( !isset($objEmpresa->idempresa) or $objEmpresa->idempresa =="0" or $objEmpresa->idempresa==""){

            $this->db->insert('tecno_empresa', $objEmpresa );             
            $resultado = $this->db->insert_id();

        }else{
            $this->db->update('tecno_empresa' , $objEmpresa, array('idempresa' =>  $objEmpresa->idempresa));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }

        return $resultado;

    }


  public function addsolicitud($objSolicitud){

        $this->db->insert('tecno_solicitudes', $objSolicitud );             
        $resultado = $this->db->insert_id();

        return $resultado;

   }

    public function addmensaje($objMensaje){
        //actualizo el estatus del mensaje
        $this->db->set('status', 'message--new'); 
        $this->db->where('idmensaje', $objMensaje->idmensaje);
        $this->db->update('tecno_mensajes');

        //agrego el detalle del mensaje
        $this->db->insert('tecno_mensajes_detalles', $objMensaje );             
        $resultado = $this->db->insert_id();

        //actualizo el estatus del mensaje
        $this->db->set('orden', $resultado); 
        $this->db->where('idmensaje', $objMensaje->idmensaje);
        $this->db->update('tecno_mensajes');

        return $resultado;

    }
    

   

   public function get_Reclutas(){
     $sql = "SELECT * 
        FROM rmw_recluta AS recluta";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;

   }
 public function get_solicitudes_recluta($idrecluta){
     $sql = "SELECT * 
              FROM tecno_solicitudes 
              WHERE idrecluta =".$idrecluta;
            
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;

   }


    public function get_Mensajes(){
        $sql = "SELECT *
        FROM tecno_mensajes tms
        INNER JOIN c_usuarios usr ON (usr.id = tms.useridfrom)
        ORDER BY orden DESC";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
            return $result;
        else
            return false;

    }
   public function get_Mensajes_recluta($idrecluta){
       $sql = "SELECT *
                FROM tecno_mensajes tms
                INNER JOIN c_usuarios usr ON (usr.id = tms.useridto)
                WHERE tms.useridto =".$idrecluta."
                ORDER BY orden DESC";

       $query = $this->db->query( $sql );
       $result = $query->result();

       if ($result)
        return $result;
    else
        return false;

}

public function ultimom_mensaje($idmensaje){
/*    $mensaje = $this->db->order_by('idmensaje',"desc")
    ->limit(1)
    ->get('tecno_mensajes')
    ->row();*/

   // var_dump($mensaje->idmensaje);exit();
    if (!empty($idmensaje)){
      
        $sql = "SELECT tmd.idmensajes,tmd.useridfrom,tmd.idmensaje, tmd.fullmensaje,tmd.file, tmd.timecreate, tmd.file_url, tmd.timecreate, tm.subject, CONCAT(usr.nombre, ' ',usr.apellido) as usuario, usr.correo AS correo_mensaje ,usr.foto 
        FROM tecno_mensajes_detalles tmd
        INNER JOIN c_usuarios usr ON (usr.id = tmd.send_userid) 
        INNER JOIN tecno_mensajes tm ON (tm.idmensaje = tmd.idmensaje) 
        WHERE tm.idmensaje = ".$idmensaje;

        $query = $this->db->query( $sql );
        $result = $query->result();

        return $result;
    }

}
public function ultimom_mensaje_recluta($idrecluta){
      $this->db->select('*');
      $this->db->from('tecno_mensajes_detalles');
      $this->db->where('useridto', $idrecluta);
      $this->db->order_by('idmensajes',"desc");
      $this->db->limit(1);

      $consulta = $this->db->get();
      $mensaje = $consulta->row();
    if ($mensaje){
        $idmensaje = intval($mensaje->idmensaje);
      
        $sql = "SELECT tmd.idmensajes,tmd.useridfrom,tmd.idmensaje, tmd.fullmensaje,tmd.file, tmd.timecreate, tmd.file_url, tmd.timecreate, tm.subject, CONCAT(usr.nombre, ' ',usr.apellido) as usuario, usr.correo AS correo_mensaje ,usr.foto 
        FROM tecno_mensajes_detalles tmd
        INNER JOIN c_usuarios usr ON (usr.id = tmd.send_userid) 
        INNER JOIN tecno_mensajes tm ON (tm.idmensaje = tmd.idmensaje) 
        WHERE tm.idmensaje = ".$idmensaje." ORDER BY tmd.idmensajes DESC";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result){
            return $result;
        }else{
            return false;
        }

    }

}

 public function detalles_mensajes($idmensaje){

    $this->db->set('status', ''); 
    $this->db->where('idmensaje', $idmensaje);
    $this->db->update('tecno_mensajes');

    $this->db->set('status', ''); 
    $this->db->where('idmensaje', $idmensaje);
    $this->db->update('tecno_mensajes_detalles');

    $sql = "SELECT tmd.idmensajes,tmd.useridfrom, tmd.fullmensaje,tmd.idmensaje,tmd.file, tmd.file_url,DATE_FORMAT(FROM_UNIXTIME(tmd.timecreate), '%e/%m/%Y %H:%i:%s') AS timecreate, tm.subject, CONCAT(usr.nombre, ' ',usr.apellido) as usuario, usr.correo AS correo_mensaje ,usr.foto 
    FROM tecno_mensajes_detalles tmd
    INNER JOIN c_usuarios usr ON (usr.id = tmd.send_userid) 
    INNER JOIN tecno_mensajes tm ON (tm.idmensaje = tmd.idmensaje) 
    WHERE tm.idmensaje = ".$idmensaje." ORDER BY tmd.idmensajes DESC";
    
    $query = $this->db->query( $sql );
    $result = $query->result();

   if ($result){
            return $result;
        }else{
            return false;
        }

    }


    
    public function get_Count_Mensajes(){
        $sql = "SELECT count(tmd.idmensajes) AS num
                FROM tecno_mensajes_detalles tmd
                WHERE tmd.status = 'message--new'";
        $query = $this->db->query( $sql );
        $result = $query->result();
        $cantidad = $result[0]->num;

        return $cantidad;


    } 



   public function get_byIdRecluta($id){
            
      $this->db->select('*');
      $this->db->from('rmw_recluta');
      $this->db->where('id_recluta', $id);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
        if ($resultado)
            return $resultado;
        else
            return false;
   }


   public function session($correo, $contrasena){

     
      $this->db->select('id, nombre, apellido');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $this->db->where('contrasena', $contrasena);
      $consulta = $this->db->get();
      $resultado = $consulta->row();

      return $resultado;
   }

   public function adduser($objUsuario){

      if( !isset($objUsuario->id) or $objUsuario->id =="0" or $objUsuario->id==""){

            $this->db->insert('c_usuarios', $objUsuario );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('c_usuarios' , $objUsuario, array('id' =>  $objUsuario->id));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

   public function get_Count_censos_corregidos(){
    $sql = "SELECT count(cns.idcenso) AS num
            FROM c_censo cns
            INNER JOIN c_contribuyente cont ON (cns.idcontribuyente = cont.idcontribuyente) 
            INNER JOIN c_actividad_economica AS cae ON (cns.idactividad = cae.idactividad)
            INNER JOIN c_inmuebles AS cin ON (cns.idinmueble = cin.idinmueble)
            INNER JOIN c_expendio_licores AS cexl ON(cns.idexpendio=cexl.idexpendio) 
            INNER JOIN c_publicidad AS pu ON(cns.idpublicidad = pu.idpublicidad) 
            INNER JOIN c_apuestas AS apu ON(cns.idapuestas = apu.idapuestas) 
            INNER JOIN c_detalles_establecimiento AS det ON(cns.iddetalles = det.iddetalles) 
            INNER JOIN c_serv_electrico AS serv ON(cns.idservicio = serv.idservicio) 
            INNER JOIN c_datos_personales AS dper ON(cns.iddatos = dper.iddatos) 
            INNER JOIN c_usuarios AS usr ON(cns.usuario = usr.id)
            WHERE cns.estatus = 'Corregida'";
             $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

   } 

    public function get_Counts_censos_transcritos(){
        $sql="SELECT count(cns.idcenso) AS num
              FROM c_censo cns
              INNER JOIN c_contribuyente cont ON (cns.idcontribuyente = cont.idcontribuyente) 
              INNER JOIN c_actividad_economica AS cae ON (cns.idactividad = cae.idactividad)
              INNER JOIN c_inmuebles AS cin ON (cns.idinmueble = cin.idinmueble)
              INNER JOIN c_expendio_licores AS cexl ON(cns.idexpendio=cexl.idexpendio) 
              INNER JOIN c_publicidad AS pu ON(cns.idpublicidad = pu.idpublicidad) 
              INNER JOIN c_apuestas AS apu ON(cns.idapuestas = apu.idapuestas) 
              INNER JOIN c_detalles_establecimiento AS det ON(cns.iddetalles = det.iddetalles) 
              INNER JOIN c_serv_electrico AS serv ON(cns.idservicio = serv.idservicio) 
              INNER JOIN c_datos_personales AS dper ON(cns.iddatos = dper.iddatos) 
              INNER JOIN c_usuarios AS usr ON(cns.usuario = usr.id)";

               $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

    }



   public function get_empadronador(){
        $sql = "SELECT * 
        FROM c_usuarios AS usr
        WHERE usr.rol = 'Empadronador'";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function checkEmail($correo){
      $this->db->select('correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
      return $resultado;
  }


    public function get_export(){
     $sql = "SELECT * 
        FROM rmw_recluta AS recluta";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;

   }
  

   public function deleteRecluta($idrecluta){

            $this->db->where('id_recluta',$idrecluta);
            $this->db->delete('rmw_recluta');

            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
   }

    public function get_data_perfil_empresa($idrecluta){
            
      $this->db->select('*');
      $this->db->from('tecno_empresa');
      $this->db->where('idrecluta', $idrecluta);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
        if ($resultado)
            return $resultado;
        else
            return false;
   }
  

    

}
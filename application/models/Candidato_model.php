<?php
class Candidato_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }

   public function aprobar($idcenso,$ArrRegisterCenso){

     $this->db->where('idcenso', $idcenso);
            $this->db->update('c_censo', $ArrRegisterCenso);


            $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

             if ($result) {
              $resultado = $idcenso;   
            }else{
              $resultado = false;
            }
    return $resultado;

   }

    public function get_Candidato($id){

            $response = new StdClass();         
            $sql = " SELECT *
                        FROM tecno_candidatos tc 
                        INNER JOIN tecno_can_datospersonales td ON (td.iddatospersonales = tc.iddatospersonales) 
                        INNER JOIN tecno_can_experiencia te ON (te.idexperiencia = tc.idexperiencia) 
                        INNER JOIN tecno_can_educacion ted ON (ted.ideducacion = tc.ideducacion) 
                        INNER JOIN c_usuarios usr ON (usr.id = tc.iduser) 
                        WHERE tc.iduser =".$id."  LIMIT 1";
            $query = $this->db->query(  $sql  ); 
            $response = $query->row();

            if ($response) {
                return $response;
            }else{
              return false;
          }
        
    }

     public function addCandidato($idcandidato,$objdatospersonales,$objeducacion,$objexperiencia,$ArrCandidato){



    if( !isset($idcandidato) or $idcandidato =="0" or $idcandidato==""){            
            
            $this->db->insert('tecno_can_datospersonales', $objdatospersonales);             
            $iddatospersonales = $this->db->insert_id();
            $ArrCandidato['iddatospersonales'] = $iddatospersonales;

            $this->db->insert('tecno_can_educacion', $objeducacion);             
            $ideducacion = $this->db->insert_id();
            $ArrCandidato['ideducacion'] = $ideducacion;

            $this->db->insert('tecno_can_experiencia', $objexperiencia);             
            $idexperiencia = $this->db->insert_id();
            $ArrCandidato['idexperiencia'] = $idexperiencia;

            $this->db->insert('tecno_candidatos',$ArrCandidato);
            $idcandidato = $this->db->insert_id();

            $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

            if ($result) {
              $resultado = $idcandidato;  
            }else{
              $resultado = false;
            }

        }else{

            $this->db->update('tecno_can_datospersonales' , $objdatospersonales, array('iddatospersonales' => $objdatospersonales->iddatospersonales));

            $this->db->update('tecno_can_educacion' , $objeducacion, array('ideducacion' => $objeducacion->ideducacion));
           
            $this->db->update('tecno_can_experiencia' , $objexperiencia, array('idexperiencia' => $objexperiencia->idexperiencia));

            $this->db->where('idcandidato', $idcandidato);
            $this->db->update('tecno_candidatos', $ArrCandidato);


            $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

             if ($result) {
              $resultado = $idcandidato;   
            }else{
              $resultado = false;
            }


        }
      return $resultado;         

   }

public function updateCandidato($objRespuestas){

            $this->db->update('mrw_candidatos' , $objRespuestas, array('idcandidatos' =>  $objRespuestas->idcandidatos));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
            $result = ($this->db->affected_rows() > 0) ? TRUE : FALSE;

            if ($result) {
              $resultado = $objRespuestas->idcandidatos;  
            }else{
              $resultado = false;
            }

      return $resultado;         

   }

   public function session($correo, $contrasena){
      $this->db->select('id, nombre, apellido');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $this->db->where('contrasena', $contrasena);
      $consulta = $this->db->get();
      $resultado = $consulta->row();

      return $resultado;
   }

   public function adduser($objUsuario){

      if( !isset($objUsuario->id) or $objUsuario->id =="0" or $objUsuario->id==""){

            $this->db->insert('c_usuarios', $objUsuario );             
            $resultado = $this->db->insert_id();
        
        }else{
            $this->db->update('c_usuarios' , $objUsuario, array('id' =>  $objUsuario->id));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

   public function get_Count_Recluta(){
    $sql = "SELECT count(rc.id_recluta) AS num
            FROM rmw_recluta rc
            order by rc.id_recluta asc";
             $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

   } 

   public function get_Count_solicitudes(){
        $sql = "SELECT count(ts.idsolicitud) AS num
                FROM tecno_solicitudes ts
                order by ts.idsolicitud asc";
                $query = $this->db->query( $sql );
                $result = $query->result();
        if ($result)
            return $result;
        else
            return false;

    } 

    public function get_last_solicitudes(){
       $sql = "SELECT ts.idsolicitud,ts.Descripcion_puesto,ts.Puesto_buscar,DATE_FORMAT(FROM_UNIXTIME(ts.fecha), '%e/%m/%Y') AS fecha
       FROM tecno_solicitudes AS ts
       INNER JOIN c_usuarios usr ON (usr.id = ts.idrecluta) 
       ORDER BY ts.idsolicitud asc LIMIT 7";

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
          return $result;
        else
            return false;

    } 

    public function get_Counts_Candidatos(){
        $sql="SELECT count(can.idcandidato) AS num
              FROM tecno_candidatos can
              order by can.idcandidato asc";

               $query = $this->db->query( $sql );
                    $result = $query->result();
                      if ($result)
                          return $result;
                      else
              return false;

    }

   public function get_Canditatos(){
      $sql = "SELECT tc.iduser, tc.idcandidato,td.Nombre,td.Apellido,DATE_FORMAT(FROM_UNIXTIME(tc.fecha), '%e/%m/%Y') AS fecha, tc.estatus
                FROM tecno_candidatos tc 
                INNER JOIN tecno_can_datospersonales td ON (td.iddatospersonales = tc.iddatospersonales) 
                INNER JOIN tecno_can_educacion tce ON (tce.ideducacion = tc.ideducacion) 
                INNER JOIN tecno_can_experiencia te ON (te.idexperiencia = tc.idexperiencia) 
                INNER JOIN c_usuarios usr ON (usr.id = tc.iduser) 
                                ORDER BY tc.idcandidato ASC ";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

    public function get_LastCanditatos(){
      $sql = "SELECT tc.iduser, tc.idcandidato,td.Nombre,td.Apellido,td.Correo,DATE_FORMAT(FROM_UNIXTIME(tc.fecha), '%e/%m/%Y') AS fecha
                FROM tecno_candidatos tc 
                INNER JOIN tecno_can_datospersonales td ON (td.iddatospersonales = tc.iddatospersonales) 
                INNER JOIN tecno_can_educacion tce ON (tce.ideducacion = tc.ideducacion) 
                INNER JOIN tecno_can_experiencia te ON (te.idexperiencia = tc.idexperiencia) 
                INNER JOIN c_usuarios usr ON (usr.id = tc.iduser) 
                                ORDER BY tc.idcandidato asc LIMIT 7";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function get_empadronador(){
        $sql = "SELECT * 
        FROM c_usuarios AS usr
        WHERE usr.rol = 'Empadronador'";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function checkEmail($correo){
      $this->db->select('correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
      return $resultado;
  }



  public function deleteCandidato($idcandidato){

    $this->db->where('idcandidato',$idcandidato);
    $this->db->delete('tecno_candidatos');

    return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
}

   public function get_export(){

    $sql = "SELECT td.Nombre,td.Apellido,td.Correo,td.Telefono,tce.titulo1,DATE_FORMAT(FROM_UNIXTIME(tc.fecha), '%e/%m/%Y') AS fecha
              FROM tecno_candidatos tc 
              INNER JOIN tecno_can_datospersonales td ON (td.iddatospersonales = tc.iddatospersonales) 
              INNER JOIN tecno_can_educacion tce ON (tce.ideducacion = tc.ideducacion) 
              INNER JOIN tecno_can_experiencia te ON (te.idexperiencia = tc.idexperiencia) 
              INNER JOIN c_usuarios usr ON (usr.id = tc.iduser) 
              ORDER BY tc.idcandidato ASC";
    
    $query = $this->db->query( $sql );
    $result = $query->result();

    if ($result)
      return $result;
    else
      return false;

  }

    public function update_status($idcandidato,$status){

         //actualizo el estatus del mensaje
        $this->db->set('estatus', $status); 
        $this->db->where('idcandidato', $idcandidato);
        $this->db->update('tecno_candidatos');

        $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        return $resultado;
    }
}
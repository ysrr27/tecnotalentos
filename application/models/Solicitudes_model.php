<?php
class Solicitudes_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }



  public function addrecluta($objRecluta){

      if( !isset($objRecluta->id_recluta) or $objRecluta->id_recluta =="0" or $objRecluta->id_recluta==""){

            $this->db->insert('rmw_recluta', $objRecluta );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('rmw_recluta' , $objRecluta, array('id_recluta' =>  $objRecluta->id_recluta));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

  public function addsolicitud($objSolicitud){

        $this->db->insert('tecno_solicitudes', $objSolicitud );             
        $resultado = $this->db->insert_id();

        return $resultado;

   }

    public function addmensaje($objMensaje){

        //agrego el mensaje
        $this->db->insert('tecno_mensajes', $objMensaje);             
        $idmensaje = $this->db->insert_id();

        return $idmensaje;

    }
    

   
    public function addmensaje_detalle($objMensaje_detalle){
      
        //agrego el detalle del mensaje
        $this->db->insert('tecno_mensajes_detalles', $objMensaje_detalle);             
        $resultado = $this->db->insert_id();

        //actualizo el estatus del mensaje
        $this->db->set('orden', $resultado); 
        $this->db->where('idmensaje', $objMensaje_detalle->idmensaje);
        $this->db->update('tecno_mensajes');

        return $resultado;

    }


 public function get_solicitudes(){
     $sql = "SELECT * 
              FROM tecno_solicitudes AS ts
              INNER JOIN c_usuarios usr ON (usr.id = ts.idrecluta) ";
            
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;

   }


   public function get_Mensajes(){
       $sql = "SELECT *
                FROM tecno_mensajes tms
                INNER JOIN c_usuarios usr ON (usr.id = tms.useridfrom)
                ORDER BY orden DESC";

       $query = $this->db->query( $sql );
       $result = $query->result();

       if ($result)
        return $result;
    else
        return false;

}

public function ultimom_mensaje(){
    $mensaje = $this->db->order_by('idmensajes',"desc")
    ->limit(1)
    ->get('tecno_mensajes_detalles')
    ->row();
    if ($mensaje){
        $idmensaje = intval($mensaje->idmensaje);
      
        $sql = "SELECT tmd.idmensajes,tmd.useridfrom,tmd.idmensaje, tmd.fullmensaje,tmd.file, tmd.timecreate, tmd.file_url, tmd.timecreate, tm.subject, CONCAT(usr.nombre, ' ',usr.apellido) as usuario, usr.correo AS correo_mensaje ,usr.foto 
        FROM tecno_mensajes_detalles tmd
        INNER JOIN c_usuarios usr ON (usr.id = tmd.send_userid) 
        INNER JOIN tecno_mensajes tm ON (tm.idmensaje = tmd.idmensaje) 
        WHERE tm.idmensaje = ".$idmensaje;

        $query = $this->db->query( $sql );
        $result = $query->result();

        return $result;
    }

}

 public function detalles_mensajes($idmensaje){

    $this->db->set('status', ''); 
    $this->db->where('idmensaje', $idmensaje);
    $this->db->update('tecno_mensajes');

    $this->db->set('status', ''); 
    $this->db->where('idmensaje', $idmensaje);
    $this->db->update('tecno_mensajes_detalles');


    $sql = "SELECT tmd.idmensajes,tmd.useridfrom, tmd.fullmensaje,tmd.idmensaje,tmd.file, tmd.file_url,DATE_FORMAT(FROM_UNIXTIME(tmd.timecreate), '%e/%m/%Y %H:%i:%s') AS timecreate, tm.subject, CONCAT(usr.nombre, ' ',usr.apellido) as usuario, usr.correo AS correo_mensaje ,usr.foto 
    FROM tecno_mensajes_detalles tmd
    INNER JOIN c_usuarios usr ON (usr.id = tmd.send_userid) 
    INNER JOIN tecno_mensajes tm ON (tm.idmensaje = tmd.idmensaje) 
    WHERE tm.idmensaje = ".$idmensaje;

    $query = $this->db->query( $sql );
    $result = $query->result();

   if ($result){
            return $result;
        }else{
            return false;
        }

    }


    
    public function get_Count_Mensajes(){
        $sql = "SELECT count(tmd.idmensajes) AS num
                FROM tecno_mensajes_detalles tmd
                WHERE tmd.status = 'message--new'";
        $query = $this->db->query( $sql );
        $result = $query->result();
        $cantidad = $result[0]->num;

        return $cantidad;


    } 



   public function get_byIdRecluta($id){
            
      $this->db->select('*');
      $this->db->from('rmw_recluta');
      $this->db->where('id_recluta', $id);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
        if ($resultado)
            return $resultado;
        else
            return false;
   }


   public function session($correo, $contrasena){

     
      $this->db->select('id, nombre, apellido');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $this->db->where('contrasena', $contrasena);
      $consulta = $this->db->get();
      $resultado = $consulta->row();

      return $resultado;
   }

   public function adduser($objUsuario){

      if( !isset($objUsuario->id) or $objUsuario->id =="0" or $objUsuario->id==""){

            $this->db->insert('c_usuarios', $objUsuario );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('c_usuarios' , $objUsuario, array('id' =>  $objUsuario->id));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }



    public function deleteSolicitud($idsolicitud){
            $this->db->where('idsolicitud',$idsolicitud);
            $this->db->delete('tecno_solicitudes');
            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
    }

    public function getSolicitudbyid($idsolicitud){
        $sql = "SELECT * 
        FROM tecno_solicitudes AS ts
        INNER JOIN c_usuarios usr ON (usr.id = ts.idrecluta) 
        WHERE idsolicitud =".$idsolicitud;

        $query = $this->db->query( $sql );
        $result = $query->result();

        if ($result)
          return $result;
        else
            return false;
    }
    
    public function update_status($idsolicitud,$status){

         //actualizo el estatus del mensaje
        $this->db->set('estatus', $status); 
        $this->db->where('idsolicitud', $idsolicitud);
        $this->db->update('tecno_solicitudes');

        $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        return $resultado;
    }



   

    

}
<?php
class Meta_model extends CI_Model { 
   public function __construct() {
      parent::__construct();
   }


   public function addmeta($objMeta){

      if( !isset($objMeta->idmeta) or $objMeta->idmeta =="0" or $objMeta->idmeta==""){

            $this->db->insert('rmw_metadatos', $objMeta );             
            $resultado = $this->db->insert_id();
         

        }else{
            $this->db->update('rmw_metadatos' , $objMeta, array('idmeta' =>  $objMeta->idmeta));
            $resultado = ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
        }
       
        return $resultado;

   }

   public function get_Metas(){
      $sql = "SELECT * FROM rmw_metadatos";
      
      $query = $this->db->query( $sql );
      $result = $query->result();

        if ($result)
            return $result;
        else
            return false;
   }

   public function checkEmail($correo){
      $this->db->select('correo');
      $this->db->from('c_usuarios');
      $this->db->where('correo', $correo);
      $consulta = $this->db->get();
      $resultado = $consulta->row();
      return $resultado;
  }

   public function get_byIdMeta($id){

      if( $id > 0){
         $response = new StdClass();         
    
         $sql = " SELECT * FROM rmw_metadatos  WHERE  idmeta = ".$id."    LIMIT 1 ";
         $query = $this->db->query(  $sql  ); 
         $response = $query->row();
    
         return $response;
        }

   }

   public function get_byNameMeta($metaname){

         $response = new StdClass();         
    
         $sql = " SELECT * FROM rmw_metadatos  WHERE  pagina = '".$metaname."'    LIMIT 1 ";
         $query = $this->db->query(  $sql  ); 
         $response = $query->row();
    
         return $response;

   }

   public function deleteUser($idusuario){

      var_dump($idusuario);
            $this->db->where('id',$idusuario);
            $this->db->delete('c_usuarios');

            return ($this->db->affected_rows() > 0) ? TRUE : FALSE; 
   }

}
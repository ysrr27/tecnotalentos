<?php

    /**
    * FileMaker PHP Site Assistant Generated File
    
	require_once APPPATH."third_party/filemakerApiVideos/fmview.php";
	require_once APPPATH."third_party/filemakerApiVideos/FileMaker.php";
	require_once APPPATH."third_party/filemakerApiVideos/error.php";
   */
   
	//require_once "filemakerApi/fmview.php";
	require_once "FileMaker/FileMaker.php";
	//require_once "FileMaker/error.php";

class filemake {

	public $cgi;
	public	$layoutName;
	public $fm;
	public $layout_object;
	public $databaseName;
	public $userName;
	public $passWord;
	   
	public function __construct($databaseName, $layoutName){
		//$this->cgi = new CGI();
    	//$this->cgi->storeFile();
    
		$this->databaseName = $databaseName;//'Videos_';
		$this->layoutName = $layoutName;//'php';

		$this->userName = "program";//$this->cgi->get('userName');
		$this->passWord =  3187; // $this->cgi->get('passWord');
		//die($this->userName."**".$this->passWord."**".$this->databaseName);

		$this->fm =  new FileMaker();
		$this->fm->setProperty('database', $this->databaseName);
		$this->fm->setProperty('username', $this->userName);
		$this->fm->setProperty('password', $this->passWord);
    
		//ExitOnError($this->fm);
		$this->layout_object = $this->fm->getLayout($this->layoutName);
		//ExitOnError($this->layout);*/
	}
	 
	public function insertFMEmpresasVideos($empresasArray , $videos, $id_programa){

		if (!isset ($recid))
			$recid = 1;	
		
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);	

		foreach($videos as $video){
			$findCommand->addFindCriterion($listofFields[0], "==".$video['VideoID']);	
			
			$findCom =	$findCommand;
			$findCom->setLogicalOperator('and');
			$result = $findCom->execute();
			$records = $result->getRecords();   

			$empresas = "";
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId());
			//echo "video -> ".$video['VideoID']."<br />";
			
			if(isset($currentRecord) ){
				$empresas =  $currentRecord->getField($listofFields[1], 0) ;
		
				$result = $empresas;			
				$threshold = false;	
				foreach($empresasArray as $counter => $val){
					//echo "entro1".$val['CompanyFMID']."-".$empresas."<br />";
					if($empresas > ""){

						if (strrpos($empresas, $val['CompanyFMID']) === false) {
							$result.="
".$val['CompanyFMID'];
							$threshold = true;						
						}
					}else{
						if($counter == 0){
							$result.= $val['CompanyFMID'];
							
						}else{
							$result.="
".$val['CompanyFMID'];	
							
						}
						$threshold = true;	
					}
				}
				
				if($threshold){		
					$tmp = array( $listofFields[1] => $result);	
					$test = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
					$delresult = $test->execute();
				}
			}
		}
	}
	
	//////******************VIDEOS*********************//////////
	public function editRecordVideo($video){
	
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		//$video->IdVideo = 10414;	
		
		$findCommand->addFindCriterion($listofFields[0], "==".$video->IdVideo);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();	
		
		/*echo "<pre>";
			print_r($video);
		echo "</pre>";*/

		if( isset($result->code) && $result->code == "401"){	
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("Vide#", trim($video->IdVideo), 0);
			$newCommand->setField("TituloHP", trim($video->TituloHP), 0);
			$newCommand->setField("TituloHP2", trim($video->TituloHP2), 0);
			$newCommand->setField("Foto", trim($video->Foto), 0);
			$newCommand->setField("Fecha", trim($video->Fecha), 0);
			$newCommand->setField("IDEmpresa", trim($video->IDEmpresa), 0);
			$newCommand->setField("Categoria2", trim($video->Categoria2FM), 0);
			$newCommand->setField("Titulo", trim($video->Titulo), 0);
			$newCommand->setField("Orden9", trim($video->Orden9), 0);
			$newCommand->setField("enelset", trim($video->enelset), 0);
			
			switch(strtolower($video->Categoria2FM)){ 
				case "vips": 	$newCommand->setField("Categoria2VIPS", trim($video->Categoria2VIPSFM), 0);		break;				
				case "contenido":   $newCommand->setField("Categoria2Contenidos", trim($video->Categoria2ContenidosFM), 0);		break;
				case "contenidos":   $newCommand->setField("Categoria2Contenidos", trim($video->Categoria2ContenidosFM), 0);	 	break;				
				case "comerciales":    $newCommand->setField("Categoria2Comerciales", trim($video->Categoria2ComercialesFM), 0);    break;				
				case "tecnologÍa": 	 $newCommand->setField("Categoria2Tecno", trim($video->Categoria2TecnoFM) , 0); break; 
				case "tecnologia":  	 $newCommand->setField("Categoria2Tecno", trim($video->Categoria2TecnoFM), 0 );  break;
				case "tecno": 	 $newCommand->setField("Categoria2Tecno", trim($video->Categoria2TecnoFM), 0);   break;
				//case "english vip":  $newCommand->setField("Categoria2VIPS", trim($video->Categoria2VIPSFM), 0); 	break;
				//case "english content": 	 $newCommand->setField("Categoria2Contenidos", trim($video->Categoria2ContenidosFM), 0);  	break;
			}
			
			if(isset($video->Portadas) && $video->Portadas == "x"){
				$newCommand->setField("Portada", "x", 0);
			}
			
			if(isset($video->actualidad) && $video->actualidad == "x"){
				$newCommand->setField("actualidad", "x", 0);
			}
			
			if(isset($video->tendencias) && $video->tendencias == "x"){
				$newCommand->setField("tendencias", "x", 0);
			}
			
			if(isset($video->Eventos) && $video->Eventos == "x"){
				$newCommand->setField("Eventos", "x", 0);
			}
			
			if(isset($video->solowwonline) && $video->solowwonline == "x"){
				$newCommand->setField("solowwonline", "x", 0);
			}
			
			if(isset($video->ottvod) && $video->ottvod == "x"){
				$newCommand->setField("ottvod", "x", 0);
			}
			
			if(isset($video->EncEspecial) && $video->EncEspecial == "x"){
				$newCommand->setField("EncEspecial", "x", 0);
			}
			
			$result = $newCommand->execute();
			
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
			
				//echo "<pre>"; print_r($listofFields); echo "</pre>";
				//die("endfilemaker");
			
				$tmp = array( "TituloHP" => trim($video->TituloHP),"Fecha" => trim($video->Fecha),"TituloHP2" => trim($video->TituloHP2),
				"Categoria2" => trim($video->Categoria2FM), "Titulo" => trim($video->Titulo),
				"Foto" => trim($video->Foto),"IDEmpresa" => trim($video->IDEmpresa) ,"Orden9" => trim($video->Orden9),"enelset" => trim($video->enelset) );
				//echo "<pre>"; print_r($tmp); echo "</pre>";
				//die("endfilemaker");
				
				$Categoria2Contenidos = "";
				$Categoria2VIPS= "";
				$Categoria2Tecno="";
				$Categoria2Comerciales = "";
				
				if($video->Categoria2FM == 'VIPS') 
					$Categoria2VIPS=  trim($video->Categoria2VIPSFM) ;				
				
				if($video->Categoria2FM == 'CONTENIDO')
					$Categoria2Contenidos= trim($video->Categoria2ContenidosFM);				
				
				if($video->Categoria2FM == 'TECNOLOGIA' OR $video->Categoria2FM == 'TECNOLOGÍA')
					$Categoria2Tecno =  trim($video->Categoria2TecnoFM) ;					
				
				if($video->Categoria2FM == 'COMERCIALES')
					$Categoria2Comerciales= trim($video->Categoria2ComercialesFM) ;	
					
				if($video->Categoria2FM == 'ENGLISH VIPS') 
					$Categoria2VIPS=  trim($video->Categoria2VIPSFM);					
				
				if($video->Categoria2FM == 'ENGLISH CONTENT')
					$Categoria2Contenidos= trim($video->Categoria2ContenidosFM) ;	
				
			
				$subCategoriaArray = array("Categoria2VIPS" => $Categoria2VIPS, "Categoria2Contenidos" => $Categoria2Contenidos, "Categoria2Tecno" => $Categoria2Tecno, "Categoria2Comerciales" => $Categoria2Comerciales );
				$tmp= array_merge($tmp, $subCategoriaArray);
				
				
				if(isset($video->Portadas) && $video->Portadas == "x"){
					 $PortadaArray= array("Portada" => "x");
					 $tmp= array_merge($tmp, $PortadaArray);
				}else{
					$PortadaArray= array("Portada" => "");
					$tmp= array_merge($tmp, $PortadaArray);
				}
				
				if(isset($video->actualidad) && $video->actualidad == "x"){
					$actualidadArray= array("actualidad" => "x");
					$tmp= array_merge($tmp, $actualidadArray);
				}else{
					$actualidadArray= array("actualidad" => "");
					$tmp= array_merge($tmp, $actualidadArray);
				}
				
				if(isset($video->tendencias) && $video->tendencias == "x"){
					$tendenciasArray= array("tendencias" => "x");
					$tmp= array_merge($tmp, $tendenciasArray);
				}else{
					$tendenciasArray= array("tendencias" => "");
					$tmp= array_merge($tmp, $tendenciasArray);
				}
			
				if(isset($video->Eventos) && $video->Eventos == "x"){
					$EventosArray= array("Eventos" => "x");
					$tmp= array_merge($tmp, $EventosArray);
				}else{
					$EventosArray= array("Eventos" => "");
					$tmp= array_merge($tmp, $EventosArray);
				}
			
				if(isset($video->solowwonline) && $video->solowwonline == "x"){
					$solowwonlineArray= array("solowwonline" => "x");
					$tmp= array_merge($tmp, $solowwonlineArray);
				}else{
					$solowwonlineArray= array("solowwonline" => "");
					$tmp= array_merge($tmp, $solowwonlineArray);
				}
				
				if(isset($video->ottvod) && $video->ottvod == "x"){
					$ottvodArray= array("ottvod" => "x");
					$tmp= array_merge($tmp, $ottvodArray);
				}else{
					$ottvodArray= array("ottvod" => "");
					$tmp= array_merge($tmp, $ottvodArray);
				}
				
				if(isset($video->EncEspecial) && $video->EncEspecial == "x"){
					$EncEspecialArray= array("EncEspecial" => "x");
					$tmp= array_merge($tmp, $EncEspecialArray);
				}else{
					$EncEspecialArray= array("EncEspecial" => "");
					$tmp= array_merge($tmp, $EncEspecialArray);
				}
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die("edit".$editResult);
			}
		//	die("this is the end EDIT".$editResult);
		}	
		
		/*$empresas = "";
		
			//echo "video -> ".$video['VideoID']."<br />";
			
			if(isset($currentRecord) ){
				$empresas =  $currentRecord->getField($listofFields[1], 0) ;
		
				$result = $empresas;			
				$threshold = false;	
				foreach($empresasArray as $counter => $val){
					//echo "entro1".$val['CompanyFMID']."-".$empresas."<br />";
					if($empresas > ""){

						if (strrpos($empresas, $val['CompanyFMID']) === false) {
							$result.="
".$val['CompanyFMID'];
							$threshold = true;						
						}
					}else{
						if($counter == 0){
							$result.= $val['CompanyFMID'];
							
						}else{
							$result.="
".$val['CompanyFMID'];	
						}
						$threshold = true;	
					}
				}
				
				if($threshold){		
					$tmp = array( $listofFields[1] => $result);	
					$test = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
					$delresult = $test->execute();
				}
			}
		*/
	}
	
	public function deleteRecordVideo($videoID){
	
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
			
		$findCommand->addFindCriterion($listofFields[0], "==".$videoID);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	}
	
	/**********************EVENTOS**********************/
	public function editRecordEvento($evento){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		//$evento->IdEvento = 1241;
		$findCommand->addFindCriterion($listofFields[0], "==".$evento->IdEvento);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
	
		
		if( isset($result->code) && $result->code == "401"){
		
			$fecha = explode("/",$evento->FechaInicio);
			$mesesN=array("01"=>"enero","02"=>"febrero","03"=>"marzo","04"=>"abril","05"=>"mayo","06"=>"junio","07"=>"julio","08"=>"agosto","09"=>"septiembre","10"=>"octubre","11"=>"noviembre","12"=>"diciembre");
				
			$mes =$mesesN[trim($fecha[0])];
		
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("Evento ID", trim($evento->IdEvento), 0);
			$newCommand->setField("Evento", trim($evento->Evento), 0);
			$newCommand->setField("Categoria", trim($evento->Categoria), 0);
			$newCommand->setField("Lugar", trim($evento->Lugar), 0);
			$newCommand->setField("Dia Inicio", trim($fecha[1]), 0);
			$newCommand->setField("Mes Inicio", trim($mes ), 0);	
			$newCommand->setField("Ano Inicio", trim($fecha[2]), 0);
			$newCommand->setField("Archivos_::Link", trim($evento->Foto), 0);
			$newCommand->setField("Pais", trim($evento->Pais), 0);
			$newCommand->setField("Ciudad", trim($evento->Ciudad), 0);
			$newCommand->setField("English", trim($evento->English), 0);
			$newCommand->setField("OTTVOD", trim($evento->OTTVOD), 0);
			
			$result = $newCommand->execute();
				
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				$fecha = explode("/",$evento->FechaInicio);
			$mesesN=array("01"=>"enero","02"=>"febrero","03"=>"marzo","04"=>"abril","05"=>"mayo","06"=>"junio","07"=>"julio","08"=>"agosto","09"=>"septiembre","10"=>"octubre","11"=>"noviembre","12"=>"diciembre");
				
			$mes =$mesesN[trim($fecha[0])];
			
				$mes =$mesesN[trim($fecha[0])];
				
				$tmp = array( "Evento" => trim($evento->Evento),"Categoria" => trim($evento->Categoria),"Lugar" => trim($evento->Lugar), "Dia Inicio" => trim($fecha[1]),
				 "Mes Inicio" => $mes ,  "Ano Inicio" => trim($fecha[2]), "Archivos_::Link" => trim($evento->Foto), "Pais" => trim($evento->Pais), "Ciudad" => trim($evento->Ciudad)
				 , "English" => trim($evento->English), "OTTVOD" => trim($evento->OTTVOD) );
				
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();

			}
		
		}	
	}
	
	public function deleteRecordEvento($eventoID){
	
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
			
		$findCommand->addFindCriterion($listofFields[0], "==".$eventoID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	}
	
	
	/**********************PERFILES**********************/
	public function editRecordPerfil($perfil){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		//$evento->IdEvento = 1241;
		$findCommand->addFindCriterion($listofFields[5], "==".$perfil->RepoFM);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( isset($result->code) && $result->code == "401"){
		
		
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("Titulo", trim($perfil->Titulo), 0);
			$newCommand->setField("Fecha", trim($perfil->Fecha), 0);
			$newCommand->setField("Contenido", trim($perfil->Contenido), 0);
			$newCommand->setField("TalentoPersonaje", trim($perfil->TalentoPersonaje), 0);
			$newCommand->setField("Foto", trim($perfil->Foto), 0);
			$newCommand->setField("Repo#", trim($perfil->RepoFM), 0);
			$newCommand->setField("Check", trim($perfil->CheckFM), 0);	
			$newCommand->setField("LeyendaFoto", trim($perfil->LeyendaFoto), 0);
			$newCommand->setField("tituloHP", trim($perfil->TituloHP), 0);
			$newCommand->setField("idiomas", trim(""), 0);
			$newCommand->setField("medidas", trim(""), 0);
			$newCommand->setField("usuario", trim($perfil->UsuarioMod), 0);
			$newCommand->setField("HomePage", trim($perfil->HomePage), 0);
			$newCommand->setField("tituloHPEmpr", trim($perfil->TituloHPEmpr), 0);
			$newCommand->setField("ContenidoEng", trim($perfil->ContenidoEng), 0);
			$newCommand->setField("TituloEng", trim($perfil->TituloEng), 0);
			$newCommand->setField("LeyendaFotoEng", trim($perfil->LeyendaFotoEng), 0);
			//$newCommand->setField("IDVideos", trim($perfil->IDVideos), 0);
			$newCommand->setField("IDContacts", trim($perfil->IDContacts), 0);
			//$newCommand->setField("IDNoticias", trim($perfil->IDNoticias), 0);
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
			
				
				$tmp = array( "Titulo" => trim($perfil->Titulo),"Fecha" => trim($perfil->Fecha),"Contenido" => trim($perfil->Contenido), "TalentoPersonaje" => trim($perfil->TalentoPersonaje),
				 "Foto" => trim($perfil->Foto) ,  "Check" => trim($perfil->CheckFM), "LeyendaFoto" => trim($perfil->LeyendaFoto), "tituloHP" => trim($perfil->TituloHP) , "idiomas" => trim(""),
				  "medidas" => trim(""),  "usuario" => trim($perfil->UsuarioMod),  "HomePage" => trim($perfil->HomePage), "tituloHPEmpr" => trim($perfil->TituloHPEmpr) , "ContenidoEng" => trim($perfil->ContenidoEng) 
				  , "TituloEng" => trim($perfil->TituloEng)  , "LeyendaFotoEng" => trim($perfil->LeyendaFotoEng) ,  "IDContacts" => trim($perfil->IDContacts) );
				
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				
			}
		
		}	
	}
	
	public function editRecordPerfilRelaciones($PerfilObj){
		
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		//$evento->IdEvento = 1241;
		$findCommand->addFindCriterion($listofFields[5], "==".$PerfilObj->PerfilID);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( !isset($result->code) ){
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				$tmp = array( "IDNoticias" => trim($PerfilObj->IDNoticias), "IDVideos" => trim($PerfilObj->IDVideos) );
								
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				
			}
		
		}	
	}
	
	public function deleteRecordPerfil($perfilID){
	
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
			
		$findCommand->addFindCriterion($listofFields[5], "==".$perfilID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	}
	
	
	/**********************ESPECIALES**********************/
	public function editRecordEspecial($especial){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		//$evento->IdEvento = 1241;
		$findCommand->addFindCriterion($listofFields[0], "==".$especial->IdEspecial);	
		
		$findCom =	$findCommand;
		
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( isset($result->code) && $result->code == "401"){
		
		
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("ID", trim($especial->IdEspecial), 0);
			$newCommand->setField("Fecha", trim($especial->Fecha), 0);
			$newCommand->setField("Titulo", trim($especial->Titulo), 0);
			$newCommand->setField("Seccion", trim($especial->Seccion), 0);
			$newCommand->setField("CiudadPais", trim($especial->CityID), 0);
			$newCommand->setField("Logo", trim($especial->Logo), 0);
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
			
				
				$tmp = array( "Titulo" => trim($especial->Titulo),"Fecha" => trim($especial->Fecha),"Seccion" => trim($especial->Seccion)
				, "CiudadPais" => trim($especial->CityID)  , "Logo" => trim($especial->Logo) );
				
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				
			}
		
		}	
	}
	
	public function deleteRecordEspecial($especialID){
	
		$listofFields = $this->layout_object->listFields();		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
			
		$findCommand->addFindCriterion($listofFields[0], "==".$especialID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			
		}
	}
	
	/**********************FOROS**********************/
	public function editRecordForo($foro){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$foro->IdForo);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( isset($result->code) && $result->code == "401"){
		
		
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("ID", trim($foro->IdForo), 0);
			$newCommand->setField("Titulo", trim($foro->Titulo), 0);
			$newCommand->setField("Fecha", trim($foro->Fecha), 0);
			$newCommand->setField("Seccion", trim($foro->Seccion), 0);
			$newCommand->setField("Descripcion", trim($foro->Descripcion), 0);
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				$tmp = array( "Titulo" => trim($foro->Titulo),"Fecha" => trim($foro->Fecha),"Descripcion" => trim($foro->Descripcion), "Seccion" => trim($foro->Seccion) );
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				
			}
		
		}	
	}
	
	public function deleteRecordForo($foroID){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$foroID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	
	}
	/**********************NOTICIA API FM**********************/
	public function editRecordNoticia($noticia){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticia->HeadlineNumber);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
			
		
		if( isset($result->code) && $result->code == "401"){
				
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("HeadlineNumber", trim($noticia->HeadlineNumber), 0);
			$newCommand->setField("Date", trim($noticia->Date), 0);
			$newCommand->setField("Time", trim($noticia->Time), 0);
			$newCommand->setField("Firma", trim($noticia->Firma), 0);
			$newCommand->setField("Headline", trim($noticia->Headline), 0);
			$newCommand->setField("HeadlineBig", trim($noticia->HeadlineBig), 0);
			//$newCommand->setField("DateHispanicPub", trim($noticia->DateHispanicPub), 0);
			$newCommand->setField("HeadlineHispanic", trim($noticia->HeadlineHispanic), 0);
			$newCommand->setField("DateMedia", trim($noticia->DateMedia), 0);
			$newCommand->setField("HeadlineAdSales", trim($noticia->HeadlineMedia), 0);
			$newCommand->setField("DateHispanicEng", trim($noticia->DateHispanicEng), 0);			
			$newCommand->setField("HeadlineEng", trim($noticia->HeadlineEng), 0);
			$newCommand->setField("Headline1000", trim($noticia->Headline1000), 0);
			$newCommand->setField("Headline1000Pos", trim($noticia->Headline1000Pos), 0);
			$newCommand->setField("LeyendaFoto1000a", trim($noticia->LeyendaFoto1000a), 0);
			$newCommand->setField("LeyendaFoto1000b", trim($noticia->LeyendaFoto1000b), 0);
			$newCommand->setField("MainSection", trim($noticia->MainSection), 0);
			$newCommand->setField("NewsBody", trim($noticia->NewsBody), 0);
			$newCommand->setField("NewsBodyEng", trim($noticia->NewsBodyEng), 0);
			$newCommand->setField("VideoDiario", trim($noticia->VideoDiario), 0);
			$newCommand->setField("VideoDiarioEng", trim($noticia->VideoDiarioEng), 0);
			$newCommand->setField("Foto", trim($noticia->Foto), 0);
			$newCommand->setField("LeyendaFoto", trim($noticia->LeyendaFoto), 0);
			$newCommand->setField("LeyendaFotoEng", trim($noticia->LeyendaFotoEng), 0);
			$newCommand->setField("LeyendaFoto1000a", trim($noticia->LeyendaFoto1000a), 0);
			$newCommand->setField("LeyendaFoto1000b", trim($noticia->LeyendaFoto1000b), 0);			
			//$newCommand->setField("fuente", trim($noticia->FuenteID), 0);			
			$newCommand->setField("accesolibre", trim($noticia->accesolibre), 0);
			$newCommand->setField("EspCrono", trim($noticia->Original), 0);
			$newCommand->setField("aparte", trim($noticia->DiarioSeccion2), 0);	
			$newCommand->setField("MediosMerc", trim($noticia->DiarioSeccion3), 0);
			$newCommand->setField("Hoy", trim($noticia->Hoy), 0);
			$newCommand->setField("DiarioSeccion1a", trim($noticia->DiarioSeccion1a), 0);
			$newCommand->setField("Ratings", trim($noticia->Ratings), 0);
			$newCommand->setField("FeedTV", trim($noticia->Diario), 0);
			$newCommand->setField("Extra", trim($noticia->Extra), 0);			
			$newCommand->setField("PublicidadSolapa1", trim($noticia->PublicidadSolapa1), 0);
			$newCommand->setField("PublicidadSolapa2", trim($noticia->PublicidadSolapa2), 0);
			$newCommand->setField("TituloHPContact", trim($noticia->TituloHPContact), 0);
			$newCommand->setField("TituloHPEmpr", trim($noticia->TituloHPEmpr), 0);			
			$newCommand->setField("FeedPub", trim($noticia->FeedHispanic), 0);
			$newCommand->setField("FeedPubEng", trim($noticia->FeedHispanicEng), 0);
			$newCommand->setField("FeedAdSales", trim($noticia->FeedMedia), 0);
			$newCommand->setField("FeedTec", trim($noticia->FeedTec), 0);
			$newCommand->setField("Paises", trim($noticia->Paises), 0);
			$newCommand->setField("Television", trim($noticia->Television), 0);
			$newCommand->setField("SinVideo", trim($noticia->SinVideo), 0);
			$newCommand->setField("IDVideos", trim($noticia->IDVideos), 0);
			
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				//, "DateHispanicPub" => trim($noticia->DateHispanicPub), "fuente" => trim($noticia->FuenteID)
				$tmp = array("Date" => trim($noticia->Date), "Time" => trim($noticia->Time),"Firma" => trim($noticia->Firma), "Headline" => trim($noticia->Headline)
				, "HeadlineBig" => trim($noticia->HeadlineBig), "HeadlineHispanic" => trim($noticia->HeadlineHispanic) , "DateMedia" => trim($noticia->DateMedia),"HeadlineAdSales" => trim($noticia->HeadlineMedia)
				, "DateHispanicEng"=> trim($noticia->DateHispanicEng), "HeadlineEng" => trim($noticia->HeadlineEng), "Headline1000"=>  trim($noticia->Headline1000),"Headline1000Pos"=>  trim($noticia->Headline1000Pos) ,
				 "Headline1000Pos"=>  trim($noticia->Headline1000Pos),"MainSection" => trim($noticia->MainSection), "NewsBody" =>  trim($noticia->NewsBody) , "NewsBodyEng" =>  trim($noticia->NewsBodyEng), "VideoDiario"=>  trim($noticia->VideoDiario) 
				, "VideoDiarioEng" => trim($noticia->VideoDiarioEng) , "Foto" => trim($noticia->Foto) , "LeyendaFoto" => trim($noticia->LeyendaFoto) , "LeyendaFotoEng" => trim($noticia->LeyendaFotoEng)
				,"LeyendaFoto1000a"=>  trim($noticia->LeyendaFoto1000a) , "LeyendaFoto1000b"=>  trim($noticia->LeyendaFoto1000b)
				, "accesolibre" => trim($noticia->accesolibre), "EspCrono" => trim($noticia->Original), "aparte"=>  trim($noticia->DiarioSeccion2),"MediosMerc"=>  trim($noticia->DiarioSeccion3), "DiarioSeccion1a"=>  trim($noticia->DiarioSeccion1a)
				,"Hoy" => trim($noticia->Hoy),"Ratings"=>  trim($noticia->Ratings),"FeedTV" =>  trim($noticia->Diario), "Extra" => trim($noticia->Extra)
				, "PublicidadSolapa1" => trim($noticia->PublicidadSolapa1), "PublicidadSolapa2" => trim($noticia->PublicidadSolapa2), "TituloHPContact" => trim($noticia->TituloHPContact),"TituloHPEmpr" => trim($noticia->TituloHPEmpr)
				, "FeedPub" =>  trim($noticia->FeedHispanic), "FeedTec" => trim($noticia->FeedTec), "FeedAdSales" => trim($noticia->FeedMedia),"FeedPubEng" =>  trim($noticia->FeedhispanicEng), "Paises" =>  trim($noticia->Paises) , "Television" =>  trim($noticia->Television), "SinVideo" => trim($noticia->SinVideo), "IDVideos" => trim($noticia->IDVideos) );
				
				//echo "<pre>"; print_r($tmp); echo "</pre>";
				//echo "<pre>"; print_r($tmp); echo "</pre>";
				//die("endfilemaker");
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die($records[0]->getRecordId()."endfilemaker".$editResult);
			}
		
		}	
	}
	
	
	public function editRecordNoticiaRelated($noticia){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticia->NoticiaID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if(!isset($result->code)){
		
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				
				//$tmp = array("IDVideos" => trim($noticia->IDVideos), "SinVideo" => trim($noticia->SinVideo),"IDContacts" => trim($noticia->IDContacts), "IDContactsNA" => trim($noticia->IDContactsNA)
				$tmp = array("IDVideos" => trim($noticia->IDVideos),"IDContacts" => trim($noticia->IDContacts), "IDContactsNA" => trim($noticia->IDContactsNA)
				, "IDCompanies" => trim($noticia->IDCompanies), "IDNoticias" => trim($noticia->IDNoticias) , "IDReportajes" => trim($noticia->IDReportajes) );
				
				//echo "<pre>"; print_r($tmp); echo "</pre>";
				//die("endfilemaker");
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die($records[0]->getRecordId()."endfilemaker".$editResult);
			}
		
		}	
	}
	
	
	public function deleteRecordNoticia($noticiaID){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticiaID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	
	}
	
	
	
	public function editRecordNoticiaE($noticia){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticia->ID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		//echo "<pre>"; print_r($noticia); echo "</pre>";
		//die("endfilemaker");
		
		if( isset($result->code) && $result->code == "401"){
				
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("ID", trim($noticia->ID), 0);
			$newCommand->setField("Headline", trim($noticia->Headline), 0);
			$newCommand->setField("Body", trim($noticia->Body), 0);
			$newCommand->setField("Foto", trim($noticia->Foto), 0);
			$newCommand->setField("Leyenda", trim($noticia->Leyenda), 0);
			$newCommand->setField("Date", trim($noticia->Date), 0);
			$newCommand->setField("Time", trim($noticia->Time), 0);
			
			$newCommand->setField("Usuario", trim($noticia->Usuario), 0);
			$newCommand->setField("Extra", trim($noticia->Extra), 0);
			$newCommand->setField("Firma", trim($noticia->Firma), 0);
			$newCommand->setField("HeadlineBig", trim($noticia->HeadlineBig), 0);	
			$newCommand->setField("VideoDiario", trim($noticia->VideoDiario), 0);	
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
			
				$tmp = array("Headline" => trim($noticia->Headline), "Body" => trim($noticia->Body),"Foto" => trim($noticia->Foto), "Leyenda" => trim($noticia->Leyenda)
				, "Date" => trim($noticia->Date), "Time" => trim($noticia->Time) , "Usuario" => trim($noticia->Usuario),"Extra" => trim($noticia->Extra)
				, "Firma"=> trim($noticia->Firma), "HeadlineBig" => trim($noticia->HeadlineBig),"VideoDiario" => trim($noticia->VideoDiario));
				//echo "<pre>"; print_r($tmp); echo "</pre>";
				//die("endfilemaker");
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die($records[0]->getRecordId()."endfilemaker".$editResult);
			}
		
		}	
	}	
	
	public function editRecordNoticiaERelated($noticia){
		
		$listofFields = $this->layout_object->listFields();		
		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticia->NoticiaID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
			
		
		if(!isset($result->code)){
		
		
			$records = $result->getRecords();
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				
				$tmp = array("IDVideos" => trim($noticia->IDVideos),"IDContacts" => trim($noticia->IDContacts)
				,"IDNoticias" => trim($noticia->IDNoticias) );
				
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die($records[0]->getRecordId()."endfilemaker".$editResult);
			}
		
		}	
	}
	
	
	
	public function deleteRecordNoticiaE($noticiaID){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[0], "==".$noticiaID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	
	}
	/**********************SUSCRI API FM**********************/
	public function editRecordSuscripcion($suscri){
	
		
		$listofFields = $this->layout_object->listFields();		
			
		//print_r($listofFields);
		//die("endfm");
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[7], "==".$suscri->ID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( isset($result->code) && $result->code == "401"){
				
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("ID", trim($suscri->ID), 0);
			$newCommand->setField("corporateID", trim($suscri->corporateID), 0);
			$newCommand->setField("fecha", trim($suscri->fecha), 0);
			$newCommand->setField("fechaCobroReal", trim($suscri->FechaUltimaCobranza), 0);
			$newCommand->setField("fechaTope", trim($suscri->fechaTope), 0);
			$newCommand->setField("fechaCobro", trim($suscri->fechaInicioCiclo), 0);
			$newCommand->setField("username", trim($suscri->username), 0);
			//$newCommand->setField("DateHispanicPub", trim($noticia->DateHispanicPub), 0);
			$newCommand->setField("password", trim($suscri->password), 0);
			$newCommand->setField("emailEnvio", trim($suscri->emailEnvio), 0);
			$newCommand->setField("plan", trim($suscri->plan), 0);
			$newCommand->setField("PrecioNiveles", trim($suscri->precioNiveles), 0);			
			$newCommand->setField("VIP", trim($suscri->VIP), 0);
			$newCommand->setField("ContactID", trim($suscri->contactID), 0);
			$newCommand->setField("promo", trim($suscri->promo), 0);
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();			
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				//, "DateHispanicPub" => trim($noticia->DateHispanicPub), "fuente" => trim($noticia->FuenteID)
				$tmp = array("fecha" => trim($suscri->fecha), "corporateID" => trim($suscri->corporateID),  "ContactID" => trim($suscri->contactID), "fechaCobroReal" => trim($suscri->FechaUltimaCobranza)
				, "fechaTope" => trim($suscri->fechaTope), "fechaCobro" => trim($suscri->fechaInicioCiclo) , "username" => trim($suscri->username),"password" => trim($suscri->password)
				, "emailEnvio"=> trim($suscri->emailEnvio), "plan" => trim($suscri->plan), "PrecioNiveles"=>  trim($suscri->precioNiveles),"VIP"=>  trim($suscri->VIP) ,
				 "promo"=>  trim($suscri->promo) );
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
				//die($records[0]->getRecordId()."endfilemaker".$editResult);
			}
		
		}	
	}
	
	public function deleteRecorSuscripcion($suscriID){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[7], "==".$suscriID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	
	}
	/**********************SUSCRI API FM**********************/
	public function editRecordCompanyFM($company){
		$listofFields = $this->layout_object->listFields();		
			
			
			
		print_r($company);
		die("endfm");
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[7], "==".$company->IdCompanyFM);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		if( isset($result->code) && $result->code == "401"){				
			$newCommand = $this->fm->newAddCommand($this->layoutName);
			$newCommand->setField("IdCompanyFM", trim($company->IdCompanyFM), 0);
			$newCommand->setField("Logo", trim($company->Logo), 0);
			
			$result = $newCommand->execute();
		}else{
		
			$records = $result->getRecords();			
			$currentRecord = $this->fm->getRecordById($this->layoutName, $records[0]->getRecordId()); 
			
			if(isset($currentRecord) ){
				
				$tmp = array("Logo"=>  trim($company->Logo) );
				
				$editComd = $this->fm->newEditCommand($this->layoutName, $records[0]->getRecordId(), $tmp);
				$editResult = $editComd->execute();
			}
		
		}	
	}
	
	public function deleteRecorSuscripcion($suscriID){
		
		$listofFields = $this->layout_object->listFields();		
		
		$findCommand = $this->fm->newFindCommand($this->layoutName);
		$findCommand->addFindCriterion($listofFields[7], "==".$suscriID);	
		$findCom =	$findCommand;
		$findCom->setLogicalOperator('and');
		$result = $findCom->execute();
		
		
		if( !isset($result->code) && $result->code != "401"){
			$records = $result->getRecords();
		
			$deleteCommand = $this->fm->newDeleteCommand($this->layoutName, $records[0]->getRecordId());
			$result = $deleteCommand->execute();
			echo "test".$result;
		}
	
	}
	
}
?>
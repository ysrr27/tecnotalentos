<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"

"http://www.w3.org/TR/html4/loose.dtd">

<?php

/**
	* FileMaker PHP Site Assistant Generated File
	*/
	global $errormessage;

?>



<html>

<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8">

    <title>Error</title>

    <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css" />

</head>

<body>
	<div id="container">
		<!-- HEADER -->
		<div id="header"><h1>Videos_</h1></div>
		<!-- PAGE BODY -->
		
		<?php $activelink = 'errorpage.php'; include_once 'navigation.php' ?>

		<div id="content">
			<h1>Error</h1>

			<h2><?php echo $errormessage ?></h2>
		</div>
	</div>

</body>

</html>


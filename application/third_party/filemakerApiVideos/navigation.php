

<?php
/**
	* FileMaker PHP Site Assistant Generated File
	*/

	$navURLs = array('Log Out' => '<a href="logout.php?">Log Out</a>',

	'Log In' => '<a href="authentication.php?-db=Videos_&amp;-lay=Form&amp;">Log In</a>',

	'Home' => '<a href="home.php?">Home</a>',

	'Add Record' => '<a href="addrecord.php?-db=Videos_&amp;-lay=Form&amp;">Add Record</a>',

	'Find' => '<a href="findrecords.php?-db=Videos_&amp;-lay=Form&amp;">Find</a>',

	'Record List' => '<a href="recordlist.php?-skip=0&amp;-db=Videos_&amp;-max=25&amp;-lay=Form&amp;">Record List</a>',

	'Find All' => '<a href="recordlist.php?-action=findall&amp;-skip=0&amp;-db=Videos_&amp;-max=25&amp;-lay=Form&amp;">Find All</a>',

	'Summary Report' => '<a href="report.php?-sortfieldone=Titulo&amp;-sortorderone=ascend&amp;-skip=0&amp;-db=Videos_&amp;-max=25&amp;-lay=Form&amp;">Summary Report</a>'

);


?>
<div id="page_nav" >
    <ul>
        <?php foreach ($navURLs as $name => $link) {
        	$loginlink = ($name == 'Log In' && array_key_exists('userName',  $_SESSION));
        	$logoutlink = ($name == 'Log Out' && !array_key_exists('userName',  $_SESSION));
        	
        	if(!$loginlink && !$logoutlink) {
	        	if (isset($activelink) && strpos($link, $activelink) !== false) { ?>
		        		<li class="nav_link" id="activelink"><?php echo $link; ?></li>
		        <?php } else { ?>
		        		<li class="nav_link"><?php echo $link; ?></li>
		        <?php } // else
	        } // if
        }// foreach link ?>
    </ul>
</div>
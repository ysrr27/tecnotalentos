<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */

    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    $databaseName = 'Videos_';
    $layoutName = 'php';
    $userName = $cgi->get('userName');
    $passWord = $cgi->get('passWord');

    $fm = & new FileMaker();
    $fm->setProperty('database', $databaseName);
    $fm->setProperty('username', $userName);
    $fm->setProperty('password', $passWord);
    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M %P';
    $submitDateOrder = 'mdy';
    $record = NULL;
    $findCom = NULL;

    //  handle the action cgi
    
  
    $action = $cgi->get('-action');
    switch ($action) {
        case "findall": {
           $cgi->clear('skip');
           $findCom = & $fm->newFindAllCommand($layoutName);
           break;
        }
        case "find": {
        // clear the recid
           $cgi->clear('recid');

        // create a find command
           $findCommand = $fm->newFindCommand($layoutName);
           ExitOnError($findCommand);
           

        // get the posted record data from the findrecords page
           $findrequestdata = $cgi->get('storedfindrequest');
           
    		/*echo "<pre>";
    			print_r($findrequestdata);
	    	echo "</pre>";*/ 
    
           if (isset($findrequestdata)) {
               $findCom = prepareFindRequest($findrequestdata, $findCommand, $cgi);

            // set the logical operator
               $logicalOperator = $cgi->get('-lop');
        		//die( $logicalOperator );               
               if (isset($logicalOperator)) {
               		$findCom->setLogicalOperator($logicalOperator);
               }
           } else
               $findCom = $fm->newFindAllCommand($layoutName);
           break;
        }
        default: {
           $findCom = & $fm->newFindAllCommand($layoutName);
           break;
        }
    }
    ExitOnError($findCom);

    // read and set, or clear the sort criteria
    $sortfield = $cgi->get('-sortfieldone');
   
    if (isset($sortfield)) {
        addSortCriteria($findCom);
    } else {
        clearSortCriteria($findCom);
    }

    // get the skip and max values
    $skip = $cgi->get('-skip');
    if (isset($skip) === false) {
        $skip = 0;
    }
    $max = $cgi->get('-max');
    if (isset($max) === false) {
        $max = 25;
    }

    // set skip and max values
    $findCom->setRange($skip, $max);

    // perform the find
    $result = $findCom->execute();
    ExitOnError($result);
    
    clearSortCriteria($findCom);
    
    // get status info; page range, found count, total count, first, prev, next, and last links
    $statusLinks = getStatusLinks("recordlist.php", $result, $skip, $max);

    // get the records
    $records = $result->getRecords();
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Record List
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
    </head>
    <body>
        <div id="container">
            <!-- HEADER -->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!-- Navigation Menu -->
            <?php $activelink = 'recordlist.php'; include_once 'navigation.php' ?><!-- PAGE BODY -->
            <div id="content">
                <h1>
                    Record List
                </h1>
                <!--  Display record list page navigation controls -->
                <div class="recordlist_nav">
                    <span class="recordlist_nav_first"><?php echo $statusLinks['first'] ?></span> <span class="recordlist_nav_prev"><?php echo $statusLinks['prev'] ?></span> <span
                    class="recordlist_nav_range">Record <?php echo $statusLinks['records']['rangestart'] ?> - <?php echo $statusLinks['records']['rangeend'] ?> of
                    <?php echo $statusLinks['records']['foundcount'] ?></span> <span class="recordlist_nav_next"><?php echo $statusLinks['next'] ?></span> <span class=
                    "recordlist_nav_last"><?php echo $statusLinks['last'] ?></span>
                </div>
                <table class="browse_records">
                    <thead>
                        <tr>
                            <th class="browse_header">
                                <a href="recordlist.php?-skip=0&amp;-db=Videos_&amp;-max=25&amp;-lay=Form&amp;">Restore Original Sort Order</a>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Titulo')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Fecha')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::Tipo')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::Titulo')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::Link')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::Ficha')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::RID')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::Fecha')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Archivos::FotitoEstrenos')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Banner')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Vide#')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('ID')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Texto')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Valida Copy')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Region')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Usuario')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Portada')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Foto')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('English')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Orden')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Grande')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('OrdenVistos')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('OrdenCate')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Categoria')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('MasVistos')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('PortadaCategoria')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('PortadaProduTV')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('FotoEstrenos')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('OrdenSec')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('TituloHP2')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('MasInfo')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('EncEspecial')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Inte')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('TituloHP')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Today')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('CategoriaCalc')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('CategoriaCalc2')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('Extra')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('HeadlineIDCountry')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('HeadlineIDContact')?>
                            </th>
                            <th class="browse_header">
                                <?php echo getSortRecordsLink('HeadlineIDCompany')?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                                $recnum = 1;
                                                foreach ($records as $fmrecord) {
                                                        $record = new RecordHighlighter($fmrecord, $cgi);

                                                        $rowclass = ($recnum % 2 == 0) ? "table_row" : "alt_row";
                                                        $recid = $record->getRecordId();
                                                        $pos = strpos($recid, "RID_!");
                                                        if ($pos !== false) {
                                                             $recid = substr($recid,0,5) . urlencode(substr($recid,strlen("RID_!")));
                                                        }
                                                ?>
                        <tr class="<?php echo $rowclass ?>">
                            <td class="browse_cell center">
                                <a href='<?php echo "browserecord.php?-action=browse&-recid=$recid";?>'><?php echo $recnum ?></a>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Titulo', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo displayDate( $record->getField('Fecha', 0), $displayDateFormat)?>
                            </td>
                            <?php
                                                    $relatedRecords = $record->getRelatedSet("Archivos");
                                                    $portal = $layout->getRelatedSet("Archivos");
                                                    if (FileMaker::isError($relatedRecords) === false) {
                                                        $master_record = $record;
                                                        $record = $relatedRecords[0]; ?>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Archivos::Tipo', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Archivos::Titulo', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Archivos::Link', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Archivos::Ficha', 0))?>
                            </td>
                            <td class="browse_cell right">
                                <?php echo nl2br( $record->getField('Archivos::RID', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo displayDate( $record->getField('Archivos::Fecha', 0), $displayDateFormat)?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Archivos::FotitoEstrenos', 0))?>
                            </td>
                            <?php
                                                    $record = $master_record;
                                                } else { ?>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <td class="browse_cell">
                            </td>
                            <?php
                                                } ?>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Banner', 0))?>
                            </td>
                            <td class="browse_cell right">
                                <?php echo nl2br( $record->getField('Vide#', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('ID', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Texto', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Valida Copy', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Region', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Usuario', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Portada', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Foto', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('English', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Orden', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Grande', 0))?>
                            </td>
                            <td class="browse_cell right">
                                <?php echo nl2br( $record->getField('OrdenVistos', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('OrdenCate', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Categoria', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('MasVistos', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('PortadaCategoria', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('PortadaProduTV', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('FotoEstrenos', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('OrdenSec', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('TituloHP2', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('MasInfo', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('EncEspecial', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Inte', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('TituloHP', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo displayDate( $record->getField('Today', 0), $displayDateFormat)?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('CategoriaCalc', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('CategoriaCalc2', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('Extra', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('HeadlineIDCountry', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('HeadlineIDContact', 0))?>
                            </td>
                            <td class="browse_cell left">
                                <?php echo nl2br( $record->getField('HeadlineIDCompany', 0))?>
                            </td>
                        </tr>
                        <?php $recnum++; } /* foreach record */?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>


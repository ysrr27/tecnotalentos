<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */

    //  clear the session on each return to the logout page
    require_once "fmview.php";
    $cgi = new CGI();
    $cgi->reset();
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
        <title>
            Log Out
        </title>
    </head>
    <body>
        <div id="container">
            <!-- HEADER -->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!-- PAGE BODY -->
            <?php $activelink = 'logout.php'; include_once 'navigation.php' ?>
            <div id="content">
                <h2>
                    You have successfully logged out.
                </h2>
            </div>
        </div>
    </body>
</html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */

    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    $databaseName = 'Videos_';
    $layoutName = 'php';
    $userName = $cgi->get('userName');
    $passWord = $cgi->get('passWord');

    $fm = & new FileMaker();
    $fm->setProperty('database', $databaseName);
    $fm->setProperty('username', $userName);
    $fm->setProperty('password', $passWord);
    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M %P';
    $submitDateOrder = 'mdy';

    $record = NULL;
    $response = NULL;
    $action = $cgi->get('-action');
    $recid = $cgi->get('-recid');

    switch ($action) {
        case "delete" :
            {
                include "deleterecord.php";
                break;
            }
        case "duplicate" :
            {

                //  create a new duplicate command
                $duplicateCommand = $fm->newDuplicateCommand($layoutName, $recid);
                $result = $duplicateCommand->execute();
                ExitOnError($result);

                //  the result contains the record
                $records = $result->getRecords();
                $record = $records[0];

                //  store the recid so we can display the duplicated record in the browse page
                $cgi->store('-recid', $record->getRecordId());
                $recid = $cgi->get('-recid');
                break;
            }
        case "edit" :
            {
                $recid = $cgi->get('-recid');

                // create an edit command for the record specified by the '-recid'
                $editCommand = $fm->newEditCommand($layoutName, $recid);
                ExitOnError($editCommand);

                // get the posted record data from the edit page
                $recorddata = $cgi->get('recorddata');
                if (isset ($recorddata)) {
                    $response = submitRecordData($recorddata, $editCommand, $cgi, $layout->listFields());
                    $cgi->clear('recorddata');
                    ExitOnError($response);
                    
                    $firstRecord = $response->getFirstRecord();
                    ExitOnError($firstRecord);
                    $cgi->store('-recid', $firstRecord->getRecordId());
                    $recid = $cgi->get('-recid');
            }

            // now get the record we just edited
    		$record = $fm->getRecordById($layoutName, $recid);
            break;
        }
        case "new" :
            {

                // create the new add command
                $newrecordrequest = $fm->newAddCommand($layoutName);
                ExitOnError($newrecordrequest);

                // get the submitted record data
                $recorddata = $cgi->get('recorddata');
                if (isset ($recorddata)) {

                    //  submit the data to the db
                   
                    $result = submitRecordData($recorddata, $newrecordrequest, $cgi, $layout->listFields());

                    //  clear the stored record data
                    $cgi->clear('recorddata');
                    ExitOnError($result);
                } else {
                    $findallCommand = $fm->newFindAllCommand($layoutName);
                    $result = $findallCommand->execute();
                }
                if ($result->getFetchCount() > 0) {
                    $records = $result->getRecords();
                    $record = $records[0];
                    $recid = $record->getRecordId();
                } else {
                    DisplayError('No Records are present.');
                }

                break;
            }
        case "browse" :
        default :
            {
                $recid = $cgi->get('-recid');
                if (!isset ($recid))
                    $recid = 1;
                $record = $fm->getRecordById($layoutName, $recid);
                ExitOnError($record);
                break;
            }
    }
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Browse Record
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
    </head>
    <body>
        <div id="container">
            <!--HEADER-->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!--Navigation Menu-->
            <?php $activelink = 'browserecord.php'; include_once 'navigation.php' ?><!--PAGE BODY-->
            <div id="content">
                <h1>
                    Browse Record
                </h1>
                <form method="post" action="editrecord.php">
                    <input type="hidden" name="-db" value="<?php echo $databaseName ?>"> <input type="hidden" name="-lay" value="<?php echo $layoutName ?>"> <input type="hidden"
                    name="-recid" value="<?php echo $recid ?>"> 
                    <table class="record">
                        <!-- Display record field values -->
                        <tr class="field">
                            <td class="field_name">
                                Titulo
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Titulo', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Fecha
                            </td>
                            <td class="field_data">
                                <?php echo displayDate( $record->getField('Fecha', 0), $displayDateFormat)?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Archivos
                            </td>
                            <td class="field_data">
                                <table class="portal">
                                    <thead>
                                        <tr>
                                            <th class="portal_header">
                                                Archivos::Tipo
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Titulo
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Link
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Ficha
                                            </th>
                                            <th class="portal_header">
                                                Archivos::RID
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Fecha
                                            </th>
                                            <th class="portal_header">
                                                Archivos::FotitoEstrenos
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                $relatedRecords = $record->getRelatedSet("Archivos");
                                                $portal = $layout->getRelatedSet("Archivos");
                                                if (FileMaker::isError($relatedRecords) === false) {
                                                    $recnum = 0;
                                                    $master_record = $record;
                                                    foreach ($relatedRecords as $record) {
                                                            $rowclass = ($recnum % 2 == 0) ? "table_row" : "alt_row";
                                                            $recnum++; ?>
                                        <tr class="<?php echo $rowclass ?>">
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::Tipo', 0))?>
                                            </td>
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::Titulo', 0))?>
                                            </td>
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::Link', 0))?>
                                            </td>
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::Ficha', 0))?>
                                            </td>
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::RID', 0))?>
                                            </td>
                                            <td>
                                                <?php echo displayDate( $record->getField('Archivos::Fecha', 0), $displayDateFormat)?>
                                            </td>
                                            <td>
                                                <?php echo nl2br( $record->getField('Archivos::FotitoEstrenos', 0))?>
                                            </td>
                                        </tr>
                                        <?php }
                                                    $record = $master_record;
                                                } ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Banner
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Banner', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Vide#
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Vide#', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                ID
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('ID', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Texto
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Texto', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Valida Copy
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Valida Copy', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Region
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Region', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Usuario
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Usuario', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Portada
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Portada', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Foto
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Foto', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                English
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('English', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Orden
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Orden', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Grande
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Grande', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenVistos
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('OrdenVistos', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenCate
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('OrdenCate', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Categoria
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Categoria', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                MasVistos
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('MasVistos', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                PortadaCategoria
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('PortadaCategoria', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                PortadaProduTV
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('PortadaProduTV', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                FotoEstrenos
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('FotoEstrenos', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenSec
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('OrdenSec', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                TituloHP2
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('TituloHP2', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                MasInfo
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('MasInfo', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                EncEspecial
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('EncEspecial', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Inte
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Inte', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                TituloHP
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('TituloHP', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Today
                            </td>
                            <td class="field_data">
                                <?php echo displayDate( $record->getField('Today', 0), $displayDateFormat)?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                CategoriaCalc
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('CategoriaCalc', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                CategoriaCalc2
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('CategoriaCalc2', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Extra
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('Extra', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDCountry
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('HeadlineIDCountry', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDContact
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('HeadlineIDContact', 0))?>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDCompany
                            </td>
                            <td class="field_data">
                                <?php echo nl2br( $record->getField('HeadlineIDCompany', 0))?>
                            </td>
                        </tr>
                        <!--Display record form controls-->
                        <tr class="submit_btn">
                            <td colspan="2">
                                <input type="submit" name="-find" value="Edit Record"> 
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>


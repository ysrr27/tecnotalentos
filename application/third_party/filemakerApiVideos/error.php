<?php

/**
	* FileMaker PHP Site Assistant Generated File
	*/

	 require_once 'FileMaker.php';

	 function ExitOnError($result) {

		$errorMessage = NULL;

		if (FileMaker :: isError($result)) {

			$errorCode = $result->getCode();

			$errorMessage = "<p>Error: " . $errorCode . " - " . $result->getErrorString() . "<br></p>";			
			if ((is_null($errorCode) === false && (($errorCode >= 200 && $errorCode <= 213)) || $errorCode == 22) || is_null($errorCode)) {

				Authenticate($errorMessage);

			} else {

				DisplayError($errorMessage);

			}
					exit;

		} else if ($result === NULL) {

			$errorMessage = "<p>Error: Error result is NULL!</p>";

			DisplayError($errorMessage);

			exit;

		}

	}

	function DisplayError($message) {

		global $errormessage;

		$errormessage = $message . "<br>";

		include "errorpage.php";

	}

	function Authenticate($message) {
		global $errormessage;

		$errormessage = $message . "<br>";

		include "authentication.php";

	}
?>
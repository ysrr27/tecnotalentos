<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */

    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    $databaseName = 'Videos_';
    $layoutName = 'php';
    $userName = $cgi->get('userName');
    $passWord = $cgi->get('passWord');

    $fm = & new FileMaker();
    $fm->setProperty('database', $databaseName);
    $fm->setProperty('username', $userName);
    $fm->setProperty('password', $passWord);
    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M %P';
    $submitDateOrder = 'mdy';

    class EmptyRecord {
        function getRelatedSet($relationName) {
            return array(new EmptyRecord());
        }

        function getField($field, $repetition = 0) {
        }

        function getRecordId() {
        }
    }

    $record = new EmptyRecord();
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Find
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
    </head>
    <body>
        <div id="container">
            <!-- HEADER -->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!-- Navigation Menu -->
            <?php $activelink = 'findrecords.php'; include_once 'navigation.php' ?><!-- PAGE BODY -->
            <div id="content">
                <h1>
                    Find Records
                </h1>
                <form method="post" action="recordlist.php">
                    <input type="hidden" name="-lay" value="<?php echo '$layoutName'?>"> <input type="hidden" name="-action" value="find"> 
                    <table class="record">
                        <!--Display search field input controls-->
                        <tr class="field">
                            <td class="field_name">
                                Titulo
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Titulo' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Titulo', 0, $record);?>" value=
                                "<?php echo $record->getField('Titulo', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Fecha
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Fecha' . '.op', 0, $record);?>">
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Fecha', 0, $record);?>" value=
                                "<?php echo submitDate( $record->getField('Fecha', 0), $submitDateOrder)  ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Archivos
                            </td>
                            <td class="field_data">
                                <table class="portal">
                                    <thead>
                                        <tr>
                                            <th class="portal_header">
                                                Archivos::Tipo
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Titulo
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Link
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Ficha
                                            </th>
                                            <th class="portal_header">
                                                Archivos::RID
                                            </th>
                                            <th class="portal_header">
                                                Archivos::Fecha
                                            </th>
                                            <th class="portal_header">
                                                Archivos::FotitoEstrenos
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                                    $relatedRecords = $record->getRelatedSet("Archivos");
                                                    $portal = $layout->getRelatedSet("Archivos");
                                                    if (FileMaker::isError($relatedRecords) === false) {
                                                        $recnum = 0;
                                                        $master_record = $record;
                                                        foreach ($relatedRecords as $record) {
                                                                $rowclass = ($recnum % 2 == 0) ? "table_row" : "alt_row";
                                                                $recnum++; ?>
                                        <tr class="<?php echo $rowclass ?>">
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::Tipo' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <select name="<?php echo getFieldFormName('Archivos::Tipo', 0, $record);?>">
                                                    <option value="">
                                                        Archivos::Tipo
                                                    </option>
                                                    <?php getMenu($layout->getValueList('Tipo', $record->getRecordId()), $record->getField('Archivos::Tipo', 0) , 'Archivos::Tipo', getFieldFormName('Archivos::Tipo', 0, $record));?>
                                                </select> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::Titulo' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::Titulo', 0, $record);?>" value=
                                                "<?php echo $record->getField('Archivos::Titulo', 0)   ;?>"> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::Link' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::Link', 0, $record);?>" value=
                                                "<?php echo $record->getField('Archivos::Link', 0)   ;?>"> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::Ficha' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::Ficha', 0, $record);?>" value=
                                                "<?php echo $record->getField('Archivos::Ficha', 0) ;?>"> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::RID' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::RID', 0, $record);?>" value=
                                                "<?php echo $record->getField('Archivos::RID', 0) ;?>"> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::Fecha' . '.op', 0, $record);?>">
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::Fecha', 0, $record);?>" value=
                                                "<?php echo submitDate( $record->getField('Archivos::Fecha', 0), $submitDateOrder)  ;?>"> 
                                            </td>
                                            <td>
                                                <select name="<?php echo getFieldFormName('Archivos::FotitoEstrenos' . '.op', 0, $record);?>">
                                                    <option value="cn" selected>
                                                        Contains
                                                    </option>
                                                    <option value="bw">
                                                        Begins With
                                                    </option>
                                                    <option value="ew">
                                                        Ends With
                                                    </option>
                                                    <option value="eq">
                                                        Equals
                                                    </option>
                                                    <option value="lt">
                                                        Less Than
                                                    </option>
                                                    <option value="lte">
                                                        Less Than or Equals
                                                    </option>
                                                    <option value="gt">
                                                        Greater Than
                                                    </option>
                                                    <option value="gte">
                                                        Greater Than or Equals
                                                    </option>
                                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Archivos::FotitoEstrenos', 0, $record);?>" value=
                                                "<?php echo $record->getField('Archivos::FotitoEstrenos', 0)   ;?>"> 
                                            </td>
                                        </tr>
                                        <?php }
                                                        $record = $master_record;
                                                    } ?>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Banner
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Banner' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('No', $record->getRecordId()), $record->getField('Banner', 0)   , getFieldFormName('Banner', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Vide#
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Vide#' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <?php echo getFieldFormName('Vide#', 0, $record);?><input type="text" size="30" name="<?php echo getFieldFormName('Vide#', 0, $record);?>" value=
                                "<?php echo $record->getField('Vide#', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                ID
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('ID' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('ID', 0, $record);?>" value="<?php echo $record->getField('ID', 0)   ;?>">
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Texto
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Texto' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Texto', 0, $record);?>" value=
                                "<?php echo $record->getField('Texto', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Valida Copy
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Valida Copy' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Valida Copy', 0, $record);?>" value=
                                "<?php echo $record->getField('Valida Copy', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Region
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Region' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Region', 0, $record);?>" value=
                                "<?php echo $record->getField('Region', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Usuario
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Usuario' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Usuario', 0, $record);?>" value=
                                "<?php echo $record->getField('Usuario', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Portada
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Portada' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('Portada', 0)   , getFieldFormName('Portada', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Foto
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Foto' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Foto', 0, $record);?>" value=
                                "<?php echo $record->getField('Foto', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                English
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('English' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('English', 0, $record);?>" value=
                                "<?php echo $record->getField('English', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Orden
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Orden' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Orden', 0, $record);?>" value=
                                "<?php echo $record->getField('Orden', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Grande
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Grande' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('Grande', 0)    , getFieldFormName('Grande', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenVistos
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('OrdenVistos' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('OrdenVistos', 0, $record);?>" value=
                                "<?php echo $record->getField('OrdenVistos', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenCate
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('OrdenCate' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('OrdenCate', 0, $record);?>" value=
                                "<?php echo $record->getField('OrdenCate', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Categoria
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Categoria' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <select name="<?php echo getFieldFormName('Categoria', 0, $record);?>">
                                    <option value="">
                                        Categoria
                                    </option>
                                    <?php getMenu($layout->getValueList('NuevaCat', $record->getRecordId()), $record->getField('Categoria', 0)  , 'Categoria', getFieldFormName('Categoria', 0, $record));?>
                                </select> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                MasVistos
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('MasVistos' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('MasVistos', 0, $record);?>" value=
                                "<?php echo $record->getField('MasVistos', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                PortadaCategoria
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('PortadaCategoria' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <select name="<?php echo getFieldFormName('PortadaCategoria', 0, $record);?>">
                                    <option value="">
                                        PortadaCategoria
                                    </option>
                                    <?php getMenu($layout->getValueList('Section', $record->getRecordId()), $record->getField('PortadaCategoria', 0)    , 'PortadaCategoria', getFieldFormName('PortadaCategoria', 0, $record));?>
                                </select> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                PortadaProduTV
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('PortadaProduTV' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('PortadaProduTV', 0)    , getFieldFormName('PortadaProduTV', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                FotoEstrenos
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('FotoEstrenos' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('FotoEstrenos', 0, $record);?>" value=
                                "<?php echo $record->getField('FotoEstrenos', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                OrdenSec
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('OrdenSec' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('OrdenSec', 0, $record);?>" value=
                                "<?php echo $record->getField('OrdenSec', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                TituloHP2
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('TituloHP2' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('TituloHP2', 0, $record);?>" value=
                                "<?php echo $record->getField('TituloHP2', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                MasInfo
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('MasInfo' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('MasInfo', 0, $record);?>" value=
                                "<?php echo $record->getField('MasInfo', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                EncEspecial
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('EncEspecial' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('EncEspecial', 0)   , getFieldFormName('EncEspecial', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Inte
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Inte' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('Inte', 0)  , getFieldFormName('Inte', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                TituloHP
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('TituloHP' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('TituloHP', 0, $record);?>" value=
                                "<?php echo $record->getField('TituloHP', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Today
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Today' . '.op', 0, $record);?>">
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('Today', 0, $record);?>" value=
                                "<?php echo submitDate( $record->getField('Today', 0), $submitDateOrder)  ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                CategoriaCalc
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('CategoriaCalc' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('CategoriaCalc', 0, $record);?>" value=
                                "<?php echo $record->getField('CategoriaCalc', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                CategoriaCalc2
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('CategoriaCalc2' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('CategoriaCalc2', 0, $record);?>" value=
                                "<?php echo $record->getField('CategoriaCalc2', 0)   ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Extra
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('Extra' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select>
                                <?php getInputChoices("checkbox", $layout->getValueList('Check', $record->getRecordId()), $record->getField('Extra', 0) , getFieldFormName('Extra', 0, $record));?>
                                
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDCountry
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('HeadlineIDCountry' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('HeadlineIDCountry', 0, $record);?>" value=
                                "<?php echo $record->getField('HeadlineIDCountry', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDContact
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('HeadlineIDContact' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('HeadlineIDContact', 0, $record);?>" value=
                                "<?php echo $record->getField('HeadlineIDContact', 0) ;?>"> 
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                HeadlineIDCompany
                            </td>
                            <td class="field_data">
                                <select name="<?php echo getFieldFormName('HeadlineIDCompany' . '.op', 0, $record);?>">
                                    <option value="cn" selected>
                                        Contains
                                    </option>
                                    <option value="bw">
                                        Begins With
                                    </option>
                                    <option value="ew">
                                        Ends With
                                    </option>
                                    <option value="eq">
                                        Equals
                                    </option>
                                    <option value="lt">
                                        Less Than
                                    </option>
                                    <option value="lte">
                                        Less Than or Equals
                                    </option>
                                    <option value="gt">
                                        Greater Than
                                    </option>
                                    <option value="gte">
                                        Greater Than or Equals
                                    </option>
                                </select> <input type="text" size="30" name="<?php echo getFieldFormName('HeadlineIDCompany', 0, $record);?>" value=
                                "<?php echo $record->getField('HeadlineIDCompany', 0) ;?>"> 
                            </td>
                        </tr>
                        <!--Display search form controls-->
                        <tr class="submit_btn">
                            <td colspan="2">
                                <input value="or" type="radio" name="-lop">Find records if any fields match<br>
                                 <input value="and" type="radio" name="-lop" checked>Find records if all fields match<br>
                                 <br>
                                 Display: <select name="-max">
                                    <option value="10">
                                        10
                                    </option>
                                    <option value="25" selected>
                                        25
                                    </option>
                                    <option value="50">
                                        50
                                    </option>
                                    <option value="100">
                                        100
                                    </option>
                                    <option value="all">
                                        Max
                                    </option>
                                </select> Records<br>
                                 <br>
                                 <input type="submit" name="-find" value="Find Records"> <input type="reset" name="Reset" value="Reset"> 
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>


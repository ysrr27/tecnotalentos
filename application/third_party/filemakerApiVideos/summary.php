<?php
/**
	* FileMaker PHP Site Assistant Generated File
	*/

require_once 'fmview.php';
require_once 'FileMaker.php';

//	Summary Report
interface Summary {
	public function update(FileMaker_Record $eachRecord);
	public function getData();
};
abstract class PrintSummary {
	public function getGroupLabel() {
		return "Group Summary";
	}
	public function getGrandLabel() {
		return "Grand Summary";
	}
	public function printGroupSummary($colSpanVal) {
		echo "<tr><td class='group_total_title'>" . $this->getGroupLabel() . ":</td><td class='group_total' colspan=".($colSpanVal-1).">" . $this->getData() . "</td></tr>";
	}
	public function printGrandSummary($colSpanVal) {
		echo "<tr><td class='grand_total_title'>". $this->getGrandLabel() . ":</td><td class='grand_total' colspan=".($colSpanVal-1).">" . $this->getData() . "</td></tr>";
	}
};
class Total extends PrintSummary implements Summary{
	private $_summaryField;
	private $_total;

	public function getGroupLabel() {
		return "Group Total";
	}
	public function getGrandLabel() {
		return "Grand Total";
	}
	function __construct($summaryField) {
		$this->_summaryField = $summaryField;
	}
	public function update(FileMaker_Record $eachRecord) {
		$this->_total += $eachRecord->getField($this->_summaryField);
	}
	public function getData() {
		return $this->_total;
	}
};

class Count extends PrintSummary implements Summary{
	private $_summaryField;
	private $_count;

	public function getGroupLabel() {
		return "Group Count";
	}
	public function getGrandLabel() {
		return "Overall Count";
	}
	function __construct($summaryField) {
		$this->_summaryField = $summaryField;
		$this->_count = 0;
	}
	public function update(FileMaker_Record $eachRecord) {
		$this->_count++;
	}
	public function getData() {
		return $this->_count;
	}
};

class Median extends PrintSummary implements Summary{
	private $_summaryField;
	private $_min;
	private $_max;

	public function getGroupLabel() {
		return "Group Median";
	}
	public function getGrandLabel() {
		return "Overall Median";
	}
	function __construct($summaryField) {
		$this->_summaryField = $summaryField;
	}
	public function update(FileMaker_Record $eachRecord) {
		$currentValue = $eachRecord->getField($this->_summaryField);
		if ($currentValue > $this->_max)
			$this->_max = $currentValue;
		elseif ($currentValue < $this->_min)
			$this->_min = $currentValue;
	}
	public function getData() {
		return ($this->_max - $this->_min) / 2;
	}
};
class Average extends PrintSummary implements Summary{
	private $_summaryField;
	private $_count;
	private $_total;

	public function getGroupLabel() {
		return "Group Average";
	}
	public function getGrandLabel() {
		return "Overall Average";
	}
	function __construct($summaryField) {
		$this->_summaryField = $summaryField;
		$this->_count = 0;
	}
	public function update(FileMaker_Record $eachRecord) {
		$currentValue = $eachRecord->getField($this->_summaryField);
		$this->_total += $currentValue;
		$this->_count ++;
	}
	public function getData() {
		return $this->_total / $this->_count;
	}
};

class Report {
	private $_groupField;
	private $_summaryField;
	private $_summaryFunction;
	private $_numberTableColumns;
	private $_firstGroup = true;

	function makeSummary() {
		switch ($this->_summaryFunction) {
			case 'Total':
				return new Total($this->_summaryField);
				break;

			case 'Average':
				return new Average($this->_summaryField);
				break;

			case 'Median':
				return new Median($this->_summaryField);
				break;

			case 'Count':
				return new Count($this->_summaryField);
				break;
				
			default:
				return new Total($this->_summaryField);
				break;
		}
	}

	function Report($groupField, $summaryField, $summaryFunction, $numberTableColumns) {
		$this->_groupField = $groupField;
		$this->_summaryField = $summaryField;
		$this->_summaryFunction = $summaryFunction;
		$this->_numberTableColumns = $numberTableColumns;
	}

	private function startGroup($groupData, $colspanVal) {
		$this->_firstGroup = false;
		echo "<tr><td class='group_header' colspan=".$colspanVal.">" . $groupData . "</td></tr>";
	}

	private function finishGroup($groupSummary, $colspanVal) {
		if ($this->_firstGroup === false) {
			$groupSummary->printGroupSummary($colspanVal);
		}
	}

	public function printReport($records, $fields, $layout) {
		$lastSeenGroup = null;
		$grandSummary = $this->makeSummary();
		$groupSummary = $this->makeSummary();
		foreach ($records as $eachRecord) {
			$grandSummary->update($eachRecord);
			if ($eachRecord->getField($this->_groupField) != $lastSeenGroup) {
				$lastSeenGroup = $eachRecord->getField($this->_groupField);
				$this->finishGroup($groupSummary,$this->_numberTableColumns);
				$groupSummary = $this->makeSummary();
				$this->startGroup($lastSeenGroup, $this->_numberTableColumns);
			}
			$groupSummary->update($eachRecord);
			echo "<tr class='table_row'>";
			foreach ($fields as $eachFieldName) {
				if ($eachFieldName != $this->_groupField) {
					
					if (isPortalField($eachRecord, $eachFieldName)) {
						$colonPos = strpos($eachFieldName, "::");
						$tableName = substr($eachFieldName, 0, $colonPos);
                        $relatedRecords = $eachRecord->getRelatedSet($tableName);
                        $portal = $layout->getRelatedSet($tableName);
						getTableRows($portal, $relatedRecords, $eachFieldName, false);
                    }
                    else{
                    	formatFieldData($eachRecord, $layout, $eachFieldName);
                    }
				}
				else{
					echo "<td class='browse_cell'></td>";
				}				
			}
			echo "</tr>";
		}
		$this->finishGroup($groupSummary,$this->_numberTableColumns);
		$grandSummary->printGrandSummary($this->_numberTableColumns);
	}
};	// Report
?>

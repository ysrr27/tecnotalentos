<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN""http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */

    require_once 'fmview.php';
    require_once 'FileMaker.php';
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    $databaseName = 'Videos_';
    $layoutName = 'Form';
    $userName = $cgi->get('userName');
    $passWord = $cgi->get('passWord');

    $fm = & new FileMaker();
    $fm->setProperty('database', $databaseName);
    $fm->setProperty('username', $userName);
    $fm->setProperty('password', $passWord);
    
    ExitOnError($fm);
    $layout = $fm->getLayout($layoutName);
    ExitOnError($layout);

    // formats for dates and times
    $displayDateFormat = '%m/%d/%Y';
    $displayTimeFormat = '%I:%M %P';
    $displayDateTimeFormat = '%m/%d/%Y %I:%M %P';
    $submitDateOrder = 'mdy';
    require_once 'summary.php';

    $record = NULL;
    $findCom = NULL;

    // get the report attributes
    $groupField = 'Titulo';
    $summaryFunction = 'Count';
    $summaryField = 'HeadlineIDCompany';

    // handle the cgi
    $action = $cgi->get('-action');
    switch ($action) {
        case "findall": {
           $cgi->clear('skip');
           $findCom = & $fm->newFindAllCommand($layoutName);
           break;
        }
        case "find": {
        // clear the recid
           $cgi->clear('recid');

        // create a find command
           $findCommand = $fm->newFindCommand($layoutName);
           ExitOnError($findCommand);

        // get the posted record data from the findrecords page
           $findrequestdata = $cgi->get('storedfindrequest');
           if (isset($findrequestdata)) {
               $findCom = prepareFindRequest($findrequestdata, $findCommand, $cgi);

            // set the logical operator
               $logicalOperator = $cgi->get('-lop');
               if (isset($logicalOperator)) {
                       $findCom->setLogicalOperator($logicalOperator);
               }
           } else
               $findCom = $fm->newFindAllCommand($layoutName);
           break;
        }
        default: {
           $findCom = & $fm->newFindAllCommand($layoutName);
           break;
        }
    }
    ExitOnError($findCom);

    // read and set, or clear the sort criteria
    $sortfield = $cgi->get('-sortfieldone');
    if (isset($sortfield)) {
        addSortCriteria($findCom);
    } else {
        clearSortCriteria($findCom);
    }

    // store the number of columns in this report table
    $numCols = 0;

    // get the skip and max values
    $skip = 0;

    // set skip and max values
    $findCom->setRange($skip, null);

    // perform the find
    $result = $findCom->execute();
    ExitOnError($result);

    // get the records
    $records = $result->getRecords();

    $fields = array('Titulo','Fecha','Archivos::Tipo','Archivos::Titulo','Archivos::Link','Archivos::Ficha','Archivos::RID','Archivos::Fecha','Archivos::FotitoEstrenos','Banner','Vide#','ID','Texto','Valida Copy','Region','Usuario','Portada','Foto','English','Orden','Grande','OrdenVistos','OrdenCate','Categoria','MasVistos','PortadaCategoria','PortadaProduTV','FotoEstrenos','OrdenSec','TituloHP2','MasInfo','EncEspecial','Inte','TituloHP','Today','CategoriaCalc','CategoriaCalc2','Extra','HeadlineIDCountry','HeadlineIDContact','HeadlineIDCompany');
;
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Summary Report
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
    </head>
    <body>
        <div id="container">
            <!-- HEADER -->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!-- Navigation Menu -->
            <?php $activelink = 'report.php'; include_once 'navigation.php' ?><!-- PAGE BODY -->
            <div id="content">
                <h1>
                    Report
                </h1>
                <table class="browse_records">
                    <thead>
                        <tr>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Titulo'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Fecha'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::Tipo'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::Titulo'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::Link'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::Ficha'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::RID'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::Fecha'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Archivos::FotitoEstrenos'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Banner'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Vide#'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'ID'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Texto'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Valida Copy'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Region'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Usuario'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Portada'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Foto'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'English'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Orden'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Grande'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'OrdenVistos'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'OrdenCate'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Categoria'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'MasVistos'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'PortadaCategoria'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'PortadaProduTV'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'FotoEstrenos'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'OrdenSec'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'TituloHP2'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'MasInfo'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'EncEspecial'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Inte'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'TituloHP'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Today'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'CategoriaCalc'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'CategoriaCalc2'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'Extra'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'HeadlineIDCountry'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'HeadlineIDContact'; ?>
                            </th>
                            <th class="browse_header">
                                <?php $numCols++; echo 'HeadlineIDCompany'; ?>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                                            $report = new Report($groupField, $summaryField, $summaryFunction, $numCols);
                                            $report->printReport($records, $fields, $layout);
                                            ?>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */
	
    require_once 'fmview.php';
    
    require_once 'FileMaker.php';
    
    require_once 'error.php';

    $cgi = new CGI();
    $cgi->storeFile();
    $userName = $cgi->get('userName');
    $passWord = $cgi->get('passWord');


    $fm = & new FileMaker();
    $fm->setProperty('database', 'Videos_');
    $fm->setProperty('username', $userName);
    $fm->setProperty('password', $passWord);
    
    ExitOnError($fm);
    
    $layoutList = $fm->listLayouts();
    ExitOnError($layoutList);


?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
        <title>
            Home
        </title>
    </head>
    <body>
        <div id="container">
            <!-- HEADER -->
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <!-- Navigation Menu -->
            <?php $activelink = 'home.php'; include_once 'navigation.php' ?><!-- PAGE BODY -->
            <div id="content">
                <h1>
                    Home
                </h1>
            </div>
        </div>
    </body>
</html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">
<?php
/**
    * FileMaker PHP Site Assistant Generated File
    */
    require_once "fmview.php";
    $cgi = new CGI();
    $cgi->clear('userName');
    $cgi->clear('passWord');
    $cgi->checkStoredFile();
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Authentication
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="plain_white.css">
    </head>
    <body>
        <div id="container">
            <div id="header">
                <h1>
                    Videos_
                </h1>
            </div>
            <?php $activelink = 'authentication.php'; include_once 'navigation.php' ?>
            <div id="content">
                <h1>
                    Log In
                </h1>
                <form action="<?php echo $cgi->get('file') ?>" method="post">
                    <input name="-action" type="hidden" value="login"> 
                    <table class="record">
                        <tr class="field">
                            <td class="field_name">
                                Account Name
                            </td>
                            <td class="field_data">
                                <input class="password_input" name="userName" type="text">
                            </td>
                        </tr>
                        <tr class="field">
                            <td class="field_name">
                                Password
                            </td>
                            <td class="field_data">
                                <input class="password_input" name="passWord" type="password">
                            </td>
                        </tr>
                        <tr class="submit_btn">
                            <td colspan="2">
                                <input name="Save" type="submit" value="Log In">
                            </td>
                        </tr>
                    </table>
                </form>
            </div>
        </div>
    </body>
</html>


<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php

    /**
    * FileMaker PHP Site Assistant Generated File
    */

    //  clear the session on each return to the logout page
    require_once "fmview.php";
    $cgi = new CGI();
    $cgi->reset();  
?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" type="text/css" media="screen" href="agentis.css">
        <title>
            Log Out
        </title>
    </head>
    <body>
        <div id="header">
            <!--HEADER-->
            <div id="headerlogo">
                Logos_ 
                <div id="headercaption">
                    <!--Caption for the Company-->
                </div>
            </div>
        </div>
        <div id="content">
            <!-- Navigation Menu -->
            <?php include_once 'navigation.php' ?>
            <table cellpadding="0" cellspacing="0" class="contentbg">
                <tr>
                    <td class="contentbgleft">
                    </td>
                    <td class="contentmid">
                        <div id="contenttitlebg">
                            <h1>
                                Log Out
                            </h1>
                        </div>
                        <div id="contentleft">
                        </div>
                        <div id="contentmid">
                        </div>
                        <!-- PAGE BODY -->
                        <table class="message" cellpadding="2" cellspacing="2">
                            <tr>
                                <td height="20">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <h2>
                                        You have successfully logged out.
                                    </h2>
                                    <p>
                                        &nbsp;
                                    </p>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="contentbgright">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="contentbgfooterleft">
                        &nbsp;
                    </td>
                    <td class="contentfooter">
                        &nbsp;
                    </td>
                    <td class="contentbgfotterright">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <!-- Footer-->
        <table class="footerwidth" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <?php include_once 'footer.php' ?>
                </td>
            </tr>
        </table>
    </body>
</html>


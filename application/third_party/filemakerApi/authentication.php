<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<?php

    /**
    * FileMaker PHP Site Assistant Generated File
    */
    require_once "fmview.php";
    $cgi = new CGI();
    $cgi->clear('userName');
    $cgi->clear('passWord');
    $cgi->checkStoredFile();
    
    $futurelink = $cgi->get('-link');
        
    if (isset($futurelink)) 
        $cgi->store('futurelink',$futurelink);
    else $cgi->store('futurelink', 'Home');

?>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <title>
            Authentication
        </title>
        <link rel="stylesheet" type="text/css" media="screen" href="agentis.css">
    </head>
    <body>
        <div id="header">
            <!-- HEADER -->
            <div id="headerlogo">
                Logos_ 
                <div id="headercaption">
                    <!-- Caption for the Company -->
                </div>
            </div>
        </div>
        <div id="content">
            <?php $cgi->store('-link', 'Log In'); include_once 'navigation.php' ?>
            <table cellpadding="0" cellspacing="0" class="contentbg">
                <tr>
                    <td class="contentbgleft">
                    </td>
                    <td class="contentmid">
                        <div id="contenttitlebg">
                            <h1>
                                Log In
                            </h1>
                        </div>
                        <div id="contentleft">
                        </div>
                        <div id="contentmid">
                        </div>
                        <table align="center" cellpadding="0" cellspacing="0" class="login">
                            <tr>
                                <td>
                                    <div id="form">
                                        <form action="<?php echo $cgi->get('file') ?>" method="post">
                                            <input name="-action" type="hidden" value="login"> <input name="-link" type="hidden" value="<?php echo $cgi->get('futurelink'); ?>"> 
                                            <table class="loginalign" border="0" align="center" cellpadding="2" cellspacing="2">
                                                <tr>
                                                    <td colspan="2" class="loginheight">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center" class="login_title">
                                                        <?php if(isset($errormessage)){echo ('<div id ="autherror">' .  $errormessage . '</div>' );} ?>Please enter a valid user
                                                        name and password.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fields">
                                                        Account Name
                                                    </td>
                                                    <td>
                                                        <input name="userName" type="text">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="fields">
                                                        Password
                                                    </td>
                                                    <td>
                                                        <input name="passWord" type="password">
                                                    </td>
                                                </tr>
                                                <tr class="submit_btn">
                                                    <td>
                                                    </td>
                                                    <td>
                                                        <input name="Save" type="submit" value="Log In" class="login_button">
                                                    </td>
                                                </tr>
                                            </table>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <td class="contentbgright">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td class="contentbgfooterleft">
                        &nbsp;
                    </td>
                    <td class="contentfooter">
                        &nbsp;
                    </td>
                    <td class="contentbgfotterright">
                        &nbsp;
                    </td>
                </tr>
            </table>
        </div>
        <!-- Footer-->
        <table class="footerwidth" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <?php include_once 'footer.php' ?>
                </td>
            </tr>
        </table>
    </body>
</html>


$('.btn-solicitud').click(function(e) {
    e.preventDefault();

    var idsolicitud = $(this).attr("id");
    var idrecluta = $(this).attr("data-rowId");
    var recluta = $(this).attr("data-rowrecluta");

    $("#idsolicitud_respuesta").val(idsolicitud);
    $("#useridto_respuesta").val(idrecluta);

    $("#rpd").empty();
    $("#rpd").html("<strong>Mensaje para: </strong>"+recluta);

});

$('#new-mensage').click(function(e) {
    e.preventDefault();
 	var form = $("#nuevo_mensaje");
    var url = form.attr('action');
    var content = "";
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data){
        	console.log(data);

            if (data != "false") {
            var obj = jQuery.parseJSON(data);
            content += '<strong>Mensaje enviado con exito</strong>';
            $('#nuevo_mensaje')[0].reset();
            $('#msn_exito').append(content);
            $('#msn_exito').show(500);
           // document.querySelector(".new-mail").classList.toggle("active");
            $('#myModal').modal('hide')
            $("#msn_exito").hide(3000);



            }
        }
    });
});

//dataTable
$('#table_solicitudes').DataTable({
    "initComplete": function(settings, json) {
        $("#reclutas").removeClass( "display" );
    },
    dom: 'Bfrtip',
    order:[1,"desc"],
    'columnDefs': [{ 
        'targets': [0], 
        'visible': false, 
    }],
    buttons: [
    ],
    language: {
        search: "Buscar",
        paginate: {
            first:      "Primero",
            previous:   "Anterior",
            next:       "Siguiente",
            last:       "Último"
        },
        info:           "",
        scrollY: 1300,
        "lengthMenu":     "_MENU_"
    }
});


$('#table_solicitudes tbody').on( 'click', 'button', function () {

    var table =  $('#table_solicitudes').DataTable();

    var id=$(this).attr('id');

    var idsolicitud = jQuery(this).attr("id");

    var rows = $(this).parents('tr');
    var url = document.location.origin+"/solicitudes/delete"; 

    alertify.confirm("¿Esta seguro que desea eliminar esta solicitud?",
        function (e) {
            if (e) {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: { 'idsolicitud' : idsolicitud},
                    success: function (data) {
                        console.log(data);
                        alertify.success('Solicitud Eliminada');
                        table.row(rows).remove().draw();
                    },error: function (err) {
                        console.log(err);
                    }
                });
                return false;
            } else {
                alertify.error("Usted ha cancelado la solicitud");

            }
        },
        function () {
            var error = alertify.error('Cancel');
        });

});
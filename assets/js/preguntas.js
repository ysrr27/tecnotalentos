$(document).on('click', '#addpregunta', function() {
    $('.addpregunta, .closet').show(500);
    $('#addpregunta').hide(500);
});

$(document).on('click', '#cerrar', function() {
    $('.addpregunta, .closet, .editpregunta').hide(500);
    $('#addpregunta').show(500);
});

$(document).on('click', '.edit', function() {
                    event.preventDefault();
                    $('html, body').animate({scrollTop: 0}, 600);
                    var idpregunta = jQuery( this ).attr("data-rowId");
                    var url = jQuery( this ).attr("data");
                    var form = $("#form_editar_pregutnas");
                    form.trigger("reset");
                    valores.splice(0,valores.length);  
                    $.ajax({
                        url: url,
                        type: 'get',
                        data: {'idpregunta' : idpregunta },
                        success: function (data) {
                            //console.log(data);
                            $.each(JSON.parse(data), function( key, value ) {
                              console.log( key + ": " + value );


                              switch(key) {
                                  case "idpregunta":
                                      $("#edit_idpregunta").val(value);
                                  break;
                                  case "pregunta":
                                     $("#edit_pregunta").val(value);
                                  break;
                                  case "formato_pregunta":
                                     $("#edit_formato_pregunta").val(value);
                                  break; 
                                  case "tipo_pregunta":
                                          switch(value) {
                                            case "text":
                                                tipopre = 'Respuesta corta';
                                            break;
                                            case "textarea":
                                                tipopre = 'Párrafo';
                                            break;
                                            case "select":
                                                tipopre = 'Desplegable';                
                                                $('.opciones_editar').show();
                                            break;
                                            case "checkbox":
                                                tipopre = 'Selección múltiple';
                                                 $('.opciones_editar').show();
                                            break;
                                            case "radio":
                                                tipopre = 'Casillas de verificación';
                                                 $('.opciones_editar').show();
                                            break;
                                            default:
                                            tipopre = "";
                                        }

                                      $("#tipopregunta").text(tipopre);
                                      $("#edit_tipo_pregunta").val(value);
                                  break;
                                  case "paso":
                                      $('#edit_paso option[value='+value+']').attr('selected','selected');

                                  break;
                                  case "opciones":
                                      $("#edit_opciones").val(value);

                                      var optionArray = value.split(',');
                                      var htmloption = '';
                                      $.each(optionArray, function(index, value) { 
                                        var key = 1 + index;
                                          $(".option1").val(value);
                                          htmloption += '<div class="new_div" style="display: none;" id="'+key+'">';
                                          htmloption += '<div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">';
                                          htmloption += '<input type="text" class="form-control has-feedback-left opcioness" id="'+key+'" value="'+value+'" placeholder="Opción ' +key+'">';
                                          htmloption += '<span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>';
                                          htmloption += '<span class="input-group-btn">';
                                          htmloption += '<button type="button" id="'+key+'" onclick="eliminaropcion(this.id)" class="btn btn-default btn-remove-opt-pregunta"><i class="fa fa-times"></i></button>';
                                          htmloption += '</span>';
                                          htmloption += '</div></div></div> ';
                                         valores.push(key);

                                      });
                                          $('#editar_options').append(htmloption);
                                          $('.new_div').show(500);

                                  break;
                                  case "requerido":
                                    if (value === "required") {
                                        $('#edit_requerido').prop('checked', true);
                                    }else{

                                        $('#edit_requerido').prop('checked', false);
                                    }
                                    $("#edit_requerido").val(value);

                                  break;
                                  default:
                                      text = "v";
                                      console.log(text);
                              }
                                  $('.editpregunta, .closet').show(500);
                                  $('#addpregunta').hide(500);
                                  $('.addpregunta').hide(500);
                          });

                      

                        },error: function (err) {
                            console.log(err);
                        }
                    });
                    return false;
               
    $(this).siblings('.save').show();
    $(this).siblings('.delete').hide();
    $(this).hide();
});


var valores = []; 


$( "#tipo_pregunta" ).change(function() {

    tipo_pregunta =$('select[name=tipo_pregunta]').val();
    pregunta = $( "#pregunta" ).val();
    requerido = ($( "input[name='requerido']:checked" ).val()) ? 'required' : '';

if(pregunta === ''){ 
    $("#err").fadeIn("fast");
    $("#err").empty();
    error = '';
    error +='* Debe agregar una pregunta';
    $("#err").append(error);
    $('#pregunta').addClass('form-control-error');

}else{

    $("#err").empty();
    $('#pregunta').removeClass('form-control-error');
      switch(tipo_pregunta) {
        case "text":
            $( ".optiondisplay,.chekdisplay" ).hide(1000);
            valores.splice(0,valores.length);         
            format = '<input type="text" ';
            format +='id="'+pregunta+'" ';
            format += 'class="form-control" ';
            format += 'name="'+pregunta+'" '+requerido+'>';
            $('#formato_pregunta').val(format);
             valores.push('Respuesta Corta');
        break;
        case "textarea":
            $( ".optiondisplay,.chekdisplay" ).hide(1000);
             valores.splice(0,valores.length);         
            format = '<textarea ';
            format +='id="'+pregunta+'" ';
            format += 'class="form-control" ';
            format += 'name="'+pregunta+'" '+requerido+'>';
            format += '</textarea>';
            $('#formato_pregunta').val(format);
            valores.push('Parrafo');
        break;
        case "select":
        $( ".optiondisplay,.chekdisplay" ).hide(1000);
        $( ".selectdisplay" ).show(1000);
        $(".new_div").empty();
        $(".agregar_opcion").attr('id', '1');
        break;
        case "checkbox":
             $( ".optiondisplay" ).hide(1000);
             $( ".selectdisplay" ).hide(1000);
             $( ".chekdisplay" ).show(1000);
             valores.splice(0,valores.length);
             $(".new_div").empty();
             $(".agregar_opcion").attr('id', '1');
        break;
        case "radio":
            $( ".optiondisplay" ).show(1000);
            $( ".chekdisplay" ).hide(1000);
            $( ".selectdisplay" ).hide(1000);
             valores.splice(0,valores.length);
             $(".new_div").empty();
             $(".agregar_opcion").attr('id', '1');
        break;
        default:
        text = "text";
    }
}


});

$('.agregar_opcion_editar').click(function(e) {
    var ultimo = valores.length + 1;
    var validatoption = valores.length;
    var htmloption = '';


    htmloption += '<div class="new_div" style="display: none;" id="'+ultimo+'">';
    htmloption += '<div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">';
    htmloption += '<input type="text" class="form-control has-feedback-left opcioness" id="'+ultimo+'" value="" placeholder="Opción ' +ultimo+'">';
    htmloption += '<span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>';
    htmloption += '<span class="input-group-btn">';
    htmloption += '<button type="button" id="'+ultimo+'" onclick="eliminaropcion(this.id)" class="btn btn-default btn-remove-opt-pregunta"><i class="fa fa-times"></i></button>';
    htmloption += '</span>';
    htmloption += '</div></div></div> ';

    $('#editar_options').append(htmloption);
    $('.new_div').show(500);

      valores.push(ultimo);

})


//agregar otra opcion

$('.agregar_opcion').click(function(e) {
      
      var newid ="#"+$(this).attr('id');
      var idcount = Number($(this).attr('id')) + 1;
      var idoptions2 ="option"+idcount;
      var htmloption = '';
      var format = '';
      pregunta = $( "#pregunta" ).val();
      tipodepregunta = $("#tipo_pregunta").val();
      requerido = $( "#requerido" ).val();


    if(Number($(this).attr('id')) === 1) {
        var option1 = "#"+tipodepregunta+"1";
     
        valores.push($(option1).val());

    }else{
        var idoptions ="#option"+Number($(this).attr('id'));

        console.log(idoptions);
        valores.push($(idoptions).val());
    }

     $(".agregar_opcion").attr('id', idcount);
     pregunta = $('#pregunta').val();



 switch(tipodepregunta) {
        case "select":
        $( ".optiondisplay,.chekdisplay" ).hide(1000);
            htmloption += '<div class="new_div" style="display: none;" id="'+idcount+'">';
            htmloption += '<div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">';
            htmloption += '<input type="text" class="form-control has-feedback-left " id="'+idoptions2+'" placeholder="Opción ' +idcount+'">';
            htmloption += '<span class="fa fa-chevron-down form-control-feedback left" aria-hidden="true"></span>';
            htmloption += '<span class="input-group-btn">';
            htmloption += '<button type="button" id="'+idcount+'" onclick="eliminaropcion(this.id)" class="btn btn-default btn-remove-opt-pregunta"><i class="fa fa-times"></i></button>';
            htmloption += '</span>';
            htmloption += '</div></div></div> ';
            $('#selects').append(htmloption);
            $('.new_div').show(500);

            format ='';
            format +='<select id="'+pregunta+'" name="'+pregunta+'" class="form-control" '+requerido+'>';
            valores.forEach(function(item, index) {
              format +='<option value="'+item+'">'+item+'</option>';
            });
            format +='</select>';

            $('#formato_pregunta').val(format);
        break;
        case "checkbox":
            htmloption += '<div class="new_div" style="display: none;" id="'+idcount+'">';
            htmloption += '<div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">';
            htmloption += '<input type="text" class="form-control has-feedback-left " id="'+idoptions2+'" placeholder="Opción ' +idcount+'">';
            htmloption += '<span class="fa fa-check-square-o form-control-feedback left" aria-hidden="true"></span>';
            htmloption += '<span class="input-group-btn">';
            htmloption += '<button type="button" id="'+idcount+'" onclick="eliminaropcion(this.id)" class="btn btn-default btn-remove-opt-pregunta"><i class="fa fa-times"></i></button>';
            htmloption += '</span>';
            htmloption += '</div></div></div> ';
            $('#checks').append(htmloption);
            $('.new_div').show(500);

            format ='';
            valores.forEach(function(item, index) {
                format += '<div class="col-md-3">';
                format += '<div class="checkbox"><label for="'+item+'">';
                format += '<input type="checkbox" '+requerido+' name="'+pregunta+'" class="flat" id="'+item+'" value="'+item+'"/> '+item+'</label>';
                format += '</div></div>';
            });
            $('#formato_pregunta').val(format);


            formato_pregunta = $("#formato_pregunta").val();
        break;
        case "radio":
            htmloption += '<div class="new_div" style="display: none;" id="'+idcount+'">';
            htmloption += '<div class="col-md-11 col-sm-11 col-xs-11 input-group  form-group has-feedback">';
            htmloption += '<input type="text" class="form-control has-feedback-left " id="'+idoptions2+'" placeholder="Opción ' +idcount+'">';
            htmloption += '<span class="fa fa-dot-circle-o form-control-feedback left" aria-hidden="true"></span>';
            htmloption += '<span class="input-group-btn">';
            htmloption += '<button type="button" id="'+idcount+'" onclick="eliminaropcion(this.id)" class="btn btn-default btn-remove-opt-pregunta"><i class="fa fa-times"></i></button>';
            htmloption += '</span>';
            htmloption += '</div></div></div> ';
            $('#options').append(htmloption);
            $('.new_div').show(500);



            format ='';
            valores.forEach(function(item, index) {
                format += '<div class="radiobutton">';
                format += '<input type="radio" name="'+pregunta+'" id="'+pregunta+item+'" value="'+item+'" '+requerido+'>';
                format += '<label for="'+pregunta+item+'">'+item+'</label></div>';
            });
            $('#formato_pregunta').val(format);

            formato_pregunta = $("#formato_pregunta").val();


        break;
        default:
        text = "text";
    }
     
     console.log(valores);

});




//Eliminar elemento del arrrays
function removeItemFromArr ( arr, item ) {
    var i = arr.indexOf( item );
 
    if ( i !== -1 ) {
        arr.splice( i, 1 );
    }
}

//Eliminar elemento del bloque de opciones
function eliminaropcion(id) {
   var idrow = "#"+id 
   var idoption=$(idrow).attr('id');

   var option_eliminar=$("#option"+idoption).val();
   
   console.log(option_eliminar);
   $("#"+idoption).empty();

   removeItemFromArr( valores, option_eliminar );
   console.log( valores );
}


//Agregar nueva pregunta
$('#agregar_pregunta').click(function(e) {
        e.preventDefault();
        idpaso = ".paso"+Number($('#paso').val());
        $('#opciones').val(valores);
        var form = $("#form_pregutnas");
        var url = form.attr('action');

        pregunta = $( "#pregunta" ).val();
        
        console.log(form.serialize());
        var htmlform = '';
        htmlform +='<fieldset class="paso1">';
        htmlform += '<div class="form-group row">';
        htmlform += '<div class="col-md-12"';
        htmlform += '<label for="sector">';
        htmlform +=  pregunta;
        htmlform += ':</label>';
        htmlform += '<span class="view-text">'+valores+'</span>';
        htmlform += '</div> ';
        htmlform += '</div> ';
        htmlform += '</fieldset>';
        $('.pasos').append(htmlform);
        console.log(form.serialize());

           $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
             $data = data;
             form.trigger("reset");
             valores.splice(0,valores.length);
             $(".new_div").empty();
             $(".agregar_opcion").attr('id', '1');
             $( ".optiondisplay" ).hide(1000);
            $("#listadepreguntas").load("https://trabajorevenuemanager.com/formulario/editar/1 #listadepreguntas");

            $('.addpregunta, .closet, .editpregunta').hide(500);
            $('#addpregunta').show(500);
         }
     });
return false;
   });





$('#editar_pregunta').click(function(e) {
        e.preventDefault();
        idpaso = ".paso"+Number($('#paso').val());
        valores.splice(0,valores.length);         




        var form = $("#form_editar_pregutnas");
        var url = form.attr('action');

        pregunta = $( "#edit_pregunta" ).val();
        requerido = $( "#edit_requerido" ).val();
        edit_formato_pregunta =  $("#edit_tipo_pregunta").val();


        switch(edit_formato_pregunta) {
            case "text":

            console.log(pregunta);

            format = '<input type="text" ';
            format +='id="'+pregunta+'" ';
            format += 'class="form-control" ';
            format += 'name="'+pregunta+'" '+requerido+'>';
            $('#edit_formato_pregunta').val(format);
            valores.push('Respuesta Corta');
            break;
            case "textarea":
            $( ".optiondisplay,.chekdisplay" ).hide(1000);
            valores.splice(0,valores.length);         
            format = '<textarea ';
            format +='id="'+pregunta+'" ';
            format += 'class="form-control" ';
            format += 'name="'+pregunta+'" '+requerido+'>';
            format += '</textarea>';
            $('#edit_formato_pregunta').val(format);
            valores.push('Parrafo');
            break;
            case "select":
                format ='';
                format +='<select id="'+pregunta+'" name="'+pregunta+'" class="form-control" '+requerido+'>';
                format +='<option value="">Seleccione</option>';
                $('input[type="text"].opcioness').each(function () {
                    console.log($(this).val());
                    format +='<option value="'+$(this).val()+'">'+$(this).val()+'</option>';
                    valores.push($(this).val());
                });
                format +='</select>';

                $('#edit_formato_pregunta').val(format);
                $('#edit_opciones').val(valores);
            
            break;
            case "checkbox":

            format ='';
            $('input[type="text"].opcioness').each(function () {
                format += '<div class="col-md-3">';
                format += '<div class="checkbox"><label for="'+$(this).val()+'" '+requerido+'>';
                format += '<input type="checkbox" name="'+pregunta+'" class="flat" id="'+$(this).val()+'" value="'+$(this).val()+'"/> '+$(this).val()+'</label>';
                format += '</div></div>';
                valores.push($(this).val());
            });

            $('#edit_formato_pregunta').val(format);
            $('#edit_opciones').val(valores);
            break;
            case "radio":


            format ='';
            $('input[type="text"].opcioness').each(function () {
                format += '<div class="radiobutton">';
                format += '<input type="radio" name="'+pregunta+'" id="'+pregunta+$(this).val()+'" value="'+$(this).val()+'" '+requerido+'>';
                format += '<label for="'+pregunta+$(this).val()+'">'+$(this).val()+'</label></div>';
                valores.push($(this).val());
            });

            $('#edit_formato_pregunta').val(format);
            $('#edit_opciones').val(valores);

            console.log("valores: "+valores);

          
            break;
            default:
            text = "text";
        }

       $.ajax({
            type: "POST",
            url: url,
            data: form.serialize(), 
            success: function(data)
            {
               $data = data;
               if (data) {

               console.log(data);
               form.trigger("reset");
               valores.splice(0,valores.length);
               $(".new_div").empty();
               $(".agregar_opcion").attr('id', '1');
               $( ".opciones_editar" ).hide(1000);
               $('.pasos').load();
               $("#listadepreguntas").load("https://trabajorevenuemanager.com/formulario/editar/1 #listadepreguntas");
               $('.addpregunta, .closet, .editpregunta').hide(500);
               $('#addpregunta').show(500);

                $('html,body').animate({
                    scrollTop: $("#listadepreguntas").offset().top
                }, 1000);

               }


             
          }
      });
return false;
   });


//Agregar nuevo paso
$("#agregar_paso").click(function(e){
    
    idpaso = Number($('#paso').val()) + 1;
    htmlpaso = '';
    htmlpaso += '<hr class="displaypaso" style="display: none;"><h4 class="text-center">';
    htmlpaso += '<b>Paso '+idpaso+'</b></h4><hr>';

    $('.pasos').append(htmlpaso);
    $('.displaypaso').show(500);
    $('#paso').val(idpaso);

    url = $('#agregar_paso').attr('data');

    $.ajax({
        url: url,
        type: 'get',
        data: { 'idpaso' : idpaso},
        success: function (data) {
            console.log(data);
        },error: function (err) {
            console.log(err);
        }
    });
    return false;

});

//eliminar pregunta
$(".eliminar_pregunta").click(function(e){
          e.preventDefault();
          var idpregunta=$(this).attr('id');
          var url = document.location.origin+"/tecnotalentos/index.php/formulario/delete"; 
          alertify.confirm("¿Esta seguro que desea eliminar esta pregunta?",
            function (e) {
              if (e) {
                $.ajax({
                  url: url,
                  type: 'get',
                  data: { 'idpregunta' : idpregunta},
                  success: function (data) {
                    console.log(data);
                    alertify.success('Pregunta Eliminada');
                    $( "#"+idpregunta ).hide(1000);
                    $("#"+idpregunta).empty();

                    table.row(rows).remove().draw();
                  },error: function (err) {
                    console.log(err);
                  }
                });
                return false;
              } else {
                alertify.error("Usted ha cancelado la solicitud");
              }
            },
            function () {
              var error = alertify.error('Cancel');
            });
});








$.expr[":"].contains = $.expr.createPseudo(function(arg) {
    return function( elem ) {
        return $(elem).text().toUpperCase().indexOf(arg.toUpperCase()) >= 0;
    };
});
$(document).ready(function() {



    $('#addTagBtn').click(function() {
        $('#tags option:selected').each(function() {
            $(this).appendTo($('#selectedTags'));
        });
    });
    $('#removeTagBtn').click(function() {
        $('#selectedTags option:selected').each(function(el) {
            $(this).appendTo($('#tags'));
        });
    });
    $('.tagRemove').click(function(event) {
        event.preventDefault();
        $(this).parent().remove();
    });
    $('ul.tags').click(function() {
        $('#search-field').focus();
    });


/*
    $('#search-field').keypress(function(event) {
        if (event.which == '13') {
            if (($(this).val() != '') && ($(".lenguaje .addedlenguaje:contains('" + $(this).val() + "') ").length == 0 ))  {
                    $('<li class="addedlenguaje">' + $(this).val() + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + $(this).val() + '" name="lenguajes[]"></li>').insertBefore('.lenguaje .lenguajeAdd');
                    $(this).val('');

            } else {
                $(this).val('');

            }
        }
    });


    $('#search-metodologia').keypress(function(event) {
      if (event.which == '13') {
        if (($(this).val() != '') && ($(".metodologia .addedmetodologia:contains('" + $(this).val() + "') ").length == 0 ))  {
          $('<li class="addedmetodologia">' + $(this).val() + '<span class="tagRemove" onclick="$(this).parent().remove();">x</span><input type="hidden" value="' + $(this).val() + '" name="metodologia[]"></li>').insertBefore('.metodologia .metodologiaAdd');
          $(this).val('');

        } else {
          $(this).val('');

        }
      }
    });*/

});



//pagina de postulacion

//disponibilidad remota
$("#Disponibilidad").change(function(event) {  

  var valor = $(this).val();


  if (valor === "A distancia") {
    $('.tdistancia,.class_17, .class_18, .class_19, .class_20').show(500);
  }else{
    $('.tdistancia,.class_17, .class_18, .class_19, .class_20').hide(500);
  }

});
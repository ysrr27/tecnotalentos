$("#archivo").change(function(event) {  
    $(".load-circle").show();
    var url = "/http://localhost/cheverecars/index.php/reclutas/subir_archivo";
    var file_data = $('#archivo').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    var filename = $("#archivo").val();
        filename = '<i class="fa fa-paperclip"></i> '+filename.substring(filename.lastIndexOf('\\')+1);
    
    $.ajax({
        url: url,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
        $('#cv_file').val("uploads/"+response);
        $('#text-file_cv').empty();
        $('#text-file_cv').html(filename);   
        $('#text-file-solicitd').removeClass('alert-success'); 
        $(".load-circle").hide();

          console.log(response);
      },
      error: function (response) {
            $('#msg').html(response); 
        }
    }); 
});

$("#inputGroupFile01").change(function(event) {  
    RecurFadeIn();
    console.log(readURL(this));
    readURL(this);    
});

$("#inputGroupFile01").on('click',function(event){
    RecurFadeIn();
});

function readURL(input) {    
    if (input.files && input.files[0]) {   
        var reader = new FileReader();
        var filename = $("#inputGroupFile01").val();
        filename = '<i class="fa fa-paperclip"></i> '+filename.substring(filename.lastIndexOf('\\')+1);
        reader.onload = function(e) {
           $('#blah, .profile-photo-small').attr('src', e.target.result);
           $('#blah, .profile-photo-small').hide();
           $('#blah, .profile-photo-small').fadeIn(500);      
           $('.custom-file-label').text(filename);    
            $('.custom-file-label').html(filename);   
            var url = "http://localhost/cheverecars/index.php/reclutas/subir_archivo";
            var file_data = $('#inputGroupFile01').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('#file').val("uploads/"+response);
                    var file = "uploads/"+response;
                   // console.log(response);
                    SavePhotoProfile(file)
                },
                error: function (response) {
                }
            });
        }
        reader.readAsDataURL(input.files[0]);    
    }
    $(".alert").removeClass("loading").hide(); 
}

function SavePhotoProfile(file){
    console.log(file);
    var url = document.location.origin+"/cheverecars/index.php/usuarios/SavePhotoProfile";  
    $.ajax({
        url: url,
        type: 'get',
        data: { 'file' : file},
        success: function (data) {  
            console.log(data);
        },error: function (err) {
            console.log(err);
        }
    }); 
}

function SavePhotoBanner(file){
    console.log(file);
    var url = document.location.origin+"/cheverecars/index.php/usuarios/SavePhotoProfile";  
    $.ajax({
        url: url,
        type: 'get',
        data: { 'file' : file},
        success: function (data) {  
            console.log(data);
        },error: function (err) {
            console.log(err);
        }
    }); 
}


function RecurFadeIn(){ 
    console.log('ran');
    FadeInAlert("Por favor espere...");  
}

function FadeInAlert(text){
    $(".alert").show();
    $(".alert").text(text).addClass("loading");  
}
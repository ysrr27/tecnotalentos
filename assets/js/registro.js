/*const 
    loginButton = document.getElementById('login-button'),
    userForms = document.getElementById('user_options-forms')*/

/**
 * Add event listener to the "Sign Up" button
 */
/*signupButton.addEventListener('click', () => {
  userForms.classList.remove('bounceRight')
  userForms.classList.add('bounceLeft')
}, false)
*/
/**
 * Add event listener to the "Login" button
 */
/*loginButton.addEventListener('click', () => {
  userForms.classList.remove('bounceLeft')
  userForms.classList.add('bounceRight')
}, false)
*/
$('#btn-cand-tab').click(function(e) {
    e.preventDefault();
   $('#btn-recl-tab').removeClass('btn-login-active');
   $('#btn-cand-tab').addClass('btn-login-active');
   $("#user").val('candidatos');
});
$('#btn-recl-tab').click(function(e) {
    e.preventDefault();
    $("#btn-cand-tab").removeClass("btn-login-active");
    $('#btn-recl-tab').addClass('btn-login-active');
    $("#user").val('reclutas');

});

$('#envar_registro').click(function(e) {
    e.preventDefault();
    var form = $("#registrate");
    var url = form.attr('action');
    var red = $("#user").val();
    var micuenta = "";
    if (red === "reclutas") {
        micuenta = document.location.origin+"/"+red+"/micuenta";
    }else{
        micuenta = document.location.origin+"/"+red+"/editar_curriculum";
    }

    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data){
            $data = data;
            var obj = jQuery.parseJSON(data);
          if (obj.estatus) {
            console.log( obj.estatus );
              //$('#registrate')[0].reset();
              window.location.replace(micuenta);
            }else{
                alert("el correo ya existe");
            }
        }
    });
});

$('#ingresa_session').click(function(e) {
    e.preventDefault();
    var red = $("#user").val();
    var form = $("#session_recluta");
    var url = form.attr('action');
    var micuenta = document.location.origin+"/tecnotalentos/"+red+"/micuenta";
    console.log(micuenta);
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data){
            $data = data;
            var obj = jQuery.parseJSON(data);
            if (obj.estatus) {
                //console.log( obj.usuario.id );
                $("#user_options-forms").removeClass("bounceLeft");
                $('#session_recluta')[0].reset();
                window.location.replace(micuenta);
            }else{
                alert("Verifique sus datos");
            }
        }
    });
});
     $('#Descripcion_empresa').blur(function()  
        {  
            var letterNumber = /^[0-9a-zA-Z]+$/;  
            if(this.value.match(letterNumber))   
            {  
                return true;  
            }  
            else  
            {   
               $(".textarea").append("<ul class='list-unstyled'><li>Haz coincidir el formato solicitado.</li></ul>");
                return false;   
            }  
        });

//Recluta perfil empresa
$('#guardar_perfil_empresa').click(function(e) {
    e.preventDefault();
    var form = $("#perfil_empresa");
    var url = form.attr('action');

    var dataform = "<p>"+$("#Razon_social_empresa").val()+"</p>";
        dataform += "<p>"+$("#Rif_empresa").val()+"</p>";
        dataform += "<p>Descripción: "+$("#Descripcion_empresa").val()+"</p>";
        dataform += "<p>Ubicación Actual: "+$("#Estado_empresa").val()+" "+$("#Ciudad_empresa").val()+"</p>";
        dataform += "<p>"+$("#Ciudad_empresa").val()+"</p>";
        dataform += "<p>"+$("#Direccion_empresa").val()+"</p>";
        dataform += "<p>"+$("#Email_contacto").val()+"</p>";

    

        var validate = $(form).validator();
        validate.validator('validate');
        var elmErr = validate.children('.has-error');
        if(elmErr && elmErr.length > 0){
            // Form validation failed
            return false;
        }else{

            var empty = true;
            $('input[type="text"]').each(function(){
                if($(this).val()==""){
                    empty =false;
                    return false;
                }
            });
            if (empty) {
                $.ajax({
                    type: "POST",
                    url: url,
                    data: form.serialize(), 
                    success: function(data){
                        $data = data;
                        var obj = jQuery.parseJSON(data);
                        console.log( obj );
                 // $('#registrate')[0].reset();
                 $('#e_data').empty();
                 $('#e_data').append(dataform);
                 $('#card-perfile-empresa, .disabled').show();
                 $('#card-edit-perfile-empresa').hide();
             }
         });
            }else{
                alert("verifique los datos")
            }
        }



});

$('#edit_perfile_empresa').click(function(e) {
    e.preventDefault();
    $('#card-perfile-empresa').hide();
    $('#card-photo-empresa').hide();
    $('#card-edit-perfile-empresa').show();
    $('#card-edit-photo-empresa').show();
});

$('.user-active').click(function(e) {
    e.preventDefault();
    var useridto = $(this).attr("id");
    var url = document.location.origin+"/mensajes/get_mensajes";   
    var subjet = "";
    var content = "";
    var classactive = "";
    $(".accounts__item").removeClass("accounts__item--active");
    $(this).addClass("accounts__item--active");
    $('.preview').empty();
    $(".scrollable__target").empty();
        $.ajax({
            url: url,
            type: 'get',
            data: { 'useridto' : useridto},
            success: function (data) {  
                $.each(JSON.parse(data), function(k, v) {
                    if (k === 0) {
                        detalle_mensaje(v.idmensaje);
                        classactive = ".message"+v.idmensaje;
                    }
                    content +='<div class="message'+v.idmensaje+' message '+v.status+'" id="'+v.idmensaje+'" onclick="detalle_mensaje(this.id);">';
                    content +='<div class="message-tags">';
                    content +='<span class="dot dot--green"></span>';
                    content +='</div>';
                    content +='<div class="message__actions">';
                    content +='<i class="message__icon far fa-square"></i>';
                    content +='<i class="message__icon fas fa-trash-alt"></i>';
                    content +='<i class="message__icon fas fa-archive"></i>';
                    content +='</div>';
                    content +='<div class="message__content">';
                    content +='<div class="message__exp">';
                    content +='<div>'+v.usuario+'</div>';
                    content +='<div class="date"></div>';
                    content +='</div>';
                    content +='<div class="message__title">'+v.subject+'</div>';
                    content +='<div class="message__expr">« '+v.smallmessage+' »</div>';
                    content +='</div>';
                    content +='</div>';
                })
                $(".scrollable__target").html(content);
                $(classactive).addClass("message--active");
            },error: function (err) {
                console.log(err);
            }
        }); 
});


document.addEventListener("DOMContentLoaded", () => {
  const cancelBonuses = document.querySelector(".app-cancel-bonuses");
  const app = document.querySelector(".app");
  const toggleBtn = document.querySelectorAll(".new-mail__toggle");
  toggleBtn.forEach(element => {
    element.addEventListener("click", () => {
      document.querySelector(".new-mail").classList.toggle("active");
      var idmensaje = $(".preview-top").attr("id");
      var rowId = $(".preview-top").attr("data-rowId");
      var subjet = $(".preview__title").attr("id");
      $("#rpd").append(subjet);
      $("#idmensaje_respuesta").val(idmensaje);
      $("#useridfrom_respuesta").val(rowId);
    });
  });

  function byeCancelButton() {
    cancelBonuses.classList.toggle("app-cancel-bonuses--active");
  }

  function toggleClassToApp(trigger, className, cancellable) {
    let bonus = document.querySelector(trigger);
  }

  toggleClassToApp(".bonus-weird-rotate", "weird-rotate", true);
  toggleClassToApp(".bonus-dark-mode", "dark-mode", false);
  toggleClassToApp(".bonus-zoom", "bonus-zoom", true);
  toggleClassToApp(".bonus-exit", "bonus-exit", true);
  toggleClassToApp(".bonus-why-so-serious", "why-so-serious", false);
  toggleClassToApp(".bonus-russia", "bonus-russia", false);
});

/**
 * Variables
 */
const signupButton = document.getElementById('signup-button'),
    loginButton = document.getElementById('login-button'),
    userForms = document.getElementById('user_options-forms')

//lista de solicitudes
var t = $('#lista_solicitudes').DataTable( {
        responsive: true,
    "initComplete": function(settings, json) {
        $("#lista_solicitudes").removeClass( "display" );
    },
    dom: 'Bfrtip',
    order:[1,"desc"],
/* 'columnDefs': [{ 
'targets': [1], 
'visible': false, 
}],*/
buttons: [
{ extend: 'copy', text: 'Copiar' },
'excelHtml5',
'csvHtml5',
'pdfHtml5'
],
language: {
    search: "Buscar",
    paginate: {
        first:      "Primero",
        previous:   "Anterior",
        next:       "Siguiente",
        last:       "Último"
    },
    info:           "",
    scrollY: 1300,
    "lengthMenu":     "_MENU_"
}
});


//solicitudes
$('#enviar_solicitud').click(function(e) {
    e.preventDefault();
    var empty = true;
    $('input[type="text"]').each(function(){
        if($(this).val()==""){
            empty =false;
            return false;
        }
    });
    var form = $("#solicitud");
    var url = form.attr('action');
    alertify.confirm("¿Esta seguro que desea enviar la solicitud?",
        function (e) {
            if (e) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), 
                        success: function(data)
                        {
                            $data = data;
                            if (data != "false") {
                                console.log( data );
                                alertify.success('Solicitud enviada con exito');
                                $('#solicitud')[0].reset();
                                $('.chosen-select').val('').trigger('chosen:updated');

                                return false;
                            }else{
                                alertify.error("Error al enviar la solicitud");
                            }
                        }
                    });

            } else {
                alertify.error("Usted ha cancelado la solicitud");
            }
        },
        function () {
            var error = alertify.error('Cancel');
        });
});



$('#lista_solicitudes tbody').on( 'click', 'button', function () {
    var table =  $('#lista_solicitudes').DataTable();
    var idsolicitud=$(this).attr('id');
    var host =document.location.origin+"/solicitudes/delete";
    var rows = $(this).parents('tr')
    alertify.confirm("¿Esta seguro que desea eliminar esta solicitud?",
        function (e) {
            if (e) {
                $.ajax({
                    url: host,
                    type: 'get',
                    data: { 'idsolicitud' : idsolicitud},
                    success: function (data) {
                        console.log(data);
                        alertify.success('Solicitud Eliminada');
                        table.row(rows).remove().draw();
                    },error: function (err) {
                        console.log(err);
                    }
                });
                return false;
            } else {
                alertify.error("Usted ha cancelado la solicitud");
            }
        },
        function () {
            var error = alertify.error('Cancel');
        });
});



//correo
function detalle_mensaje(id) {
    var mensaje = id;
    var width = $(window).width();
    //alert(width);
    var classmensaje = ".message"+id;
    $(".message").removeClass("message--new");
    $(".message").removeClass("message--active");
    $(classmensaje).addClass("message--active");
    var url = document.location.origin+"/tecnotalentos/reclutas/mensajes";   
    var urldowloand = document.location.origin+"/tecnotalentos/uploads/";   
    var idpreviw = $(".preview-top").attr("id");
  

    console.log(width);

    if (idpreviw == mensaje) {
        return false;
    }else{
        $('.preview').empty();
        var subjet = "";
        var content = "";
        $.ajax({
            url: url,
            type: 'get',
            data: { 'idmensaje' : mensaje},
            success: function (data) {  
                $.each(JSON.parse(data), function(k, v) {
                    if (k === 0) {
                        content +='<div class="preview-top" id="'+v.idmensaje+'"  data-rowId ="'+v.useridfrom+'"><div class="preview__title">'+v.subject+'</div></div><div class="scrollable"><div class="preview-content scrollable__target">';
                    }
                    content += '<div class="preview-respond"><div class="preview-respond__head">';
                    content += '<div class="profile-head">';
                    content += '<div class="profile-head__id">';
                    content += '<img class="profile-head__avatar" src="'+document.location.origin+'/assets/img/img.png" alt=""><div>';
                    content += '<div class="profile-head__name">'+v.usuario+'</div>';
                    content += '<div class="profile-head__mail">'+v.correo_mensaje+'</div></div></div>';
                    content += '<div class="date">'+v.timecreate+'</div></div></div>';
                    content += '<div class="preview-respond__content">';
                    content += '<p class="paragraph">'+v.fullmensaje+'</p>';
                    if (v.file !== "") {
                        content += '<a href="'+urldowloand+v.file+'" download class="brword"><b><i class="fa fa-paperclip"></i> '+v.file+' </b></a>';
                    }
                    content += '</div>';
                    content += '</div>';
                });
                content += '</div>';
                content += '</div>';
                content += '<div class="preview-foot">';
                content += '<button onclick="new_mail();" class="new-mail__toggle btn preview-foot__button"><i class="fa fa-mail-reply"></i> Responder</button>';
                content += '</div>';
                $(".preview").html(content);
                $("#"+mensaje).removeClass("message--new");
                if (width <= 768) {
                    $(".message-list").hide();
                    $(".preview,#volver_sms").removeClass("m_disable");
                    $("#volver_sms").removeClass("m_disable2");
                }
            },error: function (err) {
                console.log(err);
            }
        }); 
    }
}

$("#volver_sms").click(function(e){
    e.preventDefault();
    $(".message-list").show();
    $(".preview").addClass("m_disable");
    $("#volver_sms").addClass("m_disable2");

});

function new_mail() {
    document.querySelector(".new-mail").classList.toggle("active");
    document.getElementById('fullmensaje').focus();
    var idmensaje = $(".preview-top").attr("id");
    var rowId = $(".preview-top").attr("data-rowId");
    $("#idmensaje_respuesta").val(idmensaje);
    $("#useridfrom_respuesta").val(rowId);
}

$('#btn-cancel').click(function(e) {
    e.preventDefault();
    var filename = '<i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i>';
    $('#new_mensaje')[0].reset();
    $('.custom-file-label').html(filename);
    $("#inputGroupFile01").val();
});

$('#send-message').click(function(e) {
    e.preventDefault();
    var form = $("#new_mensaje");
    var url = form.attr('action');
    var content = "";
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data){
            if (data != "false") {
            var obj = jQuery.parseJSON(data);
            content += '<div class="preview-respond">';
            content += '<div class="preview-respond__head">';
            content += '<div class="profile-head">';
            content += '<div class="profile-head__id">';
            content += '<img class="profile-head__avatar" src="'+document.location.origin+'/assets/img/img.png" alt="">';
            content += '<div>';
            content += '<div class="profile-head__name">'+obj.usuario+'</div>';
            content += '<div class="profile-head__mail">'+obj.correo_mensaje+'</div>';
            content += '</div>';
            content += '</div>';
            content += '<div class="date">'+obj.timecreate+'</div>';
            content += '</div>';
            content += '</div>';
            content += '<div class="preview-respond__content">';
            content += '<p class="paragraph">'+obj.fullmensaje+'</p>';
            if (obj.file !== "") {
                content += '<a href="#"><b><i class="fa fa-paperclip"></i> '+obj.file+' </b></a>';    
            }
            content += '</div>';
            content += '</div>';

            var filename = '<i class="new-mail-foot__icon preview-foot__button fa fa-paperclip"  style="padding: 7px 25px;"></i>';
            $('#new_mensaje')[0].reset();
            $('.custom-file-label').html(filename);
            $("#inputGroupFile01").val();
            document.querySelector(".new-mail").classList.toggle("active");
            $('.preview-content').append(content);
            }
        }
    });
});

$("#archivo").change(function(event) {  
    $(".load-circle").show();
    var url = "/http://localhost/cheverecars/index.php/reclutas/subir_archivo";
    var file_data = $('#archivo').prop('files')[0];
    var form_data = new FormData();
    form_data.append('file', file_data);
    var filename = $("#archivo").val();
        filename = '<i class="fa fa-paperclip"></i> '+filename.substring(filename.lastIndexOf('\\')+1);
    
    $.ajax({
        url: url,
        dataType: 'text',
        cache: false,
        contentType: false,
        processData: false,
        data: form_data,
        type: 'post',
        success: function (response) {
        $('#cv_file').val("uploads/"+response);
        $('#text-file_cv').empty();
        $('#text-file_cv').html(filename);   
        $('#text-file-solicitd').removeClass('alert-success'); 
        $(".load-circle").hide();

          console.log(response);
      },
      error: function (response) {
            $('#msg').html(response); 
        }
    }); 
});

$("#inputGroupFile01").change(function(event) {  
    RecurFadeIn();
    console.log(readURL(this));
    readURL(this);    
});


$("#inputGroupFile01").on('click',function(event){
    RecurFadeIn();
});

function readURL(input) {    
    if (input.files && input.files[0]) {   
        var reader = new FileReader();
        var filename = $("#inputGroupFile01").val();
        filename = '<i class="fa fa-paperclip"></i> '+filename.substring(filename.lastIndexOf('\\')+1);
        reader.onload = function(e) {
           $('#blah, .profile-photo-small').attr('src', e.target.result);
           $('#blah, .profile-photo-small').hide();
           $('#blah, .profile-photo-small').fadeIn(500);      
           $('.custom-file-label').text(filename);    
            $('.custom-file-label').html(filename);   
            var url = "http://localhost/cheverecars/index.php/reclutas/subir_archivo";
            var file_data = $('#inputGroupFile01').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('#file').val("uploads/"+response);
                    var file = "uploads/"+response;
                   // console.log(response);
                    SavePhotoProfile(file)
                },
                error: function (response) {
                }
            });
        }
        reader.readAsDataURL(input.files[0]);    
    }
    $(".alert").removeClass("loading").hide(); 
}

$("#banner_image").change(function(event) {  
    if (this.files && this.files[0]) {   
        var reader = new FileReader();
        var filename = $("#banner_image").val();
        filename = '<i class="fa fa-paperclip"></i> '+filename.substring(filename.lastIndexOf('\\')+1);
        reader.onload = function(e) {
           $('#blah, .profile-photo-small, .img-avatar').attr('src', e.target.result);
           $('#blah, .profile-photo-small, .img-avatar').hide();
           $('#blah, .profile-photo-small, .img-avatar').fadeIn(500);      
           $('.custom-file-label').text(filename);    
            $('.custom-file-label').html(filename);   
            var url = "http://localhost/cheverecars/index.php/reclutas/subir_archivo";
            var file_data = $('#banner_image').prop('files')[0];
            var form_data = new FormData();
            form_data.append('file', file_data);
            $.ajax({
                url: url,
                dataType: 'text',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                type: 'post',
                success: function (response) {
                    $('#file').val("uploads/"+response);
                    $('#imagen').val("uploads/"+response);
                    var file = "uploads/"+response;
                    console.log(response);
                  // SavePhotoBanner(file);
                },
                error: function (response) {
                }
            });
        }
        reader.readAsDataURL(this.files[0]);    
    }
    $(".alert").removeClass("loading").hide(); 
});

function SavePhotoProfile(file){
    console.log(file);
    var url = document.location.origin+"/cheverecars/index.php/usuarios/SavePhotoProfile";  
    $.ajax({
        url: url,
        type: 'get',
        data: { 'file' : file},
        success: function (data) {  
            console.log(data);
        },error: function (err) {
            console.log(err);
        }
    }); 
}

function SavePhotoBanner(file){
    console.log(file);
    var url = document.location.origin+"/cheverecars/index.php/banners/SavePhotoBanner";  
    $.ajax({
        url: url,
        type: 'get',
        data: { 'file' : file},
        success: function (data) {  
            console.log(data);
        },error: function (err) {
            console.log(err);
        }
    }); 
}


function RecurFadeIn(){ 
    console.log('ran');
    FadeInAlert("Por favor espere...");  
}

function FadeInAlert(text){
    $(".alert").show();
    $(".alert").text(text).addClass("loading");  
}

$("#profile-tab2").on('click',function(event){
    $('#cant_mensajes').empty();
});

$('#actualizar_datos').click(function(e) {
    e.preventDefault();
    var form = $("#perfil_recluta");
    var url = form.attr('action');
    var micuenta = document.location.origin+"/reclutas/micuenta";
    $.ajax({
        type: "POST",
        url: url,
        data: form.serialize(), 
        success: function(data)
        {
            $data = data;
            var obj = jQuery.parseJSON(data);
            if (data) {
              $('#perfil_recluta')[0].reset();
                window.location.replace(micuenta);
           }else{
            alert("Verifique los datos");
           }
        }
    });
});

$(".ranking").change(function(event) {  
    var valor = $(this).val();
    var id=$(this).attr('id');

    var url = document.location.origin+"/tecnotalentos/candidatos/status";   
    alertify.confirm("¿Esta seguro que desea actualizar este estatus?",
        function (e) {
            if (e) {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: { 'idcandidato' : id,'status' : valor},
                    success: function (data) {  
                        console.log(data);
                        if (data) {
                            alertify.success('Estatus actualizado');
                        }else{
                            alertify.error("Error verifique los datos");
                        }
                    },error: function (err) {
                        console.log(err);
                    }
                }); 

            } else {
                alertify.error("Usted ha cancelado la solicitud");

            }
        },
        function () {
            var error = alertify.error('Cancel');
        });

});

$(".status_solicitud").change(function(event) {  
    var valor = $(this).val();
    var id=$(this).attr('id');

    var url = document.location.origin+"/tecnotalentos/solicitudes/status";   
    alertify.confirm("¿Esta seguro que desea actualizar este estatus?",
        function (e) {
            if (e) {
                $.ajax({
                    url: url,
                    type: 'get',
                    data: { 'idsolicitud' : id,'status' : valor},
                    success: function (data) {  
                        console.log(data);
                        if (data) {
                            alertify.success('Estatus actualizado');
                        }else{
                            alertify.error("Error verifique los datos");
                        }
                    },error: function (err) {
                        console.log(err);
                    }
                }); 

            } else {
                alertify.error("Usted ha cancelado la solicitud");

            }
        },
        function () {
            var error = alertify.error('Cancel');
        });

});

//editar curriculum 2020092401

$('#fullname').click(function(){
    var name = $(this).text();
    $(this).html('');
/*    $('<input>')
        .attr({
            'type': 'text',
            'name': 'fname',
            'id': 'txt_fullname',
            'size': '30',
            'value': name
        })
        .appendTo('#fullname');*/
    $("<textarea>").attr("value", 'result.exampleMessage','id', 'txt_fullname').appendTo('#fullname');
    $('#txt_fullname').focus();
});

$(document).on('blur','#txt_fullname', function(){
    var name = $(this).val();
   // alert('Make an AJAX call and pass this parameter >> name=' + name);
    $('#fullname').text(name);
}).keyup(function (e) {
    if ((e.which && e.which == 13) || (e.keyCode && e.keyCode == 13)) {
       // alert("done");
        return false;
    } else {
        return true;
    }
});


  // textarea
  var option = {type: 'textarea',trigger: $(".btn_edit")};
  $('.textarea .editable').editable(option, function(e){
    //$(".textarea .editable").css("max-width", "100px");
        var d = $(this).attr("data-name");     
        var ad = $(this).attr("data-pk");     
alert(ad);
    console.log(e);
  });



  //editar curriculum
$('.save_curriculum').click(function(e) {
    e.preventDefault();
    var form = $("#formcandidato");
    var url = form.attr('action');
    var empty = true;
    $('input[type="text"]').each(function(){
        if($(this).val()==""){
            empty =false;
            return false;
        }
    });
    alertify.confirm("¿Esta seguro que desea guardar los datos?",
        function (e){
            if (e){
                //if (empty) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), 
                        success: function(data)
                        {
                            $data = data;
                            if (data != "false") {
                                console.log( data );
                                alertify.success('Solicitud enviada con exito');
                                $("#cv_guardado").modal('show');
                                return false;
                            }else{
                                alertify.error("Error al enviar la solicitud");
                                $("#cv_guardado").modal('show');
                                $("#menss").text("Ups!, hubo un error, por favor verifique sus datos");

                            }
                        }
                    });
                /*}{
                    alertify.error("Faltan datos por completar, por favor verifique.");
                    return false;
                }*/
            } else {
                alertify.error("Usted ha cancelado la solicitud");
            }
        },
        function () {
            var error = alertify.error('Cancel');
        });
});

$('#cambiar-contrasena').click(function(e) {
    e.preventDefault();
    $(".form_register").toggle();
});

$('#guardar-settins').click(function(e) {
    e.preventDefault();
    var form = $("#settins_user");
    var url = form.attr('action');
    var empty = true;

    var users = $("#nombre").val()+" "+$("#apellido").val();
    $('input[type="text"]').each(function(){
        if($(this).val()==""){
            empty =false;
            return false;
        }
    });
    alertify.confirm("¿Esta seguro que desea actualizar los datos?",
        function (e){
            if (e){
                //if (empty) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: form.serialize(), 
                        success: function(data)
                        {
                            $data = data;
                            if (data) {
                                alertify.success('Solicitud enviada con exito');
                                //$('#formcandidato')[0].reset();
                                $("#datosguardados").modal('show');
                                $(".usr").text(users);
                                $("#sal_user").text("¡HOLA, "+users.toUpperCase()+"!");

                                return false;
                            }else{
                                 $("#datosguardados").modal('show');
                                 $("#menss").text("Ups!, hubo un error, por favor verifique sus datos");
        
                                alertify.error("Error al enviar la solicitud");
                            }
                        }
                    });
                /*}{
                    alertify.error("Faltan datos por completar, por favor verifique.");
                    return false;
                }*/
            } else {
                alertify.error("Usted ha cancelado la solicitud");
            }
        },
        function () {
            var error = alertify.error('Cancel');
        });

});

//document.getElementById('output').innerHTML = location.search;
$(".chosen-select").chosen();

function cargarArea() {
    var array = ["Administración de sistemas", "Calidad", "Seguridad", "Programación", "Telecomunicaciones","Otro"];
   // array.sort();
    addOptions("Area", array);
}


//Función para agregar opciones a un <select>.
function addOptions(domElement, array) {
      var selector = document.getElementsByName(domElement)[0];
      for (area in array) {
        var opcion = document.createElement("option");
        opcion.text = array[area];
    // Añadimos un value a los option para hacer mas facil escoger los cargos
    opcion.value = array[area].toLowerCase()
    selector.add(opcion);
    }
}



function cargos() {
    // Objeto de areas con cargos
    var listaCargos = {
      administracióndesistemas: ["Asistente", "Analista", "Especialista", "Cordinación", "Jefatura","Gerencia"],
      calidad: ["Asistente", "Analista", "Especialista", "Cordinación", "Jefatura","Gerencia"],
      seguridad: ["Asistente", "Analista", "Especialista", "Cordinación", "Jefatura","Gerencia"],
      programación: ["Asistente", "Analista", "Especialista", "Cordinación", "Jefatura","Gerencia"],
      telecomunicaciones: ["Asistente", "Analista", "Especialista", "Cordinación", "Jefatura","Gerencia"],
      otro: [""]
    }

    var areas = document.getElementById('Area')
    var cargos = document.getElementById('cargo')
    var areaSeleccionada = areas.value

    // Se limpian los cargos
    cargos.innerHTML = '<option value="">- Seleccione -</option>'

    if(areaSeleccionada !== ''){

        if (areaSeleccionada === 'otro' ) {
            $("#disabled").show();
            $("#especificacion").hide();
        }else if (areaSeleccionada === 'programación'){
            $("#especificacion").show();
            $("#disabled").hide();
        }else{
            $("#disabled,#especificacion").hide();
            // Se seleccionan los cargos y se ordenan 
            areaSeleccionada = listaCargos[areaSeleccionada.replace(/ /g, "")]
            areaSeleccionada.sort()

            // Insertamos los cargos
            areaSeleccionada.forEach(function(cargo){
                  let opcion = document.createElement('option')
                  opcion.value = cargo
                  opcion.text = cargo
                  cargos.add(opcion)
            });     
        }
    }

}

// Iniciar la carga de areas solo para comprobar que funciona
cargarArea();

$('.especifique_otros').change(function(e) {
    var id = "#especifique_"+this.id;
    var Seleccionada = this.value
    if (Seleccionada  === 'Otro') {
        $(id).show();
    }else{
        $(id).hide();
    }
});